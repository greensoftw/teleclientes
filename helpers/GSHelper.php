<?php
/**
 * User: jhon
 * Date: 14/12/16
 * Time: 04:23 PM
 */

namespace app\helpers;


class GSHelper
{
  public static $dropBooleans = [
    ''=>'Todo',
    '1'=>'Si',
    '0'=>'NO'
  ];
  public static function sumarMeses($fecha, $meses)
  {
    $fecha = new \DateTime();
    $fecha->modify('+' . $meses . ' month');
    return $fecha->format('Y-m-d');
  }

  public static function sumarDias($dias){
    $fecha = new \DateTime();
    $fecha->modify('+' . $dias . ' day');
    return $fecha->format('Y-m-d');
  }

}