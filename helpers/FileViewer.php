<?php
/**
 * User: jhon
 * Date: 23/12/16
 * Time: 01:51 PM
 */

namespace app\helpers;


use app\models\Archivo;
use yii\base\Widget;

class FileViewer extends Widget
{
  /** @var  Archivo */
  public $model;

  public function init()
  {
    parent::init();
  }

  public function run()
  {
    $view = $this->getViewer();
    return $this->render("/extra-views/$view",[
      'model'=>$this->model
    ]);
  }

  private function getViewer()
  {
    switch ($this->model->type) {
      case "image/png":
        return "imagen";
      case  "image/jpg":
        return "imagen";
      case  "image/pjpeg":
        return "imagen";
      case  "image/jpeg":
        return "imagen";

      case "application/pdf":
        return "pdf";
      case "application/vnd.ms-excel":
        return "excel";
      case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
        return "excel";
      case "application/octet-stream":
        return "texto";
      case "text/plain":
        return "texto";
      case "text/xml":
        return "texto";
      case "text/html":
        return "html";
      default:
        return "DESCONOCIDO";
    }
  }
}