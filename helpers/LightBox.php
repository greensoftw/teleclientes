<?php
/**
 * Created by PhpStorm.
 * User: jhon
 * Date: 7/02/17
 * Time: 08:45 PM
 */

namespace app\helpers;


use p2made\assets\Lightbox2Asset;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class LightBox extends Widget
{
  /** @var array Contains the attributes for the images. */
  public $files = [];

  /** @inheritdoc */
  public function init()
  {
    Lightbox2Asset::register($this->getView());
  }

  /** @inheritdoc */
  public function run()
  {
    $html = '';
    foreach ($this->files as $file) {
      if (!isset($file['thumb']) || !isset($file['original'])) {
        continue;
      }
      $attributes = [
        'data-title' => isset($file['title']) ? $file['title'] : '',
      ];
      if (isset($file['group'])) {
        $attributes['data-lightbox'] = $file['group'];
      } else {
        $attributes['data-lightbox'] = 'image-' . uniqid();
      }
      $thumbOptions = isset($file['thumbOptions']) ? $file['thumbOptions'] : [];
      $linkOptions = isset($file['linkOptions']) ? $file['linkOptions'] : [];
      if($file['format'] == 'IMAGEN'){
        $img = Html::img($file['thumb'], $thumbOptions);
        $a = Html::a($img, $file['original'], ArrayHelper::merge($attributes, $linkOptions));
      }else{
        $a = Html::a('<span class="fa fa-file fa-lg"></span> '.$file['format'], $file['altUrl'],
          ['class'=>'btn btn-default','style'=>'height: 50px','title'=>$file['altTitle']]);
      }
      $html .= $a;
    }
    return $html;
  }
}