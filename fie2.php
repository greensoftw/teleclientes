<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <title>contrato mensual</title>
    <link href="../estilos/estilog.css" type="text/css" rel="stylesheet">

  </head>
  <script language="javascript" src="../js/validar.js"></script>
  <script type="text/javascript" src="../js/AlertaDiv.js"></script>
  <script type="text/javascript" src="../js/ConfirmacionDiv.js"></script>
  <script language="javascript">
    function limpiar(frm, campo) {
      eval("frm." + campo + ".value=''");
    }

    function buscar2(frm) {
      frm.operacion.value = 180;
      frm.action = "ctr_contratomensualp.php";
      frm.submit()

    }//cierre de paginado
    function validar(frm, op) {
      frm.target = "_self";
      frm.action = 'ctr_contratomensual.php';
      if (op == 'a') {
        frm.operacion.value = 20;
        frm.submit();
      } else if (!chequearOpcion()) {
        alertaDIV.newInnerDivAlert('Debe seleccionar un registro del listado');
      } else {
        if (op == 'm') {
          frm.operacion.value = 40;
          frm.submit();
        } else if (op == 'e') {
          //}else if(op=='e'&& confirm('¿Esta seguro de eliminar la salida?')){
          frm.operacion.value = 60;
          frm.submit();
        } else if (op == 'v') {
          //}else if(op=='e'&& confirm('¿Esta seguro de eliminar la salida?')){
          frm.operacion.value = 80;
          frm.submit();

        } else if (op == 'p') {
          frm.operacion.value = 10;
          frm.submit();
        } else if (op == 'i') {
          frm.operacion.value = 100;
          frm.submit();
        }

      }
    }
  </script>
<body>
<form name="form1" id="form1" method="post" target="_self">
  <H4>BUSCAR CONTRATO MENSUAL</H4>
  <table width="100%" border="0">
    <tr>
      <td width="50%">Seleccione un criterio de busqueda</td>
      <td width="50%">Buscar</td>
    </tr>
    <tr>
      <td>
        <select name="campo" class="combos">
          <?php
          $campoviene = $_POST["campo"];
          if ($campoviene == "conm_ano") {
            echo "<option value='conm_ano' selected>AÑO</option>";
          } else {
            echo "<option value='conm_ano'>AÑO</option>";
          }

          if ($campoviene == "conm_nombre") {
            echo "<option value='conm_nombre' selected>NOMBRE</option>";
          } else {
            echo "<option value='conm_nombre'>NOMBRE</option>";
          }

          if ($campoviene == "conm_mes") {
            echo "<option value='conm_mes' selected>MES</option>";
          } else {
            echo "<option value='conm_mes'>MES</option>";
          }
          ?>
        </select>
      </td>
      <td>
        <input type="text" name="cbuscar" id="cbuscar" value="<?php echo $_POST["cbuscar"]; ?>"
               size="30" onDblClick="limpiar(document.forms['form1'],'cbuscar')">
      </td>
    </tr>
    <tr style="text-align: right">
      <td width="100%" colspan="3">
        <button class="btn" onclick="buscar2(document.forms['form1'])">Buscar</button>
      </td>
    </tr>
  </table>
  <h4>CONTRATOS MENSUAL</h4>
  <table width="100%" border="0" class="resultado">
    <tr class="fondo_celda_2">
      <td width="10%">AÑO</td>
      <td width="20%">MES</td>
      <td width="60%">NOMBRE</td>
      <td width="10%">ARCHIVO</td>
    </tr>
    <?php
    $fondoCelda = "fondo_celda_3";
    if ($filas > 0) {
      //lo hace hasta todos los registros
      for ($i = 0; $i < $filas; $i++) {
        ?>
        <tr>
          <td><?php echo mysql_result($lista, $i, "conm_ano") ?></td>
          <td align="left"><?php echo mysql_result($lista, $i, "conm_mes") ?></td>
          <td align="left"><?php echo mysql_result($lista, $i, "conm_nombre") ?></td>

          <?php $ruta = "archivo/" . mysql_result($lista, $i, "conm_archivo") ?>
          <td style="text-align: center"><a href="<?php echo $ruta; ?>" target="_blank">
              <img src="download.png" width="30" height="34"/></a>
          </td>
        </tr>
        <?
      }
    } else {
      ?>
      <tr>
        <td colspan="4"><?php echo "No se encontraron registros"; ?></td>
      </tr>
    <? } ?>


  </table>
  <input name="operacion" type="hidden" value="130">
</form>
</body>
</html>