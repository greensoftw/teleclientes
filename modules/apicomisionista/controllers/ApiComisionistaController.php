<?php

namespace app\modules\apicomisionista\controllers;

use app\models\ComisionistaServicio;
use app\models\EmpresaTelecliente;
use app\models\LoginForm;
use app\models\Newpass;
use app\models\Pagina;
use app\models\Trabajo;
use Yii;
use yii\db\Connection;
use yii\filters\AccessControl;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\rest\Controller;

/**
 * Default controller for the `apicomisionista` module
 */
class ApiComisionistaController extends Controller
{

  public function beforeAction($action)
  {
//    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Credentials: true");
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
    header('Access-Control-Max-Age: 1000');
    header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');
    if (Yii::$app->controller->action->id !== 'index') {
      if (Yii::$app->request->get('empresa_id')) {
        $empresa = EmpresaTelecliente::findOne(Yii::$app->request->get('empresa_id'));
        Yii::$app->db = new Connection(Json::decode($empresa->conexion));
      }
    }
    return parent::beforeAction($action);
  }

  public function behaviors()
  {
    $behaviors = parent::behaviors();
    $behaviors['authenticator'] = [
      'class' => CompositeAuth::className(),
      'authMethods' => [
        HttpBearerAuth::className(),
        QueryParamAuth::className(),
      ],
      'only' => ['enviar', 'comisiones', 'comision', 'cuenta','new-pass','eliminar'],
    ];
    $behaviors['access'] = [
      'class' => AccessControl::className(),
      'only' => ['enviar', 'comisiones', 'comision', 'cuenta','new-pass','eliminar'],
      'rules' => [
        [
          'actions' => ['enviar', 'comisiones', 'comision', 'cuenta','new-pass','eliminar'],
          'allow' => true,
          'roles' => ['@'],
        ],
      ],
    ];
    return $behaviors;
  }

  public function actionIndex()
  {
    return EmpresaTelecliente::find()
      ->select('id,nombre')->all();
  }

  public function actionTerms(){
    return Pagina::findOne(1);
  }

  public function actionLogin()
  {
    $model = new LoginForm();
    if ($model->load(Yii::$app->request->post(), '') and $model->login()) {
      Yii::info(Yii::$app->user->identity);
      $response = [
        'access_token' => Yii::$app->user->identity->access_token,
        'comisionista' => Yii::$app->user->identity->comisionista,
        'cuenta' => [
          'user_id' => Yii::$app->user->id,
          'nombre' => Yii::$app->user->identity->nombres
        ],
        'empresa_id'=>$model->empresa_id
      ];
      return $response;
    } else {
      return $model;
    }
  }

  public function actionEnviar()
  {
    $model = new ComisionistaServicio([
      'comisionista_id' => Yii::$app->user->identity->comisionista->id,
    ]);
    if ($model->load(Json::decode(Yii::$app->getRequest()->getRawBody()), '')) {
      $model->monto = $model->trabajo->comision;
      if ($model->save()) {
        return ['success' => 'ok'];
      }
      $errores = "";
      foreach ($model->getErrors() as $item) {
        $errores .= " $item[0]";
      }
      return ['success' => $errores];
    }
    return ['success' => 'No se puede cargar'];
  }

  public function actionComisiones()
  {
    $comisionista = Yii::$app->user->identity->comisionista;
    $comisiones = $comisionista->getComisionistaServicios()
      ->where(['arreglado' => 0])
      ->orderBy(['id'=>SORT_DESC])
      ->all();
    $total = $comisionista->getComisionistaServicios()
      ->where(['arreglado' => 0, 'concretado' => 1, 'cerrado'=>1])
      ->sum('monto');
    return [
      'comisiones' => $comisiones,
      'total' => $total
    ];
  }

  public function actionEliminar($id){
    $model = ComisionistaServicio::findOne($id);
    if($model != null)
      $model->delete();
    return ['success'=>'ok'];
  }

  public function actionData()
  {
    return Trabajo::find()
      ->select(['id', 'nombre'])
      ->asArray()
      ->where(['visible_comisionista' => true])
      ->all();
  }

  public function actionNewPass(){
    $model = new Newpass([
      'scenario'=>'uptpass'
    ]);
    if($model->load(Yii::$app->request->post(),'')){
      if($model->validate()){
        $user = Yii::$app->user->identity;
        $user->setPassword($model->password);
        $user->save(false);
        return ['success'=>'ok'];
      }
      return ['success'=>'error','mensaje'=>Html::errorSummary($model)];
    }
    return ['success'=>'error','mensaje'=>'no load'];
  }
}
