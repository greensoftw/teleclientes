<?php

namespace app\modules\apicomisionista;

/**
 * apicomisionista module definition class
 */
class Module extends \yii\base\Module
{
  public $defaultRoute = 'api-comisionista';
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\apicomisionista\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
