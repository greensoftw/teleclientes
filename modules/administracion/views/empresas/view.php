<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\EmpresaTelecliente */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Empresa Telecliente', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empresa-telecliente-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Empresa Telecliente'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'nombre',
        'conexion',
        'config:ntext',
        'fecha_vence',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerArchivoEmpresa->totalCount){
    $gridColumnArchivoEmpresa = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'titulo',
            'ruta',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerArchivoEmpresa,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-archivo-empresa']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Archivo Empresa'),
        ],
        'export' => false,
        'columns' => $gridColumnArchivoEmpresa
    ]);
}
?>
    </div>
</div>
