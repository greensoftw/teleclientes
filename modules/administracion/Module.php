<?php

namespace app\modules\administracion;

/**
 * administracion module definition class
 */
class Module extends \yii\base\Module
{
  /**
   * @inheritdoc
   */
  public $controllerNamespace = 'app\modules\administracion\controllers';

  /**
   * @inheritdoc
   */
  public function init()
  {
    parent::init();
  }
}
