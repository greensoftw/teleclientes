<?php

namespace app\modules\apitecnico;

/**
 * apicomisionista module definition class
 */
class Module extends \yii\base\Module
{
  public $defaultRoute = 'api-tecnico';
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\apitecnico\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
