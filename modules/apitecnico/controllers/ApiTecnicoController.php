<?php

namespace app\modules\apitecnico\controllers;

use app\actions\UploadFile;
use app\helpers\GSHelper;
use app\models\AbonoServicio;
use app\models\Cliente;
use app\models\CodigoIsis;
use app\models\Distribuidor;
use app\models\Empresa;
use app\models\EmpresaTelecliente;
use app\models\Equipo;
use app\models\LoginForm;
use app\models\Marca;
use app\models\Newpass;
use app\models\Presupuesto;
use app\models\Repuestos;
use app\models\search\ServicioSearch;
use app\models\Servicio;
use app\models\ServicioAEquipo;
use app\models\ServicioEstado;
use app\models\ServicioProgramado;
use app\models\TipoEquipo;
use app\models\Trabajo;
use Yii;
use yii\base\Exception;
use yii\db\Connection;
use yii\filters\AccessControl;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\rest\Controller;
use yii\rest\Serializer;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `apicomisionista` module
 */
class ApiTecnicoController extends Controller
{

  public $empresa;

  public function beforeAction($action)
  {
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
    header('Access-Control-Max-Age: 1000');
    header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');
    if (Yii::$app->controller->action->id !== 'index') {
      if (Yii::$app->request->get('empresa_id')) {
        $this->empresa = EmpresaTelecliente::findOne(Yii::$app->request->get('empresa_id'));
        Yii::$app->db = new Connection(Json::decode($this->empresa->conexion));
      }
    }
    return parent::beforeAction($action);
  }

  public function behaviors()
  {
    $behaviors = parent::behaviors();
    $behaviors['authenticator'] = [
      'class' => CompositeAuth::className(),
      'authMethods' => [
        HttpBearerAuth::className(),
        QueryParamAuth::className(),
      ],
      'only' => ['listado', 'det_servicios', 'addpresupuesto', 'data', 'new-pass', 'upload-file-equipo', 'fotos', 'list-clientes', 'abonar', 'agregar-servicio','asignar-factura'],
    ];
    $behaviors['access'] = [
      'class' => AccessControl::className(),
      'only' => ['listado', 'det_servicios', 'addpresupuesto', 'data', 'new-pass', 'upload-file-equipo', 'fotos', 'list-clientes', 'abonar', 'agregar-servicio','asignar-factura'],
      'rules' => [
        [
          'actions' => ['listado', 'det_servicios', 'addpresupuesto', 'data', 'new-pass', 'upload-file-equipo', 'fotos', 'list-clientes', 'abonar', 'agregar-servicio','asignar-factura'],
          'allow' => true,
          'roles' => ['@'],
        ],
      ],
    ];
    return $behaviors;
  }

  public function actions()
  {
    return [
      'det_servicios' => [
        'class' => 'yii\rest\ViewAction',
        'modelClass' => Servicio::className(),
      ],
      'listado' => [
        'class' => 'yii\rest\IndexAction',
        'modelClass' => Servicio::className(),
        'prepareDataProvider' => [$this, 'prepareDataProvider']
      ],
      'upload-file-equipo' => [
        'class' => UploadFile::className(),
        'model' => ServicioAEquipo::className(),
        'model_id' => Yii::$app->request->get('id'),
        'folder' => 'servicios/servicio-' . Yii::$app->request->get('servicio_id') . '/equipo-' . Yii::$app->request->get('id') . ''
      ],
    ];
  }

  public function prepareDataProvider()
  {
    $search = new ServicioSearch();
    $search->cerrado = false;
    $search->empleado_id = Yii::$app->user->identity->empleado->id;
    return $search->search(Yii::$app->request->queryParams);
  }

  public function actionIndex()
  {
    return EmpresaTelecliente::find()->all();
  }

  public function actionListado()
  {
    $search = new ServicioSearch();
    $search->empleado_id = Yii::$app->user->identity->empleado->id;
    return [
      'result' => $search->search(Yii::$app->request->queryParams)->models,
      'estados' => ServicioEstado::$dropEstados
    ];
  }

  public function actionFotos($id)
  {
    $model = ServicioAEquipo::findOne($id);
    $s = new Serializer();
    return [
      'equipo' => $s->serializeModel($model->equipo),
      'fotos' => $model->files
    ];
  }

  public function actionListClientes()
  {
    $clientes = Cliente::find()->joinWith('usuario')
      ->orderBy('usuario.nombres')->all();
    $marcas = Marca::find()->orderBy('nombre')
      ->all();
    $serializer = new Serializer();
    return [
      'clientes' => $serializer->serializeModels($clientes),
      'marcas' => $serializer->serializeModels($marcas)
    ];
  }

  public function actionAgregarServicio()
  {
    $model = new Servicio([
      'total' => 0,
      'comision_servicio_id' => Yii::$app->request->get('com_id'),
      'empleado_id' => Yii::$app->user->identity->empleado->id
    ]);
    $estado = new ServicioEstado([
      'fecha' => date('Y-m-d H:i:s'),
      'estado' => ServicioEstado::INGRESADO,
      'descripcion' => 'Servicio Registrado',
      'actual' => 1
    ]);
    if ($model->load(Yii::$app->request->post())) {
      if ($model->fecha_tecnico) {
        $model->fecha_tecnico = new \DateTime($model->fecha_tecnico);
        $model->fecha_tecnico = $model->fecha_tecnico->format('Y-m-d H:i:s');
      }
      if ($model->save()) {
        $estado->servicio_id = $model->id;
        $estado->save();
        return ['success' => 'ok', 'id' => $model->id];
      } else
        return ['success' => Html::errorSummary($model)];
    }
    return ['success' => 'Servicio no cargado'];
  }

  public function actionAddPresupuesto($id)
  {
    $servicio = Servicio::findOne($id);
    if ($servicio != null) {
      if (Yii::$app->request->post('Presupuesto')) {
        foreach (Yii::$app->request->post('Presupuesto') as $item) {
          $model = new Presupuesto($item);
          if ($model->save())
            $servicio->total += $model->precio;
        }
        $servicio->save();
      }
      return $servicio->presupuestos;
    }
    throw new Exception('Página no encontrada', 404);
  }

  public function actionData()
  {
    return [
      [
        'name' => 'Trabajo',
        'class' => Trabajo::className(),
        'values' => Trabajo::find()->select(['id', 'nombre', 'precio'])->orderBy('nombre')->all()
      ],
      [
        'name' => 'Repuesto',
        'class' => Repuestos::className(),
        'values' => Repuestos::find()->select(['id', 'nombre', 'precio'])->orderBy('nombre')->all()
      ]
    ];
  }

  public function actionEstados($id)
  {
    $ser = new Serializer();
    $model = Servicio::findOne($id);
    if (Yii::$app->request->post('ServicioEstado')) {
      ServicioEstado::updateAll(['actual' => 0], ['servicio_id' => $model->id]);
      $estado = new ServicioEstado([
        'fecha' => date('Y-m-d H:i:s'),
        'servicio_id' => $model->id,
        'actual' => 1,
      ]);
      if ($estado->load(Yii::$app->request->post())) {
        if ($estado->save()) {
          if ($estado->estado == ServicioEstado::ENTREGADO) {
            if ($model->pagado)
              $model->cerrado = true;
            $model->save(false);
            //progama servicio para proxima vez
            if ($model->servicioAEquipo) {
              if ($model->servicioAEquipo->equipo->tiempo_mantenimiento != "NO") {
                $meses = explode(' ', $model->servicioAEquipo->equipo->tiempo_mantenimiento);
                $meses = $meses[0];
                $servicioProgramado = new ServicioProgramado([
                  'cliente_id' => $model->cliente_id,
                  'equipo_id' => $model->servicioAEquipo->equipo_id,
                  'fecha' => GSHelper::sumarMeses(time(), $meses),
                  'servicio_id' => $model->id
                ]);
                $servicioProgramado->save();
              }
            }
            if ($model->comisionServicio) {
              $model->comisionServicio->cerrado = 1;
              $model->comisionServicio->save();
            }
          }
          return [
            'success' => 'ok',
            'drop' => ServicioEstado::getDropDown($estado),
            'servicio' => $ser->serializeModel(Servicio::findOne($model->id))
          ];
        } else
          return ['success' => 'error', 'mensaje' => 'Seleccione un estado'];
      }
    }

    return [
      'servicio' => $ser->serializeModel($model),
      'estados' => ServicioEstado::getDropDown($model->estadoActual)
    ];
  }

  public function actionEquipo_cliente($id)
  {
    $servicio = Servicio::findOne($id);
    $servicio_equipo = $servicio->servicioAEquipo;
    if ($servicio_equipo == null)
      $servicio_equipo = new ServicioAEquipo();
    $equipos = $servicio->cliente->equipos;
    $serializze = new Serializer();
    if ($servicio_equipo->load(Yii::$app->request->post())) {
      $servicio_equipo->servicio_id = $servicio->id;
      $servicio_equipo->save();
    }
    return [
      'servicio' => $servicio,
      'servicio_equipo' => $serializze->serializeModel($servicio_equipo),
      'equipos' => $serializze->serializeModels($equipos),
      'isis' => CodigoIsis::find()->orderBy('codigo')->all(),
      'marcas' => Marca::find()->orderBy('nombre')->all(),
      'distribuidores' => Distribuidor::find()->orderBy('nombre')->all(),
      'tiposEquipo' => TipoEquipo::find()->orderBy('nombre')->all(),
      'tiempos' => Equipo::$tiempos_mantenimientos
    ];

  }

  public function actionAbonar($id)
  {
    $servicio = Servicio::findOne($id);
    $abono = new AbonoServicio([
      'fecha' => date('Y-m-d H:i:s'),
      'servicio_id' => $servicio->id,
      'empleado_id' => Yii::$app->user->identity->empleado->id
    ]);
    if ($abono->load(Yii::$app->request->post())) {
      if ($abono->save()) {
        return ['success' => 'Abono registrado correctamente'];
      } else
        return ['success' => $abono->getFirstError('monto')];
    }
    throw new NotFoundHttpException('objeto no encontrado');
  }

  public function actionAddEquipoCliente($id)
  {
    $model = Servicio::findOne($id);
    $equipo = new Equipo([
      'cliente_id' => $model->cliente_id
    ]);
    $servicioAEquipo = new ServicioAEquipo([
      'servicio_id' => $model->id,
      'selected' => 1
    ]);
    if ($equipo->load(Yii::$app->request->post()) and
      $servicioAEquipo->load(Yii::$app->request->post())
    ) {
      if ($equipo->save()) {
        $servicioAEquipo->equipo_id = $equipo->id;
        $servicioAEquipo->save();
        return ['success' => 'ok'];
      } else
        return [
          'success' => 'error',
          'mensaje' => Html::errorSummary([$equipo, $servicioAEquipo])
        ];
    }
    return ['success' => 'no load'];
  }

  public function actionAsignarFactura($id){
    $model = Servicio::findOne($id);
    if($model != null and $model->load(Yii::$app->request->post())){
      return $model->save();
    }
    return false;
  }

  public function actionNewPass()
  {
    $model = new Newpass([
      'scenario' => 'uptpass'
    ]);
    if ($model->load(Yii::$app->request->post(), '')) {
      if ($model->validate()) {
        $user = Yii::$app->user->identity;
        $user->setPassword($model->password);
        $user->save(false);
        return ['success' => 'ok'];
      }
      return ['success' => 'error', 'mensaje' => Html::errorSummary($model)];
    }
    return ['success' => 'error', 'mensaje' => 'no load'];
  }

  public function actionSendEmail($id)
  {
    $servicio = Servicio::findOne($id);
    if ($servicio != null) {
      if ($servicio->cliente->usuario->email != "") {
        $config = new Empresa();
        $sendGrid = Yii::$app->mailer;
        $sendGrid->compose('confirmar_presupuesto', [
          'model' => $servicio,
          'empresa' => $this->empresa,
          'config' => $config
        ])
          ->setSubject("Servicio tecnico N° $servicio->id - " . Yii::$app->name)
          ->setFrom("info@teleclientes.com")
          ->setTo($servicio->cliente->usuario->email)
          ->send();
      }
      return ['success' => 'ok'];
    }
    return ['success' => 'Servicio no encontrado'];
  }

  public function actionLogin()
  {
    $model = new LoginForm();
    if ($model->load(Yii::$app->request->post(), '') and $model->login()) {
      Yii::info(Yii::$app->user->identity);
      $response = [
        'access_token' => Yii::$app->user->identity->access_token,
        'cuenta' => [
          'user_id' => Yii::$app->user->id,
          'nombre' => Yii::$app->user->identity->nombres
        ],
        'empresa_id' => $model->empresa_id
      ];
      return $response;
    } else {
      return $model;
    }
  }

}
