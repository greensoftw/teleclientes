<?php

namespace app\models;

use \app\models\base\Trabajo as BaseTrabajo;

/**
 * This is the model class for table "trabajo".
 */
class Trabajo extends BaseTrabajo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nombre', 'precio'], 'required'],
            [['precio', 'comision'], 'integer'],
            [['visible_comisionista'], 'boolean'],
            [['nombre', 'descripcion'], 'string', 'max' => 250]
        ]);
    }
	
}
