<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Equipo]].
 *
 * @see Equipo
 */
class EquipoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Equipo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Equipo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}