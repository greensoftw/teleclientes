<?php

namespace app\models;

use \app\models\base\ItemCompra as BaseItemCompra;

/**
 * This is the model class for table "item_compra".
 */
class ItemCompra extends BaseItemCompra
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['compra_id', 'repuesto_id', 'precio'], 'required'],
            [['compra_id', 'repuesto_id', 'precio'], 'integer']
        ]);
    }
	
}
