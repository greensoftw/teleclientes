<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TipoEquipo]].
 *
 * @see TipoEquipo
 */
class TipoEquipoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TipoEquipo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TipoEquipo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}