<?php

namespace app\models;

use app\models\base\AbonoServicio as BaseAbonoServicio;

/**
 * This is the model class for table "abono_servicio".
 */
class AbonoServicio extends BaseAbonoServicio
{
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return array_replace_recursive(parent::rules(),
      [
        [['servicio_id', 'monto'], 'required'],
        [['servicio_id', 'monto'], 'integer'],
        [['monto'], 'validaMonto'],
        [['fecha'], 'safe']
      ]);
  }

  public function validaMonto()
  {
    $servicio = $this->servicio;
    if (!($this->monto <= ($servicio->total - $servicio->getAbonado()))) {
      $this->addError('monto', 'El monto no puede ser superior al total en deuda');
    }
  }

  public function afterSave($insert, $changedAttributes)
  {
    $abonado = $this->servicio->getAbonado();
    \Yii::warning($abonado);
    if ($abonado == $this->servicio->total) {
      $this->servicio->pagado = 1;
      if($this->servicio->estadoActual->estado == ServicioEstado::ENTREGADO){
        $this->servicio->cerrado = 1;
      }
      if ($this->servicio->comisionServicio) {
        $this->servicio->comisionServicio->cerrado = 1;
        $this->servicio->comisionServicio->save(false);
      }
      if ($this->servicio->empleado and $this->servicio->empleado->tipo_empleado != Empleado::CON_SUELDO) {
        $porcentaje = $this->servicio->empleado->getPercent();
        $pagoTecnico = new EmpleadoComision([
          'servicio_id' => $this->servicio_id,
          'empleado_id' => $this->servicio->empleado_id,
          'fecha' => date('Y-m-d H:i:s'),
          'arreglado' => 0,
          'monto' => $this->servicio->getTotalTrabajos() * $porcentaje,
        ]);
        $pagoTecnico->save();
      }
      $this->servicio->save(false);
    }
    parent::afterSave($insert, $changedAttributes);
  }
}
