<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[AbonoServicio]].
 *
 * @see AbonoServicio
 */
class AbonoServicioQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return AbonoServicio[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return AbonoServicio|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}