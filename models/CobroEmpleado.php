<?php

namespace app\models;

use app\models\base\CobroEmpleado as BaseCobroEmpleado;

/**
 * This is the model class for table "cobro_empleado".
 */
class CobroEmpleado extends BaseCobroEmpleado
{
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return array_replace_recursive(parent::rules(),
      [
        ['monto','validaMonto'],
        [['empleado_id', 'fecha', 'monto', 'saldo'], 'required'],
        [['empleado_id', 'monto', 'saldo'], 'integer'],
        [['fecha'], 'safe']
      ]);
  }

  public function validaMonto(){
    $ant = ($this->empleado->getDeuda()+$this->empleado->getSaldoDebe());
    \Yii::info("total=".$ant);
    if($this->monto >= $ant){
      $this->addError('monto','El monto no puede superar los '.
        \Yii::$app->formatter->asCurrency($ant));
    }
  }
}
