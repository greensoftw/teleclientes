<?php

namespace app\models;

use app\models\base\Equipo as BaseEquipo;

/**
 * This is the model class for table "equipo".
 */
class Equipo extends BaseEquipo
{
  public static $tiempos_mantenimientos = [
    'NO' => 'No especificar',
    '1 Meses' => '1 Meses',
    '2 Meses' => '2 Meses',
    '3 Meses' => '3 Meses',
    '4 Meses' => '4 Meses',
    '5 Meses' => '5 Meses',
    '6 Meses' => '6 Meses',
    '7 Meses' => '7 Meses',
    '8 Meses' => '8 Meses',
    '9 Meses' => '9 Meses',
    '10 Meses' => '10 Meses',
    '11 Meses' => '11 Meses',
    '12 Meses' => '12 Meses',
    '18 Meses' => '18 Meses',
    '24 Meses' => '24 Meses',
  ];

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return array_replace_recursive(parent::rules(),
      [
        [['cliente_id', 'marca_id', 'tipo_equipo_id', 'tiempo_mantenimiento'], 'required'],
        [['cliente_id', 'marca_id', 'distribuidor_id', 'tipo_equipo_id'], 'integer'],
        [['modelo', 'serial', 'nota'], 'string', 'max' => 250],
        [['tiempo_mantenimiento'], 'string', 'max' => 100]
      ]);
  }

  public function extraFields()
  {
    return [
      'marca'=>'marca',
      'tipoEquipo'=>'tipoEquipo'
    ];
  }

  public function __toString()
  {
    return "T<b>ipo de Equipo:</b> " . $this->tipoEquipo->nombre . "<br>
      <b>Marca:</b> " . $this->marca->nombre . "<br>
      <b>Modelo:</b> " . $this->modelo . "<br>
      <b>Serial:</b> " . $this->serial .'<br>
      <b>Ubicación:</b> '.$this->nota;
  }

  public function getStringMarca(){
    return "<b>Tipo de Equipo:</b> " . $this->tipoEquipo->nombre . "<br>
      <b>Marca:</b> " . $this->marca->nombre . "<br>
      <b>Modelo:</b> " . $this->modelo . "<br>
      <b>Serial:</b> " . $this->serial. "<br>
      <b>Distribuidor:</b> ".$this->distribuidor->nombre;
  }

  public function getStringDistribuidor(){
    return "<b>Tipo de Equipo:</b> " . $this->tipoEquipo->nombre . "<br>
      <b>Marca:</b> " . $this->marca->nombre . "<br>
      <b>Modelo:</b> " . $this->modelo . "<br>
      <b>Serial:</b> " . $this->serial;
  }
}
