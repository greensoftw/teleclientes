<?php

namespace app\models;

use \app\models\base\CodigoIsis as BaseCodigoIsis;

/**
 * This is the model class for table "codigo_isis".
 */
class CodigoIsis extends BaseCodigoIsis
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['marca_id', 'tipo_equipo_id', 'codigo', 'descripcion'], 'required'],
            [['marca_id', 'tipo_equipo_id'], 'integer'],
            [['codigo'], 'string', 'max' => 20],
            [['descripcion'], 'string', 'max' => 250]
        ]);
    }
	
}
