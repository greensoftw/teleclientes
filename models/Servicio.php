<?php

namespace app\models;

use app\models\base\Servicio as BaseServicio;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 * This is the model class for table "servicio".
 * @property ServicioEstado $ingreso
 * @property Archivo[] files
 * @property ServicioEstado estadoActual
 */
class Servicio extends BaseServicio
{
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
        [['numero','numero_garantia'],'unique','when' => function (ActiveRecord $model, $attribute) {
          return $model->{$attribute} !== $model->getOldAttribute($attribute);
        }],
        [['empleado_id', 'cliente_id', 'comision_servicio_id', 'total', 'pagado', 'cerrado'], 'integer'],
        [['marca_id','numero_garantia'], 'safe'],
        [['cliente_id', 'total', 'fecha_tecnico','numero'], 'required'],
        [['no_facturado'], 'string', 'max' => 250]
      ];
  }

  public function getAbonado()
  {
    $abonado = $this->getAbonoServicios()->sum('monto');
    if ($abonado != null)
      return $abonado;
    return 0;
  }

  /**
   * @return array|null|ServicioEstado
   */
  public function getIngreso()
  {
    return $this->getServicioEstados()
      ->orderBy(['id' => SORT_DESC])
      ->one();
  }

  /**
   * @return array|null|ServicioEstado
   */
  public function getEstadoActual()
  {
    return $this->getServicioEstados()->where(['actual' => 1])->one();
  }

  /**
   * @return array|null|ServicioEstado
   */
  public function getEntrega()
  {
    return $this->getServicioEstados()->where(['estado'=>ServicioEstado::ENTREGADO])->one();
  }

  public function getFiles()
  {
    return $this->hasMany(Archivo::className(), ['model_id' => 'id'])
      ->andWhere(['model' => Servicio::className()]);
  }

  public function extraFields()
  {
    return [
      'estadoActual' => 'estadoActual',
      'abonoServicios' => 'abonoServicios',
      'empleadoComisions' => 'empleadoComisions',
      'presupuestos' => 'presupuestos',
      'cliente' => 'cliente',
      'comisionServicio' => 'comisionServicio',
      'empleado' => 'empleado',
      'marca' => 'marca',
      'equipo' => 'servicioAEquipo',
      'estados' => 'servicioEstados',
      'servicioProgramados' => 'servicioProgramados',
      'comision' => 'comision',
      'abonado'=>'abonado'
    ];
  }

  public function getComision()
  {
    if ($this->comisionServicio)
      return [
        'nombres' => $this->comisionServicio->comisionista->usuario->nombres,
        'monto' => $this->comisionServicio->monto
      ];
    return null;
  }

  public function getTotalTrabajos()
  {
    $total = $this->getPresupuestos()->where(['model'=>Trabajo::className()])
      ->sum('precio');
    if ($total != null)
      return $total;
    return 0;
  }

  public function getEnDeuda(){
    return $this->total-$this->getAbonado();
  }
}