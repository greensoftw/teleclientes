<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ServicioProgramado]].
 *
 * @see ServicioProgramado
 */
class ServicioProgramadoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return ServicioProgramado[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ServicioProgramado|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}