<?php

namespace app\models;

use app\models\base\AbonoCompra as BaseAbonoCompra;

/**
 * This is the model class for table "abono_compra".
 */
class AbonoCompra extends BaseAbonoCompra
{
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return array_replace_recursive(parent::rules(),
      [
        [['compra_id', 'monto'], 'required'],
        [['compra_id', 'monto'], 'integer'],
        [['fecha'], 'string', 'max' => 250],
        [['monto'],'validaMonto','on'=>'abonar'],
      ]);
  }

  public function validaMonto()
  {
    $compra = $this->compra;
    if (!($this->monto <= ($compra->total - $compra->getAbonado()))) {
      $this->addError('monto', 'El monto no puede ser superior al total en deuda');
    }
  }

  public function afterSave($insert, $changedAttributes)
  {
    $abonado = $this->compra->getAbonado();
    \Yii::warning($abonado);
    if(($abonado+$this->monto) == $this->compra->total){
      $this->compra->pagado = 1;
      $this->compra->save(false);
    }
    parent::afterSave($insert, $changedAttributes);
  }

}
