<?php

namespace app\models;

use app\models\base\Proveedor as BaseProveedor;

/**
 * This is the model class for table "proveedor".
 */
class Proveedor extends BaseProveedor
{

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return array_replace_recursive(parent::rules(),
      [
        [['usuario_id', 'nombre', 'direccion'], 'required'],
        [['usuario_id'], 'integer'],
        [['nombre', 'direccion'], 'string', 'max' => 250]
      ]);
  }

}
