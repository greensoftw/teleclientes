<?php

namespace app\models;

use app\models\base\TipoEquipo as BaseTipoEquipo;

/**
 * This is the model class for table "tipo_equipo".
 */
class TipoEquipo extends BaseTipoEquipo
{
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return array_replace_recursive(parent::rules(),
      [
        [['nombre'], 'required'],
        [['nombre'], 'string', 'max' => 250]
      ]);
  }

}
