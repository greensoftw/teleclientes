<?php

namespace app\models;

use app\models\base\PagoEmpleado as BasePagoEmpleado;

/**
 * This is the model class for table "pago_empleado".
 */
class PagoEmpleado extends BasePagoEmpleado
{
  public $cobrar_saldo_debe;
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return array_replace_recursive(parent::rules(),
      [
        [['empleado_id', 'fecha', 'monto', 'saldo_anterior','comentario'], 'required'],
        [['empleado_id', 'monto', 'saldo_anterior','cobrar_saldo_debe'], 'integer'],
        [['fecha'], 'safe']
      ]);
  }

}
