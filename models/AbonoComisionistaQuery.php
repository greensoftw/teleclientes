<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[AbonoComisionista]].
 *
 * @see AbonoComisionista
 */
class AbonoComisionistaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return AbonoComisionista[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return AbonoComisionista|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}