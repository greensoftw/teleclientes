<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[CodigoIsis]].
 *
 * @see CodigoIsis
 */
class CodigoIsisQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return CodigoIsis[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CodigoIsis|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}