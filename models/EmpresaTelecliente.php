<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "empresa".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $conexion
 * @property string $config
 */
class EmpresaTelecliente extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'empresa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'conexion', 'config'], 'required'],
            [['config'], 'string'],
            [['nombre'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'conexion' => 'Conexion',
            'config' => 'Config',
        ];
    }
}
