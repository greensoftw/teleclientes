<?php

namespace app\models;

use app\models\base\Presupuesto as BasePresupuesto;

/**
 * This is the model class for table "presupuesto".
 */
class Presupuesto extends BasePresupuesto
{
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return array_replace_recursive(parent::rules(),
      [
        [['servicio_id', 'model_id', 'precio','model'], 'required'],
        [['servicio_id', 'model_id'], 'integer'],
        [['model'], 'string', 'max' => 250]
      ]);
  }

  public function extraFields()
  {
    return [
      'relacion'=>'relacion'
    ];
  }



}
