<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Repuestos]].
 *
 * @see Repuestos
 */
class RepuestosQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Repuestos[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Repuestos|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}