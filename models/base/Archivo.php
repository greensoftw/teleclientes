<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "archivo".
 *
 * @property integer $id
 * @property string $model
 * @property integer $model_id
 * @property string $ruta
 * @property string $title
 * @property string $type
 */
class Archivo extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model', 'model_id', 'title', 'type'], 'required'],
            [['model_id'], 'integer'],
            [['model', 'ruta', 'titulo'], 'string', 'max' => 250],
            [['type'], 'string', 'max' => 100]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'archivo';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'model' => 'Model',
            'model_id' => 'Model ID',
            'ruta' => 'Ruta',
            'title' => 'Titulo',
            'type' => 'Type',
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\ArchivoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\ArchivoQuery(get_called_class());
    }
}
