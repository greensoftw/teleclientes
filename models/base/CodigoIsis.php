<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "codigo_isis".
 *
 * @property integer $id
 * @property integer $marca_id
 * @property integer $tipo_equipo_id
 * @property string $codigo
 * @property string $descripcion
 *
 * @property \app\models\TipoEquipo $tipoEquipo
 * @property \app\models\Marca $marca
 * @property \app\models\ServicioAEquipo[] $servicioAEquipos
 */
class CodigoIsis extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['marca_id', 'tipo_equipo_id', 'codigo', 'descripcion'], 'required'],
            [['marca_id', 'tipo_equipo_id'], 'integer'],
            [['codigo'], 'string', 'max' => 20],
            [['descripcion'], 'string', 'max' => 250]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'codigo_isis';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'marca_id' => 'Marca',
            'tipo_equipo_id' => 'Tipo Equipo',
            'codigo' => 'Codigo',
            'descripcion' => 'Descripcion',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoEquipo()
    {
        return $this->hasOne(\app\models\TipoEquipo::className(), ['id' => 'tipo_equipo_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarca()
    {
        return $this->hasOne(\app\models\Marca::className(), ['id' => 'marca_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicioAEquipos()
    {
        return $this->hasMany(\app\models\ServicioAEquipo::className(), ['codigo_isis_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\models\CodigoIsisQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\CodigoIsisQuery(get_called_class());
    }
}
