<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "archivo_pdf".
 *
 * @property integer $id
 * @property integer $usuario_id
 * @property string $nombre
 * @property string $descripcion
 * @property string $ruta_archivo
 *
 * @property \app\models\Usuario $usuario
 */
class ArchivoPdf extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['usuario_id', 'nombre', 'descripcion', 'ruta_archivo'], 'required'],
            [['usuario_id'], 'integer'],
            [['nombre'], 'string', 'max' => 250],
            [['descripcion'], 'string', 'max' => 500],
            [['ruta_archivo'], 'string', 'max' => 200]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'archivo_pdf';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario_id' => 'Usuario ID',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'ruta_archivo' => 'Ruta Archivo',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(\app\models\Usuario::className(), ['id' => 'usuario_id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\models\ArchivoPdfQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\ArchivoPdfQuery(get_called_class());
    }
}
