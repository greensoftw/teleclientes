<?php

namespace app\models\base;

/**
 * This is the base model class for table "servicio".
 *
 * @property integer $id
 * @property integer $numero
 * @property integer $numero_garantia
 * @property integer $empleado_id
 * @property integer $cliente_id
 * @property integer $comision_servicio_id
 * @property integer $total
 * @property integer $pagado
 * @property string $no_facturado
 * @property integer $cerrado
 * @property string $token
 * @property int $marca_id
 * @property string $fecha_tecnico
 *
 * @property \app\models\AbonoServicio[] $abonoServicios
 * @property \app\models\EmpleadoComision $empleadoComision
 * @property \app\models\Presupuesto[] $presupuestos
 * @property \app\models\Cliente $cliente
 * @property \app\models\ComisionistaServicio $comisionServicio
 * @property \app\models\Empleado $empleado
 * @property \app\models\Marca $marca
 * @property \app\models\ServicioAEquipo $servicioAEquipo
 * @property \app\models\ServicioEstado[] $servicioEstados
 * @property \app\models\ServicioProgramado[] $servicioProgramados
 */
class Servicio extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'servicio';
  }

  /**
   * @inheritdoc
   * @return \app\models\ServicioQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\ServicioQuery(get_called_class());
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['numero','numero_garantia'],'unique'],
      [['empleado_id', 'cliente_id', 'comision_servicio_id', 'total', 'pagado', 'cerrado'], 'integer'],
      [['marca_id','numero_garantia'],'safe'],
      [['cliente_id', 'total','fecha_tecnico','numero'], 'required'],
      [['no_facturado'], 'string', 'max' => 250]
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'numero' => 'Orden N°',
      'empleado_id' => 'Tecnico',
      'cliente_id' => 'Cliente',
      'comision_servicio_id' => 'Comisionista',
      'total' => 'Total',
      'pagado' => 'Pagado',
      'marca_id'=>'Garantia',
      'numero_garantia'=>'Orden N° (Garantia)',
      'no_facturado' => 'No Factura',
      'cerrado' => 'Cerrado',
      'fecha_tecnico' => 'fecha visita del tecnico'
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getAbonoServicios()
  {
    return $this->hasMany(\app\models\AbonoServicio::className(), ['servicio_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getEmpleadoComision()
  {
    return $this->hasOne(\app\models\EmpleadoComision::className(), ['servicio_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getPresupuestos()
  {
    return $this->hasMany(\app\models\Presupuesto::className(), ['servicio_id' => 'id'])
      ->orderBy(['model'=>SORT_DESC]);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCliente()
  {
    return $this->hasOne(\app\models\Cliente::className(), ['id' => 'cliente_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getComisionServicio()
  {
    return $this->hasOne(\app\models\ComisionistaServicio::className(), ['id' => 'comision_servicio_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getEmpleado()
  {
    return $this->hasOne(\app\models\Empleado::className(), ['id' => 'empleado_id']);
  }

  public function getMarca(){
    return $this->hasOne(Marca::className(),['id'=>'marca_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getServicioAEquipo()
  {
    return $this->hasOne(\app\models\ServicioAEquipo::className(), ['servicio_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getServicioEstados()
  {
    return $this->hasMany(\app\models\ServicioEstado::className(), ['servicio_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getServicioProgramados()
  {
    return $this->hasMany(\app\models\ServicioProgramado::className(), ['servicio_id' => 'id']);
  }
}
