<?php

namespace app\models\base;

/**
 * This is the base model class for table "empleado_comision".
 *
 * @property integer $id
 * @property integer $servicio_id
 * @property integer $empleado_id
 * @property integer $pago_empleado_id
 * @property string $fecha
 * @property integer $monto
 * @property integer $arreglado
 *
 * @property PagoEmpleado $pagoEmpleado
 * @property Servicio $servicio
 * @property Empleado $empleado
 */
class EmpleadoComision extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['servicio_id', 'empleado_id', 'fecha', 'monto', 'arreglado'], 'required'],
      [['servicio_id', 'empleado_id', 'pago_empleado_id', 'monto', 'arreglado'], 'integer'],
      [['fecha'], 'safe']
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'empleado_comision';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'servicio_id' => 'Servicio ID',
      'empleado_id' => 'Empleado ID',
      'pago_empleado_id' => 'Pago Empleado ID',
      'fecha' => 'Fecha',
      'monto' => 'Monto',
      'arreglado' => 'Arreglado',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getPagoEmpleado()
  {
    return $this->hasOne(PagoEmpleado::className(), ['id' => 'pago_empleado_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getServicio()
  {
    return $this->hasOne(Servicio::className(), ['id' => 'servicio_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getEmpleado()
  {
    return $this->hasOne(Empleado::className(), ['id' => 'empleado_id']);
  }

  /**
   * @inheritdoc
   * @return \app\models\EmpleadoComisionQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\EmpleadoComisionQuery(get_called_class());
  }
}
