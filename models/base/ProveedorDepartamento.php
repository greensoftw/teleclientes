<?php

namespace app\models\base;

use mootensai\relation\RelationTrait;
use Yii;

/**
 * This is the base model class for table "proveedor_departamento".
 *
 * @property integer $id
 * @property integer $proveedor_id
 * @property string $nombre
 * @property string $email
 *
 * @property \app\models\Proveedor $proveedor
 */
class ProveedorDepartamento extends \yii\db\ActiveRecord
{
    use RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proveedor_id', 'nombre', 'email'], 'required'],
            [['proveedor_id'], 'integer'],
            [['nombre'], 'string', 'max' => 200],
            [['email'], 'string', 'max' => 100]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proveedor_departamento';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'proveedor_id' => 'Proveedor ID',
            'nombre' => 'Nombre',
            'email' => 'Email',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProveedor()
    {
        return $this->hasOne(\app\models\Proveedor::className(), ['id' => 'proveedor_id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\ProveedorDepartamentoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\ProveedorDepartamentoQuery(get_called_class());
    }
}
