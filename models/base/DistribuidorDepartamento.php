<?php

namespace app\models\base;

/**
 * This is the base model class for table "distribuidor_departamento".
 *
 * @property integer $id
 * @property integer $distribuidor_id
 * @property string $nombre
 * @property string $email
 *
 * @property \app\models\Distribuidor $distribuidor
 */
class DistribuidorDepartamento extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'distribuidor_departamento';
  }

  /**
   * @inheritdoc
   * @return \app\models\DistribuidorDepartamentoQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\DistribuidorDepartamentoQuery(get_called_class());
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['distribuidor_id', 'nombre', 'email'], 'required'],
      [['distribuidor_id'], 'integer'],
      [['nombre'], 'string', 'max' => 200],
      [['email'], 'string', 'max' => 100]
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'distribuidor_id' => 'Distribuidor ID',
      'nombre' => 'Nombre',
      'email' => 'Email',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getDistribuidor()
  {
    return $this->hasOne(\app\models\Distribuidor::className(), ['id' => 'distribuidor_id']);
  }
}
