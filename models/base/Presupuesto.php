<?php

namespace app\models\base;

/**
 * This is the base model class for table "presupuesto".
 *
 * @property integer $id
 * @property integer $servicio_id
 * @property integer $model_id
 * @property string $model
 * @property int $precio
 *
 * @property \app\models\Servicio $servicio
 * @property \app\models\Repuestos|Trabajo $relacion
 */
class Presupuesto extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['servicio_id', 'model_id', 'precio'], 'required'],
      [['servicio_id', 'model_id'], 'integer'],
      [['model'], 'string', 'max' => 250]
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'presupuesto';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'servicio_id' => 'Servicio',
      'model_id' => 'Seleccione',
      'model' => 'Model',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getServicio()
  {
    return $this->hasOne(\app\models\Servicio::className(), ['id' => 'servicio_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getRelacion()
  {
    return $this->hasOne($this->model, ['id' => 'model_id']);
  }

  /**
   * @inheritdoc
   * @return \app\models\PresupuestoQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\PresupuestoQuery(get_called_class());
  }
}
