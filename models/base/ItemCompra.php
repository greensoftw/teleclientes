<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "item_compra".
 *
 * @property integer $id
 * @property integer $compra_id
 * @property integer $repuesto_id
 * @property integer $precio
 *
 * @property \app\models\Compra $compra
 * @property \app\models\Repuestos $repuesto
 */
class ItemCompra extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['compra_id', 'repuesto_id', 'precio'], 'required'],
            [['compra_id', 'repuesto_id', 'precio'], 'integer']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item_compra';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'compra_id' => 'Compra ID',
            'repuesto_id' => 'Repuesto ID',
            'precio' => 'Precio',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompra()
    {
        return $this->hasOne(\app\models\Compra::className(), ['id' => 'compra_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepuesto()
    {
        return $this->hasOne(\app\models\Repuestos::className(), ['id' => 'repuesto_id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\models\ItemCompraQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\ItemCompraQuery(get_called_class());
    }
}
