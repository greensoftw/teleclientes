<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "proveedor".
 *
 * @property integer $id
 * @property integer $usuario_id
 * @property string $nombre
 * @property string $direccion
 *
 * @property \app\models\Compra[] $compras
 * @property \app\models\Usuario $usuario
 * @property \app\models\ProveedorDepartamento[] $proveedorDepartamentos
 */
class Proveedor extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['usuario_id', 'nombre', 'direccion'], 'required'],
            [['usuario_id'], 'integer'],
            [['nombre', 'direccion'], 'string', 'max' => 250]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proveedor';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario_id' => 'Usuario ID',
            'nombre' => 'Nombre',
            'direccion' => 'Direccion',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompras()
    {
        return $this->hasMany(\app\models\Compra::className(), ['proveedor_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(\app\models\Usuario::className(), ['id' => 'usuario_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProveedorDepartamentos()
    {
        return $this->hasMany(\app\models\ProveedorDepartamento::className(), ['proveedor_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\ProveedorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\ProveedorQuery(get_called_class());
    }
}
