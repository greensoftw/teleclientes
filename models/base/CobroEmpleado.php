<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "cobro_empleado".
 *
 * @property integer $id
 * @property integer $empleado_id
 * @property string $fecha
 * @property integer $monto
 * @property integer $saldo
 *
 * @property \app\models\AbonoServicio[] $abonoServicios
 * @property \app\models\Empleado $empleado
 */
class CobroEmpleado extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['empleado_id', 'fecha', 'monto', 'saldo'], 'required'],
            [['empleado_id', 'monto', 'saldo'], 'integer'],
            [['fecha'], 'safe']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cobro_empleado';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'empleado_id' => 'Empleado ID',
            'fecha' => 'Fecha',
            'monto' => 'Monto',
            'saldo' => 'Saldo',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonoServicios()
    {
        return $this->hasMany(\app\models\AbonoServicio::className(), ['cobro_empleado_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleado()
    {
        return $this->hasOne(\app\models\Empleado::className(), ['id' => 'empleado_id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\models\CobroEmpleadoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\CobroEmpleadoQuery(get_called_class());
    }
}
