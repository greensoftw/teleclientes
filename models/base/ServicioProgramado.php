<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "servicio_programado".
 *
 * @property integer $id
 * @property integer $cliente_id
 * @property integer $servicio_id
 * @property integer $equipo_id
 * @property string $fecha
 *
 * @property \app\models\Servicio $servicio
 * @property \app\models\Equipo $equipo
 * @property \app\models\Cliente $cliente
 */
class ServicioProgramado extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cliente_id', 'fecha'], 'required'],
            [['cliente_id', 'servicio_id', 'equipo_id'], 'integer'],
            [['fecha'], 'safe']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'servicio_programado';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cliente_id' => 'Cliente',
            'servicio_id' => 'Servicio',
            'equipo_id' => 'Equipo',
            'fecha' => 'Fecha',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicio()
    {
        return $this->hasOne(\app\models\Servicio::className(), ['id' => 'servicio_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEquipo()
    {
        return $this->hasOne(\app\models\Equipo::className(), ['id' => 'equipo_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCliente()
    {
        return $this->hasOne(\app\models\Cliente::className(), ['id' => 'cliente_id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\models\ServicioProgramadoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\ServicioProgramadoQuery(get_called_class());
    }
}
