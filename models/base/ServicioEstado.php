<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "servicio_estado".
 *
 * @property integer $id
 * @property integer $servicio_id
 * @property string $fecha
 * @property string $estado
 * @property string $descripcion
 * @property integer $actual
 *
 * @property \app\models\Servicio $servicio
 */
class ServicioEstado extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['servicio_id', 'fecha', 'estado'], 'required'],
            [['servicio_id'], 'integer'],
            [['fecha'], 'safe'],
            [['estado', 'descripcion'], 'string', 'max' => 250]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'servicio_estado';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'servicio_id' => 'Servicio ID',
            'fecha' => 'Fecha',
            'estado' => 'Estado',
            'descripcion' => 'Descripcion',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicio()
    {
        return $this->hasOne(\app\models\Servicio::className(), ['id' => 'servicio_id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\models\ServicioEstadoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\ServicioEstadoQuery(get_called_class());
    }
}
