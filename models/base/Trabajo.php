<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "trabajo".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $descripcion
 * @property integer $precio
 * @property boolean $visible_comisionista
 * @property integer $comision
 *
 * @property \app\models\ComisionistaServicio[] $comisionistaServicios
 */
class Trabajo extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'precio'], 'required'],
            [['precio', 'comision'], 'integer'],
            [['visible_comisionista'], 'boolean'],
            [['nombre', 'descripcion'], 'string', 'max' => 250]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trabajo';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'precio' => 'Precio minimo',
            'visible_comisionista' => 'Visible Comisionista',
            'comision' => 'Comision',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComisionistaServicios()
    {
        return $this->hasMany(\app\models\ComisionistaServicio::className(), ['trabajo_id' => 'id']);
    }


    /**
     * @inheritdoc
     * @return \app\models\TrabajoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\TrabajoQuery(get_called_class());
    }
}
