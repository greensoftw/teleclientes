<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "repuestos".
 *
 * @property integer $id
 * @property string $codigo
 * @property integer $marca_id
 * @property string $no_parte
 * @property string $nombre
 * @property integer $costo
 * @property integer $precio
 *
 * @property \app\models\ItemCompra[] $itemCompras
 * @property \app\models\Marca $marca
 */
class Repuestos extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codigo', 'nombre', 'costo', 'precio'], 'required'],
            [['marca_id', 'costo', 'precio'], 'integer'],
            [['codigo', 'no_parte', 'nombre'], 'string', 'max' => 250]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'repuestos';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo' => 'Codigo',
            'marca_id' => 'Marca',
            'no_parte' => 'No Parte',
            'nombre' => 'Nombre',
            'costo' => 'Costo',
            'precio' => 'Precio',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemCompras()
    {
        return $this->hasMany(\app\models\ItemCompra::className(), ['repuesto_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarca()
    {
        return $this->hasOne(\app\models\Marca::className(), ['id' => 'marca_id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\models\RepuestosQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\RepuestosQuery(get_called_class());
    }
}
