<?php

namespace app\models\base;

use app\models\ComisionistaQuery;
use mootensai\relation\RelationTrait;
use yii\db\ActiveRecord;

/**
 * This is the base model class for table "comisionista".
 *
 * @property integer $id
 * @property integer $usuario_id
 * @property integer $distribuidor_id
 * @property string $telefonos
 *
 * @property AbonoComisionista[] $abonoComisionistas
 * @property Distribuidor $distribuidor
 * @property Usuario $usuario
 * @property ComisionistaServicio[] $comisionistaServicios
 */
class Comisionista extends ActiveRecord
{
    use RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['usuario_id','documento'], 'required'],
            [['usuario_id', 'distribuidor_id'], 'integer'],
            [['telefonos','documento'], 'string', 'max' => 100],
            [['usuario_id'], 'unique']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comisionista';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario_id' => 'Usuario',
            'distribuidor_id' => 'Distribuidor',
            'telefonos' => 'Telefonos',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonoComisionistas()
    {
        return $this->hasMany(AbonoComisionista::className(), ['comisionista_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistribuidor()
    {
        return $this->hasOne(Distribuidor::className(), ['id' => 'distribuidor_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComisionistaServicios()
    {
        return $this->hasMany(ComisionistaServicio::className(), ['comisionista_id' => 'id']);
    }


    /**
     * @inheritdoc
     * @return ComisionistaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ComisionistaQuery(get_called_class());
    }
}
