<?php

namespace app\models\base;

/**
 * This is the base model class for table "tipo_equipo".
 *
 * @property integer $id
 * @property string $nombre
 *
 * @property \app\models\CodigoIsis[] $codigoIses
 * @property \app\models\Equipo[] $equipos
 */
class TipoEquipo extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'tipo_equipo';
  }

  /**
   * @inheritdoc
   * @return \app\models\TipoEquipoQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\TipoEquipoQuery(get_called_class());
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['nombre'], 'required'],
      [['nombre'], 'string', 'max' => 250]
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'nombre' => 'Nombre',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCodigoIses()
  {
    return $this->hasMany(\app\models\CodigoIsis::className(), ['tipo_equipo_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getEquipos()
  {
    return $this->hasMany(\app\models\Equipo::className(), ['tipo_equipo_id' => 'id']);
  }
}
