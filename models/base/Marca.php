<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "marca".
 *
 * @property integer $id
 * @property integer $usuario_id
 * @property string $nombre
 * @property string $ciudad
 * @property string $direccion
 * @property string $telefonos
 *
 * @property \app\models\CodigoIsis[] $codigoIses
 * @property \app\models\Equipo[] $equipos
 * @property \app\models\Usuario $usuario
 * @property \app\models\MarcaDepartamento[] $marcaDepartamentos
 * @property \app\models\Repuestos[] $repuestos
 */
class Marca extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['usuario_id', 'nombre'], 'required'],
            [['usuario_id'], 'integer'],
            [['nombre', 'ciudad', 'direccion', 'telefonos'], 'string', 'max' => 250]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'marca';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario_id' => 'Usuario ID',
            'nombre' => 'Nombre',
            'ciudad' => 'Ciudad',
            'direccion' => 'Dirección',
            'telefonos' => 'Télefonos',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoIses()
    {
        return $this->hasMany(\app\models\CodigoIsis::className(), ['marca_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEquipos()
    {
        return $this->hasMany(\app\models\Equipo::className(), ['marca_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(\app\models\Usuario::className(), ['id' => 'usuario_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarcaDepartamentos()
    {
        return $this->hasMany(\app\models\MarcaDepartamento::className(), ['marca_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepuestos()
    {
        return $this->hasMany(\app\models\Repuestos::className(), ['marca_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\models\MarcaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\MarcaQuery(get_called_class());
    }
}
