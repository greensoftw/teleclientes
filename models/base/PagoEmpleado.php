<?php

namespace app\models\base;

/**
 * This is the base model class for table "pago_empleado".
 *
 * @property integer $id
 * @property integer $empleado_id
 * @property string $fecha
 * @property integer $monto
 * @property integer $saldo_anterior
 * @property string $comentario
 * @property  integer $arreglado
 *
 * @property EmpleadoComision[] $empleadoComisions
 * @property Empleado $empleado
 */
class PagoEmpleado extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['empleado_id', 'fecha', 'monto', 'saldo_anterior','comentario'], 'required'],
      [['empleado_id', 'monto', 'saldo_anterior'], 'integer'],
      [['fecha'], 'safe']
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'pago_empleado';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'empleado_id' => 'Empleado',
      'fecha' => 'Fecha',
      'monto' => 'Monto',
      'saldo_anterior' => 'Saldo Anterior',
      'arreglado' => 'Pagado'
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getEmpleadoComisions()
  {
    return $this->hasMany(EmpleadoComision::className(), ['pago_empleado_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getEmpleado()
  {
    return $this->hasOne(Empleado::className(), ['id' => 'empleado_id']);
  }

  /**
   * @inheritdoc
   * @return \app\models\PagoEmpleadoQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\PagoEmpleadoQuery(get_called_class());
  }
}
