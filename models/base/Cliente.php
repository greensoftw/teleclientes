<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "cliente".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $comisionista_id
 * @property string $telefonos
 * @property string $direccion
 *
 * @property \app\models\Comisionista $comisionista
 * @property \app\models\Usuario $usuario
 * @property \app\models\Equipo[] $equipos
 * @property \app\models\Servicio[] $servicios
 */
class Cliente extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'comisionista_id'], 'integer'],
            [['telefonos', 'direccion'], 'required'],
            [['telefonos', 'direccion'], 'string', 'max' => 250]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cliente';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Usuario',
            'comisionista_id' => 'Comisionista',
            'telefonos' => 'Telefonos',
            'direccion' => 'Direccion',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComisionista()
    {
        return $this->hasOne(\app\models\Comisionista::className(), ['id' => 'comisionista_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(\app\models\Usuario::className(), ['id' => 'user_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEquipos()
    {
        return $this->hasMany(\app\models\Equipo::className(), ['cliente_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicios()
    {
        return $this->hasMany(\app\models\Servicio::className(), ['cliente_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\models\ClienteQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\ClienteQuery(get_called_class());
    }
}
