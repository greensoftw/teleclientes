<?php

namespace app\models\base;

use Yii;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "marca_departamento".
 *
 * @property integer $id
 * @property integer $marca_id
 * @property string $nombre
 * @property string $email
 *
 * @property \app\models\Marca $marca
 */
class MarcaDepartamento extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['marca_id', 'nombre', 'email'], 'required'],
            [['marca_id'], 'integer'],
            [['nombre'], 'string', 'max' => 200],
            [['email'], 'string', 'max' => 100]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'marca_departamento';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'marca_id' => 'Marca ID',
            'nombre' => 'Nombre',
            'email' => 'Email',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarca()
    {
        return $this->hasOne(\app\models\Marca::className(), ['id' => 'marca_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\MarcaDepartamentoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\MarcaDepartamentoQuery(get_called_class());
    }
}
