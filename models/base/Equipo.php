<?php

namespace app\models\base;

/**
 * This is the base model class for table "equipo".
 *
 * @property integer $id
 * @property integer $cliente_id
 * @property integer $marca_id
 * @property integer $distribuidor_id
 * @property integer $tipo_equipo_id
 * @property string $modelo
 * @property string $serial
 * @property string $nota
 * @property string $tiempo_mantenimiento
 *
 * @property \app\models\Marca $marca
 * @property \app\models\Distribuidor $distribuidor
 * @property \app\models\Cliente $cliente
 * @property \app\models\TipoEquipo $tipoEquipo
 * @property \app\models\ServicioAEquipo[] $servicioAEquipos
 * @property \app\models\ServicioProgramado[] $servicioProgramados
 */
class Equipo extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'equipo';
  }

  /**
   * @inheritdoc
   * @return \app\models\EquipoQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\EquipoQuery(get_called_class());
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['cliente_id', 'marca_id', 'tipo_equipo_id', 'tiempo_mantenimiento'], 'required'],
      [['cliente_id', 'marca_id', 'distribuidor_id', 'tipo_equipo_id'], 'integer'],
      [['modelo', 'serial', 'nota'], 'string', 'max' => 250],
      [['tiempo_mantenimiento'], 'string', 'max' => 100]
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'cliente_id' => 'Cliente',
      'marca_id' => 'Marca',
      'distribuidor_id' => 'Distribuidor',
      'tipo_equipo_id' => 'Tipo Equipo',
      'modelo' => 'Modelo',
      'serial' => 'Serial',
      'nota' => 'Ubicacion del equipo',
      'tiempo_mantenimiento' => 'Tiempo Mantenimiento',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getMarca()
  {
    return $this->hasOne(\app\models\Marca::className(), ['id' => 'marca_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getDistribuidor()
  {
    return $this->hasOne(\app\models\Distribuidor::className(), ['id' => 'distribuidor_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCliente()
  {
    return $this->hasOne(\app\models\Cliente::className(), ['id' => 'cliente_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getTipoEquipo()
  {
    return $this->hasOne(\app\models\TipoEquipo::className(), ['id' => 'tipo_equipo_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getServicioAEquipos()
  {
    return $this->hasMany(\app\models\ServicioAEquipo::className(), ['equipo_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getServicioProgramados()
  {
    return $this->hasMany(\app\models\ServicioProgramado::className(), ['equipo_id' => 'id']);
  }


}
