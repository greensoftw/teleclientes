<?php

namespace app\models\base;

/**
 * This is the base model class for table "empleado".
 *
 * @property integer $id
 * @property integer $usuario_id
 * @property string $telefono
 * @property string $tipo_empleado
 * @property integer $sueldo
 * @property string $proximo_pago
 * @property integer $porcentaje
 * @property integer $saldo
 * @property boolean $is_tecnico
 * @property string $perfil
 * @property string $direccion
 *
 * @property \app\models\AbonoServicio[] $abonoServicios
 * @property \app\models\CobroEmpleado[] $cobroEmpleados
 * @property \app\models\Usuario $usuario
 * @property \app\models\EmpleadoComision[] $empleadoComisions
 * @property \app\models\PagoEmpleado[] $pagoEmpleados
 * @property \app\models\Servicio[] $servicios
 */
class Empleado extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['usuario_id', 'telefono', 'tipo_empleado', 'saldo', 'direccion'], 'required'],
      [['usuario_id', 'sueldo', 'porcentaje', 'saldo'], 'integer'],
      [['proximo_pago'], 'safe'],
      [['is_tecnico'], 'boolean'],
      [['telefono', 'tipo_empleado', 'direccion'], 'string', 'max' => 250],
      [['perfil'], 'string', 'max' => 1000]
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'empleado';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'usuario_id' => 'Usuario',
      'telefono' => 'Telefono',
      'tipo_empleado' => 'Tipo Empleado',
      'sueldo' => 'Sueldo',
      'proximo_pago' => 'Proximo Pago',
      'porcentaje' => 'Porcentaje',
      'saldo' => 'Saldo',
      'is_tecnico' => 'Es Tecnico',
      'perfil' => 'Perfil',
      'direccion' => 'Direccion',
      'percent' => 'Porcentaje'
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getAbonoServicios()
  {
    return $this->hasMany(\app\models\AbonoServicio::className(), ['empleado_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCobroEmpleados()
  {
    return $this->hasMany(\app\models\CobroEmpleado::className(), ['empleado_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUsuario()
  {
    return $this->hasOne(\app\models\Usuario::className(), ['id' => 'usuario_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getEmpleadoComisions()
  {
    return $this->hasMany(\app\models\EmpleadoComision::className(), ['empleado_id' => 'id'])
      ->orderBy(['empleado_comision.id'=>SORT_DESC]);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getPagoEmpleados()
  {
    return $this->hasMany(\app\models\PagoEmpleado::className(), ['empleado_id' => 'id'])
      ->orderBy(['pago_empleado.id'=>SORT_DESC]);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getServicios()
  {
    return $this->hasMany(\app\models\Servicio::className(), ['empleado_id' => 'id'])
      ->orderBy(['servicio.id'=>SORT_DESC]);
  }

  /**
   * @inheritdoc
   * @return \app\models\EmpleadoQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\EmpleadoQuery(get_called_class());
  }
}
