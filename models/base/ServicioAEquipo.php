<?php

namespace app\models\base;

/**
 * This is the base model class for table "servicio_a_equipo".
 *
 * @property integer $id
 * @property integer $servicio_id
 * @property integer $equipo_id
 * @property integer $codigo_isis_id
 * @property string $descripcion
 *
 * @property Equipo $equipo
 * @property Servicio $servicio
 * @property CodigoIsis $codigoIsis
 */
class ServicioAEquipo extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'servicio_a_equipo';
  }

  /**
   * @inheritdoc
   * @return \app\models\ServicioAEquipoQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\ServicioAEquipoQuery(get_called_class());
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['servicio_id', 'equipo_id'], 'required'],
      [['servicio_id', 'equipo_id', 'codigo_isis_id'], 'integer'],
      [['descripcion'], 'string', 'max' => 250]
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'servicio_id' => 'Servicio',
      'equipo_id' => 'Equipo',
      'codigo_isis_id' => 'Codigo de error',
      'descripcion' => 'Falla segun cliente',
      'selected' => 'Agregar al servicio'
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getEquipo()
  {
    return $this->hasOne(\app\models\Equipo::className(), ['id' => 'equipo_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getServicio()
  {
    return $this->hasOne(\app\models\Servicio::className(), ['id' => 'servicio_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCodigoIsis()
  {
    return $this->hasOne(\app\models\CodigoIsis::className(), ['id' => 'codigo_isis_id']);
  }
}
