<?php

namespace app\models\base;

/**
 * This is the base model class for table "pagina".
 *
 * @property integer $id
 * @property integer $titulo
 * @property string $texto
 */
class Pagina extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['titulo', 'texto'], 'required'],
      [['texto'], 'string']
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'pagina';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'titulo' => 'Titulo',
      'texto' => 'Texto',
    ];
  }

  /**
   * @inheritdoc
   * @return \app\models\PaginaQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\PaginaQuery(get_called_class());
  }
}
