<?php

namespace app\models\base;

/**
 * This is the base model class for table "comisionista_servicio".
 *
 * @property integer $id
 * @property integer $trabajo_id
 * @property integer $comisionista_id
 * @property integer $abono_comisionista_id
 * @property string $nombre_cliente
 * @property string $correo
 * @property string $direccion_cliente
 * @property string $telefono_cliente
 * @property boolean $arreglado
 * @property integer $monto
 * @property boolean $concretado
 * @property boolean $cerrado
 *
 * @property \app\models\Comisionista $comisionista
 * @property \app\models\Trabajo $trabajo
 * @property \app\models\Servicio $servicio
 */
class ComisionistaServicio extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['trabajo_id', 'comisionista_id', 'nombre_cliente', 'telefono_cliente', 'monto'], 'required'],
      [['trabajo_id', 'comisionista_id', 'abono_comisionista_id', 'monto'], 'integer'],
      [['arreglado', 'concretado', 'cerrado'], 'boolean'],
      [['nombre_cliente', 'correo', 'direccion_cliente', 'telefono_cliente'], 'string', 'max' => 250],
      ['razon', 'string', 'max' => 500]
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'comisionista_servicio';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'trabajo_id' => 'Trabajo',
      'comisionista_id' => 'Comisionista',
      'abono_comisionista_id' => 'Abono',
      'nombre_cliente' => 'Nombre Cliente',
      'correo' => 'Correo',
      'direccion_cliente' => 'Direccion Cliente',
      'telefono_cliente' => 'Telefono Cliente',
      'arreglado' => 'Pagado',
      'monto' => 'Monto',
      'concretado' => 'Concretado',
      'cerrado' => 'Cerrado',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getComisionista()
  {
    return $this->hasOne(\app\models\Comisionista::className(), ['id' => 'comisionista_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getTrabajo()
  {
    return $this->hasOne(\app\models\Trabajo::className(), ['id' => 'trabajo_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getServicio()
  {
    return $this->hasOne(\app\models\Servicio::className(), ['comision_servicio_id' => 'id']);
  }

  /**
   * @inheritdoc
   * @return \app\models\ComisionistaServicioQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\ComisionistaServicioQuery(get_called_class());
  }
}
