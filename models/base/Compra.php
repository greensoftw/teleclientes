<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "compra".
 *
 * @property integer $id
 * @property integer $proveedor_id
 * @property string $fecha
 * @property boolean $pagado
 * @property integer $total
 *
 * @property \app\models\AbonoCompra[] $abonoCompras
 * @property \app\models\Proveedor $proveedor
 * @property \app\models\ItemCompra[] $itemCompras
 */
class Compra extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proveedor_id', 'fecha', 'total'], 'required'],
            [['proveedor_id', 'total'], 'integer'],
            [['fecha'], 'safe'],
            [['pagado'], 'boolean']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'compra';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'proveedor_id' => 'Proveedor',
            'fecha' => 'Fecha',
            'pagado' => 'Pagado',
            'total' => 'Total',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbonoCompras()
    {
        return $this->hasMany(\app\models\AbonoCompra::className(), ['compra_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProveedor()
    {
        return $this->hasOne(\app\models\Proveedor::className(), ['id' => 'proveedor_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemCompras()
    {
        return $this->hasMany(\app\models\ItemCompra::className(), ['compra_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\models\CompraQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\CompraQuery(get_called_class());
    }
}
