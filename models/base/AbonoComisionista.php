<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "abono_comisionista".
 *
 * @property integer $id
 * @property integer $comisionista_id
 * @property string $fecha
 * @property integer $monto
 *
 * @property \app\models\Comisionista $comisionista
 * @property \app\models\ComisionistaServicio[] $comisionistaServicios
 */
class AbonoComisionista extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comisionista_id', 'fecha', 'monto'], 'required'],
            [['comisionista_id', 'monto'], 'integer'],
            [['fecha'], 'safe']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'abono_comisionista';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'comisionista_id' => 'Comisionista ID',
            'fecha' => 'Fecha',
            'monto' => 'Monto',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComisionista()
    {
        return $this->hasOne(\app\models\Comisionista::className(), ['id' => 'comisionista_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComisionistaServicios()
    {
        return $this->hasMany(\app\models\ComisionistaServicio::className(), ['abono_comisionista_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\models\AbonoComisionistaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\AbonoComisionistaQuery(get_called_class());
    }
}
