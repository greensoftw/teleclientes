<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the base model class for table "configuracion".
 *
 * @property integer $id
 * @property string $key
 * @property string $value
 */
class Configuracion extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key', 'value'], 'required'],
            [['key', 'value'], 'string', 'max' => 250],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'configuracion';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'value' => 'Value',
        ];
    }
    /**
     * @inheritdoc
     * @return \app\models\ConfiguracionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\ConfiguracionQuery(get_called_class());
    }


}
