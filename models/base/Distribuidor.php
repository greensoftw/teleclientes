<?php

namespace app\models\base;

use mootensai\relation\RelationTrait;

/**
 * This is the base model class for table "distribuidor".
 *
 * @property integer $id
 * @property integer $usuario_id
 * @property string $nombre
 * @property string $direccion
 * @property string $telefono
 *
 * @property \app\models\Comisionista[] $comisionistas
 * @property \app\models\Usuario $usuario
 * @property \app\models\DistribuidorDepartamento[] $distribuidorDepartamentos
 * @property \app\models\Equipo[] $equipos
 */
class Distribuidor extends \yii\db\ActiveRecord
{
  use RelationTrait;

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'distribuidor';
  }

  /**
   * @inheritdoc
   * @return \app\models\DistribuidorQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\DistribuidorQuery(get_called_class());
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['usuario_id', 'nombre', 'direccion', 'telefono'], 'required'],
      [['usuario_id'], 'integer'],
      [['nombre', 'direccion', 'telefono'], 'string', 'max' => 250]
    ];
  }

  /**
   *
   * @return string
   * overwrite function optimisticLock
   * return string name of field are used to stored optimistic lock
   *
   */
  public function optimisticLock()
  {
    return 'lock';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'usuario_id' => 'Usuario',
      'nombre' => 'Nombre',
      'direccion' => 'Direccion',
      'telefono' => 'Telefono',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getComisionistas()
  {
    return $this->hasMany(\app\models\Comisionista::className(), ['distribuidor_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUsuario()
  {
    return $this->hasOne(\app\models\Usuario::className(), ['id' => 'usuario_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getDistribuidorDepartamentos()
  {
    return $this->hasMany(\app\models\DistribuidorDepartamento::className(), ['distribuidor_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getEquipos()
  {
    return $this->hasMany(\app\models\Equipo::className(), ['distribuidor_id' => 'id']);
  }
}
