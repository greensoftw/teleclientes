<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "abono_compra".
 *
 * @property integer $id
 * @property integer $compra_id
 * @property string $fecha
 * @property integer $monto
 *
 * @property \app\models\Compra $compra
 */
class AbonoCompra extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['compra_id', 'monto'], 'required'],
            [['compra_id', 'monto'], 'integer'],
            [['fecha'], 'string', 'max' => 250]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'abono_compra';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'compra_id' => 'Compra ID',
            'fecha' => 'Fecha',
            'monto' => 'Monto',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompra()
    {
        return $this->hasOne(\app\models\Compra::className(), ['id' => 'compra_id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\models\AbonoCompraQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\AbonoCompraQuery(get_called_class());
    }
}
