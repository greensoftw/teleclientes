<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "usuario".
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $nombres
 * @property string $avatar
 * @property bool $receive_email
 * @property string $email
 * @property string $authKey
 * @property string $access_token
 * @property string $password_reset_token
 *
 * @property \app\models\ArchivoPdf[] $archivoPdfs
 * @property \app\models\Cliente $cliente
 * @property \app\models\Comisionista $comisionista
 * @property \app\models\Distribuidor $distribuidor
 * @property \app\models\Empleado $empleado
 * @property \app\models\Marca $marca
 * @property \app\models\MovimientosCaja[] $movimientosCajas
 * @property \app\models\Proveedor $proveedor
 */
class Usuario extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password', 'nombres', 'email'], 'required'],
            [['username'], 'string', 'max' => 50],
            [['password', 'authKey', 'access_token', 'password_reset_token'], 'string', 'max' => 500],
            [['nombres', 'email'], 'string', 'max' => 250],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['avatar'], 'safe']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usuario';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Nombres del usuario con acceso',
            'password' => 'Contraseña',
            'nombres' => 'Nombres',
            'email' => 'Email',
            'authKey' => 'Auth Key',
            'access_token' => 'Access Token',
            'password_reset_token' => 'Password Reset Token',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArchivoPdfs()
    {
        return $this->hasMany(\app\models\ArchivoPdf::className(), ['usuario_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCliente()
    {
        return $this->hasOne(\app\models\Cliente::className(), ['user_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComisionista()
    {
        return $this->hasOne(\app\models\Comisionista::className(), ['usuario_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistribuidor()
    {
        return $this->hasOne(\app\models\Distribuidor::className(), ['usuario_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleado()
    {
        return $this->hasOne(\app\models\Empleado::className(), ['usuario_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarca()
    {
        return $this->hasOne(\app\models\Marca::className(), ['usuario_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMovimientosCajas()
    {
        return $this->hasMany(\app\models\MovimientosCaja::className(), ['usuario_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProveedor()
    {
        return $this->hasOne(\app\models\Proveedor::className(), ['usuario_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\models\UsuarioQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\UsuarioQuery(get_called_class());
    }
}
