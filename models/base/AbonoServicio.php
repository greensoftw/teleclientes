<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "abono_servicio".
 *
 * @property integer $id
 * @property integer $servicio_id
 * @property integer $monto
 * @property string $fecha
 * @property integer $empleado_id
 * @property integer $cobro_empleado_id
 *
 * @property \app\models\Servicio $servicio
 * @property \app\models\Empleado $empleado
 * @property \app\models\CobroEmpleado $cobroEmpleado
 */
class AbonoServicio extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['servicio_id', 'monto'], 'required'],
            [['servicio_id', 'monto', 'empleado_id', 'cobro_empleado_id'], 'integer'],
            [['fecha'], 'safe']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'abono_servicio';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'servicio_id' => 'Servicio ID',
            'monto' => 'Monto',
            'fecha' => 'Fecha',
            'empleado_id' => 'Empleado',
            'cobro_empleado_id' => 'Cobro Empleado',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicio()
    {
        return $this->hasOne(\app\models\Servicio::className(), ['id' => 'servicio_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleado()
    {
        return $this->hasOne(\app\models\Empleado::className(), ['id' => 'empleado_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCobroEmpleado()
    {
        return $this->hasOne(\app\models\CobroEmpleado::className(), ['id' => 'cobro_empleado_id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\models\AbonoServicioQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\AbonoServicioQuery(get_called_class());
    }
}
