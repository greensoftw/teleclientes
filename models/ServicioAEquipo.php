<?php

namespace app\models;

use app\models\base\ServicioAEquipo as BaseServicioAEquipo;

/**
 * This is the model class for table "servicio_a_equipo".
 * @var Archivo[] $files
 */
class ServicioAEquipo extends BaseServicioAEquipo
{
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return array_replace_recursive(parent::rules(),
      [
        [['servicio_id', 'equipo_id'], 'required'],
        [['servicio_id', 'equipo_id', 'codigo_isis_id'], 'integer'],
        [['descripcion'], 'string', 'max' => 250]
      ]);
  }

  public function fields()
  {
    $fields = parent::fields();
    $fields['filesCount'] = 'filesCount';
    return $fields;
  }

  public function extraFields()
  {
    return [
      'equipo'=>'equipo',
      'codigoIsis'=>'codigoIsis'
    ];
  }

  public function getFiles()
  {
    return $this->hasMany(Archivo::className(), ['model_id' => 'id'])
      ->andWhere(['model' => ServicioAEquipo::className()]);
  }

  public function getFilesCount(){
    return $this->getFiles()->count();
  }
}
