<?php

namespace app\models;

use \app\models\base\Pagina as BasePagina;

/**
 * This is the model class for table "pagina".
 */
class Pagina extends BasePagina
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['titulo', 'texto'], 'required'],
            [['texto'], 'string']
        ]);
    }
	
}
