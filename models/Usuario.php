<?php

namespace app\models;

use app\models\base\Usuario as BaseUsuario;
use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "usuario".
 */
class Usuario extends BaseUsuario implements \yii\web\IdentityInterface
{
  /**
   * Finds an identity by the given ID.
   * @param string|integer $id the ID to be looked for
   * @return IdentityInterface the identity object that matches the given ID.
   * Null should be returned if such an identity cannot be found
   * or the identity is not in an active state (disabled, deleted, etc.)
   */
  public static function findIdentity($id)
  {
    return static::findOne($id);
  }

  /**
   * @param $username
   * @return ActiveRecord|Usuario
   */
  public static function findByUsername($username)
  {
    return static::find()
      ->where(['username' => $username])
      ->orWhere(['email' => $username])
      ->one();
  }

  /**
   * Finds an identity by the given token.
   * @param mixed $token the token to be looked for
   * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
   * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
   * @return IdentityInterface the identity object that matches the given token.
   * Null should be returned if such an identity cannot be found
   * or the identity is not in an active state (disabled, deleted, etc.)
   */
  public static function findIdentityByAccessToken($token, $type = null)
  {
    return static::findOne(['access_token' => $token]);
    //iregularidad
  }

  public static function findByPasswordResetToken($token)
  {
    $expire = \Yii::$app->params['user.passwordResetTokenExpire'];
    $parts = explode('_', $token);
    $timestamp = (int)end($parts);
    if ($timestamp + $expire < time()) {
      return null;
    }

    return static::findOne([
      'password_reset_token' => $token
    ]);
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['username', 'password', 'nombres', 'email'], 'required'],
      [['username'], 'string', 'max' => 50],
      [['password', 'authKey', 'access_token', 'password_reset_token'], 'string', 'max' => 500],
      [['nombres', 'email'], 'string', 'max' => 250],
      [['username', 'email'], 'unique', 'when' => function (ActiveRecord $model, $attribute) {
        return $model->{$attribute} !== $model->getOldAttribute($attribute);
      }],
    ];
  }

  public function validatePassword($password)
  {
    return Yii::$app->getSecurity()->validatePassword($password, $this->password);
  }

  /**
   * Returns an ID that can uniquely identify a user identity.
   * @return string|integer an ID that uniquely identifies a user identity.
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Returns a key that can be used to check the validity of a given identity ID.
   *
   * The key should be unique for each individual user, and should be persistent
   * so that it can be used to check the validity of the user identity.
   *
   * The space of such keys should be big enough to defeat potential identity attacks.
   *
   * This is required if [[User::enableAutoLogin]] is enabled.
   * @return string a key that is used to check the validity of a given identity ID.
   * @see validateAuthKey()
   */
  public function getAuthKey()
  {
    return $this->authKey;
  }

  /**
   * Validates the given auth key.
   *
   * This is required if [[User::enableAutoLogin]] is enabled.
   * @param string $authKey the given auth key
   * @return boolean whether the given auth key is valid.
   * @see getAuthKey()
   */
  public function validateAuthKey($authKey)
  {
    return $this->authKey === $authKey;
  }

  public function beforeSave($insert)
  {
    if (parent::beforeSave($insert)) {
      if ($this->isNewRecord) {
        $this->authKey = Yii::$app->getSecurity()->generateRandomString();
      }
      return true;
    }
    return false;
  }

  public function getAvatarImage()
  {
    return Yii::getAlias($this->avatar);
  }

  /**
   * Generates password hash from password and sets it to the model
   *
   * @param string $password
   */
  public function setPassword($password)
  {
    $this->password = Yii::$app->security->generatePasswordHash($password);
  }

  public function generateAuthKey()
  {
    $this->authKey = Yii::$app->security->generateRandomString();
    $this->access_token = Yii::$app->security->generateRandomString();
  }

  /**
   * Generates new password reset token
   */
  public function generatePasswordResetToken()
  {
    $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
  }

  public static function isPasswordResetTokenValid($token)
  {
    if (empty($token)) {
      return false;
    }
    $timestamp = (int)substr($token, strrpos($token, '_') + 1);
    $expire = Yii::$app->params['user.passwordResetTokenExpire'];
    return $timestamp + $expire >= time();
  }

  /**
   * Removes password reset token
   */
  public function removePasswordResetToken()
  {
    $this->password_reset_token = null;
  }

  public function fields()
  {
    return ['username', 'nombres'];
  }
}
