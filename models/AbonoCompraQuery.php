<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[AbonoCompra]].
 *
 * @see AbonoCompra
 */
class AbonoCompraQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return AbonoCompra[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return AbonoCompra|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}