<?php

namespace app\models;

use app\models\base\Compra as BaseCompra;

/**
 * This is the model class for table "compra".
 */
class Compra extends BaseCompra
{
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return array_replace_recursive(parent::rules(),
      [
        [['proveedor_id', 'fecha', 'total'], 'required'],
        [['proveedor_id', 'total'], 'integer'],
        [['fecha'], 'safe'],
        [['pagado'], 'boolean']
      ]);
  }

  public function getAbonado()
  {
    $abonado = 0;
    foreach ($this->abonoCompras as $abono) {
      $abonado += $abono->monto;
    }
    return $abonado;
  }
}
