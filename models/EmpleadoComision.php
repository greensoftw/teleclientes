<?php

namespace app\models;

use app\models\base\EmpleadoComision as BaseEmpleadoComision;

/**
 * This is the model class for table "empleado_comision".
 */
class EmpleadoComision extends BaseEmpleadoComision
{
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return array_replace_recursive(parent::rules(),
      [
        [['servicio_id', 'empleado_id', 'fecha', 'monto', 'arreglado'], 'required'],
        [['servicio_id', 'empleado_id', 'pago_empleado_id', 'monto', 'arreglado'], 'integer'],
        [['fecha'], 'safe']
      ]);
  }

}
