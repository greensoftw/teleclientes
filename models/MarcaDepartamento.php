<?php

namespace app\models;

use \app\models\base\MarcaDepartamento as BaseMarcaDepartamento;

/**
 * This is the model class for table "marca_departamento".
 */
class MarcaDepartamento extends BaseMarcaDepartamento
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['marca_id', 'nombre', 'email'], 'required'],
            [['marca_id'], 'integer'],
            [['nombre'], 'string', 'max' => 200],
            [['email'], 'string', 'max' => 100]
        ]);
    }
	
}
