<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PagoEmpleado]].
 *
 * @see PagoEmpleado
 */
class PagoEmpleadoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PagoEmpleado[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PagoEmpleado|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}