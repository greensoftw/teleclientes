<?php

namespace app\models;

use \app\models\base\Repuestos as BaseRepuestos;

/**
 * This is the model class for table "repuestos".
 */
class Repuestos extends BaseRepuestos
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['codigo', 'nombre', 'costo', 'precio'], 'required'],
            [['marca_id', 'costo', 'precio'], 'integer'],
            [['codigo', 'no_parte', 'nombre'], 'string', 'max' => 250]
        ]);
    }
	
}
