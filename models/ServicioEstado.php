<?php

namespace app\models;

use app\models\base\ServicioEstado as BaseServicioEstado;

/**
 * This is the model class for table "servicio_estado".
 */
class ServicioEstado extends BaseServicioEstado
{
  public $comision;

  const INGRESADO = "Ingresado";
  const COTIZADO = "Cotizado";
  const POR_CONFIRMACION = "Por Confirmación";
  const CONFIRMADO = "Confirmado";
  const EN_LUGAR = "En lugar de reparación";
  const RETIRADO_LUGAR = "Retirado del lugar";
  const REPARADO = "Reparado";
  const EN_PRUEBA = "En prueba";
  const ENTREGADO = "Entregado";

  public static $dropEstados = [
    ServicioEstado::INGRESADO => ServicioEstado::INGRESADO,
    ServicioEstado::COTIZADO => ServicioEstado::COTIZADO,
    ServicioEstado::POR_CONFIRMACION => ServicioEstado::POR_CONFIRMACION,
    ServicioEstado::CONFIRMADO => ServicioEstado::CONFIRMADO,
    ServicioEstado::EN_LUGAR => ServicioEstado::EN_LUGAR,
    ServicioEstado::RETIRADO_LUGAR => ServicioEstado::RETIRADO_LUGAR,
    ServicioEstado::REPARADO => ServicioEstado::REPARADO,
    ServicioEstado::EN_PRUEBA => ServicioEstado::EN_PRUEBA,
    ServicioEstado::ENTREGADO => ServicioEstado::ENTREGADO,
  ];

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return array_replace_recursive(parent::rules(),
      [
        [['servicio_id', 'fecha', 'estado'], 'required'],
        [['servicio_id','comision','actual'], 'integer'],
        [['fecha'], 'safe'],
        [['estado', 'descripcion'], 'string', 'max' => 250]
      ]);
  }

  public function __toString()
  {
    return $this->estado." - ".\Yii::$app->formatter->asDatetime($this->fecha);
  }

  /**
   * @param $estado ServicioEstado
   * @return array
   */
  public static function getDropDown($estado){
    $estados = ServicioEstado::$dropEstados;
    foreach ($estados as $key => $estadoDrop) {
      if($estado->estado == $estadoDrop){
        unset($estados[$key]);
        break;
      }
        unset($estados[$key]);
    }
    return $estados;
  }
}
