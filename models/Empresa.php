<?php

namespace app\models;

use yii\base\Model;
use yii\helpers\ArrayHelper;

;

/**
 * This is the model class for table "archivo".
 */
class Empresa extends Model
{

  public $isNewRecord = true;
  public $picture;

  public $nombre;
  public $nit;
  public $tel_fijo;
  public $tel_cel;
  public $direccion;
  public $email;
  public $web;


  public function rules()
  {
    return [
      [['nombre', 'nit', 'tel_fijo', 'tel_cel', 'direccion', 'email', 'web'], 'required'],
      [['email'], 'email'],
      [['picture'], 'file','extensions'=>'jpg,png,jpeg'],
    ];
  }


  public function Guardar()
  {
    $objetc = ArrayHelper::toArray($this);
    \Yii::info($objetc);
    foreach ($objetc as $key => $obj) {
      if ($key != 'isNewRecord') {
        if ($this->isNewRecord) {
          $config = new Configuracion([
            'key' => $key,
            'value' => $obj
          ]);
          if($key == 'nombre'){
            $ini = explode(' ',$obj);
            $iniciales = '';
            foreach ($ini as $item) {
              $iniciales .= strtoupper(substr($item,0,1));
            }
            $conf = new Configuracion([
              'key'=>'iniciales',
              'value'=>$iniciales
            ]);
            $conf->save();
          }
        } else {
          $config = Configuracion::findOne(['key' => $key]);
          if ($config != null)
            $config->value = $obj;
        }
        if ($config != null)
          if ($config->save()) ;
      }
    }
  }

  public function init()
  {
    $config = Configuracion::find()->all();
    foreach ($config as $key => $item) {
      $attribute = $item->key;
      try{
        $this->$attribute = $item->value;
      }catch (\Exception $e){}
      $this->isNewRecord = false;
    }
  }


  public static function find()
  {
    return new static();
  }
}
