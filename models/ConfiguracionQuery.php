<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Configuracion]].
 *
 * @see Configuracion
 */
class ConfiguracionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Configuracion[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Configuracion|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}