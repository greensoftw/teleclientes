<?php

namespace app\models;

use \app\models\base\Cliente as BaseCliente;

/**
 * This is the model class for table "cliente".
 */
class Cliente extends BaseCliente
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['user_id', 'comisionista_id'], 'integer'],
            [['telefonos', 'direccion'], 'required'],
            [['telefonos', 'direccion'], 'string', 'max' => 250]
        ]);
    }

    public function fields()
    {
      $fields = parent::fields();
      $fields['usuario'] = 'usuario';
      return $fields;
    }
}
