<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Trabajo]].
 *
 * @see Trabajo
 */
class TrabajoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Trabajo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Trabajo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}