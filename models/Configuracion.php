<?php

namespace app\models;

use \app\models\base\Configuracion as BaseConfiguracion;

/**
 * This is the model class for table "configuracion".
 */
class Configuracion extends BaseConfiguracion
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['key', 'value'], 'required'],
            [['key', 'value'], 'string', 'max' => 250],
        ]);
    }


	
}
