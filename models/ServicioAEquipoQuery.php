<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ServicioAEquipo]].
 *
 * @see ServicioAEquipo
 */
class ServicioAEquipoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return ServicioAEquipo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ServicioAEquipo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}