<?php

namespace app\models;

use \app\models\base\Distribuidor as BaseDistribuidor;

/**
 * This is the model class for table "distribuidor".
 */
class Distribuidor extends BaseDistribuidor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['usuario_id', 'nombre', 'direccion', 'telefono'], 'required'],
            [['usuario_id'], 'integer'],
            [['nombre', 'direccion', 'telefono'], 'string', 'max' => 250]
        ]);
    }

    public function __toString()
    {
      return $this->nombre;
    }

}
