<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Archivo]].
 *
 * @see Archivo
 */
class ArchivoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Archivo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Archivo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}