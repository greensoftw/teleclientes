<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[MarcaDepartamento]].
 *
 * @see MarcaDepartamento
 */
class MarcaDepartamentoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return MarcaDepartamento[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MarcaDepartamento|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}