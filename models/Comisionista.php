<?php

namespace app\models;

use app\models\base\Comisionista as BaseComisionista;

/**
 * This is the model class for table "comisionista".
 */
class Comisionista extends BaseComisionista
{
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return array_replace_recursive(parent::rules(),
      [
        [['usuario_id','documento'], 'required'],
        [['usuario_id', 'distribuidor_id'], 'integer'],
        [['telefonos','documento'], 'string', 'max' => 100],
        [['usuario_id'], 'unique']
      ]);
  }

  public function getGanancia()
  {
    return $this->getComisionistaServicios()
      ->where(['concretado'=>1,'arreglado'=>0,'cerrado'=>0])
      ->sum('monto');
  }

  public function fields()
  {
    $fields = parent::fields();
    $fields['ganancia'] = 'ganancia';
    return $fields;
  }

  public function getTotal(){
    return $this->getComisionistaServicios()
      ->where(['arreglado' => 0, 'concretado' => 1, 'cerrado'=>1])
      ->sum('monto');
  }

  public function getTotalPendiente(){
    return $this->getComisionistaServicios()
      ->where(['arreglado' => 0, 'concretado' => 1, 'cerrado'=>0])
      ->sum('monto');
  }
}
