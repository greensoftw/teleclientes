<?php
namespace app\models;

use yii\base\Model;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $nombres;
    public $email;
    public $password;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            [['email','password','nombres','username'], 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['password', 'string', 'min' => 4],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email'=>'Correo electronico',
            'nombres'=>'Su nombre',
            'username'=>'Nombre de usuario',
            'password'=>'Contraseña'
        ];
    }

    /**
     * Signs user up.
     *
     * @return Usuario|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new Usuario();
        $user->nombres = $this->nombres;
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        
        return $user->save() ? $user : null;
    }
}
