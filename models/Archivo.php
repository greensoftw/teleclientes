<?php

namespace app\models;

use app\models\base\Archivo as BaseArchivo;

/**
 * This is the model class for table "archivo".
 * @property  Empleado|Servicio $relacion
 */
class Archivo extends BaseArchivo
{
  public $file;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['file'], 'file', 'extensions' => 'jpg, png, pdf, jpeg, txt, xml, xls, xlsx, json, html',
        'checkExtensionByMimeType' => false,'on'=>'fileUP'],
      ['file','safe'],
      [['model', 'model_id', 'title', 'type'], 'required'],
      [['model_id'], 'integer'],
      [['model', 'ruta', 'title'], 'string', 'max' => 250],
      [['type'], 'string', 'max' => 100]
    ];
  }

  public function getTipoArchivo()
  {
    switch ($this->type) {
      case "image/png":
        return "IMAGEN";
      case  "image/jpg":
        return "IMAGEN";
      case  "image/pjpeg":
        return "IMAGEN";
      case  "image/jpeg":
        return "IMAGEN";

      case "application/pdf":
        return "PDF";
      case "application/vnd.ms-excel":
        return "EXCEL";
      case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
        return "EXCEL";
      case "application/octet-stream":
        return "JSON";
      case "text/plain":
        return "TEXTO";
      case "text/xml":
        return "XML";
      case "text/html":
        return "HTML";
      default:
        return "DESCONOCIDO";
    }
  }

  public function getRelacion()
  {
    return $this->hasOne($this->model, ['id' => 'model_id']);
  }

}
