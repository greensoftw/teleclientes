<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ArchivoPdf]].
 *
 * @see ArchivoPdf
 */
class ArchivoPdfQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return ArchivoPdf[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ArchivoPdf|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}