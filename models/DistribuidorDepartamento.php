<?php

namespace app\models;

use app\models\base\DistribuidorDepartamento as BaseDistribuidorDepartamento;

/**
 * This is the model class for table "distribuidor_departamento".
 */
class DistribuidorDepartamento extends BaseDistribuidorDepartamento
{
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return array_replace_recursive(parent::rules(),
      [
        [['distribuidor_id', 'nombre', 'email'], 'required'],
        [['distribuidor_id'], 'integer'],
        [['nombre'], 'string', 'max' => 200],
        [['email'], 'string', 'max' => 100]
      ]);
  }

}
