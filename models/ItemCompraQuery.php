<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ItemCompra]].
 *
 * @see ItemCompra
 */
class ItemCompraQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return ItemCompra[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ItemCompra|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}