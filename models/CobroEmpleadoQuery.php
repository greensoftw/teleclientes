<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[CobroEmpleado]].
 *
 * @see CobroEmpleado
 */
class CobroEmpleadoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return CobroEmpleado[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CobroEmpleado|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}