<?php
/**
 * Created by PhpStorm.
 * User: Andres RS
 * Date: 01/06/2015
 * Time: 3:58 PM
 */

namespace app\models;


use Yii;
use yii\base\Model;
use yii\db\Connection;
use yii\helpers\Json;

class UserRepass extends Model
{
    public $empresa_id;
    public $correo;
    public $verifyCode;

    public function rules()
    {
        return [
            [['correo','empresa_id'], 'required'],
            ['correo', 'email'],
            ['verifyCode', 'captcha','captchaAction'=>'sitio/captcha'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'empresa_id'=>'Seleccione su empresa',
            'correo' => 'introduzca su email',
            'verifyCode' => 'Introduzca el código Captcha',
        ];
    }

    public function enviarEmail()
    {
        if ($this->validate()) {
            $empresa = EmpresaTelecliente::findOne($this->empresa_id);
            Yii::$app->db = new Connection(Json::decode($empresa->conexion));
            /** @var Usuario $user */
            $user = Usuario::find()->where('email = :email', [':email' => $this->correo])->one();
            if ($user != null) {
                $user->generatePasswordResetToken();
                $user->save(false);
                return Yii::$app->mailer->compose('recordar_contrasena', [
                    'model' => $user,
                    'empresa' => $empresa
                ])->setSubject("Recuperacion de contraseña : " . Yii::$app->name)
                    ->setFrom("info@teleclientes.com")
                    ->setTo($user->email)
                    ->send();
            } else
                $this->addError('correo', 'correo no registrado !');

        }
        return false;
    }
}