<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Connection;
use yii\helpers\Json;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
  public $empresa_id;
  public $username;
  public $password;
  public $rememberMe = true;

  private $_user = false;
  private $_empresa;


  /**
   * @return array the validation rules.
   */
  public function rules()
  {
    return [
      [['username', 'password', 'empresa_id'], 'required'],
      ['password', 'validatePassword'],
      ['rememberMe', 'boolean'],
    ];
  }

  public function attributeLabels()
  {
    return [
      'username' => 'Nombre de acceso o email',
      'password' => 'Contraseña',
      'rememberMe' => 'Mantenme Logueado',
      'empresa_id' => 'Empresa'
    ];
  }

  public function validatePassword()
  {
    if (!$this->hasErrors()) {
      $this->_empresa = EmpresaTelecliente::findOne($this->empresa_id);
      Yii::$app->db = new Connection(Json::decode($this->_empresa->conexion));
      Yii::info(Yii::$app->db);
      $user = $this->getUser();
      if (!$user || !$user->validatePassword($this->password)) {
        $this->addError('password', 'Usuario o contraseña incorrecta.');
        return false;
      }
    }
    return true;
  }

  /**
   * Logs in a user using the provided username and password.
   * @return bool whether the user is logged in successfully
   */
  public function login()
  {
    if ($this->validate()) {
      Yii::$app->session->set('empresa_id',$this->_empresa->id);
      return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
    }
    return false;
  }

  /**
   * Finds user by [[username]]
   *
   * @return Usuario|null
   */
  public function getUser()
  {
    if ($this->_user === false) {
      $this->_user = Usuario::findByUsername($this->username);
    }

    return $this->_user;
  }
}
