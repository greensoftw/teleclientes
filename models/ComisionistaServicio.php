<?php

namespace app\models;

use app\models\base\ComisionistaServicio as BaseComisionistaServicio;

/**
 * This is the model class for table "comisionista_servicio".
 */
class ComisionistaServicio extends BaseComisionistaServicio
{
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return array_replace_recursive(parent::rules(),
      [
        [['trabajo_id', 'comisionista_id', 'nombre_cliente', 'telefono_cliente', 'monto'], 'required'],
        [['trabajo_id', 'comisionista_id', 'abono_comisionista_id', 'monto'], 'integer'],
        [['arreglado', 'concretado', 'cerrado'], 'boolean'],
        [['nombre_cliente', 'correo', 'direccion_cliente', 'telefono_cliente'], 'string', 'max' => 250],
        ['razon','string','max' => 500]
      ]);
  }
}
