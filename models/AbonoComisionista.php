<?php

namespace app\models;

use \app\models\base\AbonoComisionista as BaseAbonoComisionista;

/**
 * This is the model class for table "abono_comisionista".
 */
class AbonoComisionista extends BaseAbonoComisionista
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['comisionista_id', 'fecha', 'monto'], 'required'],
            [['comisionista_id', 'monto'], 'integer'],
            [['fecha'], 'safe']
        ]);
    }
	
}
