<?php

namespace app\models;

use \app\models\base\ServicioProgramado as BaseServicioProgramado;

/**
 * This is the model class for table "servicio_programado".
 */
class ServicioProgramado extends BaseServicioProgramado
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['cliente_id', 'fecha'], 'required'],
            [['cliente_id', 'servicio_id', 'equipo_id'], 'integer'],
            [['fecha'], 'safe']
        ]);
    }
	
}
