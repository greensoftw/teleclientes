<?php

namespace app\models;

use app\models\base\Empleado as BaseEmpleado;

/**
 * This is the model class for table "empleado".
 *
 * @property \app\models\Archivo[] $files
 */
class Empleado extends BaseEmpleado
{
  public $rol;

  const CON_COMISION_AN_SUELDO = "Sueldo mas Comision";
  const CON_SUELDO = "Sueldo";
  const CON_COMISION = "Por Comision";
  public static $dropTipos = [
    Empleado::CON_COMISION => Empleado::CON_COMISION,
    Empleado::CON_SUELDO => Empleado::CON_SUELDO,
    Empleado::CON_COMISION_AN_SUELDO => Empleado::CON_COMISION_AN_SUELDO
  ];

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return array_replace_recursive(parent::rules(),
      [
        [['usuario_id', 'telefono', 'tipo_empleado', 'saldo','direccion'], 'required'],
        [['usuario_id', 'sueldo', 'saldo','porcentaje'], 'integer'],
        [['proximo_pago'], 'safe'],
        [['is_tecnico'], 'boolean'],
        [['telefono', 'tipo_empleado','direccion','rol'], 'string', 'max' => 250],
        [['perfil'], 'string', 'max' => 1000]
      ]);
  }

  public function getFiles(){
    return $this->hasMany(Archivo::className(),['model_id'=>'id'])
      ->andWhere(['model'=>Empleado::className()]);
  }

  public function attributeLabels()
  {
    $labels = parent::attributeLabels();
    $labels['rol'] = 'Asignar Rol';
    return $labels;
  }

  public function getDeuda(){
    return $this->getAbonoServicios()->where(['is','cobro_empleado_id',null])
      ->sum('monto');
  }

  public function getSaldoDebe(){
    /** @var CobroEmpleado $model */
    $model = $this->getCobroEmpleados()->orderBy(['id'=>SORT_DESC])->one();
    if($model != null)
      return $model->saldo;
    return 0;
  }

  public function getPercent(){
    return $this->porcentaje <= 9 ? floatval("0.0" . $this->porcentaje) :
      floatval("0." . $this->porcentaje);
  }
}
