<?php

namespace app\models;

use \app\models\base\Marca as BaseMarca;

/**
 * This is the model class for table "marca".
 */
class Marca extends BaseMarca
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nombre'], 'required'],
            [['usuario_id'], 'integer'],
            [['nombre', 'ciudad', 'direccion', 'telefonos'], 'string', 'max' => 250]
        ]);
    }
	
}
