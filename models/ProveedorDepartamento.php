<?php

namespace app\models;

use \app\models\base\ProveedorDepartamento as BaseProveedorDepartamento;

/**
 * This is the model class for table "proveedor_departamento".
 */
class ProveedorDepartamento extends BaseProveedorDepartamento
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['proveedor_id', 'nombre', 'email'], 'required'],
            [['proveedor_id'], 'integer'],
            [['nombre'], 'string', 'max' => 200],
            [['email'], 'string', 'max' => 100]
        ]);
    }
	
}
