<?php

namespace app\models\search;

use app\models\ComisionistaServicio;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * app\models\search\ComisionistaServicioSearch represents the model behind the search form about `app\models\ComisionistaServicio`.
 */
class ComisionistaServicioSearch extends ComisionistaServicio
{
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['id', 'trabajo_id', 'comisionista_id'], 'integer'],
      [['nombre_cliente', 'correo', 'direccion_cliente', 'telefono_cliente'], 'safe'],
      [['arreglado', 'concretado', 'cerrado'], 'boolean'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   *
   * @return ActiveDataProvider
   */
  public function search($params)
  {
    $query = ComisionistaServicio::find()->orderBy(['id' => SORT_DESC]);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);
    $this->arreglado = 0;
    $this->load($params);

    $query->andFilterWhere([
      'id' => $this->id,
      'trabajo_id' => $this->trabajo_id,
      'comisionista_id' => $this->comisionista_id,
      'arreglado' => $this->arreglado,
      'concretado' => $this->concretado,
      'cerrado' => $this->cerrado,
    ]);

    $query->andFilterWhere(['like', 'nombre_cliente', $this->nombre_cliente])
      ->andFilterWhere(['like', 'correo', $this->correo])
      ->andFilterWhere(['like', 'direccion_cliente', $this->direccion_cliente])
      ->andFilterWhere(['like', 'telefono_cliente', $this->telefono_cliente]);
    return $dataProvider;
  }


  public function searchAccess($params)
  {
    $query = ComisionistaServicio::find()->orderBy(['id' => SORT_DESC]);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);
    $this->arreglado = 0;
    $this->load($params);

    $query->andFilterWhere([
      'id' => $this->id,
      'trabajo_id' => $this->trabajo_id,
      'comisionista_id' => $this->comisionista_id,
      'arreglado' => $this->arreglado,
      'concretado' => $this->concretado,
      'cerrado' => $this->cerrado,
    ]);

    $query->andFilterWhere(['like', 'nombre_cliente', $this->nombre_cliente])
      ->andFilterWhere(['like', 'correo', $this->correo])
      ->andFilterWhere(['like', 'direccion_cliente', $this->direccion_cliente])
      ->andFilterWhere(['like', 'telefono_cliente', $this->telefono_cliente]);
    return $dataProvider;
  }
}
