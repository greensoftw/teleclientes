<?php

namespace app\models\search;

use app\models\Cliente;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * app\models\search\ClienteSearch represents the model behind the search form about `app\models\Cliente`.
 */
class ClienteSearch extends Cliente
{
  public $correo;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['id', 'user_id'], 'integer'],
      [['telefonos', 'direccion', 'correo'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   *
   * @return ActiveDataProvider
   */
  public function search($params)
  {
    $query = Cliente::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);

    $this->load($params);

    if (!$this->validate()) {
      // uncomment the following line if you do not want to return any records when validation fails
      // $query->where('0=1');
      return $dataProvider;
    }

    $query->andFilterWhere([
      'id' => $this->id,
      'user_id' => $this->user_id,
    ]);
    $query->joinWith('usuario')
      ->andFilterWhere(['like','usuario.correo',$this->correo]);

    $query->andFilterWhere(['like', 'telefonos', $this->telefonos])
      ->andFilterWhere(['like', 'direccion', $this->direccion]);

    return $dataProvider;
  }
}
