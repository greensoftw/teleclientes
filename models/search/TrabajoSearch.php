<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Trabajo;

/**
 * app\models\search\TrabajoSearch represents the model behind the search form about `app\models\Trabajo`.
 */
 class TrabajoSearch extends Trabajo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'precio', 'comision'], 'integer'],
            [['nombre', 'descripcion'], 'safe'],
            [['visible_comisionista'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Trabajo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'precio' => $this->precio,
            'visible_comisionista' => $this->visible_comisionista,
            'comision' => $this->comision,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion]);

        return $dataProvider;
    }
}
