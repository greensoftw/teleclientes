<?php

namespace app\models\search;

use app\models\Servicio;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * app\models\search\ServicioSearch represents the model behind the search form about `app\models\Servicio`.
 */
class ServicioSearch extends Servicio
{
  public $fecha_desde;
  public $fecha_hasta;
  public $estado;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['id', 'empleado_id', 'cliente_id', 'comision_servicio_id', 'total', 'pagado'], 'integer'],
      [['no_facturado', 'cerrado', 'fecha_desde', 'fecha_hasta',
        'fecha_tecnico', 'estado', 'marca_id', 'numero', 'numero_garantia'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   * @return ActiveDataProvider
   */
  public function search($params)
  {
    $query = Servicio::find()->orderBy(['id' => SORT_DESC]);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'sort' => false
    ]);
    $this->load($params);

    if(!empty($this->numero)){
      $query->where(['numero'=>$this->numero]);
    }else{
      $query->andFilterWhere([
        'cliente_id' => $this->cliente_id,
        'total' => $this->total,
        'pagado' => $this->pagado,
        'marca_id' => $this->marca,
        'cerrado' => $this->cerrado,
        'numero_garantia' => $this->numero_garantia
      ]);
      \Yii::info($this->empleado_id);
      if ($this->empleado_id != null and $this->empleado_id == 0)
        $query->andWhere(['is', 'empleado_id', null]);
      else
        $query->andFilterWhere(['empleado_id' => $this->empleado_id]);

      $query->joinWith(['servicioEstados']);
      if (!empty($this->fecha_tecnico) and (strlen($this->fecha_tecnico) > 10)) {
        $this->fecha_tecnico = substr($this->fecha_tecnico, 0, 10);
        \Yii::info($this->fecha_tecnico);
      }

      $query->andFilterWhere(['between', 'servicio_estado.fecha', $this->fecha_desde, $this->fecha_hasta]);
      $query->andFilterWhere(['LIKE', 'fecha_tecnico', $this->fecha_tecnico]);
      if (!empty($this->estado))
        $query->andFilterWhere(['servicio_estado.estado' => $this->estado])
          ->andWhere(['servicio_estado.actual' => 1]);
      $query->andFilterWhere(['like', 'no_facturado', $this->no_facturado]);
    }
    return $dataProvider;
  }

  public function searchMarca($params, $marca_id)
  {
    $query = Servicio::find()->orderBy(['id' => SORT_DESC]);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'sort' => false
    ]);
    $this->load($params);

    $query->joinWith('servicioAEquipo.equipo')
      ->where(['equipo.marca_id' => $marca_id]);

    if($this->marca_id != ''){
      if ($this->marca_id)
        $query->andWhere(['IS NOT', 'servicio.marca_id', null]);
      else
        $query->andWhere(['IS', 'servicio.marca_id', null]);
    }


    $query->andFilterWhere([
      'id' => $this->id,
      'numero' => $this->numero,
      'numero_garantia' => $this->numero_garantia
    ]);
    return $dataProvider;
  }

  public function searchDistribuidor($params, $distribuidor_id)
  {
    $query = Servicio::find()->orderBy(['id' => SORT_DESC]);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'sort' => false
    ]);
    $this->load($params);

    $query->joinWith('servicioAEquipo.equipo')
      ->where(['equipo.distribuidor_id' => $distribuidor_id]);

    $query->andFilterWhere([
      'id' => $this->id,
      'numero' => $this->numero,
      'numero_garantia' => $this->numero_garantia
    ]);
    return $dataProvider;
  }
}
