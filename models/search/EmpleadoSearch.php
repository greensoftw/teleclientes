<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Empleado;

/**
 * app\models\search\EmpleadoSearch represents the model behind the search form about `app\models\Empleado`.
 */
 class EmpleadoSearch extends Empleado
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'usuario_id', 'sueldo', 'saldo'], 'integer'],
            [['telefono', 'tipo_empleado', 'proximo_pago', 'perfil','direccion'], 'safe'],
            [['is_tecnico'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Empleado::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'usuario_id' => $this->usuario_id,
            'sueldo' => $this->sueldo,
            'proximo_pago' => $this->proximo_pago,
            'saldo' => $this->saldo,
            'is_tecnico' => $this->is_tecnico,
        ]);

        $query->andFilterWhere(['like', 'telefono', $this->telefono])
            ->andFilterWhere(['like', 'tipo_empleado', $this->tipo_empleado])
            ->andFilterWhere(['like', 'perfil', $this->perfil])
          ->andFilterWhere(['like','direccion',$this->direccion]);

        return $dataProvider;
    }
}
