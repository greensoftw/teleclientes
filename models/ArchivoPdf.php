<?php

namespace app\models;

use \app\models\base\ArchivoPdf as BaseArchivoPdf;

/**
 * This is the model class for table "archivo_pdf".
 */
class ArchivoPdf extends BaseArchivoPdf
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['usuario_id', 'nombre', 'descripcion', 'ruta_archivo'], 'required'],
            [['usuario_id'], 'integer'],
            [['nombre'], 'string', 'max' => 250],
            [['descripcion'], 'string', 'max' => 500],
            [['ruta_archivo'], 'string', 'max' => 200]
        ]);
    }
	
}
