<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[DistribuidorDepartamento]].
 *
 * @see DistribuidorDepartamento
 */
class DistribuidorDepartamentoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return DistribuidorDepartamento[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return DistribuidorDepartamento|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}