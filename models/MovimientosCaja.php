<?php

namespace app\models;

use \app\models\base\MovimientosCaja as BaseMovimientosCaja;

/**
 * This is the model class for table "movimientos_caja".
 */
class MovimientosCaja extends BaseMovimientosCaja
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['fecha', 'monto', 'descripcion', 'usuario_id'], 'required'],
            [['fecha'], 'safe'],
            [['monto', 'usuario_id'], 'integer'],
            [['descripcion'], 'string', 'max' => 250],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
