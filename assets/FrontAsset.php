<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class FrontAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'theme/css/bootstrap.min.css',
        'theme/fonts/open-sans/stylesheet.css',
        'theme/css/font-awesome.css',
        'theme/css/simple-line-icons.css',
        'theme/css/overwrite.css',
        'theme/css/owl.carousel.css',
        'theme/css/owl.theme.css',
        'theme/css/owl.transitions.css',
        'theme/css/diamonds.css',
        'theme/css/animate.min.css',
        'theme/css/flexslider.css',
        'theme/css/style.css',
        'theme/skins/default.css',
    ];
    public $js = [
        'theme/js/ie10-viewport-bug-workaround.js',
        'theme/js/ie-emulation-modes-warning.js',
        'theme/js/navigation/waypoints.min.js',
        'theme/js/parallax/jquery.parallax-1.1.3.js',
        'theme/js/parallax/setting.js',
        'theme/js/owlcarousel/owl.carousel.js',
        'theme/js/owlcarousel/setting.js',
        'theme/js/flexslider/jquery.flexslider.js',
        'theme/js/flexslider/setting.js',
        'theme/js/diamonds/jquery.diamonds.js',
        'theme/js/diamonds/setting.js',
        'theme/js/tweecool/tweecool.js',
        'theme/js/jquery.easing.1.3.js',
        'theme/js/totop/jquery.ui.totop.js',
        'theme/js/totop/setting.js',
        'theme/js/validation.js',
        'theme/js/custom.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
