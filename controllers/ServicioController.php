<?php

namespace app\controllers;

use app\actions\DeleteFile;
use app\actions\UploadFile;
use app\actions\ViewFile;
use app\helpers\GSHelper;
use app\models\AbonoServicio;
use app\models\base\EmpleadoComision;
use app\models\base\Trabajo;
use app\models\Empresa;
use app\models\Equipo;
use app\models\Presupuesto;
use app\models\Repuestos;
use app\models\search\ServicioSearch;
use app\models\Servicio;
use app\models\ServicioAEquipo;
use app\models\ServicioEstado;
use app\models\ServicioProgramado;
use kartik\form\ActiveForm;
use kartik\mpdf\Pdf;
use mdm\admin\components\AccessControl;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * ServicioController implements the CRUD actions for Servicio model.
 */
class ServicioController extends BaseController
{
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
      'access' => [
        'class' => AccessControl::className(),
        'allowActions' => ['add-presupuesto', 'add-servicio-aequipo', 'precio'],
      ]
    ];
  }

  /**
   * Lists all Servicio models.
   * @return mixed
   */
  public function actionIndex()
  {
    $searchModel = new ServicioSearch([
      'cerrado' => 0
    ]);
    if (Yii::$app->user->can('Tecnico')) {
      $searchModel->empleado_id = Yii::$app->user->identity->empleado->id;
    }
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  public function actions()
  {
    return [
      'upload-file' => [
        'class' => UploadFile::className(),
        'model' => Servicio::className(),
        'model_id' => Yii::$app->request->get('id'),
        'folder' => 'servicios/servicio-' . Yii::$app->request->get('id')
      ],
      'upload-file-equipo' => [
        'class' => UploadFile::className(),
        'model' => ServicioAEquipo::className(),
        'model_id' => Yii::$app->request->get('equipo_id'),
        'folder' => 'servicio-' . Yii::$app->request->get('id') . '-archivos/equipos'
      ],
      'delete-file' => [
        'class' => DeleteFile::className()
      ],
      'view-file' => [
        'class' => ViewFile::className(),
        'columns' => [
          [
            'attribute' => 'empleado.usuario.nombres',
            'label' => 'Empleado',
          ],
          [
            'attribute' => 'cliente.usuario.nombres',
            'label' => 'Cliente',
          ],
          [
            'attribute' => 'marca.nombre',
            'label' => 'Garantia'
          ],
          'total:currency',
          'pagado:boolean',
          'no_facturado',
          'cerrado:boolean',
        ]
      ]
    ];
  }

  /**
   * Displays a single Servicio model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
    $model = $this->findModel($id);
//    return var_dump($model->servicioAEquipo);
    return $this->render('view', [
      'model' => $model
    ]);
  }

  /**
   * Finds the Servicio model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Servicio the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = Servicio::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
   * Creates a new Servicio model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate()
  {
    $model = new Servicio([
      'total' => 0,
      'comision_servicio_id' => Yii::$app->request->get('com_id'),
      'cliente_id' => Yii::$app->request->get('cliente_id')
    ]);
    $estado = new ServicioEstado([
      'fecha' => date('Y-m-d H:i:s'),
      'estado' => ServicioEstado::INGRESADO,
      'descripcion' => 'Servicio Registrado',
      'actual' => 1
    ]);
    if ($model->load(Yii::$app->request->post()) and
      $estado->load(Yii::$app->request->post())
    ) {
      if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validateMultiple([$model, $estado]);
      }
      /** @var Presupuesto[] $presupuestos */
      $presupuestos = [];

      if (Yii::$app->request->post('Presupuesto')) {
        foreach (Yii::$app->request->post('Presupuesto') as $presupuesto) {
          $presupuestos[] = new Presupuesto($presupuesto);
        }
      }
      if ($model->save()) {
        if ($model->comisionServicio) {
          $model->comisionServicio->concretado = true;
          $model->comisionServicio->save(false);
        }
        if (Yii::$app->request->post('ServicioAEquipo')) {
          $equipo = new ServicioAEquipo(Yii::$app->request->post('ServicioAEquipo'));
          $equipo->servicio_id = $model->id;
          $equipo->save();
        }
        foreach ($presupuestos as $presupuesto) {
          $presupuesto->servicio_id = $model->id;
          $presupuesto->save(false);
        }
        $estado->servicio_id = $model->id;
        $estado->save();
        return $this->redirect(['view', 'id' => $model->id]);
      } else
        Yii::$app->session->setFlash('danger', 'no se pudo guardar el servicio !');
    }
    return $this->render('create', [
      'model' => $model,
      'estado' => $estado
    ]);
  }

  /**
   * Updates an existing Servicio model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id)
  {
    $model = $this->findModel($id);
    $estado = $model->estadoActual;
    if ($model->load(Yii::$app->request->post())) {
      if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validate($model);
      }
      /** @var Presupuesto[] $presupuestos */
      $presupuestos = [];
      if (Yii::$app->request->post('servicioEstado')) {
        $estado->load(Yii::$app->request->post());
        $estado->save(false);
      }
      if (Yii::$app->request->post('Presupuesto')) {
        foreach (Yii::$app->request->post('Presupuesto') as $presupuesto) {
          $presupuestos[] = new Presupuesto($presupuesto);
        }
        foreach ($model->presupuestos as $item) {
          $item->delete();
        }
      }
      if ($model->save()) {
        if (Yii::$app->request->post('ServicioAEquipo')) {
          $equipo = $model->servicioAEquipo ? $model->servicioAEquipo :
            new ServicioAEquipo(['servicio_id' => $model->id]);
          $equipo->setAttributes(Yii::$app->request->post('ServicioAEquipo'));
          $equipo->save();
        }
        foreach ($presupuestos as $presupuesto) {
          $presupuesto->servicio_id = $model->id;
          $presupuesto->save(false);
        }
        return $this->redirect(['view', 'id' => $model->id]);
      } else
        Yii::$app->session->setFlash('danger', 'no se pudo guardar el servicio !');
    }
    return $this->render('update', [
      'model' => $model,
      'estado' => $estado
    ]);
  }

  public function actionAgregarEquipo($id)
  {
    $model = $this->findModel($id);
    $equipo = new Equipo([
      'cliente_id' => $model->cliente_id
    ]);
    $servicioAEquipo = new ServicioAEquipo([
      'servicio_id' => $model->id,
      'selected' => 1
    ]);
    if ($equipo->load(Yii::$app->request->post()) and
      $servicioAEquipo->load(Yii::$app->request->post())
    ) {
      if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validateMultiple([$equipo, $servicioAEquipo]);
      }
      if ($equipo->save()) {
        $servicioAEquipo->equipo_id = $equipo->id;
        $servicioAEquipo->save();
        return $this->redirect(['view', 'id' => $model->id]);
      }
    }
    return $this->render('_form_agregar-equipo', [

    ]);
  }

  /**
   * Deletes an existing Servicio model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id)
  {
    $this->findModel($id)->deleteWithRelated();

    return $this->redirect(['index']);
  }

  /**
   *
   * Export Servicio information into PDF format.
   * @param integer $id
   * @return mixed
   */
  public function actionPdf($id)
  {
    $model = $this->findModel($id);
    $providerAbonoServicio = new ArrayDataProvider([
      'allModels' => $model->abonoServicios,
    ]);
    $providerPresupuesto = new ArrayDataProvider([
      'allModels' => $model->presupuestos,
    ]);
    $providerServicioAEquipo = new ArrayDataProvider([
      'allModels' => $model->servicioAEquipo,
    ]);
    $providerServicioEstado = new ArrayDataProvider([
      'allModels' => $model->servicioEstados,
    ]);
    $providerServicioProgramado = new ArrayDataProvider([
      'allModels' => $model->servicioProgramados,
    ]);

    $content = $this->renderPartial('_pdf', [
      'model' => $model,
      'providerAbonoServicio' => $providerAbonoServicio,
      'providerPresupuesto' => $providerPresupuesto,
      'providerServicioAEquipo' => $providerServicioAEquipo,
      'providerServicioEstado' => $providerServicioEstado,
      'providerServicioProgramado' => $providerServicioProgramado,
    ]);

    $pdf = new Pdf([
      'mode' => Pdf::MODE_CORE,
      'format' => Pdf::FORMAT_A4,
      'orientation' => Pdf::ORIENT_PORTRAIT,
      'destination' => Pdf::DEST_BROWSER,
      'content' => $content,
      'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
      'cssInline' => '.kv-heading-1{font-size:18px}',
      'options' => ['title' => \Yii::$app->name],
      'methods' => [
        'SetHeader' => [\Yii::$app->name],
        'SetFooter' => ['{PAGENO}'],
      ]
    ]);

    return $pdf->render();
  }

  public function actionAbonar($id)
  {
    $model = $this->findModel($id);
    $abono = new AbonoServicio([
      'servicio_id' => $model->id,
      'fecha' => date('Y-m-d H:i:s'),
      'empleado_id' => Yii::$app->user->can('Tecnico') ?
        Yii::$app->user->identity->empleado->id : null
    ]);
    if ($abono->load(Yii::$app->request->post())) {
      if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validate($abono);
      }
      if ($abono->save()) {
        return $this->redirect(['view', 'id' => $model->id]);
      }
    }
    return $this->render('abonar', [
      'model' => $model,
      'abono' => $abono
    ]);
  }

  public function actionAgregarEstado($id)
  {
    $model = $this->findModel($id);
    $estado = new ServicioEstado([
      'servicio_id' => $model->id,
      'fecha' => date('Y-m-d H:i:s'),
      'actual' => 1
    ]);
    if ($estado->load(Yii::$app->request->post())) {
      if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return ActiveForm::validate($estado);
      }
      ServicioEstado::updateAll(['actual' => 0], ['servicio_id' => $model->id]);
      if ($estado->save()) {
        if ($estado->estado == ServicioEstado::ENTREGADO) {
          if($model->pagado)
            $model->cerrado = true;
          $model->save(false);
          //progama servicio para proxima vez
          if ($model->servicioAEquipo) {
            if ($model->servicioAEquipo->equipo->tiempo_mantenimiento != "NO") {
              $meses = explode(' ', $model->servicioAEquipo->equipo->tiempo_mantenimiento);
              $meses = $meses[0];
              $servicioProgramado = new ServicioProgramado([
                'cliente_id' => $model->cliente_id,
                'equipo_id' => $model->servicioAEquipo->equipo_id,
                'fecha' => GSHelper::sumarMeses(time(), $meses),
                'servicio_id' => $model->id
              ]);
              $servicioProgramado->save();
            }
          }
        }
        return $this->redirect(['view', 'id' => $id]);
      }
    }
    return $this->render('addestado', ['model' => $model,
      'estado' => $estado,]);
  }

  public
  function actionComisionar($id)
  {
    $model = $this->findModel($id);
    if (!$model->empleadoComision) {
      $comision = new EmpleadoComision([
        'servicio_id' => $model->id,
        'empleado_id' => $model->empleado_id,
        'arreglado' => 0,
        'fecha' => date('Y-m-d H:i:s'),
      ]);
      if ($comision->load(Yii::$app->request->post()) and $comision->monto > 0) {
        if ($comision->save()) {
          Yii::$app->session->setFlash('success', 'Comision por el servicio guardada correctamente');
          return $this->redirect(['view', 'id' => $model->id]);
        }
      }
    } else {
      Yii::$app->session->setFlash('danger', 'La comision por este servicio ya fue registrada');
      return $this->redirect(['view', 'id' => $model->id]);
    }
    return $this->render('comisionar', [
      'model' => $model,
      'comision' => $comision
    ]);
  }

  public
  function actionAddPresupuesto()
  {
    if (Yii::$app->request->isAjax) {
      $row = [];
      if (Yii::$app->request->post('Presupuesto')) {
        foreach (Yii::$app->request->post('Presupuesto') as $item) {
          $row[] = new Presupuesto($item);
        }
      }
      if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
        if (empty($row))
          $row = [new Presupuesto()];
        else
          $row[] = new Presupuesto();
      return $this->renderAjax('_formPresupuesto', ['row' => $row]);
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  public
  function actionAddServicioAequipo($cliente_id)
  {
    if (Yii::$app->request->isAjax) {
      $equipos = Equipo::find()->where(['cliente_id' => $cliente_id])->all();
      $model = new ServicioAEquipo();
      return $this->renderAjax('_formServicioAEquipo', [
        'model' => $model,
        'values' => ArrayHelper::map($equipos, 'id', function ($model) {
          return (string)$model;
        })
      ]);
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  public
  function actionEnviarenlace($id)
  {
    $servicio = Servicio::findOne($id);
    $servicio->token = Yii::$app->security->generateRandomString(10);
    $config = new Empresa();
    Yii::$app->view->params = [
      'user' => $servicio->cliente->usuario,
      'empresa' => $this->empresa
    ];
    if ($servicio->save()) {
      if ($servicio->cliente->usuario->email != "" and $servicio->cliente->usuario->receive_email) {
        $sendGrid = Yii::$app->mailer;
        $sendGrid->compose('confirmar_presupuesto', [
          'model' => $servicio,
          'empresa' => $this->empresa,
          'config' => $config
        ])
          ->setSubject("Servicio tecnico N° $servicio->numero - " . Yii::$app->name)
          ->setFrom(["info@teleclientes.com" => $config->nombre])
          ->setTo($servicio->cliente->usuario->email)
          ->send();
        Yii::$app->session->setFlash('success', ' Correo enviado correctamente');
      } else {
        Yii::$app->session->setFlash('info', ' El cliente ha desactivado el recibo de correos');
      }
    }
    return $this->redirect(Yii::$app->request->referrer);
  }

  public
  function actionPrecio()
  {
    if (Yii::$app->request->isAjax) {
      Yii::$app->response->format = Response::FORMAT_JSON;
      if (Yii::$app->request->get('model') == 'Trabajos') {
        return Trabajo::findOne(Yii::$app->request->get('id'));
      } else
        return Repuestos::findOne(Yii::$app->request->get('id'));
    }
    return $this->redirect(['index']);
  }

  public
  function actionAgregarPresupuesto()
  {
    $model = new Presupuesto();
    if ($model->load(Yii::$app->request->post()) and
      $model->save()
    ) {
      $model->servicio->total += $model->precio;
      $model->servicio->save(false);
      $msj = 'Guardado Correctamente';

    } else
      $msj = 'No se pudo guardar';
    return $this->renderPartial('_dataPresupuesto', [
      'model' => $model->servicio,
      'mensaje' => $msj
    ]);
  }

  public
  function actionBorrarPresupuesto($id)
  {
    $pres = Presupuesto::findOne($id);
    if ($pres != null) {
      $pres->servicio->total -= $pres->precio;
      $pres->servicio->save();
      $pres->delete();
      return $this->redirect(Yii::$app->request->referrer);
    }
    return $this->redirect(['index']);
  }
}
