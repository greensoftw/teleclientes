<?php

namespace app\controllers;

use app\models\base\Usuario;
use app\models\Proveedor;
use app\models\search\ProveedorSearch;
use kartik\mpdf\Pdf;
use mdm\admin\components\AccessControl;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * ProveedorController implements the CRUD actions for Proveedor model.
 */
class ProveedorController extends BaseController
{
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
      'access' => [
        'class' => AccessControl::className(),
        'allowActions' => ['add-proveedor-departamento'],
      ]
    ];
  }

  /**
   * Lists all Proveedor models.
   * @return mixed
   */
  public function actionIndex()
  {
    $searchModel = new ProveedorSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Proveedor model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
    $model = $this->findModel($id);
    $providerCompra = new ArrayDataProvider([
      'allModels' => $model->compras,
    ]);
    $providerProveedorDepartamento = new ArrayDataProvider([
      'allModels' => $model->proveedorDepartamentos,
    ]);
    return $this->render('view', [
      'model' => $this->findModel($id),
      'providerCompra' => $providerCompra,
      'providerProveedorDepartamento' => $providerProveedorDepartamento,
    ]);
  }

  /**
   * Finds the Proveedor model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Proveedor the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = Proveedor::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
   * Creates a new Proveedor model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate()
  {
    $model = new Proveedor();
    $modelUSer = new Usuario();
    if ($model->loadAll(Yii::$app->request->post()) and $modelUSer->load(Yii::$app->request->post())) {
      if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validateMultiple([$modelUSer, $model]);
      }
      $model->usuario->password = Yii::$app->security->generatePasswordHash("$model->nombre");
      if ($model->usuario->save()) {
        $model->usuario_id = $model->usuario->id;
        if ($model->saveAll(['usuario', 'compra'])) {
          return $this->redirect(['view', 'id' => $model->id]);
        }
      }
    }
    return $this->render('create', [
      'model' => $model,
      'modelUser' => $modelUSer
    ]);
  }

  /**
   * Updates an existing Proveedor model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id)
  {
    $model = $this->findModel($id);
    $modelUSer = $model->usuario;
    if ($model->loadAll(Yii::$app->request->post()) and $modelUSer->load(Yii::$app->request->post())) {
      if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validateMultiple([$modelUSer, $model]);
      }
      if ($modelUSer->isNewRecord) {
        $modelUSer->password = Yii::$app->security->generatePasswordHash("teleclientes");
      }
      $modelUSer->save();
      $model->usuario_id = $modelUSer->id;
      if ($model->saveAll(['usuario', 'compra'])) {
        return $this->redirect(['view', 'id' => $model->id]);
      }

    }
    return $this->render('update', [
      'model' => $model,
      'modelUser' => $modelUSer
    ]);
  }

  /**
   * Deletes an existing Proveedor model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id)
  {
    $this->findModel($id)->deleteWithRelated();

    return $this->redirect(['index']);
  }

  /**
   *
   * Export Proveedor information into PDF format.
   * @param integer $id
   * @return mixed
   */
  public function actionPdf($id)
  {
    $model = $this->findModel($id);
    $providerCompra = new ArrayDataProvider([
      'allModels' => $model->compras,
    ]);
    $providerProveedorDepartamento = new ArrayDataProvider([
      'allModels' => $model->proveedorDepartamentos,
    ]);

    $content = $this->renderPartial('_pdf', [
      'model' => $model,
      'providerCompra' => $providerCompra,
      'providerProveedorDepartamento' => $providerProveedorDepartamento,
    ]);

    $pdf = new Pdf([
      'mode' => Pdf::MODE_CORE,
      'format' => Pdf::FORMAT_A4,
      'orientation' => Pdf::ORIENT_PORTRAIT,
      'destination' => Pdf::DEST_BROWSER,
      'content' => $content,
      'cssInline' => '.kv-heading-1{font-size:18px}',
      'options' => ['title' => \Yii::$app->name],
      'methods' => [
        'SetHeader' => [\Yii::$app->name],
        'SetFooter' => ['{PAGENO}'],
      ]
    ]);

    return $pdf->render();
  }

  /**
   * Action to load a tabular form grid
   * for ProveedorDepartamento
   * @author Yohanes Candrajaya <moo.tensai@gmail.com>
   * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
   * @return mixed
   * @throws NotFoundHttpException
   */
  public function actionAddProveedorDepartamento()
  {
    if (Yii::$app->request->isAjax) {
      $row = Yii::$app->request->post('ProveedorDepartamento');
      if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
        $row[] = [];
      return $this->renderAjax('_formProveedorDepartamento', ['row' => $row]);
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
