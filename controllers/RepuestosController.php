<?php

namespace app\controllers;

use app\models\Repuestos;
use app\models\search\RepuestosSearch;
use kartik\mpdf\Pdf;
use mdm\admin\components\AccessControl;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * RepuestosController implements the CRUD actions for Repuestos model.
 */
class RepuestosController extends BaseController
{
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
      'access' => [
        'class' => AccessControl::className()
      ]
    ];
  }

  /**
   * Lists all Repuestos models.
   * @return mixed
   */
  public function actionIndex()
  {
    $searchModel = new RepuestosSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Repuestos model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
    $model = $this->findModel($id);
    $providerItemCompra = new \yii\data\ArrayDataProvider([
      'allModels' => $model->itemCompras,
    ]);
    $providerPresupuesto = new \yii\data\ArrayDataProvider([
      'allModels' => $model->presupuestos,
    ]);
    return $this->render('view', [
      'model' => $this->findModel($id),
      'providerItemCompra' => $providerItemCompra,
      'providerPresupuesto' => $providerPresupuesto,
    ]);
  }

  /**
   * Finds the Repuestos model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Repuestos the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = Repuestos::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
   * Creates a new Repuestos model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate()
  {
    $model = new Repuestos();

    if ($model->loadAll(Yii::$app->request->post(), ['*']) && $model->saveAll(['*'])) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('create', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Updates an existing Repuestos model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id)
  {
    if (Yii::$app->request->post('_asnew') == '1') {
      $model = new Repuestos();
    } else {
      $model = $this->findModel($id);
    }

    if ($model->loadAll(Yii::$app->request->post(), ['*']) && $model->saveAll(['*'])) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('update', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Deletes an existing Repuestos model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id)
  {
    $this->findModel($id)->deleteWithRelated();

    return $this->redirect(['index']);
  }

  /**
   *
   * Export Repuestos information into PDF format.
   * @param integer $id
   * @return mixed
   */
  public function actionPdf($id)
  {
    $model = $this->findModel($id);
    $providerItemCompra = new \yii\data\ArrayDataProvider([
      'allModels' => $model->itemCompras,
    ]);
    $providerPresupuesto = new \yii\data\ArrayDataProvider([
      'allModels' => $model->presupuestos,
    ]);

    $content = $this->renderPartial('_pdf', [
      'model' => $model,
      'providerItemCompra' => $providerItemCompra,
      'providerPresupuesto' => $providerPresupuesto,
    ]);

    $pdf = new Pdf([
      'format' => Pdf::FORMAT_A4,
      'orientation' => Pdf::ORIENT_PORTRAIT,
      'destination' => Pdf::DEST_BROWSER,
      'content' => $content,
      'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
      'cssInline' => '.kv-heading-1{font-size:18px}',
      'options' => ['title' => \Yii::$app->name],
      'methods' => [
        'SetHeader' => [\Yii::$app->name],
        'SetFooter' => ['{PAGENO}'],
      ]
    ]);

    return $pdf->render();
  }

  /**
   * Creates a new Repuestos model by another data,
   * so user don't need to input all field from scratch.
   * If creation is successful, the browser will be redirected to the 'view' page.
   *
   * @param type $id
   * @return type
   */
  public function actionSaveAsNew($id)
  {
    $model = new Repuestos();

    if (Yii::$app->request->post('_asnew') != '1') {
      $model = $this->findModel($id);
    }

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('saveAsNew', [
        'model' => $model,
      ]);
    }
  }

  public function actionAddItemCompra()
  {
    if (Yii::$app->request->isAjax) {
      $row = Yii::$app->request->post('ItemCompra');
      if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
        $row[] = [];
      return $this->renderAjax('_formItemCompra', ['row' => $row]);
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  public function actionAddPresupuesto()
  {
    if (Yii::$app->request->isAjax) {
      $row = Yii::$app->request->post('Presupuesto');
      if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
        $row[] = [];
      return $this->renderAjax('_formPresupuesto', ['row' => $row]);
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
