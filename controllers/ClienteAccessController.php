<?php

namespace app\controllers;

use app\models\Empresa;
use app\models\search\ServicioSearch;
use app\models\Servicio;
use kartik\mpdf\Pdf;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * ServicioController implements the CRUD actions for Servicio model.
 */
class ClienteAccessController extends BaseController
{
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
      'access' => [
        'class' => AccessControl::className(),
        'rules'=>[
          [
            'actions' => ['index','pdf'],
            'allow' => true,
            'matchCallback'=>function(){
              return Yii::$app->user->identity->cliente;
            }
          ]
        ]
      ]
    ];
  }

  /**
   * Lists all Servicio models.
   * @return mixed
   */
  public function actionIndex()
  {
    $dataProvider = new ActiveDataProvider([
      'query'=>Servicio::find()->where(['cliente_id'=>Yii::$app->user->identity->cliente->id])
      ->orderBy(['id'=>SORT_DESC])
    ]);
    return $this->render('index', [
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   *
   * Export Servicio information into PDF format.
   * @param integer $id
   * @return mixed
   */
  public function actionPdf()
  {
    $model = Yii::$app->user->identity->cliente;
    $providerEquipo = new ArrayDataProvider([
      'allModels' => $model->equipos,
    ]);
    $providerServicio = new ArrayDataProvider([
      'allModels' => $model->servicios,
    ]);

    $content = $this->renderPartial('_pdf', [
      'model' => $model,
      'providerEquipo' => $providerEquipo,
      'providerServicio' => $providerServicio,
      'empresa'=>new Empresa()
    ]);

    $pdf = new Pdf([
      'format' => Pdf::FORMAT_A4,
      'orientation' => Pdf::ORIENT_LANDSCAPE,
      'destination' => Pdf::DEST_BROWSER,
      'content' => $content,
      'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.css',
      'cssInline' => '.kv-heading-1{font-size:18px}',
      'options' => ['title' => \Yii::$app->name],
      'methods' => [
        'SetHeader' => [\Yii::$app->name.".com"],
        'SetFooter' => ["Generado por www.".Yii::$app->name.".com"],
      ]
    ]);

    return $pdf->render();
  }
}
