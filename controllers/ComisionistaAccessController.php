<?php
/**
 * Created by PhpStorm.
 * User: jhon
 * Date: 22/02/17
 * Time: 06:47 PM
 */

namespace app\controllers;


use app\models\AbonoComisionista;
use app\models\base\ComisionistaServicio;
use app\models\search\ComisionistaServicioSearch;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class ComisionistaAccessController extends BaseController
{
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
      'access' => [
        'class' => AccessControl::className(),
        'rules' => [
          [
            'actions' => ['index', 'registrar', 'historial-pagos', 'borrar'],
            'allow' => true,
            'matchCallback' => function () {
              return Yii::$app->user->identity->comisionista;
            }
          ]
        ]
      ]
    ];
  }

  public function actionIndex()
  {
    $comisionista = Yii::$app->user->identity->comisionista;
    $searchModel = new ComisionistaServicioSearch([
      'comisionista_id' => Yii::$app->user->identity->comisionista->id,
    ]);
    $dataProvider = $searchModel->searchAccess(Yii::$app->request->queryParams);
    $total = $comisionista->getTotal();
    $totalPendiente = $comisionista->getTotalPendiente();
    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
      'total' => $total,
      'totalPendiente' => $totalPendiente
    ]);
  }

  public function actionRegistrar()
  {
    $model = new ComisionistaServicio([
      'comisionista_id' => Yii::$app->user->identity->comisionista->id
    ]);
    if ($model->load(Yii::$app->request->post())) {
      $model->monto = $model->trabajo->comision;
      if ($model->save()) {
        Yii::$app->session->setFlash('success', 'Comisión registrada correctamente');
        return $this->redirect(['index']);
      }
    }
    return $this->render('create', [
      'model' => $model
    ]);
  }

  public function actionHistorialPagos()
  {
    $provider = new ActiveDataProvider([
      'query' => AbonoComisionista::find()
        ->orderBy(['id' => SORT_DESC])
        ->where(['comisionista_id' => Yii::$app->user->identity->comisionista->id])
    ]);
    return $this->render('historial', [
      'provider' => $provider
    ]);
  }

  public function actionBorrar($id)
  {
    $model = ComisionistaServicio::findOne($id);
    if ($model != null and $model->comisionista_id == Yii::$app->user->identity->comisionista->id) {
      $model->delete();
      Yii::$app->session->setFlash('success', 'Servicio Eliminado correctamente');
    }
    return $this->redirect(['index']);
  }
}