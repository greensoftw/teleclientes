<?php
/**
 * Created by PhpStorm.
 * User: jhon
 * Date: 10/01/17
 * Time: 05:26 PM
 */

namespace app\controllers;

use app\models\ContactForm;
use app\models\EmpresaTelecliente;
use app\models\LoginForm;
use app\models\Newpass;
use app\models\SignupForm;
use app\models\UserRepass;
use app\models\Usuario;
use Yii;
use yii\db\Connection;
use yii\helpers\Json;
use yii\web\Controller;

class SitioController extends Controller
{
  public $layout = 'mainFront';

  public function actionIndex()
  {
    return $this->render('index', [
      'modelContact' => new ContactForm(),
    ]);
  }

  public function actionRegistrar()
  {
    $registro = new SignupForm();
    $empresaTelecliente = new EmpresaTelecliente();
    if ($registro->load(Yii::$app->request->post()) and
      $empresaTelecliente->load(Yii::$app->request->post())
    ) {
      if ($registro->validate() and $empresaTelecliente->validate(['nombre'])) {
        $empresaTelecliente->conexion = 'Preparando...';
        $empresaTelecliente->config = Json::encode(['nombre' => $empresaTelecliente->nombre]);
        $empresaTelecliente->save();
        $empresaTelecliente->conexion = [
          'dsn' => 'mysql:host=localhost;dbname=teleclientes_empresa_' . $empresaTelecliente->id,
          'username' => 'teleclt_user_' . $empresaTelecliente->id,
          'password' => Yii::$app->security->generateRandomString(10),
          'charset' => 'utf8',
        ];
        $cpaneluser = 'merkapuntos';

        $function = [
          'db' => 'teleclientes_empresa_' . $empresaTelecliente->id,
          'cpanel_jsonapi_user' => $cpaneluser,
          'cpanel_jsonapi_module' => 'MysqlFE',
          'cpanel_jsonapi_func' => 'createdb',
          'cpanel_jsonapi_apiversion' => 2
        ];
        $params = http_build_query($function, '', '&');
        $url = "https://p3plcpnl0604.prod.phx3.secureserver.net:2083/json-api/cpanel?$params";
        $result = $this->getCurl($url);
        Yii::info(Json::decode($result));


        $function = [
          'dbuser' => $empresaTelecliente->conexion['username'],
          'password' => $empresaTelecliente->conexion['password'],
          'cpanel_jsonapi_user' => $cpaneluser,
          'cpanel_jsonapi_module' => 'MysqlFE',
          'cpanel_jsonapi_func' => 'createdbuser',
          'cpanel_jsonapi_apiversion' => 2
        ];
        $params = http_build_query($function, '', '&');
        $url = "https://p3plcpnl0604.prod.phx3.secureserver.net:2083/json-api/cpanel?$params";
        $result = $this->getCurl($url);
        Yii::info(Json::decode($result));

        $function = [
          'db' => 'teleclientes_empresa_' . $empresaTelecliente->id,
          'dbuser' => $empresaTelecliente->conexion['username'],
          'password' => $empresaTelecliente->conexion['password'],
          'privileges' => 'ALL%20PRIVILEGES',
          'cpanel_jsonapi_user' => $cpaneluser,
          'cpanel_jsonapi_module' => 'MysqlFE',
          'cpanel_jsonapi_func' => 'setdbuserprivileges',
          'cpanel_jsonapi_apiversion' => 2
        ];
        $params = http_build_query($function, '', '&');
        $url = "https://p3plcpnl0604.prod.phx3.secureserver.net:2083/json-api/cpanel?$params";
        $result = $this->getCurl($url);
        Yii::info(Json::decode($result));


        $conn = new Connection($empresaTelecliente->conexion);
        $sql = file_get_contents('teleclientes.sql');
        $conn->createCommand($sql)->execute();

        $oldDB = Yii::$app->db;
        Yii::$app->db = $conn;
        $user = $registro->signup();
        Yii::$app->db = $oldDB;
        $empresaTelecliente->conexion = Json::encode($empresaTelecliente->conexion);
        $empresaTelecliente->save();
        Yii::$app->db = $conn;
        Yii::$app->authManager->db = $conn;
        Yii::$app->authManager->assign(Yii::$app->authManager->getRole('Administrador'),$user->id);
        Yii::$app->user->login($user, 20000);
        Yii::$app->session->set('empresa_id', $empresaTelecliente->id);
        return $this->redirect(['site/index'])->send();
      }
    }
    return $this->render('registro', [
      'registro' => $registro,
      'empresa' => $empresaTelecliente
    ]);
  }

  private function getCurl($url)
  {
    Yii::info($url);
    $user = "merkapuntos";
    $auth = "guillermo1!A";
    $auth = 'Authorization: Basic ' . base64_encode($user . ':' . $auth) . "\r\n";
    $header[0] = $auth .
      "Content-Type: application/x-www-form-urlencoded\r\n" .
      "Content-Length: " . strlen($url) . "\r\n" . "\r\n" . $url;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    return curl_exec($ch);
  }

  public function actionLogin()
  {
    $empresas = EmpresaTelecliente::find()->asArray()->all();
    if (Yii::$app->session->get('empresa_id', false)) {
      return $this->redirect(['site/index']);
    }

    $model = new LoginForm();
    if ($model->load(Yii::$app->request->post()) && $model->login()) {
      return $this->redirect(['site/access']);
    }
    return $this->render('login', [
      'model' => $model,
      'empresas' => $empresas
    ]);
  }

  public function actionRecordarContrasena()
  {
    $empresas = EmpresaTelecliente::find()->asArray()->all();
    $model = new UserRepass();
    if ($model->load(Yii::$app->request->post())) {

      if ($model->enviarEmail()) {
        Yii::$app->session->setFlash('success', 'Su contraseña ha sido enviada a su correo');
      }
    }
    return $this->render('recordar_contrasena', [
      'model' => $model,
      'empresas' => $empresas
    ]);
  }

  public function actionRecuperarContrasena($token, $empresa_id)
  {
    $empresa = EmpresaTelecliente::findOne($empresa_id);
    $user = null;
    $model = new Newpass();
    if ($empresa != null) {
      Yii::$app->db = new Connection(Json::decode($empresa->conexion));
      $user = Usuario::findOne(['password_reset_token' => $token]);
      if ($user != null and Usuario::isPasswordResetTokenValid($user->password_reset_token)) {
        if ($model->load(Yii::$app->request->post())) {
          if ($model->validate()) {
            $user->removePasswordResetToken();
            $user->setPassword($model->password);
            $user->save(false);
            Yii::$app->session->setFlash('success', 'Contraseña cambiada correctamente');
            return $this->redirect(['/sitio/login']);
          }
        }
      } else {
        Yii::$app->session->setFlash('danger', 'Token invalido, solicite una nueva contraseña');
        return $this->redirect(['sitio/recordar-contrasena']);
      }
    }
    return $this->render('recuperar_contrasena', [
      'empresa' => $empresa,
      'usuario' => $user,
      'model' => $model
    ]);
  }

  public function actionContacto()
  {
    return 'OK';
  }

  public function actions()
  {
    return [
      'captcha' => [
        'class' => 'yii\captcha\CaptchaAction',
        'foreColor' => 0x333333,
        'transparent' => true
      ],
    ];
  }
}