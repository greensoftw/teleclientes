<?php

namespace app\controllers;

use app\models\Usuario;
use app\models\CodigoIsis;
use app\models\Marca;
use app\models\MarcaDepartamento;
use app\models\search\MarcaSearch;
use kartik\form\ActiveForm;
use kartik\mpdf\Pdf;
use mdm\admin\components\AccessControl;
use Yii;
use yii\bootstrap\Html;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * MarcaController implements the CRUD actions for Marca model.
 */
class MarcaController extends BaseController
{
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
      'access' => [
        'class' => AccessControl::className(),
        'allowActions' => ['add-marca-departamento', 'add-codigo-isis']
      ]
    ];
  }

  /**
   * Lists all Marca models.
   * @return mixed
   */
  public function actionIndex()
  {
    $searchModel = new MarcaSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Marca model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
    $model = $this->findModel($id);
    $providerCodigoIsis = new ArrayDataProvider([
      'allModels' => $model->codigoIses,
    ]);
    $providerEquipo = new ArrayDataProvider([
      'allModels' => $model->equipos,
    ]);
    $providerMarcaDepartamento = new ArrayDataProvider([
      'allModels' => $model->marcaDepartamentos,
    ]);
    return $this->render('view', [
      'model' => $this->findModel($id),
      'providerCodigoIsis' => $providerCodigoIsis,
      'providerEquipo' => $providerEquipo,
      'providerMarcaDepartamento' => $providerMarcaDepartamento,
    ]);
  }

  /**
   * Finds the Marca model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Marca the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = Marca::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
   * Creates a new Marca model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate()
  {
    $trans = Yii::$app->db->beginTransaction();
    $model = new Marca();
    $modelUser = new Usuario();
    if ($model->loadAll(Yii::$app->request->post(), ['equipo', 'usuario']) and
      $modelUser->load(Yii::$app->request->post())
    ) {
      if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validateMultiple([$model, $modelUser]);
      }
      if ($modelUser->validate(['nombres', 'email']) and $model->validate()) {
        $modelUser->username = Yii::$app->security->generateRandomString(10);
        $modelUser->setPassword(Yii::$app->name);
        $modelUser->generateAuthKey();
        $modelUser->save();
        $model->usuario_id = $modelUser->id;
        if ($model->saveAll(['equipo', 'usuario'])){
          $trans->commit();
          return $this->redirect(['view', 'id' => $model->id]);
        }else{
          $trans->rollBack();
        }
      }
    }
    return $this->render('create', [
      'model' => $model,
      'modelUser' => $modelUser
    ]);
  }

  /**
   * Updates an existing Marca model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id)
  {
    $model = $this->findModel($id);
    $modelUser = $model->usuario;
    $codigos = $model->codigoIses;
    $departamentos = $model->marcaDepartamentos;
    if ($model->load(Yii::$app->request->post()) and
      $modelUser->load(Yii::$app->request->post())
    ) {
      if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validateMultiple([$model, $modelUser]);
      }
      if ($modelUser->validate(['username', 'nombres', 'email']) and $model->validate(['nombre'])) {
        $modelUser->save();
        $model->usuario_id = $modelUser->id;
        $model->save();

        $error = false;
        if (Yii::$app->request->post('CodigoIsis')) {
          foreach (Yii::$app->request->post('CodigoIsis') as $key => $codigoIsis) {
            if (isset($codigos[$key])) {
              $codigos[$key]->setAttributes($codigoIsis);
              if (!$codigos[$key]->save())
                $error = true;
            } else {
              $codigo = new CodigoIsis($codigoIsis);
              $codigo->marca_id = $model->id;
              if (!$codigo->save()) {
                $error = true;
              }
            }
            if ($error)
              Yii::$app->session->setFlash('danger', Html::errorSummary($codigos));
            else
              Yii::$app->session->setFlash('success', "Actualizado correctamente");
          }
        }
        if (Yii::$app->request->post('MarcaDepartamento')) {
          $error = false;
          foreach (Yii::$app->request->post('MarcaDepartamento') as $key => $marcaDepartamento) {
            if (isset($departamentos[$key])) {
              $departamentos[$key]->setAttributes($marcaDepartamento);
              if (!$departamentos[$key]->save())
                $error = true;
            } else {
              $departamento = new MarcaDepartamento($marcaDepartamento);
              $departamento->marca_id = $model->id;
              if (!$departamento->save())
                $error = true;
            }
          }
        } elseif ($model->marcaDepartamentos) {
          foreach ($model->marcaDepartamentos as $item) {
            $item->delete();
          }
        }
        return $this->redirect(['view', 'id' => $model->id]);
      }
    }
    return $this->render('update', [
      'model' => $model,
      'modelUser' => $modelUser
    ]);

  }

  /**
   * Deletes an existing Marca model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id)
  {
    $this->findModel($id)->deleteWithRelated();

    return $this->redirect(['index']);
  }

  /**
   *
   * Export Marca information into PDF format.
   * @param integer $id
   * @return mixed
   */
  public function actionPdf($id)
  {
    $model = $this->findModel($id);
    $providerCodigoIsis = new ArrayDataProvider([
      'allModels' => $model->codigoIses,
    ]);
    $providerEquipo = new ArrayDataProvider([
      'allModels' => $model->equipos,
    ]);
    $providerMarcaDepartamento = new ArrayDataProvider([
      'allModels' => $model->marcaDepartamentos,
    ]);

    $content = $this->renderPartial('_pdf', [
      'model' => $model,
      'providerCodigoIsis' => $providerCodigoIsis,
      'providerEquipo' => $providerEquipo,
      'providerMarcaDepartamento' => $providerMarcaDepartamento,
    ]);

    $pdf = new Pdf([
      'format' => Pdf::FORMAT_A4,
      'orientation' => Pdf::ORIENT_PORTRAIT,
      'destination' => Pdf::DEST_BROWSER,
      'content' => $content,
      'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
      'cssInline' => '.kv-heading-1{font-size:18px}',
      'options' => ['title' => \Yii::$app->name],
      'methods' => [
        'SetHeader' => [\Yii::$app->name],
        'SetFooter' => ['{PAGENO}'],
      ]
    ]);

    return $pdf->render();
  }

  public function actionAddCodigoIsis()
  {
    if (Yii::$app->request->isAjax) {
      $row = Yii::$app->request->post('CodigoIsis');
      if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
        $row[] = [];
      return $this->renderAjax('_formCodigoIsis', ['row' => $row]);
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  public function actionAddMarcaDepartamento()
  {
    if (Yii::$app->request->isAjax) {
      $row = Yii::$app->request->post('MarcaDepartamento');
      if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
        $row[] = [];
      return $this->renderAjax('_formMarcaDepartamento', ['row' => $row]);
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
