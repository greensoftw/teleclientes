<?php

namespace app\controllers;

use app\models\AbonoCompra;
use app\models\Compra;
use app\models\search\CompraSearch;
use kartik\mpdf\Pdf;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * CompraController implements the CRUD actions for Compra model.
 */
class CompraController extends BaseController
{
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
      'access' => [
        'class' => \yii\filters\AccessControl::className(),
        'rules' => [
          [
            'allow' => true,
            'actions' => ['index', 'view', 'create', 'update', 'delete', 'pdf',
              'add-abono-compra', 'add-item-compra', 'abonar'],
            'roles' => ['@']
          ],
          [
            'allow' => false
          ]
        ]
      ]
    ];
  }

  /**
   * Lists all Compra models.
   * @return mixed
   */
  public function actionIndex()
  {
    $searchModel = new CompraSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Compra model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
    $model = $this->findModel($id);
    $providerAbonoCompra = new \yii\data\ArrayDataProvider([
      'allModels' => $model->abonoCompras,
    ]);
    $providerItemCompra = new \yii\data\ArrayDataProvider([
      'allModels' => $model->itemCompras,
    ]);
    return $this->render('view', [
      'model' => $this->findModel($id),
      'providerAbonoCompra' => $providerAbonoCompra,
      'providerItemCompra' => $providerItemCompra,
    ]);
  }

  /**
   * Finds the Compra model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Compra the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = Compra::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
   * Creates a new Compra model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate()
  {
    $model = new Compra();

    if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('create', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Updates an existing Compra model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id)
  {
    $model = $this->findModel($id);

    if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('update', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Deletes an existing Compra model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id)
  {
    $this->findModel($id)->deleteWithRelated();

    return $this->redirect(['index']);
  }

  /**
   *
   * Export Compra information into PDF format.
   * @param integer $id
   * @return mixed
   */
  public function actionPdf($id)
  {
    $model = $this->findModel($id);
    $providerAbonoCompra = new \yii\data\ArrayDataProvider([
      'allModels' => $model->abonoCompras,
    ]);
    $providerItemCompra = new \yii\data\ArrayDataProvider([
      'allModels' => $model->itemCompras,
    ]);

    $content = $this->renderPartial('_pdf', [
      'model' => $model,
      'providerAbonoCompra' => $providerAbonoCompra,
      'providerItemCompra' => $providerItemCompra,
    ]);

    $pdf = new Pdf([
      'mode' => Pdf::MODE_CORE,
      'format' => Pdf::FORMAT_A4,
      'orientation' => Pdf::ORIENT_PORTRAIT,
      'destination' => Pdf::DEST_BROWSER,
      'content' => $content,
      'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
      'cssInline' => '.kv-heading-1{font-size:18px}',
      'options' => ['title' => \Yii::$app->name],
      'methods' => [
        'SetHeader' => [\Yii::$app->name],
        'SetFooter' => ['{PAGENO}'],
      ]
    ]);

    return $pdf->render();
  }

  public function actionAbonar($id)
  {
    $model = $this->findModel($id);
    $abono = new AbonoCompra([
      'scenario' => 'abonar',
      'compra_id' => $model->id,
      'fecha' => date('Y-m-d H:i:s')
    ]);
    if ($abono->load(Yii::$app->request->post())) {
      if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validate($abono);
      }
      if ($abono->save()) {
        return $this->redirect(['view', 'id' => $model->id]);
      }
    }
    return $this->render('abonar', [
      'model' => $model,
      'abono' => $abono
    ]);
  }

  public function actionAddItemCompra()
  {
    if (Yii::$app->request->isAjax) {
      $row = Yii::$app->request->post('ItemCompra');
      if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
        $row[] = [];
      return $this->renderAjax('_formItemCompra', ['row' => $row]);
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
