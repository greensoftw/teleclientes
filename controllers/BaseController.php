<?php
/**
 * Created by PhpStorm.
 * User: jhon
 * Date: 11/01/17
 * Time: 10:50 PM
 */

namespace app\controllers;


use app\models\Empresa;
use app\models\EmpresaTelecliente;
use Yii;
use yii\db\Connection;
use yii\helpers\Json;
use yii\rbac\DbManager;
use yii\web\Controller;

class BaseController extends Controller
{
  public $empresa;

  public $empresa_config;

  public function beforeAction($action)
  {
    if (Yii::$app->session->get('empresa_id', false)) {
      if (Yii::$app->controller->action->id != 'error') {
        $this->empresa = EmpresaTelecliente::findOne(Yii::$app->session->get('empresa_id'));
        Yii::$app->db = new Connection(Json::decode($this->empresa->conexion));
        /** @var DbManager db */
        Yii::$app->authManager->db = Yii::$app->db;
        $this->empresa_config = Empresa::find();
      }
    } else {
      return $this->redirect(['/sitio/login'])->send();
    }
    return parent::beforeAction($action);
  }

}