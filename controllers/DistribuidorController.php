<?php

namespace app\controllers;

use app\models\Distribuidor;
use app\models\search\DistribuidorSearch;
use app\models\Usuario;
use kartik\form\ActiveForm;
use kartik\mpdf\Pdf;
use mdm\admin\components\AccessControl;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * DistribuidorController implements the CRUD actions for Distribuidor model.
 */
class DistribuidorController extends BaseController
{
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
      'access' => [
        'class' => AccessControl::className(),
        'allowActions' => ['add-distribuidor-departamento']
      ]
    ];
  }

  /**
   * Lists all Distribuidor models.
   * @return mixed
   */
  public function actionIndex()
  {
    $searchModel = new DistribuidorSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Distribuidor model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
    $model = $this->findModel($id);
    $providerComisionista = new ArrayDataProvider([
      'allModels' => $model->comisionistas,
    ]);
    $providerDistribuidorDepartamento = new ArrayDataProvider([
      'allModels' => $model->distribuidorDepartamentos,
    ]);
    return $this->render('view', [
      'model' => $this->findModel($id),
      'providerComisionista' => $providerComisionista,
      'providerDistribuidorDepartamento' => $providerDistribuidorDepartamento,
    ]);
  }

  /**
   * Finds the Distribuidor model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Distribuidor the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = Distribuidor::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
   * Creates a new Distribuidor model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate()
  {
    $trans = Yii::$app->db->beginTransaction();
    $model = new Distribuidor();
    $modelUser = new Usuario();
    if ($model->loadAll(Yii::$app->request->post(), ['equipos'])
      and $modelUser->load(Yii::$app->request->post())
    ) {
      if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validateMultiple([$model, $modelUser]);
      }
      if ($modelUser->validate(['nombres', 'email']) and
        $model->validate(['nombre', 'direccion', 'telefono'])
      ) {
        $modelUser->username = Yii::$app->security->generateRandomString(15);
        $modelUser->setPassword(Yii::$app->name);
        $modelUser->generateAuthKey();
        $modelUser->save();
        $model->usuario_id = $modelUser->id;
        if ($model->saveAll(['comisionistas', 'equipos', 'usuario'])) {
          $trans->commit();
          return $this->redirect(['view', 'id' => $model->id]);
        } else
          $trans->rollBack();

      }

    }
    return $this->render('create', [
      'model' => $model,
      'modelUser' => $modelUser
    ]);
  }

  /**
   * Updates an existing Distribuidor model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id)
  {
    if (Yii::$app->request->post('_asnew') == '1') {
      $model = new Distribuidor();
      $modelUser = new Usuario();
    } else {
      $model = $this->findModel($id);
      $modelUser = $model->usuario;
    }
    if ($model->loadAll(Yii::$app->request->post(), ['equipos'])
      and $modelUser->load(Yii::$app->request->post())
    ) {
      if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validateMultiple([$model, $modelUser]);
      }
      if ($modelUser->validate(['nombres', 'username', 'email']) and
        $model->validate(['nombre', 'direccion', 'telefono'])
      ) {
        if ($modelUser->isNewRecord)
          $modelUser->password = Yii::$app->security->generatePasswordHash("teleclientes");
        $modelUser->save();
        $model->usuario_id = $modelUser->id;
        $model->saveAll(['comisionistas', 'equipos', 'usuario']);
        return $this->redirect(['view', 'id' => $model->id]);
      }

    }
    return $this->render('update', [
      'model' => $model,
      'modelUser' => $modelUser
    ]);
  }

  /**
   * Deletes an existing Distribuidor model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id)
  {
    $this->findModel($id)->deleteWithRelated();

    return $this->redirect(['index']);
  }

  /**
   *
   * Export Distribuidor information into PDF format.
   * @param integer $id
   * @return mixed
   */
  public function actionPdf($id)
  {
    $model = $this->findModel($id);
    $providerComisionista = new ArrayDataProvider([
      'allModels' => $model->comisionistas,
    ]);
    $providerDistribuidorDepartamento = new ArrayDataProvider([
      'allModels' => $model->distribuidorDepartamentos,
    ]);

    $content = $this->renderPartial('_pdf', [
      'model' => $model,
      'providerComisionista' => $providerComisionista,
      'providerDistribuidorDepartamento' => $providerDistribuidorDepartamento,
    ]);

    $pdf = new Pdf([
      'mode' => Pdf::MODE_CORE,
      'format' => Pdf::FORMAT_A4,
      'orientation' => Pdf::ORIENT_PORTRAIT,
      'destination' => Pdf::DEST_BROWSER,
      'content' => $content,
      'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
      'cssInline' => '.kv-heading-1{font-size:18px}',
      'options' => ['title' => \Yii::$app->name],
      'methods' => [
        'SetHeader' => [\Yii::$app->name],
        'SetFooter' => ['{PAGENO}'],
      ]
    ]);

    return $pdf->render();
  }


  /**
   * @return string
   * @throws NotFoundHttpException
   */
  public function actionAddDistribuidorDepartamento()
  {
    if (Yii::$app->request->isAjax) {
      $row = Yii::$app->request->post('DistribuidorDepartamento');
      if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
        $row[] = [];
      return $this->renderAjax('_formDistribuidorDepartamento', ['row' => $row]);
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
