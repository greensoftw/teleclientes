<?php

namespace app\controllers;

use app\models\Archivo;
use app\models\Configuracion;
use app\models\ContactForm;
use app\models\LoginForm;
use app\models\Servicio;
use app\models\ServicioEstado;
use app\models\Usuario;
use mdm\admin\components\AccessControl;
use Yii;
use yii\filters\VerbFilter;

class SiteController extends BaseController
{

  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'allowActions' => ['logout', 'truncate', 'init', 'repair', 'access'],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'logout' => ['post'],
        ],
      ],
    ];
  }

  /**
   * @inheritdoc
   */
  public function actions()
  {
    return [
      'error' => [
        'class' => 'yii\web\ErrorAction',
      ],
    ];
  }

  /**
   * Displays homepage.
   *
   * @return string
   */
  public function actionIndex()
  {
    $config = Configuracion::find()->one();
    if ($config == null) {
      Yii::$app->session->setFlash('danger', 'Por favor configure los datos de su empresa');
      return $this->redirect(['/configuracion']);
    }
    if (Yii::$app->user->identity->validatePassword(Yii::$app->name)) {
      Yii::$app->session->setFlash('danger', 'Por favor configure una nueva comtraseña');
      return $this->redirect(['/cuenta/contrasena']);
    }
    //Numero de servicios por estado
    $estados = [];
    foreach (ServicioEstado::$dropEstados as $estado) {
      $estados[$estado] = Servicio::find()->joinWith('servicioEstados')
        ->where(['servicio_estado.actual' => 1])
        ->andWhere(['servicio_estado.estado' => $estado])
        ->count();
    }
    //numero de servicios por asignar tecnico
    $sinAsignar = Servicio::find()->where(['IS', 'empleado_id', null])
      ->count();
    //numero de servicios por (asignar equipos/asignar presupuesto)
    $serviciosIncompletos = Servicio::find()->joinWith(['presupuestos', 'servicioAEquipo'])
      ->where(['is', 'presupuesto.id', null])
      ->orWhere(['is', 'servicio_a_equipo.id', null])
      ->count();

    $cerradosPorCobrar = Servicio::find()
      ->where(['cerrado' => 1])
      ->andWhere(['pagado' => 0])
      ->count();
    $totalPorCobrar = Servicio::find()
      ->where(['cerrado' => 1])
      ->andWhere(['pagado' => 0])
      ->sum('total');
    return $this->render('index', [
      'estados' => $estados,
      'sinAsignar' => $sinAsignar,
      'cerradosPorCobrar' => $cerradosPorCobrar,
      'serviciosIncompletos' => $serviciosIncompletos,
      'totalPorCobrar' => $totalPorCobrar
    ]);
  }

  /**
   * Login action.
   *
   * @return string
   */
  public function actionLogin()
  {
    if (!Yii::$app->user->isGuest) {
      return $this->redirect(['site/index']);
    }

    $model = new LoginForm();
    if ($model->load(Yii::$app->request->post()) && $model->login()) {
      return $this->goBack();
    }
    return $this->render('login', [
      'model' => $model,
    ]);
  }

  /**
   * Logout action.
   *
   * @return string
   */
  public function actionLogout()
  {
    Yii::$app->user->logout();

    return $this->goHome();
  }

  /**
   * Displays contact page.
   *
   * @return string
   */
  public function actionContact()
  {
    $model = new ContactForm();
    if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
      Yii::$app->session->setFlash('contactFormSubmitted');

      return $this->refresh();
    }
    return $this->render('contact', [
      'model' => $model,
    ]);
  }

  public function actionDownloadFile($id)
  {
    Yii::$app->response->format = "json";
    $model = Archivo::findOne($id);
    if (file_exists($model->ruta)) {
      return Yii::$app->response->sendFile($model->ruta);
    }
    return $this->redirect(['index']);
  }

  public function actionInit()
  {
    $user = new Usuario([
      'username' => 'admin',
      'password' => Yii::$app->security->generatePasswordHash('admin'),
      'nombres' => 'Guillermo balaguera',
      'email' => 'frioalpes@hotmail.com'
    ]);
    $user->save();
  }

  /**
   * Displays about page.
   *
   * @return string
   */
  public function actionAbout()
  {
    return $this->render('about');
  }

  public function actionTruncate()
  {
    Yii::$app->db->createCommand("
    set foreign_key_checks=0;
    TRUNCATE `abono_comisionista`;
    TRUNCATE `abono_compra`;
    TRUNCATE `abono_servicio`;
    TRUNCATE `archivo`;
    TRUNCATE `archivo_pdf`;
    TRUNCATE `auth_assignment`;
    TRUNCATE `auth_rule`;
    TRUNCATE `chat`;
    TRUNCATE `cliente`;
    TRUNCATE `codigo_isis`;
    TRUNCATE `comisionista`;
    TRUNCATE `comisionista_servicio`;
    TRUNCATE `compra`;
    TRUNCATE `configuracion`;
    TRUNCATE `distribuidor`;
    TRUNCATE `distribuidor_departamento`;
    TRUNCATE `empleado`;
    TRUNCATE `empleado_comision`;
    TRUNCATE `equipo`;
    TRUNCATE `item_compra`;
    TRUNCATE `marca`;
    TRUNCATE `marca_departamento`;
    TRUNCATE `migration`;
    TRUNCATE `movimientos_caja`;
    TRUNCATE `pago_empleado`;
    TRUNCATE `presupuesto`;
    TRUNCATE `proveedor`;
    TRUNCATE `proveedor_departamento`;
    TRUNCATE `repuestos`;
    TRUNCATE `servicio`;
    TRUNCATE `servicio_a_equipo`;
    TRUNCATE `servicio_estado`;
    TRUNCATE `servicio_programado`;
    TRUNCATE `tipo_equipo`;
    TRUNCATE `trabajo`;
    TRUNCATE `usuario`;")->execute();
    return $this->redirect(['index']);
  }

  public function actionRepair()
  {
    $users = Usuario::find()->all();
    foreach ($users as $user) {
      $user->generateAuthKey();
      $user->save(false);
    }
  }

  public function actionAccess()
  {
    if (Yii::$app->user->identity->cliente) {
      return $this->redirect(['/cliente-access/index']);
    }
    if (Yii::$app->user->identity->distribuidor) {
      return $this->redirect(['/distribuidor-access/index']);
    }
    if (Yii::$app->user->identity->marca) {
      return $this->redirect(['/marca-access/index']);
    }
    if (Yii::$app->user->identity->comisionista) {
      return $this->redirect(['/comisionista-access/index']);
    }

    return $this->redirect(['index']);
  }
}