<?php

namespace app\controllers;

use app\models\base\ComisionistaServicio;
use app\models\Usuario;
use app\models\Cliente;
use app\models\Equipo;
use app\models\search\ClienteSearch;
use kartik\form\ActiveForm;
use kartik\mpdf\Pdf;
use mdm\admin\components\AccessControl;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * ClienteController implements the CRUD actions for Cliente model.
 */
class ClienteController extends BaseController
{
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
      'access' => [
        'class' => AccessControl::className(),
        'allowActions'=>['add-equipo']
      ]
    ];
  }

  /**
   * Lists all Cliente models.
   * @return mixed
   */
  public function actionIndex()
  {
    $searchModel = new ClienteSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Cliente model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
    $model = $this->findModel($id);
    $providerEquipo = new ArrayDataProvider([
      'allModels' => $model->equipos,
    ]);
    $providerServicio = new ArrayDataProvider([
      'allModels' => $model->servicios,
    ]);
    return $this->render('view', [
      'model' => $this->findModel($id),
      'providerEquipo' => $providerEquipo,
      'providerServicio' => $providerServicio,
    ]);
  }

  /**
   * Finds the Cliente model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Cliente the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = Cliente::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
   * Creates a new Cliente model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate()
  {
    $comision = ComisionistaServicio::findOne(Yii::$app->request->get('ser_id'));
    $model = new Cliente([
      'comisionista_id' => Yii::$app->request->get('com_id')
    ]);

    $trans = Usuario::getDb()->beginTransaction();
    $modelUser = new Usuario();
    if($comision != null){
      $modelUser->nombres = $comision->nombre_cliente;
      $modelUser->email = $comision->correo;
      $model->direccion = $comision->direccion_cliente;
      $model->telefonos = $comision->telefono_cliente;
    }
    $equipos = [new Equipo()];
    if ($model->loadAll(Yii::$app->request->post(), ['servicio', 'usuario']) && $modelUser->load(Yii::$app->request->post())) {
      if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validateMultiple([$model, $modelUser]);
      }
      $modelUser->username = Yii::$app->security->generateRandomString(10);
      $modelUser->setPassword(Yii::$app->name);
      $modelUser->generateAuthKey();
      if ($modelUser->save()) {
        $model->user_id = $modelUser->id;
        if($model->saveAll(['usuario', 'servicio'])){
          $trans->commit();
          return $this->redirect(['view', 'id' => $model->id]);
        } else{
         $trans->rollBack();
        }
      }
    }
    return $this->render('create', [
      'model' => $model,
      'modelUser' => $modelUser,
      'equipos' => $equipos
    ]);
  }

  /**
   * Updates an existing Cliente model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id)
  {
    $model = $this->findModel($id);
    $modelUser = $model->usuario;
    $equipos = $model->equipos;
    if ($model->load(Yii::$app->request->post()) and
      $modelUser->load(Yii::$app->request->post())
    ) {
      if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validateMultiple([$model, $modelUser]);
      }
      if ($modelUser->save()) {
        if ($model->save()) {
          $error = false;
          if (Yii::$app->request->post('Equipo')) {
            foreach (Yii::$app->request->post('Equipo') as $key => $item) {
              if (isset($equipos[$key])) {
                $equipos[$key]->setAttributes($item);
                if (!$equipos[$key]->save()) {
                  $error = true;
                  Yii::$app->session->setFlash('danger', "El equipo " . ($key + 1) . " no pudo ser guardado : "
                    . Html::errorSummary($equipos));
                }
              } else {
                $nuevoEquipo = new Equipo($item);
                $nuevoEquipo->cliente_id = $model->id;
                if (!$nuevoEquipo->save()) {
                  Yii::$app->session->setFlash('danger', "El equipo " . ($key + 1) . " no pudo ser guardado"
                    . Html::errorSummary($equipos));
                  $error = true;
                }

                $equipos[] = $nuevoEquipo;
              }
            }
          }
          if (!$error)
            return $this->redirect(['view', 'id' => $model->id]);
        } else
          Yii::$app->session->setFlash('danger', 'El cliente no pudo ser guardado');
      }
    }
    return $this->render('update', [
      'model' => $model,
      'modelUser' => $modelUser,
      'equipos' => $equipos
    ]);
  }

  /**
   * Deletes an existing Cliente model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id)
  {
    $this->findModel($id)->deleteWithRelated();

    return $this->redirect(['index']);
  }

  /**
   *
   * Export Cliente information into PDF format.
   * @param integer $id
   * @return mixed
   */
  public function actionPdf($id)
  {
    $model = $this->findModel($id);
    $providerEquipo = new ArrayDataProvider([
      'allModels' => $model->equipos,
    ]);
    $providerServicio = new ArrayDataProvider([
      'allModels' => $model->servicios,
    ]);

    $content = $this->renderPartial('_pdf', [
      'model' => $model,
      'providerEquipo' => $providerEquipo,
      'providerServicio' => $providerServicio,
    ]);

    $pdf = new Pdf([
      'format' => Pdf::FORMAT_A4,
      'orientation' => Pdf::ORIENT_PORTRAIT,
      'destination' => Pdf::DEST_BROWSER,
      'content' => $content,
      'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.css',
      'cssInline' => '.kv-heading-1{font-size:18px}',
      'options' => ['title' => \Yii::$app->name],
      'methods' => [
        'SetHeader' => [\Yii::$app->name],
        'SetFooter' => ['{PAGENO}'],
      ]
    ]);

    return $pdf->render();
  }

  public function actionAddEquipo()
  {
    if (Yii::$app->request->isAjax) {
      $row = [];
      if (Yii::$app->request->post('Equipo')) {
        foreach (Yii::$app->request->post('Equipo') as $item) {
          $row[] = new Equipo($item);
        }
      }
      if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
        if (empty($row))
          $row = [new Equipo()];
        else
          $row[] = new Equipo();
      return $this->renderAjax('_formEquipo', ['row' => $row]);
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

}
