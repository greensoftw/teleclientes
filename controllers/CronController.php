<?php
/**
 * Created by PhpStorm.
 * User: jhon
 * Date: 18/01/17
 * Time: 10:56 PM
 */

namespace app\controllers;


use app\helpers\GSHelper;
use app\models\Empleado;
use app\models\EmpresaTelecliente;
use app\models\ServicioProgramado;
use Yii;
use yii\db\Connection;
use yii\helpers\Json;
use yii\web\Controller;

class CronController extends Controller
{
  public function actionIndex()
  {
    $this->serviciosProgramados();
    $this->sueldoEmpleados();
  }

  private function serviciosProgramados()
  {
    /** @var ServicioProgramado $servicio_programado */
    $empresas = EmpresaTelecliente::find()->all();
    foreach ($empresas as $empresa) {
      Yii::$app->db = new Connection(Json::decode($empresa->conexion));
      $servicio_programado = ServicioProgramado::find()->where(['fecha' => GSHelper::sumarDias(5)])->all();
      if ($servicio_programado != null) {
        foreach ($servicio_programado as $servicio) {
          Yii::$app->mailer->compose('confirmar_servicio_programado',
            ['servicio' => $servicio, 'empresa' => $empresa])
            ->setFrom($empresa['config']['email'])
            ->setTo($servicio_programado->cliente->usuario->email)
            ->setSubject('Recordatorio mantenimiento de equipo ' . $servicio->equipo->tipoEquipo->nombre . ' ' . $servicio->equipo->marca->nombre)
            ->send();
        }
      }
      Yii::$app->db->close();
    }
  }

  private function sueldoEmpleados()
  {
    $empresas = EmpresaTelecliente::find()->all();
    foreach ($empresas as $empresa) {
      Yii::$app->db = new Connection(Json::decode($empresa->conexion));
      $empleados = Empleado::find()
        ->where(['tipo_empleado' => Empleado::CON_COMISION_AN_SUELDO])
        ->orWhere(['tipo_empleado' => Empleado::CON_SUELDO])
        ->andWhere(['proximo_pago'=>date('Y-m-d')])
        ->all();
      foreach ($empleados as $empleado) {
        $empleado->saldo += $empleado->sueldo;
        $empleado->proximo_pago = GSHelper::sumarMeses($empleado->proximo_pago, 1);
        $empleado->save();
      }
      Yii::$app->db->close();
    }
  }

  private function borrarServicioComisionistas()
  {

  }
}