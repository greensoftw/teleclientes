<?php

namespace app\controllers;

use app\models\search\ServicioSearch;
use app\models\Servicio;
use kartik\mpdf\Pdf;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * ServicioController implements the CRUD actions for Servicio model.
 */
class DistribuidorAccessController extends BaseController
{
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
      'access' => [
        'class' => AccessControl::className(),
        'rules'=>[
          [
            'actions' => ['index','pdf'],
            'allow' => true,
            'matchCallback'=>function(){
              return Yii::$app->user->identity->distribuidor;
            }
          ]
        ]
      ]
    ];
  }

  /**
   * Lists all Servicio models.
   * @return mixed
   */
  public function actionIndex()
  {
    $searchModel = new ServicioSearch();
    $dataProvider = $searchModel->searchDistribuidor(Yii::$app->request->queryParams,
      Yii::$app->user->identity->distribuidor->id);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Servicio model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
    $model = $this->findModel($id);
    $providerAbonoServicio = new ArrayDataProvider([
      'allModels' => $model->abonoServicios,
    ]);
    $providerPresupuesto = new ArrayDataProvider([
      'allModels' => $model->presupuestos,
    ]);
    $providerServicioAEquipo = new ArrayDataProvider([
      'allModels' => $model->servicioAEquipos,
    ]);
    $providerServicioEstado = new ArrayDataProvider([
      'allModels' => $model->servicioEstados,
    ]);
    $providerServicioProgramado = new ArrayDataProvider([
      'allModels' => $model->servicioProgramados,
    ]);
    return $this->render('view', [
      'model' => $this->findModel($id),
      'providerAbonoServicio' => $providerAbonoServicio,
      'providerPresupuesto' => $providerPresupuesto,
      'providerServicioAEquipo' => $providerServicioAEquipo,
      'providerServicioEstado' => $providerServicioEstado,
      'providerServicioProgramado' => $providerServicioProgramado,
    ]);
  }

  /**
   * Finds the Servicio model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Servicio the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = Servicio::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
   *
   * Export Servicio information into PDF format.
   * @param integer $id
   * @return mixed
   */
  public function actionPdf($id)
  {
    $model = $this->findModel($id);
    $providerAbonoServicio = new ArrayDataProvider([
      'allModels' => $model->abonoServicios,
    ]);
    $providerPresupuesto = new ArrayDataProvider([
      'allModels' => $model->presupuestos,
    ]);
    $providerServicioAEquipo = new ArrayDataProvider([
      'allModels' => $model->servicioAEquipos,
    ]);
    $providerServicioEstado = new ArrayDataProvider([
      'allModels' => $model->servicioEstados,
    ]);
    $providerServicioProgramado = new ArrayDataProvider([
      'allModels' => $model->servicioProgramados,
    ]);

    $content = $this->renderPartial('_pdf', [
      'model' => $model,
      'providerAbonoServicio' => $providerAbonoServicio,
      'providerPresupuesto' => $providerPresupuesto,
      'providerServicioAEquipo' => $providerServicioAEquipo,
      'providerServicioEstado' => $providerServicioEstado,
      'providerServicioProgramado' => $providerServicioProgramado,
    ]);

    $pdf = new Pdf([
      'mode' => Pdf::MODE_CORE,
      'format' => Pdf::FORMAT_A4,
      'orientation' => Pdf::ORIENT_PORTRAIT,
      'destination' => Pdf::DEST_BROWSER,
      'content' => $content,
      'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
      'cssInline' => '.kv-heading-1{font-size:18px}',
      'options' => ['title' => \Yii::$app->name],
      'methods' => [
        'SetHeader' => [\Yii::$app->name],
        'SetFooter' => ['{PAGENO}'],
      ]
    ]);
    return $pdf->render();
  }
}
