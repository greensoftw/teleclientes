<?php

namespace app\controllers;

use app\models\Empresa;
use Yii;
use yii\web\UploadedFile;

class ConfiguracionController extends BaseController
{
  public function actionIndex()
  {
    $config = new Empresa();
    if ($config->load(\Yii::$app->request->post())) {
      $config->picture = UploadedFile::getInstance($config, 'picture');
      if ($config->picture != null && $config->picture != '') {
        $ruta = 'uploads/config/' . $config->picture->baseName . '' . $config->picture->extension;
        $config->picture->saveAs($ruta);
        $config->picture = $ruta;
      }
      if ($config->Guardar())
        Yii::$app->session->setFlash('success', 'Información guardada correctamente');
    }
    return $this->render('index', [
      'config' => $config
    ]);
  }

}
