<?php
/**
 * User: jhon
 * Date: 14/12/16
 * Time: 05:52 PM
 */

namespace app\controllers;


use app\models\base\Servicio;
use app\models\base\ServicioProgramado;
use app\models\base\Usuario;
use app\models\EmpresaTelecliente;
use app\models\ServicioEstado;
use yii\db\Connection;
use yii\helpers\Json;
use yii\web\Controller;

class ConfirmacionController extends Controller
{
  public $layout = "blank";

  public function actionConfirmarPresupuesto($id, $token, $empresa_id)
  {
    $empresa = EmpresaTelecliente::findOne($empresa_id);
    \Yii::$app->db = new Connection(Json::decode($empresa->conexion));
    $servicio = Servicio::find()->where(['id' => $id, 'token' => $token])->one();
    if ($servicio != null) {
      $servicio->token = null;
      $servicio->save(false);
      ServicioEstado::updateAll(['actual' => 0], ['servicio_id' => $servicio->id]);
      $estado = new ServicioEstado([
        'servicio_id' => $servicio->id,
        'fecha' => date('Y-m-d H:i:s'),
        'descripcion' => 'Presupuesto confirmado por correo',
        'actual' => 1,
        'estado' => ServicioEstado::CONFIRMADO
      ]);
      $estado->save();
      return $this->render('success',[
        'title'=>'Confirmación exitosa',
        'content'=>'Gracias por confirmar la cotización de su servicio'
      ]);
    }
    return $this->redirect(['sitio/index']);
  }


  public function actionUnsuscribe($user,$empresa_id){
    $empresa = EmpresaTelecliente::findOne($empresa_id);
    \Yii::$app->db = new Connection(Json::decode($empresa->conexion));
    if($empresa != null){
      $usr = Usuario::findOne($user);
      $usr->receive_email = 0;
      $usr->save(false);
      return $this->render('success',[
        'title'=>'Recibo de email cancelado',
        'content'=>'A partir de este momento ya no recibiras correos de '.$empresa->nombre
      ]);
    }
    return $this->redirect(['sitio/index']);
  }


}