<?php

namespace app\controllers;

use app\models\base\Cliente;
use app\models\ComisionistaServicio;
use app\models\search\ComisionistaServicioSearch;
use kartik\mpdf\Pdf;
use mdm\admin\components\AccessControl;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * ComisionistaServicioController implements the CRUD actions for ComisionistaServicio model.
 */
class ComisionistaServicioController extends BaseController
{
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
      'access' => [
        'class' => AccessControl::className(),
      ]
    ];
  }

  /**
   * Lists all ComisionistaServicio models.
   * @return mixed
   */
  public function actionIndex()
  {
    $searchModel = new ComisionistaServicioSearch([
      'pagado'=>0
    ]);
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single ComisionistaServicio model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
    $model = $this->findModel($id);
    $cliente = Cliente::find()->where(['LIKE', 'telefonos', $model->telefono_cliente])
      ->joinWith('usuario')
      ->orFilterWhere(['LIKE', 'usuario.email', $model->correo])
      ->one();
    return $this->render('view', [
      'model' => $this->findModel($id),
      'clienteNoExiste' => ($cliente == null)
    ]);
  }

  public function actionCerrar($id){
    $model = $this->findModel($id);
    if($model->load(Yii::$app->request->post())){
      $model->cerrado = 1;
      $model->save(false);
      Yii::$app->session->setFlash('success','Servicio del comisionista cerrado correctamente');
    }
    return $this->redirect(Yii::$app->request->referrer);
  }

  /**
   * Finds the ComisionistaServicio model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return ComisionistaServicio the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = ComisionistaServicio::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
   * Creates a new ComisionistaServicio model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate()
  {
    $model = new ComisionistaServicio();

    if ($model->load(Yii::$app->request->post())) {
      $model->monto = $model->trabajo->comision;
      if ($model->save())
        return $this->redirect(['view', 'id' => $model->id]);
    }
    return $this->render('create', [
      'model' => $model,
    ]);
  }

  /**
   * Updates an existing ComisionistaServicio model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id)
  {
    $model = $this->findModel($id);

    if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('update', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Deletes an existing ComisionistaServicio model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id)
  {
    $this->findModel($id)->deleteWithRelated();

    return $this->redirect(['index']);
  }

  /**
   *
   * Export ComisionistaServicio information into PDF format.
   * @param integer $id
   * @return mixed
   */
  public function actionPdf($id)
  {
    $model = $this->findModel($id);
    $providerServicio = new ArrayDataProvider([
      'allModels' => $model->servicios,
    ]);

    $content = $this->renderPartial('_pdf', [
      'model' => $model,
      'providerServicio' => $providerServicio,
    ]);

    $pdf = new Pdf([
      'mode' => Pdf::MODE_CORE,
      'format' => Pdf::FORMAT_A4,
      'orientation' => Pdf::ORIENT_PORTRAIT,
      'destination' => Pdf::DEST_BROWSER,
      'content' => $content,
      'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
      'cssInline' => '.kv-heading-1{font-size:18px}',
      'options' => ['title' => \Yii::$app->name],
      'methods' => [
        'SetHeader' => [\Yii::$app->name],
        'SetFooter' => ['{PAGENO}'],
      ]
    ]);

    return $pdf->render();
  }
}
