<?php

namespace app\controllers;

use sintret\chat\ChatRoom;

class ChatController extends BaseController
{
  public function actionIndex()
  {
    return $this->render('index');
  }

  public function actionSendChat()
  {
    echo(ChatRoom::sendChat($_POST));
  }

}
