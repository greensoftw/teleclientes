<?php

namespace app\controllers;

use app\models\search\TipoEquipoSearch;
use app\models\TipoEquipo;
use mdm\admin\components\AccessControl;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * TipoEquipoController implements the CRUD actions for TipoEquipo model.
 */
class TipoEquipoController extends BaseController
{
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
      'access' => [
        'class' => AccessControl::className(),
      ]
    ];
  }

  /**
   * Lists all TipoEquipo models.
   * @return mixed
   */
  public function actionIndex()
  {
    $searchModel = new TipoEquipoSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single TipoEquipo model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
    $model = $this->findModel($id);
    $providerCodigoIsis = new \yii\data\ArrayDataProvider([
      'allModels' => $model->codigoIses,
    ]);
    $providerEquipo = new \yii\data\ArrayDataProvider([
      'allModels' => $model->equipos,
    ]);
    return $this->render('view', [
      'model' => $this->findModel($id),
      'providerCodigoIsis' => $providerCodigoIsis,
      'providerEquipo' => $providerEquipo,
    ]);
  }

  /**
   * Finds the TipoEquipo model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return TipoEquipo the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = TipoEquipo::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
   * Creates a new TipoEquipo model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate()
  {
    $model = new TipoEquipo();

    if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('create', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Updates an existing TipoEquipo model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id)
  {
    $model = $this->findModel($id);

    if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('update', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Deletes an existing TipoEquipo model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id)
  {
    $this->findModel($id)->deleteWithRelated();

    return $this->redirect(['index']);
  }
}
