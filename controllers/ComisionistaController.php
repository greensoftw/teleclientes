<?php

namespace app\controllers;

use app\models\AbonoComisionista;
use app\models\Comisionista;
use app\models\ComisionistaServicio;
use app\models\search\ComisionistaSearch;
use app\models\Usuario;
use kartik\form\ActiveForm;
use kartik\mpdf\Pdf;
use mdm\admin\components\AccessControl;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * ComisionistaController implements the CRUD actions for Comisionista model.
 */
class ComisionistaController extends BaseController
{
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
      'access' => [
        'class' => AccessControl::className(),
      ]
    ];
  }

  /**
   * Lists all Comisionista models.
   * @return mixed
   */
  public function actionIndex()
  {
    $searchModel = new ComisionistaSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Comisionista model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
    $model = $this->findModel($id);
    $providerAbonoComisionista = new ArrayDataProvider([
      'allModels' => $model->abonoComisionistas,
    ]);
    $providerComisionistaServicio = new ArrayDataProvider([
      'allModels' => $model->getComisionistaServicios()->where(['arreglado'=>0])->all(),
    ]);
    return $this->render('view', [
      'model' => $this->findModel($id),
      'providerAbonoComisionista' => $providerAbonoComisionista,
      'providerComisionistaServicio' => $providerComisionistaServicio,
    ]);
  }

  /**
   * Finds the Comisionista model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Comisionista the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = Comisionista::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  public function actionPagar($id)
  {
    $model = $this->findModel($id);
    $pago = new AbonoComisionista([
      'comisionista_id' => $model->id,
      'fecha' => date('Y-m-d'),
    ]);
    /** @var ComisionistaServicio[] $comisiones */
    $comisiones = $model->getComisionistaServicios()
      ->where(['concretado' => 1, 'arreglado' => 0, 'cerrado' => 1])->all();
    $pago->monto = $model->getTotal();
    if ($pago->load(Yii::$app->request->post())) {
      if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validate($model);
      }

      $pago->save();
      foreach ($comisiones as $comision) {
        $comision->abono_comisionista_id = $pago->id;
        $comision->arreglado = 1;
        $comision->save(false);
      }
      Yii::$app->session->setFlash('success','Pago registrado correctamente');
      $this->redirect(['index']);
    }
    return $this->render('pagar', [
      'model' => $model,
      'pago' => $pago,
      'comisiones'=>$comisiones
    ]);
  }

  public function actionVerPago($id)
  {
    $model = AbonoComisionista::findOne($id);
    if ($model != null) {
      return $this->render('ver-pago', [
        'model' => $model
      ]);
    }
    throw  new NotFoundHttpException('Pagina no encontrada');
  }

  /**
   * Creates a new Comisionista model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate()
  {
    $model = new Comisionista();
    $modelUser = new Usuario();
    if ($model->load(Yii::$app->request->post()) && $modelUser->load(Yii::$app->request->post())) {
      if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validateMultiple([$model, $modelUser]);
      }
      if ($modelUser->validate(['nombres', 'email']) and $model->validate(['telefonos'])) {
        $modelUser->username = Yii::$app->security->generateRandomString(10);
        $modelUser->setPassword(Yii::$app->name);
        $modelUser->generateAuthKey();
        if ($modelUser->save()) {
          $model->usuario_id = $modelUser->id;
          $model->save();
        }
        return $this->redirect(['view', 'id' => $model->id]);
      }
    }
    return $this->render('create', [
      'model' => $model,
      'modelUser' => $modelUser
    ]);
  }

  /**
   * Updates an existing Comisionista model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id)
  {
    $model = $this->findModel($id);
    $modelUser = $model->usuario;
    if ($model->load(Yii::$app->request->post()) && $modelUser->load(Yii::$app->request->post())) {
      if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validateMultiple([$model, $modelUser]);
      }
      $modelUser->save(false);
      $model->save();
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('update', [
        'model' => $model,
        'modelUser' => $modelUser
      ]);
    }
  }

  /**
   * Deletes an existing Comisionista model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id)
  {
    $this->findModel($id)->deleteWithRelated();

    return $this->redirect(['index']);
  }

  /**
   *
   * Export Comisionista information into PDF format.
   * @param integer $id
   * @return mixed
   */
  public function actionPdf($id)
  {
    $model = $this->findModel($id);
    $providerAbonoComisionista = new ArrayDataProvider([
      'allModels' => $model->abonoComisionistas,
    ]);
    $providerComisionistaServicio = new ArrayDataProvider([
      'allModels' => $model->comisionistaServicios,
    ]);

    $content = $this->renderPartial('_pdf', [
      'model' => $model,
      'providerAbonoComisionista' => $providerAbonoComisionista,
      'providerComisionistaServicio' => $providerComisionistaServicio,
    ]);

    $pdf = new Pdf([
      'format' => Pdf::FORMAT_A4,
      'orientation' => Pdf::ORIENT_PORTRAIT,
      'destination' => Pdf::DEST_BROWSER,
      'content' => $content,
      'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
      'cssInline' => '.kv-heading-1{font-size:18px}',
      'options' => ['title' => \Yii::$app->name],
      'methods' => [
        'SetHeader' => [\Yii::$app->name],
        'SetFooter' => ['{PAGENO}'],
      ]
    ]);

    return $pdf->render();
  }

  public function actionSaveAsNew($id)
  {
    $model = new Comisionista();

    if (Yii::$app->request->post('_asnew') != '1') {
      $model = $this->findModel($id);
    }

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('saveAsNew', [
        'model' => $model,
      ]);
    }
  }

  public function actionAddAbonoComisionista()
  {
    if (Yii::$app->request->isAjax) {
      $row = Yii::$app->request->post('AbonoComisionista');
      if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
        $row[] = [];
      return $this->renderAjax('_formAbonoComisionista', ['row' => $row]);
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  public function actionAddComisionistaServicio()
  {
    if (Yii::$app->request->isAjax) {
      $row = Yii::$app->request->post('ComisionistaServicio');
      if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
        $row[] = [];
      return $this->renderAjax('_formComisionistaServicio', ['row' => $row]);
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
