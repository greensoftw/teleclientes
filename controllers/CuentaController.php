<?php
/**
 * Created by PhpStorm.
 * User: jhon
 * Date: 7/02/17
 * Time: 11:51 AM
 */

namespace app\controllers;


use app\models\Newpass;
use Yii;

class CuentaController extends BaseController
{
  public function actionContrasena(){
    $model = new Newpass([
      'scenario'=>'uptpass'
    ]);
    if($model->load(Yii::$app->request->post()) and $model->validate()){
      $u = Yii::$app->user->identity;
      $u->setPassword($model->password);
      $u->save();
      Yii::$app->session->setFlash('success','Contraseña cambiada correctamente');
    }
    return $this->render('cuenta',[
      'model'=>$model
    ]);
  }
}