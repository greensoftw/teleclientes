<?php

namespace app\controllers;

use app\actions\DeleteFile;
use app\actions\UploadFile;
use app\actions\ViewFile;
use app\models\base\AbonoServicio;
use app\models\CobroEmpleado;
use app\models\Empleado;
use app\models\EmpleadoComision;
use app\models\PagoEmpleado;
use app\models\search\EmpleadoSearch;
use app\models\Usuario;
use kartik\form\ActiveForm;
use kartik\mpdf\Pdf;
use mdm\admin\components\AccessControl;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * EmpleadoController implements the CRUD actions for Empleado model.
 */
class EmpleadoController extends BaseController
{
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
      'access' => [
        'class' => AccessControl::className(),
      ]
    ];
  }

  public function actions()
  {
    return [
      'upload-file' => [
        'class' => UploadFile::className(),
        'model' => Empleado::className(),
        'model_id' => Yii::$app->request->get('id'),
        'folder' => 'empleado-archivos'
      ],
      'delete-file' => [
        'class' => DeleteFile::className()
      ],
      'view-file' => [
        'class' => ViewFile::className(),
        'columns' => [
          [
            'attribute' => 'usuario.nombres',
            'label' => 'Usuario',
          ],
          'direccion',
          'telefono',
          'tipo_empleado',
          'sueldo',
          'proximo_pago:date',
          'saldo',
          'is_tecnico:boolean',
          'perfil',
        ]
      ]
    ];
  }

  /**
   * Lists all Empleado models.
   * @return mixed
   */
  public function actionIndex()
  {
    $searchModel = new EmpleadoSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Empleado model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
    $model = $this->findModel($id);
    $providerEmpleadoComision = new ArrayDataProvider([
      'allModels' => $model->getEmpleadoComisions()
        ->where(['arreglado' => 0])->orderBy(['id' => SORT_DESC])->all(),
    ]);
    $providerServicio = new ArrayDataProvider([
      'allModels' => $model->getServicios()
        ->where(['cerrado' => 0])->all(),
    ]);
    $roles = false;
    if (Yii::$app->user->can('Administrador')) {
      $auth = Yii::$app->authManager;
      $roles = $auth->getAssignments($model->usuario_id);
    }

    return $this->render('view', [
      'model' => $this->findModel($id),
      'providerEmpleadoComision' => $providerEmpleadoComision,
      'providerServicio' => $providerServicio,
      'roles' => $roles
    ]);
  }

  /**
   * Finds the Empleado model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Empleado the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = Empleado::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  public function actionAbonar($id)
  {
    $model = $this->findModel($id);
    $pago = new PagoEmpleado([
      'empleado_id' => $model->id,
      'fecha' => date('Y-m-d'),
      'saldo_anterior' => $model->saldo
    ]);
    $ultimoPago = $model->getPagoEmpleados()->orderBy(['id' => SORT_DESC])
      ->one();
    $saldoDebe = $model->getSaldoDebe();
    $comision = 0;
    $comisiones = $model->getEmpleadoComisions()
      ->where(['arreglado' => 0])->all();

    if ($model->tipo_empleado == Empleado::CON_COMISION ||
      $model->tipo_empleado == Empleado::CON_COMISION_AN_SUELDO
    ) {
      $comision = $model->getEmpleadoComisions()
        ->select('monto')
        ->where(['arreglado' => 0])
        ->sum('monto');
      $pago->saldo_anterior += $comision;
    }
    if ($pago->load(Yii::$app->request->post())) {
      if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return \yii\widgets\ActiveForm::validate($pago);
      }
      $pago->save();
      if ($model->tipo_empleado == Empleado::CON_COMISION ||
        $model->tipo_empleado == Empleado::CON_COMISION_AN_SUELDO) {
        /** @var EmpleadoComision $com */
        foreach ($comisiones as $com) {
          $com->arreglado = 1;
          $com->pago_empleado_id = $pago->id;
          $com->save();
        }
      }
      if ($pago->cobrar_saldo_debe) {
        $model->saldo = ($pago->saldo_anterior - ($pago->monto + $saldoDebe));
        $pago->comentario = "Se cobro un saldo que debia por : ".$saldoDebe;
        /** @var CobroEmpleado $modelDeuda */
        $modelDeuda = $model->getCobroEmpleados()->orderBy(['id'=>SORT_DESC])->one();
        if($modelDeuda != null) {
          $modelDeuda->saldo = 0;
          $modelDeuda->save(false);
        }
      }else
        $model->saldo = $pago->saldo_anterior - $pago->monto;

      $model->save(false);
      $pago->save();
      $this->redirect(['view', 'id' => $model->id]);
    }
    return $this->render('abonar', [
      'model' => $model,
      'pago' => $pago,
      'ultimoPago' => $ultimoPago,
      'comision' => $comision,
      'saldoDebe' => $saldoDebe,
      'comisiones' => $comisiones
    ]);
  }

  public function actionCobrar($id)
  {
    $model = $this->findModel($id);
    /** @var CobroEmpleado $lastCobro */
    $lastCobro = $model->getCobroEmpleados()->orderBy(['id' => SORT_DESC])->one();
    /** @var AbonoServicio[] $cobros */
    $cobros = $model->getAbonoServicios()->where(['is', 'cobro_empleado_id', null])->all();
    if ($lastCobro != null and $lastCobro->saldo = 0) {
      Yii::$app->session->setFlash('danger', 'El empleado no tiene cobros pendientes');
      return $this->redirect(['view', 'id' => $model->id]);
    }
    $cobroTotal = $model->getAbonoServicios()->where(['is', 'cobro_empleado_id', null])->sum('monto');
    if ($lastCobro != null)
      $cobroTotal += $lastCobro->saldo;
    $cobro = new CobroEmpleado([
      'empleado_id' => $model->id,
      'fecha' => date('Y-m-d H:i:s'),
    ]);
    if ($cobro->load(Yii::$app->request->post())) {
      if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validate($cobro);
      }
      $cobro->saldo = ($model->getSaldoDebe() + $model->getDeuda()) - $cobro->monto;
      if ($cobro->validate()) {
        $cobro->save();
        foreach ($cobros as $cob) {
          $cob->cobro_empleado_id = $cobro->id;
          $cob->save(false);
        }
        Yii::$app->session->setFlash('success', 'Cobro guardado correctamente');
        return $this->redirect(['view', 'id' => $model->id]);
      }
    }
    return $this->render('cobrar', [
      'model' => $model,
      'cobro' => $cobro,
      'ultimoCobro' => $lastCobro,
      'cobroTotal' => $cobroTotal,
      'cobros' => $cobros
    ]);
  }

  /**
   * Creates a new Empleado model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate()
  {
    $roles = Yii::$app->authManager->getRoles();
    $model = new Empleado([
      'sueldo' => 0
    ]);
    $modelUser = new Usuario();
    if ($model->load(Yii::$app->request->post()) and $modelUser->load(Yii::$app->request->post())) {
      if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validateMultiple([$model, $modelUser]);
      }
      $modelUser->username = Yii::$app->security->generateRandomString(10);
      $modelUser->setPassword(Yii::$app->name);
      $modelUser->generateAuthKey();
      if ($modelUser->save()) {
        $model->usuario_id = $modelUser->id;
        if ($model->save()) {
          if (!empty($model->rol))
            Yii::$app->authManager->assign(Yii::$app->authManager->getRole($model->rol), $modelUser->id);
          return $this->redirect(['view', 'id' => $model->id]);
        }
      }
    }
    return $this->render('create', [
      'model' => $model,
      'modelUser' => $modelUser,
      'roles' => $roles
    ]);
  }

  /**
   * Updates an existing Empleado model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id)
  {

    $model = $this->findModel($id);
    $modelUser = $model->usuario;

    if ($model->load(Yii::$app->request->post()) and $modelUser->load(Yii::$app->request->post())) {
      if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validateMultiple([$model, $modelUser]);
      }
      if ($modelUser->save() and $model->save()) {
        return $this->redirect(['view', 'id' => $model->id]);
      }
    }
    return $this->render('update', [
      'model' => $model,
      'modelUser' => $modelUser,
      'roles' => null
    ]);
  }

  /**
   * Deletes an existing Empleado model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id)
  {
    $this->findModel($id)->deleteWithRelated();

    return $this->redirect(['index']);
  }

  /**
   *
   * Export Empleado information into PDF format.
   * @param integer $id
   * @return mixed
   */
  public function actionPdf($id) {
    $model = $this->findModel($id);
    $providerAbonoServicio = new \yii\data\ArrayDataProvider([
      'allModels' => $model->abonoServicios,
    ]);
    $providerCobroEmpleado = new \yii\data\ArrayDataProvider([
      'allModels' => $model->cobroEmpleados,
    ]);
    $providerEmpleadoComision = new \yii\data\ArrayDataProvider([
      'allModels' => $model->empleadoComisions,
    ]);
    $providerPagoEmpleado = new \yii\data\ArrayDataProvider([
      'allModels' => $model->pagoEmpleados,
    ]);
    $providerServicio = new \yii\data\ArrayDataProvider([
      'allModels' => $model->servicios,
    ]);

    $content = $this->renderPartial('_pdf', [
      'model' => $model,
      'providerAbonoServicio' => $providerAbonoServicio,
      'providerCobroEmpleado' => $providerCobroEmpleado,
      'providerEmpleadoComision' => $providerEmpleadoComision,
      'providerPagoEmpleado' => $providerPagoEmpleado,
      'providerServicio' => $providerServicio,
    ]);

    $pdf = new Pdf([
      'mode' => Pdf::MODE_CORE,
      'format' => Pdf::FORMAT_A4,
      'orientation' => Pdf::ORIENT_PORTRAIT,
      'destination' => Pdf::DEST_BROWSER,
      'content' => $content,
      'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
      'cssInline' => '.kv-heading-1{font-size:18px}',
      'options' => ['title' => \Yii::$app->name],
      'methods' => [
        'SetHeader' => [\Yii::$app->name],
        'SetFooter' => ['{PAGENO}'],
      ]
    ]);

    return $pdf->render();
  }
}
