<?php
/**
 * User: jhon
 * Date: 18/02/16
 * Time: 01:57 PM
 * @var $this \yii\web\View
 * @var $orden \app\models\Orden
 */
?>
<tr>
  <td align="center" valign="top">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#F8F8F8">
      <tr>
        <td align="center" valign="top">
          <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
            <tr>
              <td align="center" valign="top" width="500" class="flexibleContainerCell">
                <table border="0" cellpadding="30" cellspacing="0" width="100%">
                  <tr>
                    <td align="center" valign="top">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3
                                style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,
                                sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">
                              Hola ! <?= $orden->cliente->nombre ?></h3>

                            <div
                                style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                              gracias por elegir a <?= Yii::$app->name ?> como su centro de servicio
                              técnico, se acaba de registrar su orden de servicio N°
                              <?= $orden->id ?> correspondiente a :
                              <p>Descripción del equipo : <?= $orden->equipo ?></p>
                              <p>Descripción del problema : <?= $orden->problema ?></p>
                            </div>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </td>
</tr>
