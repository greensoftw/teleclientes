<?php
/**
 * Date: 18/02/16
 * Time: 01:57 PM
 * @var $this \yii\web\View
 * @var $model \app\models\Servicio
 * @var $empresa \app\models\EmpresaTelecliente
 * @var $config \app\models\Empresa
 */
use yii\helpers\Url;

?>
<tr>
  <td align="center" valign="top">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#F8F8F8">
      <tr>
        <td align="center" valign="top">
          <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
            <tr>
              <td align="center" valign="top" width="500" class="flexibleContainerCell">
                <table border="0" cellpadding="30" cellspacing="0" width="100%">
                  <tr>
                    <td align="center" valign="top">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3 style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">
                              Hola <?= strtoupper($model->cliente->usuario->nombres)?></h3>
                            <div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                              <table>
                                <thead>
                                <tr><td colspan="2">gracias por elegir a <?= $config->nombre ?> como su centro de servicio técnico de confianza.</td></tr>
                                <tr><td colspan="2"> A continuación le enviamos la información de la cotización:</td></tr>
                                </thead>
                                <tbody>
                                <tr>
                                  <td>FECHA DE INGRESO <?= Yii::$app->formatter->asDatetime($model->ingreso->fecha) ?></td>
                                </tr>
                                <tr>
                                  <td>FECHA DE VISITA <?= Yii::$app->formatter->asDatetime($model->fecha_tecnico) ?></td>
                                </tr>
                                <tr>
                                  <td colspan="2"></td>
                                </tr>
                                <tr>
                                  <td>Estado actual :</td>
                                  <td><?= $model->estadoActual->estado ?></td>
                                </tr>
                                <tr>
                                  <td colspan="2">
                                    <hr>
                                  </td>
                                </tr>
                                <tr>
                                  <td colspan="2">
                                    EQUIPO
                                    <hr>
                                  </td>
                                </tr>
                                <?php if($model->servicioAEquipo): ?>
                                  <tr>
                                    <td colspan="2">
                                      Equipo : <?= $model->servicioAEquipo->equipo->tipoEquipo->nombre ?><br>
                                      Marca : <?= $model->servicioAEquipo->equipo->marca->nombre ?><br>
                                      Modelo: <?= $model->servicioAEquipo->equipo->modelo ?><br>
                                      Serial : <?= $model->servicioAEquipo->equipo->serial ?><>
                                    </td>
                                  </tr>
                                <?php else: ?>
                                  <tr>
                                    <td colspan="2">Sin equipos registrados</td>
                                  </tr>
                                <?php endif; ?>
                                <tr>
                                  <td colspan="2">
                                    <hr>
                                  </td>
                                </tr>
                                <tr>
                                  <td colspan="2">
                                    PRESUPUESTO
                                    <hr>
                                  </td>
                                </tr>
                                <?php foreach ($model->presupuestos as $presupuesto): ?>
                                  <tr>
                                    <td><?= $presupuesto->relacion->nombre ?></td>
                                    <td><?= Yii::$app->formatter->asCurrency($presupuesto->precio)?></td>
                                  </tr>
                                <?php endforeach; ?>
                                <tr>
                                  <td colspan=>Total:</td>
                                  <td colspan=><?= Yii::$app->formatter->asCurrency($model->total) ?></td>
                                </tr>
                                <tr>
                                  <td colspan="2">Confirmar presupuesto:
                                    <?= \yii\bootstrap\Html::a('Confirmar',Url::to(['confirmacion/confirmar-presupuesto',
                                      'id'=>$model->id, 'token'=>$model->token,'empresa_id'=>$empresa->id],true),
                                      ['class'=>'buttonContent'])?>
                                  </td>
                                </tr>
                                </tbody>
                              </table>
                            </div>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </td>
</tr>
