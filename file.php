<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8"/>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  <link href="file.css" type="text/css" rel="stylesheet">
  <title>Informe Plan de Desarrollo - AMC</title>
</head>
<script language="javascript">
  function validar(frm, archivo) {
    frm.action = archivo;
    frm.target = "_blank";
    frm.submit();
  }
</script>
<body>
<img src="Politicas-Metropolitanas-de-Paz.png" style="width: 100%">
<h4>CONVENIO 051/2015</h4>
<br>
<form name="form1" id="form1" method="get">
  <table class="resultado" width="100%">
    <tr>
      <td width="80%" align="center">&nbsp;</td>
      <td width="10%" align="center"><b>PDF</b></td>
    </tr>
    <tr>
      <td align="left">&nbsp;Memoria Talleres, Seminarios-Conversatorios<br/>
        &nbsp;&nbsp;&bull;&nbsp;&nbsp;Memorias intercambio de experiencia en Medell&igrave;n-AMC <br/>
        &nbsp;&nbsp;&bull;&nbsp;&nbsp;Plan Estrategico Memoria <br/>
        &nbsp;&nbsp;&bull;&nbsp;&nbsp;Plan Estrategico Politicas<br/>
        &nbsp;&nbsp;&bull;&nbsp;&nbsp;Presentaci&oacute;n Prepar&eacute;monos para la paz Gobernaci&oacute;n Antioquia
        nov 2015<br/>
        &nbsp;&nbsp;&bull;&nbsp;&nbsp;Primer Taller Politica<br/></td>
      <td align="center"><br/><a
          href="javascript:validar(document.forms['form1'],'Memorias intercambio de experiencia en Medellìn-AMC.pdf');"><img
            src="download.png" /></a>
        <br/><a href="javascript:validar(document.forms['form1'],'Plan Estrategico Memoria.pdf');"><img
            src="download.png" /></a>
        <br/><a href="javascript:validar(document.forms['form1'],'Plan Estrategico Politicas.pdf');"><img
            src="download.png" /></a>
        <br/><a
          href="javascript:validar(document.forms['form1'],'Presentación Preparémonos para la paz Gobernación Antioquia nov 2015.pdf');"><img
            src="download.png" /></a>
        <br/><a href="javascript:validar(document.forms['form1'],'Primer Taller Politica.pdf');"><img
            src="download.png" /></a></td>
    </tr>
    <tr>
      <td align="left">&nbsp;Politicas P&uacute;blicas de Paz</td>
      <td align="center">&nbsp;</td>
    </tr>
    <tr>
      <td align="left">&nbsp;Memoria Hist&oacute;rica del Conflicto</td>
      <td align="center">&nbsp;</td>
    </tr>
  </table>

  <div style="height: 30px; text-align: right; padding-top: 20px">
    <a href="http://www.amc.gov.co/convenio_051/convenio_051.html" class="btn">Atras</a>
  </div>
</form>
</body>
</html>
