<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=base_teleclientes',
    'username' => 'root',
    'password' => 'some_pass',
    'charset' => 'utf8',
];
