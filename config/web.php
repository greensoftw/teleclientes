<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'Teleclientes',
    'language' => 'es',
    'name' => 'Teleclientes',
    'timeZone' => 'America/Bogota',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'FTfcIgmTViluvQV2haR5S4TIFMq1jPKS',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\Usuario',
            'enableAutoLogin' => false,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'bryglen\sendgrid\Mailer',
            'username' => 'jhonjtoloza',
            'password' => 'jhonkairo1!_A',
            'options' => ['turn_off_ssl_verification' => true]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['guest'],
        ],
    ],
    'modules' => [
        'apicomisionista' => [
            'class' => 'app\modules\apicomisionista\Module',
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module',
        ],
        'datecontrol' => [
            'class' => '\kartik\datecontrol\Module',
        ],
        'treemanager' => [
            'class' => '\kartik\tree\Module',
        ],
        'admin' => [
            'class' => 'mdm\admin\Module',
            'layout' => 'left-menu', // it can be '@path/to/your/layout'.
            'mainLayout' => '@app/views/layouts/primary.php',
            'controllerMap' => [
                'assignment' => [
                    'class' => 'mdm\admin\controllers\AssignmentController',
                    'userClassName' => 'app\models\Usuario',
                    'idField' => 'id',
                    'fullnameField'=>'nombres',
                    'extraColumns'=>['nombres']
                ],
            ],
            'menus' => [
                'assignment' => [
                    'label' => 'Conceder acceso' // change label
                ],
                'route' => null,
                'permission' => null,
                'rule' => null
            ],
            'defaultUrlLabel'=>'Control de acceso'
        ],
    ],
    'params' => $params,
    'defaultRoute' => 'sitio/index'
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
