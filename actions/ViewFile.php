<?php
/**
 * User: jhon
 * Date: 24/12/16
 * Time: 12:08 AM
 */

namespace app\actions;


use app\models\Archivo;
use yii\base\Action;

class ViewFile extends Action
{
  public $columns;

  public function run($id){
    $model = Archivo::findOne($id);
    return $this->controller->render('/extra-views/view_file',[
      'model'=>$model,
      'columns'=>$this->columns
    ]);
  }
}