<?php
namespace app\actions;

use app\models\Archivo;
use Yii;
use yii\web\Response;

/**
 * Created by PhpStorm.
 * User: Andres RS
 * Date: 28/04/2016
 * Time: 6:49 PM
 */
class DeleteFile extends \yii\base\Action
{

  public function run()
  {
    Yii::$app->response->format = Response::FORMAT_JSON;
    /** @var Archivo $model */
    $model = Archivo::findOne(Yii::$app->request->get('id'));
    if($model != null and file_exists($model->ruta)){
      unlink($model->ruta);
      $model->delete();
    }
    return $this->controller->redirect(Yii::$app->request->referrer);
  }
}