-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 09-02-2017 a las 14:02:14
-- Versión del servidor: 5.7.17-0ubuntu0.16.04.1
-- Versión de PHP: 5.6.29-1+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `teleclientes`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `abono_comisionista`
--

CREATE TABLE `abono_comisionista` (
  `id` int(11) NOT NULL,
  `comisionista_id` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `monto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `abono_compra`
--

CREATE TABLE `abono_compra` (
  `id` int(11) NOT NULL,
  `compra_id` int(11) NOT NULL,
  `fecha` varchar(250) DEFAULT NULL,
  `monto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `abono_servicio`
--

CREATE TABLE `abono_servicio` (
  `id` int(11) NOT NULL,
  `servicio_id` int(11) NOT NULL,
  `monto` int(11) NOT NULL,
  `fecha` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivo`
--

CREATE TABLE `archivo` (
  `id` int(11) NOT NULL,
  `model` varchar(250) NOT NULL,
  `model_id` int(11) NOT NULL,
  `ruta` varchar(250) NOT NULL DEFAULT '',
  `title` varchar(250) NOT NULL,
  `type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivo_pdf`
--

CREATE TABLE `archivo_pdf` (
  `id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `descripcion` varchar(500) NOT NULL,
  `ruta_archivo` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` varchar(64) NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('/admin/*', 2, 'Módulo de control de acceso', NULL, NULL, 1484519769, 1484519769),
('/admin/assignment/index', 2, 'Módulo de control de acceso', NULL, NULL, 1484529537, 1484529537),
('/chat/*', 2, 'Acceso a chat', NULL, NULL, 1484504268, 1484504268),
('/cliente/*', 2, 'Módulo de clientes', NULL, NULL, 1484504291, 1484504291),
('/cliente/create', 2, 'Crear clientes', NULL, NULL, 1484504291, 1484504291),
('/cliente/delete', 2, 'Eliminar clientes', NULL, NULL, 1484504291, 1484504291),
('/cliente/index', 2, 'Listado de clientes', NULL, NULL, 1484504291, 1484504291),
('/cliente/pdf', 2, 'Exportar resultados de búsquedas de clientes', NULL, NULL, 1484504291, 1484504291),
('/cliente/update', 2, 'Actualizar cliente', NULL, NULL, 1484504291, 1484504291),
('/cliente/view', 2, 'Ver detalle del cliente', NULL, NULL, 1484504291, 1484504291),
('/comisionista-servicio/*', 2, 'Módulo comisionista crear servicios ', NULL, NULL, 1484530772, 1484530772),
('/comisionista-servicio/create', 2, 'Crear servicios a comisionista', NULL, NULL, 1484530771, 1484530771),
('/comisionista-servicio/delete', 2, 'Eliminar servicios a comisionista', NULL, NULL, 1484530771, 1484530771),
('/comisionista-servicio/index', 2, 'Listado de servicios de comisionista', NULL, NULL, 1484518686, 1484518686),
('/comisionista-servicio/pdf', 2, 'Exportar resultado de búsquedas de  servicios de comisionista', NULL, NULL, 1484530771, 1484530771),
('/comisionista-servicio/update', 2, 'Actualizar servicios a comisionista', NULL, NULL, 1484530771, 1484530771),
('/comisionista-servicio/view', 2, 'Ver detalles de servicios a comisionista', NULL, NULL, 1484518690, 1484518690),
('/comisionista/*', 2, 'Módulo de comisionista', NULL, NULL, 1484518463, 1484518463),
('/comisionista/create', 2, 'Crear comisionista', NULL, NULL, 1484518463, 1484518463),
('/comisionista/index', 2, 'Listado de comisionista', NULL, NULL, 1484518463, 1484518463),
('/comisionista/pagar', 2, 'Pagar comisionista', NULL, NULL, 1484518463, 1484518463),
('/comisionista/pdf', 2, 'Exportar resultados de búsquedas de comisionista', NULL, NULL, 1484518463, 1484518463),
('/comisionista/update', 2, 'Actualizar comisionista', NULL, NULL, 1484518463, 1484518463),
('/comisionista/ver-pago', 2, 'Ver pagos hechos a comisionista', NULL, NULL, 1484518463, 1484518463),
('/comisionista/view', 2, 'Ver detalles del comisionista', NULL, NULL, 1484518463, 1484518463),
('/compra/*', 2, 'Módulo de compras', NULL, NULL, 1484518810, 1484518810),
('/compra/abonar', 2, 'Abonar a la compra', NULL, NULL, 1484518816, 1484518816),
('/compra/create', 2, 'Crear compra', NULL, NULL, 1484518816, 1484518816),
('/compra/delete', 2, 'Eliminar compra', NULL, NULL, 1484518816, 1484518816),
('/compra/index', 2, 'Listado  de compras', NULL, NULL, 1484518816, 1484518816),
('/compra/pdf', 2, 'Exportar resultado de búsquedas de compras', NULL, NULL, 1484518816, 1484518816),
('/compra/update', 2, 'Actualizar compras', NULL, NULL, 1484518816, 1484518816),
('/compra/view', 2, 'Ver detalles de las compras', NULL, NULL, 1484518816, 1484518816),
('/configuracion/*', 2, 'Módulo de configuración', NULL, NULL, 1484519751, 1484519751),
('/distribuidor/*', 2, 'Módulo de distribuidor', NULL, NULL, 1484519627, 1484519627),
('/distribuidor/create', 2, 'Crear distribuidor', NULL, NULL, 1484519626, 1484519626),
('/distribuidor/delete', 2, 'Eliminar distribuidor', NULL, NULL, 1484519627, 1484519627),
('/distribuidor/index', 2, 'Listado de distribuidores', NULL, NULL, 1484519626, 1484519626),
('/distribuidor/pdf', 2, 'Exportar resultados de búsquedas de distribuidores', NULL, NULL, 1484519627, 1484519627),
('/distribuidor/update', 2, 'Actualizar distribuidor', NULL, NULL, 1484519626, 1484519626),
('/distribuidor/view', 2, 'Ver detalles del distribuidor', NULL, NULL, 1484519626, 1484519626),
('/empleado/*', 2, 'Módulo de empleados', NULL, NULL, 1484519508, 1484519508),
('/empleado/abonar', 2, 'Abonar al empleado', NULL, NULL, 1484519508, 1484519508),
('/empleado/create', 2, 'Crear empleado', NULL, NULL, 1484519508, 1484519508),
('/empleado/delete-file', 2, 'Eliminar archivos del empleado', NULL, NULL, 1484519508, 1484519508),
('/empleado/index', 2, 'Listado de empleados', NULL, NULL, 1484519508, 1484519508),
('/empleado/pdf', 2, 'Exportar resultados de búsquedas de empleados', NULL, NULL, 1484519508, 1484519508),
('/empleado/update', 2, 'Actualizar empleados', NULL, NULL, 1484519508, 1484519508),
('/empleado/upload-file', 2, 'Cargar archivos a los empleados', NULL, NULL, 1484519508, 1484519508),
('/empleado/view', 2, 'Ver detalles del empleado', NULL, NULL, 1484519508, 1484519508),
('/empleado/view-file', 2, 'Ver archivos cargados a empleados', NULL, NULL, 1484519508, 1484519508),
('/marca/*', 2, 'Módulo de marca', NULL, NULL, 1484519428, 1484519428),
('/marca/create', 2, 'Crear marca', NULL, NULL, 1484519432, 1484519432),
('/marca/delete', 2, 'Eliminar marca', NULL, NULL, 1484519432, 1484519432),
('/marca/index', 2, 'Listado de marcas', NULL, NULL, 1484519426, 1484519426),
('/marca/pdf', 2, 'Exportar resultados de búsquedas de marcas', NULL, NULL, 1484519432, 1484519432),
('/marca/update', 2, 'Actualizar marca', NULL, NULL, 1484519432, 1484519432),
('/marca/view', 2, 'Ver detalle de la marca', NULL, NULL, 1484519432, 1484519432),
('/proveedor/*', 2, 'Módulo de proveedores', NULL, NULL, 1484519517, 1484519517),
('/proveedor/create', 2, 'Crear proveedor', NULL, NULL, 1484519517, 1484519517),
('/proveedor/delete', 2, 'Eliminar proveedoor', NULL, NULL, 1484519517, 1484519517),
('/proveedor/index', 2, 'Listado de proveedores', NULL, NULL, 1484519517, 1484519517),
('/proveedor/pdf', 2, 'Exportar resultados de búsquedas de proveedores', NULL, NULL, 1484519517, 1484519517),
('/proveedor/update', 2, 'Actualizar proveedor', NULL, NULL, 1484519517, 1484519517),
('/proveedor/view', 2, 'Ver detalle del proveedor', NULL, NULL, 1484519517, 1484519517),
('/repuestos/*', 2, 'Módulo de repuestos', NULL, NULL, 1484518841, 1484518841),
('/repuestos/create', 2, 'Crear repuestos', NULL, NULL, 1484518843, 1484518843),
('/repuestos/delete', 2, 'Eliminar repuestos', NULL, NULL, 1484518843, 1484518843),
('/repuestos/index', 2, 'Listado de repuestos', NULL, NULL, 1484518843, 1484518843),
('/repuestos/pdf', 2, 'Exportar resultado de búsqueda de repuestos', NULL, NULL, 1484518843, 1484518843),
('/repuestos/update', 2, 'Actualizar repuesto', NULL, NULL, 1484518843, 1484518843),
('/repuestos/view', 2, 'Ver detalle del repuesto', NULL, NULL, 1484518843, 1484518843),
('/servicio-programado/*', 2, 'Módulo  de servicios programados', NULL, NULL, 1484504246, 1484504246),
('/servicio-programado/create', 2, 'Crear servicio programado', NULL, NULL, 1484504246, 1484504246),
('/servicio-programado/delete', 2, 'Eliminar servicio programado', NULL, NULL, 1484504246, 1484504246),
('/servicio-programado/index', 2, 'Listado de servicios programados', NULL, NULL, 1484504246, 1484504246),
('/servicio-programado/pdf', 2, 'Exportar resultado de búsqueda de servicio programado', NULL, NULL, 1484504246, 1484504246),
('/servicio-programado/update', 2, 'Actualizar servicio programado', NULL, NULL, 1484504246, 1484504246),
('/servicio-programado/view', 2, 'Ver detalle servicio programado', NULL, NULL, 1484504246, 1484504246),
('/servicio/*', 2, 'Módulo de servicio', NULL, NULL, 1484504158, 1484504158),
('/servicio/abonar', 2, 'Abonar al servicio', NULL, NULL, 1484504174, 1484504174),
('/servicio/agregar-equipo', 2, 'Agregar equipo al servicio', NULL, NULL, 1484519532, 1484519532),
('/servicio/agregar-estado', 2, 'Agregar estado al servicio', NULL, NULL, 1484504176, 1484504176),
('/servicio/agregar-presupuesto', 2, NULL, NULL, NULL, 1485660496, 1485660496),
('/servicio/borrar-presupuesto', 2, NULL, NULL, NULL, 1485844698, 1485844698),
('/servicio/comisionar', 2, NULL, NULL, NULL, 1485675390, 1485675390),
('/servicio/create', 2, 'Crear servicio', NULL, NULL, 1484504150, 1484504150),
('/servicio/delete-file', 2, 'Eliminar archivos cargados al servicio', NULL, NULL, 1484504141, 1484504141),
('/servicio/enviarenlace', 2, 'Enviar enlace de servicio', NULL, NULL, 1484504181, 1484504181),
('/servicio/index', 2, 'Listado de servicios', NULL, NULL, 1484504146, 1484504146),
('/servicio/pdf', 2, 'Exportar resultados de búsquedas de servicios', NULL, NULL, 1484504171, 1484504171),
('/servicio/update', 2, 'Actualizar servicios', NULL, NULL, 1484504150, 1484504150),
('/servicio/upload-file', 2, 'Cargar archivos a los servicios', NULL, NULL, 1484504139, 1484504139),
('/servicio/view', 2, 'Ver detalle del servicio', NULL, NULL, 1484504150, 1484504150),
('/servicio/view-file', 2, 'Ver archivos cargados al servicio', NULL, NULL, 1484504143, 1484504143),
('/site/download-file', 2, NULL, NULL, NULL, 1486590745, 1486590745),
('/site/index', 2, 'Inicio (site/Index)', NULL, NULL, 1484519602, 1484519602),
('/tipo-equipo/*', 2, 'Módulo de tipos de equipo', NULL, NULL, 1484528977, 1484528977),
('/tipo-equipo/create', 2, 'Crear tipos de equipo', NULL, NULL, 1484528977, 1484528977),
('/tipo-equipo/delete', 2, 'Eliminar tipos de equipo', NULL, NULL, 1484528977, 1484528977),
('/tipo-equipo/index', 2, 'Listado de tipos de equipo', NULL, NULL, 1484528977, 1484528977),
('/tipo-equipo/update', 2, 'Actualizar tipos de equipo', NULL, NULL, 1484528977, 1484528977),
('/tipo-equipo/view', 2, 'Ver detalle del tipo de equipo', NULL, NULL, 1484528977, 1484528977),
('/trabajo/*', 2, 'Módulo de trabajo', NULL, NULL, 1484518853, 1484518853),
('/trabajo/create', 2, 'Crear trabajo', NULL, NULL, 1484518855, 1484518855),
('/trabajo/delete', 2, 'Eliminar trabajo', NULL, NULL, 1484518855, 1484518855),
('/trabajo/index', 2, 'Listado de trabajos', NULL, NULL, 1484518855, 1484518855),
('/trabajo/pdf', 2, 'Exportar resultados de búsqueda de trabajos', NULL, NULL, 1484518855, 1484518855),
('/trabajo/update', 2, 'Actualizar trabajo', NULL, NULL, 1484518855, 1484518855),
('/trabajo/view', 2, 'Ver detalle del trabajo', NULL, NULL, 1484518855, 1484518855),
('Administrador', 1, 'Administrador', NULL, NULL, 1484350658, 1484350928),
('Tecnico', 1, 'Técnico', NULL, NULL, 1484542150, 1484542150);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('Administrador', '/admin/*'),
('Administrador', '/chat/*'),
('Administrador', '/cliente/*'),
('Administrador', '/comisionista-servicio/*'),
('Administrador', '/comisionista/*'),
('Administrador', '/compra/*'),
('Administrador', '/distribuidor/*'),
('Administrador', '/empleado/*'),
('Administrador', '/marca/*'),
('Administrador', '/repuestos/*'),
('Administrador', '/servicio-programado/*'),
('Tecnico', '/servicio-programado/create'),
('Tecnico', '/servicio-programado/index'),
('Tecnico', '/servicio-programado/pdf'),
('Tecnico', '/servicio-programado/update'),
('Tecnico', '/servicio-programado/view'),
('Administrador', '/servicio/*'),
('Tecnico', '/servicio/abonar'),
('Tecnico', '/servicio/agregar-equipo'),
('Tecnico', '/servicio/agregar-estado'),
('Tecnico', '/servicio/agregar-presupuesto'),
('Tecnico', '/servicio/borrar-presupuesto'),
('Tecnico', '/servicio/enviarenlace'),
('Tecnico', '/servicio/index'),
('Tecnico', '/servicio/pdf'),
('Tecnico', '/servicio/update'),
('Tecnico', '/servicio/upload-file'),
('Tecnico', '/servicio/view'),
('Tecnico', '/servicio/view-file'),
('Administrador', '/site/download-file'),
('Administrador', '/site/index'),
('Administrador', '/tipo-equipo/*'),
('Administrador', '/trabajo/*');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `chat`
--

CREATE TABLE `chat` (
  `id` int(11) NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `message` text,
  `updateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `comisionista_id` int(11) DEFAULT NULL,
  `telefonos` varchar(250) NOT NULL,
  `direccion` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `codigo_isis`
--

CREATE TABLE `codigo_isis` (
  `id` int(11) NOT NULL,
  `marca_id` int(11) NOT NULL,
  `tipo_equipo_id` int(11) NOT NULL,
  `codigo` varchar(20) NOT NULL,
  `descripcion` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comisionista`
--

CREATE TABLE `comisionista` (
  `id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `distribuidor_id` int(11) DEFAULT NULL,
  `telefonos` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comisionista_servicio`
--

CREATE TABLE `comisionista_servicio` (
  `id` int(11) NOT NULL,
  `trabajo_id` int(11) NOT NULL,
  `comisionista_id` int(11) NOT NULL,
  `abono_comisionista_id` int(11) DEFAULT NULL,
  `nombre_cliente` varchar(250) NOT NULL,
  `correo` varchar(250) DEFAULT NULL,
  `direccion_cliente` varchar(250) DEFAULT NULL,
  `telefono_cliente` varchar(250) NOT NULL,
  `arreglado` tinyint(1) NOT NULL DEFAULT '0',
  `monto` int(11) NOT NULL,
  `concretado` tinyint(1) NOT NULL DEFAULT '0',
  `cerrado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compra`
--

CREATE TABLE `compra` (
  `id` int(11) NOT NULL,
  `proveedor_id` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `pagado` bit(1) NOT NULL,
  `total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

CREATE TABLE `configuracion` (
  `id` int(11) NOT NULL,
  `key` varchar(250) NOT NULL,
  `value` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `distribuidor`
--

CREATE TABLE `distribuidor` (
  `id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `direccion` varchar(250) NOT NULL,
  `telefono` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `distribuidor_departamento`
--

CREATE TABLE `distribuidor_departamento` (
  `id` int(11) NOT NULL,
  `distribuidor_id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado`
--

CREATE TABLE `empleado` (
  `id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `telefono` varchar(250) NOT NULL,
  `tipo_empleado` varchar(250) NOT NULL,
  `sueldo` int(11) DEFAULT NULL,
  `proximo_pago` datetime DEFAULT NULL,
  `saldo` int(11) NOT NULL,
  `is_tecnico` bit(1) NOT NULL,
  `perfil` varchar(1000) DEFAULT NULL,
  `direccion` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado_comision`
--

CREATE TABLE `empleado_comision` (
  `id` int(11) NOT NULL,
  `servicio_id` int(11) NOT NULL,
  `empleado_id` int(11) NOT NULL,
  `pago_empleado_id` int(11) DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `monto` int(11) NOT NULL,
  `arreglado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipo`
--

CREATE TABLE `equipo` (
  `id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `marca_id` int(11) NOT NULL,
  `distribuidor_id` int(11) DEFAULT NULL,
  `tipo_equipo_id` int(11) NOT NULL,
  `modelo` varchar(250) DEFAULT NULL,
  `serial` varchar(250) DEFAULT NULL,
  `nota` varchar(250) DEFAULT NULL,
  `tiempo_mantenimiento` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `item_compra`
--

CREATE TABLE `item_compra` (
  `id` int(11) NOT NULL,
  `compra_id` int(11) NOT NULL,
  `repuesto_id` int(11) NOT NULL,
  `precio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE `marca` (
  `id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `ciudad` varchar(250) DEFAULT NULL,
  `direccion` varchar(250) DEFAULT NULL,
  `telefonos` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca_departamento`
--

CREATE TABLE `marca_departamento` (
  `id` int(11) NOT NULL,
  `marca_id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `route` varchar(256) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `data` blob
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`id`, `name`, `parent`, `route`, `order`, `data`) VALUES
(1, 'Servicio', NULL, '/servicio/index', 1, NULL),
(2, 'Agregar servicio', 1, '/servicio/create', 2, NULL),
(3, 'Servicios Programados', NULL, '/servicio-programado/index', 3, NULL),
(4, 'Clientes', NULL, '/cliente/index', 2, NULL),
(5, 'Comisionistas', NULL, '/comisionista/index', 4, NULL),
(6, 'Servicios de comisionistas', NULL, '/comisionista-servicio/index', 6, NULL),
(7, 'Compras', NULL, '/compra/index', 7, NULL),
(8, 'Trabajos', NULL, '/trabajo/index', 5, NULL),
(9, 'Marcas', NULL, '/marca/index', 8, 0x312c32),
(10, 'Distribuidoras', NULL, '/distribuidor/index', 9, NULL),
(11, 'Tipos de Equipo', NULL, '/tipo-equipo/index', 10, NULL),
(12, 'Repuestos', NULL, '/repuestos/index', 11, NULL),
(13, 'Empleado', NULL, '/empleado/index', 10, NULL),
(14, 'Listado de Servicios', 1, '/servicio/index', 1, NULL),
(15, 'Administracion de acceso', NULL, '/admin/assignment/index', 12, NULL),
(16, 'Listado de marcas', 9, '/marca/index', 1, NULL),
(17, 'Listado de clientes', 4, '/cliente/index', 1, NULL),
(18, 'Agregar cliente', 4, '/cliente/create', 2, NULL),
(19, 'Listado de comisionistas', 5, '/comisionista/index', 1, NULL),
(20, 'Agregar comisionista', 5, '/comisionista/create', 2, NULL),
(21, 'Listado de servicios', 6, '/comisionista-servicio/index', 1, NULL),
(22, 'Agregar servicio', 6, '/comisionista-servicio/create', 2, NULL),
(23, 'Listado de compras', 7, '/compra/index', 1, NULL),
(24, 'Agregar compra', 7, '/compra/create', 2, NULL),
(25, 'Listado de distribuidores', 10, '/distribuidor/index', 1, NULL),
(26, 'Agregar distribuidor', 10, '/distribuidor/create', 2, NULL),
(27, 'Listado de empleados', 13, '/empleado/index', 1, NULL),
(28, 'Agregar empleado', 13, '/empleado/create', 2, NULL),
(29, 'Listado de repuestos', 12, '/repuestos/index', 1, NULL),
(30, 'Agregar repuesto', 12, '/repuestos/create', 2, NULL),
(31, 'Listado de servicios', 3, '/servicio-programado/index', 1, NULL),
(32, 'Agregar servicio', 3, '/servicio-programado/create', 2, NULL),
(33, 'Listado de tipos de equipo', 11, '/tipo-equipo/index', 1, NULL),
(34, 'Agregar tipo de equipo', 11, '/tipo-equipo/create', 2, NULL),
(35, 'Listado de trabajos', 8, '/trabajo/index', 1, NULL),
(36, 'Agregar  trabajo', 8, '/trabajo/create', 2, NULL),
(37, 'Proveedores', NULL, '/proveedor/index', 20, NULL),
(38, 'Listado de proveedores', 37, '/proveedor/index', 1, NULL),
(39, 'Agregar proveedor', 37, '/proveedor/create', 2, NULL),
(40, 'Crear marca', 9, '/marca/create', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movimientos_caja`
--

CREATE TABLE `movimientos_caja` (
  `id` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `monto` int(11) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `usuario_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago_empleado`
--

CREATE TABLE `pago_empleado` (
  `id` int(11) NOT NULL,
  `empleado_id` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `monto` int(11) NOT NULL,
  `saldo_anterior` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `presupuesto`
--

CREATE TABLE `presupuesto` (
  `id` int(11) NOT NULL,
  `servicio_id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `model` varchar(250) DEFAULT NULL,
  `precio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `direccion` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor_departamento`
--

CREATE TABLE `proveedor_departamento` (
  `id` int(11) NOT NULL,
  `proveedor_id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `repuestos`
--

CREATE TABLE `repuestos` (
  `id` int(11) NOT NULL,
  `codigo` varchar(250) NOT NULL,
  `marca_id` int(11) DEFAULT NULL,
  `no_parte` varchar(250) DEFAULT NULL,
  `nombre` varchar(250) NOT NULL,
  `costo` int(11) NOT NULL,
  `precio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio`
--

CREATE TABLE `servicio` (
  `id` int(11) NOT NULL,
  `numero` varchar(100) NOT NULL,
  `empleado_id` int(11) DEFAULT NULL,
  `cliente_id` int(11) NOT NULL,
  `comision_servicio_id` int(11) DEFAULT NULL,
  `total` int(11) NOT NULL,
  `pagado` smallint(1) NOT NULL DEFAULT '0',
  `no_facturado` varchar(250) DEFAULT NULL,
  `cerrado` smallint(1) NOT NULL DEFAULT '0',
  `token` varchar(50) DEFAULT NULL,
  `marca_id` int(11) DEFAULT NULL,
  `numero_garantia` varchar(100) DEFAULT NULL,
  `fecha_tecnico` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio_a_equipo`
--

CREATE TABLE `servicio_a_equipo` (
  `id` int(11) NOT NULL,
  `servicio_id` int(11) NOT NULL,
  `equipo_id` int(11) NOT NULL,
  `codigo_isis_id` int(11) DEFAULT NULL,
  `descripcion` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio_estado`
--

CREATE TABLE `servicio_estado` (
  `id` int(11) NOT NULL,
  `servicio_id` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `estado` varchar(250) NOT NULL,
  `descripcion` varchar(250) DEFAULT NULL,
  `actual` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio_programado`
--

CREATE TABLE `servicio_programado` (
  `id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `servicio_id` int(11) DEFAULT NULL,
  `equipo_id` int(11) DEFAULT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_equipo`
--

CREATE TABLE `tipo_equipo` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajo`
--

CREATE TABLE `trabajo` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `descripcion` varchar(250) DEFAULT NULL,
  `precio` int(11) NOT NULL,
  `visible_comisionista` bit(1) DEFAULT NULL,
  `comision` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(500) NOT NULL,
  `nombres` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `avatar` varchar(250) DEFAULT NULL,
  `receive_email` tinyint(1) NOT NULL DEFAULT '1',
  `authKey` varchar(500) DEFAULT NULL,
  `access_token` varchar(500) DEFAULT NULL,
  `password_reset_token` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `abono_comisionista`
--
ALTER TABLE `abono_comisionista`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comisionista_id` (`comisionista_id`);

--
-- Indices de la tabla `abono_compra`
--
ALTER TABLE `abono_compra`
  ADD PRIMARY KEY (`id`),
  ADD KEY `compra_id` (`compra_id`);

--
-- Indices de la tabla `abono_servicio`
--
ALTER TABLE `abono_servicio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `servicio_id` (`servicio_id`);

--
-- Indices de la tabla `archivo`
--
ALTER TABLE `archivo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `archivo_pdf`
--
ALTER TABLE `archivo_pdf`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usuario_id` (`usuario_id`);

--
-- Indices de la tabla `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Indices de la tabla `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `type` (`type`);

--
-- Indices de la tabla `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indices de la tabla `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indices de la tabla `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comisionista_id` (`comisionista_id`),
  ADD KEY `cliente_ibfk_1` (`user_id`);

--
-- Indices de la tabla `codigo_isis`
--
ALTER TABLE `codigo_isis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `marca_id` (`marca_id`),
  ADD KEY `tipo_equipo_id` (`tipo_equipo_id`);

--
-- Indices de la tabla `comisionista`
--
ALTER TABLE `comisionista`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `usuario_id` (`usuario_id`),
  ADD KEY `comisionista_ibfk_1` (`distribuidor_id`);

--
-- Indices de la tabla `comisionista_servicio`
--
ALTER TABLE `comisionista_servicio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comisionista_id` (`comisionista_id`),
  ADD KEY `trabajo_id` (`trabajo_id`),
  ADD KEY `abono_comisionista_id` (`abono_comisionista_id`);

--
-- Indices de la tabla `compra`
--
ALTER TABLE `compra`
  ADD PRIMARY KEY (`id`),
  ADD KEY `proveedor_id` (`proveedor_id`);

--
-- Indices de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `distribuidor`
--
ALTER TABLE `distribuidor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usuario_id` (`usuario_id`);

--
-- Indices de la tabla `distribuidor_departamento`
--
ALTER TABLE `distribuidor_departamento`
  ADD PRIMARY KEY (`id`),
  ADD KEY `distribuidor_id` (`distribuidor_id`);

--
-- Indices de la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usuario_id` (`usuario_id`);

--
-- Indices de la tabla `empleado_comision`
--
ALTER TABLE `empleado_comision`
  ADD PRIMARY KEY (`id`),
  ADD KEY `empleado_id` (`empleado_id`),
  ADD KEY `servicio_id` (`servicio_id`),
  ADD KEY `pago_empleado_id` (`pago_empleado_id`);

--
-- Indices de la tabla `equipo`
--
ALTER TABLE `equipo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `marca_id` (`marca_id`),
  ADD KEY `distribuidor_id` (`distribuidor_id`),
  ADD KEY `cliente_id` (`cliente_id`),
  ADD KEY `tipo_equipo_id` (`tipo_equipo_id`);

--
-- Indices de la tabla `item_compra`
--
ALTER TABLE `item_compra`
  ADD PRIMARY KEY (`id`),
  ADD KEY `compra_id` (`compra_id`),
  ADD KEY `repuesto_id` (`repuesto_id`);

--
-- Indices de la tabla `marca`
--
ALTER TABLE `marca`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usuario_id` (`usuario_id`);

--
-- Indices de la tabla `marca_departamento`
--
ALTER TABLE `marca_departamento`
  ADD PRIMARY KEY (`id`),
  ADD KEY `marca_id` (`marca_id`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`);

--
-- Indices de la tabla `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indices de la tabla `movimientos_caja`
--
ALTER TABLE `movimientos_caja`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usuario_id` (`usuario_id`);

--
-- Indices de la tabla `pago_empleado`
--
ALTER TABLE `pago_empleado`
  ADD PRIMARY KEY (`id`),
  ADD KEY `empleado_id` (`empleado_id`);

--
-- Indices de la tabla `presupuesto`
--
ALTER TABLE `presupuesto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `servicio_id` (`servicio_id`),
  ADD KEY `model_id` (`model_id`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usuario_id` (`usuario_id`);

--
-- Indices de la tabla `proveedor_departamento`
--
ALTER TABLE `proveedor_departamento`
  ADD PRIMARY KEY (`id`),
  ADD KEY `proveedor_id` (`proveedor_id`);

--
-- Indices de la tabla `repuestos`
--
ALTER TABLE `repuestos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `marca_id` (`marca_id`);

--
-- Indices de la tabla `servicio`
--
ALTER TABLE `servicio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente_id` (`cliente_id`),
  ADD KEY `comision_servicio_id` (`comision_servicio_id`),
  ADD KEY `empleado_id` (`empleado_id`);

--
-- Indices de la tabla `servicio_a_equipo`
--
ALTER TABLE `servicio_a_equipo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `equipo_id` (`equipo_id`),
  ADD KEY `servicio_id` (`servicio_id`),
  ADD KEY `codigo_isis_id` (`codigo_isis_id`);

--
-- Indices de la tabla `servicio_estado`
--
ALTER TABLE `servicio_estado`
  ADD PRIMARY KEY (`id`),
  ADD KEY `servicio_id` (`servicio_id`);

--
-- Indices de la tabla `servicio_programado`
--
ALTER TABLE `servicio_programado`
  ADD PRIMARY KEY (`id`),
  ADD KEY `servicio_id` (`servicio_id`),
  ADD KEY `equipo_id` (`equipo_id`),
  ADD KEY `cliente_id` (`cliente_id`);

--
-- Indices de la tabla `tipo_equipo`
--
ALTER TABLE `tipo_equipo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `trabajo`
--
ALTER TABLE `trabajo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `abono_comisionista`
--
ALTER TABLE `abono_comisionista`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `abono_compra`
--
ALTER TABLE `abono_compra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `abono_servicio`
--
ALTER TABLE `abono_servicio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `archivo`
--
ALTER TABLE `archivo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `archivo_pdf`
--
ALTER TABLE `archivo_pdf`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `codigo_isis`
--
ALTER TABLE `codigo_isis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `comisionista`
--
ALTER TABLE `comisionista`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `comisionista_servicio`
--
ALTER TABLE `comisionista_servicio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `compra`
--
ALTER TABLE `compra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `distribuidor`
--
ALTER TABLE `distribuidor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `distribuidor_departamento`
--
ALTER TABLE `distribuidor_departamento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `empleado`
--
ALTER TABLE `empleado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `empleado_comision`
--
ALTER TABLE `empleado_comision`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `equipo`
--
ALTER TABLE `equipo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `item_compra`
--
ALTER TABLE `item_compra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `marca`
--
ALTER TABLE `marca`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `marca_departamento`
--
ALTER TABLE `marca_departamento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT de la tabla `movimientos_caja`
--
ALTER TABLE `movimientos_caja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pago_empleado`
--
ALTER TABLE `pago_empleado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `presupuesto`
--
ALTER TABLE `presupuesto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `proveedor_departamento`
--
ALTER TABLE `proveedor_departamento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `repuestos`
--
ALTER TABLE `repuestos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `servicio`
--
ALTER TABLE `servicio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `servicio_a_equipo`
--
ALTER TABLE `servicio_a_equipo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `servicio_estado`
--
ALTER TABLE `servicio_estado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `servicio_programado`
--
ALTER TABLE `servicio_programado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tipo_equipo`
--
ALTER TABLE `tipo_equipo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `trabajo`
--
ALTER TABLE `trabajo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `abono_comisionista`
--
ALTER TABLE `abono_comisionista`
  ADD CONSTRAINT `abono_comisionista_ibfk_1` FOREIGN KEY (`comisionista_id`) REFERENCES `comisionista` (`id`);

--
-- Filtros para la tabla `abono_compra`
--
ALTER TABLE `abono_compra`
  ADD CONSTRAINT `abono_compra_ibfk_1` FOREIGN KEY (`compra_id`) REFERENCES `compra` (`id`);

--
-- Filtros para la tabla `abono_servicio`
--
ALTER TABLE `abono_servicio`
  ADD CONSTRAINT `abono_servicio_ibfk_1` FOREIGN KEY (`servicio_id`) REFERENCES `servicio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `archivo_pdf`
--
ALTER TABLE `archivo_pdf`
  ADD CONSTRAINT `archivo_pdf_ibfk_1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`);

--
-- Filtros para la tabla `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `cliente_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `cliente_ibfk_2` FOREIGN KEY (`comisionista_id`) REFERENCES `comisionista` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `codigo_isis`
--
ALTER TABLE `codigo_isis`
  ADD CONSTRAINT `codigo_isis_ibfk_1` FOREIGN KEY (`marca_id`) REFERENCES `marca` (`id`),
  ADD CONSTRAINT `codigo_isis_ibfk_2` FOREIGN KEY (`tipo_equipo_id`) REFERENCES `tipo_equipo` (`id`);

--
-- Filtros para la tabla `comisionista`
--
ALTER TABLE `comisionista`
  ADD CONSTRAINT `comisionista_ibfk_1` FOREIGN KEY (`distribuidor_id`) REFERENCES `distribuidor` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `comisionista_ibfk_2` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `comisionista_servicio`
--
ALTER TABLE `comisionista_servicio`
  ADD CONSTRAINT `comisionista_servicio_ibfk_1` FOREIGN KEY (`comisionista_id`) REFERENCES `comisionista` (`id`),
  ADD CONSTRAINT `comisionista_servicio_ibfk_2` FOREIGN KEY (`trabajo_id`) REFERENCES `trabajo` (`id`),
  ADD CONSTRAINT `comisionista_servicio_ibfk_3` FOREIGN KEY (`abono_comisionista_id`) REFERENCES `abono_comisionista` (`id`);

--
-- Filtros para la tabla `compra`
--
ALTER TABLE `compra`
  ADD CONSTRAINT `compra_ibfk_1` FOREIGN KEY (`proveedor_id`) REFERENCES `proveedor` (`id`);

--
-- Filtros para la tabla `distribuidor`
--
ALTER TABLE `distribuidor`
  ADD CONSTRAINT `distribuidor_ibfk_1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`);

--
-- Filtros para la tabla `distribuidor_departamento`
--
ALTER TABLE `distribuidor_departamento`
  ADD CONSTRAINT `distribuidor_departamento_ibfk_1` FOREIGN KEY (`distribuidor_id`) REFERENCES `distribuidor` (`id`);

--
-- Filtros para la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD CONSTRAINT `empleado_ibfk_1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`);

--
-- Filtros para la tabla `empleado_comision`
--
ALTER TABLE `empleado_comision`
  ADD CONSTRAINT `empleado_comision_ibfk_1` FOREIGN KEY (`servicio_id`) REFERENCES `servicio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `empleado_comision_ibfk_2` FOREIGN KEY (`empleado_id`) REFERENCES `empleado` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `empleado_comision_ibfk_3` FOREIGN KEY (`pago_empleado_id`) REFERENCES `pago_empleado` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Filtros para la tabla `equipo`
--
ALTER TABLE `equipo`
  ADD CONSTRAINT `equipo_ibfk_1` FOREIGN KEY (`marca_id`) REFERENCES `marca` (`id`),
  ADD CONSTRAINT `equipo_ibfk_2` FOREIGN KEY (`distribuidor_id`) REFERENCES `distribuidor` (`id`),
  ADD CONSTRAINT `equipo_ibfk_3` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`),
  ADD CONSTRAINT `equipo_ibfk_4` FOREIGN KEY (`tipo_equipo_id`) REFERENCES `tipo_equipo` (`id`);

--
-- Filtros para la tabla `item_compra`
--
ALTER TABLE `item_compra`
  ADD CONSTRAINT `item_compra_ibfk_1` FOREIGN KEY (`compra_id`) REFERENCES `compra` (`id`),
  ADD CONSTRAINT `item_compra_ibfk_2` FOREIGN KEY (`repuesto_id`) REFERENCES `repuestos` (`id`);

--
-- Filtros para la tabla `marca`
--
ALTER TABLE `marca`
  ADD CONSTRAINT `marca_ibfk_1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`);

--
-- Filtros para la tabla `marca_departamento`
--
ALTER TABLE `marca_departamento`
  ADD CONSTRAINT `marca_departamento_ibfk_1` FOREIGN KEY (`marca_id`) REFERENCES `marca` (`id`);

--
-- Filtros para la tabla `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `menu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `movimientos_caja`
--
ALTER TABLE `movimientos_caja`
  ADD CONSTRAINT `movimientos_caja_ibfk_1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`);

--
-- Filtros para la tabla `pago_empleado`
--
ALTER TABLE `pago_empleado`
  ADD CONSTRAINT `pago_empleado_ibfk_1` FOREIGN KEY (`empleado_id`) REFERENCES `empleado` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `presupuesto`
--
ALTER TABLE `presupuesto`
  ADD CONSTRAINT `presupuesto_ibfk_1` FOREIGN KEY (`servicio_id`) REFERENCES `servicio` (`id`);

--
-- Filtros para la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD CONSTRAINT `proveedor_ibfk_1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`);

--
-- Filtros para la tabla `proveedor_departamento`
--
ALTER TABLE `proveedor_departamento`
  ADD CONSTRAINT `proveedor_departamento_ibfk_1` FOREIGN KEY (`proveedor_id`) REFERENCES `proveedor` (`id`);

--
-- Filtros para la tabla `repuestos`
--
ALTER TABLE `repuestos`
  ADD CONSTRAINT `repuestos_ibfk_1` FOREIGN KEY (`marca_id`) REFERENCES `marca` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Filtros para la tabla `servicio`
--
ALTER TABLE `servicio`
  ADD CONSTRAINT `servicio_ibfk_1` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `servicio_ibfk_2` FOREIGN KEY (`comision_servicio_id`) REFERENCES `comisionista_servicio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `servicio_ibfk_3` FOREIGN KEY (`empleado_id`) REFERENCES `empleado` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `servicio_a_equipo`
--
ALTER TABLE `servicio_a_equipo`
  ADD CONSTRAINT `servicio_a_equipo_ibfk_1` FOREIGN KEY (`equipo_id`) REFERENCES `equipo` (`id`),
  ADD CONSTRAINT `servicio_a_equipo_ibfk_2` FOREIGN KEY (`servicio_id`) REFERENCES `servicio` (`id`),
  ADD CONSTRAINT `servicio_a_equipo_ibfk_3` FOREIGN KEY (`codigo_isis_id`) REFERENCES `codigo_isis` (`id`);

--
-- Filtros para la tabla `servicio_estado`
--
ALTER TABLE `servicio_estado`
  ADD CONSTRAINT `servicio_estado_ibfk_1` FOREIGN KEY (`servicio_id`) REFERENCES `servicio` (`id`);

--
-- Filtros para la tabla `servicio_programado`
--
ALTER TABLE `servicio_programado`
  ADD CONSTRAINT `servicio_programado_ibfk_1` FOREIGN KEY (`servicio_id`) REFERENCES `servicio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `servicio_programado_ibfk_2` FOREIGN KEY (`equipo_id`) REFERENCES `equipo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `servicio_programado_ibfk_3` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
