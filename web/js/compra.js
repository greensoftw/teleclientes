/**
 * Created by jhon on 19/11/16.
 */

function addRowItemCompra() {
  var data = $('#add-item-compra').find(':input').serializeArray();
  data.push({name: '_action', value : 'add'});
  $.ajax({
    type: 'POST',
    url: '/teleclientes/web/compra/add-item-compra',
    data: data,
    success: function (data) {
      $('#add-item-compra').html(data);
      agregarAcciones();
    }
  });
}
function delRowItemCompra(id) {
  $('#add-item-compra').find('tr[data-key=' + id + ']').remove();
}
function agregarAcciones() {
  var container = $('#add-item-compra');
  var inputs = container.find('[id*="-precio"]');
  var input_total = $('#compra-total');
  $.each(inputs, function (index, obj) {
    $(obj).keyup(function () {
      capturarTotal();
    });
    $(obj).change(function () {
      capturarTotal();
    })
  });

  function capturarTotal() {
    var total = 0;
    inputs.each(function (ind, el) {
      if ($(el).val() != "")
        total += parseInt($(el).val())
    });
    input_total.val(total);
  }
}