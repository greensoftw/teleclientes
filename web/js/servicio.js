/**
 * Created by jhon on 19/11/16.
 */

function addRowPresupuesto() {
  var data = $('#add-presupuesto').find(':input').serializeArray();
  var cliente_id = $('#servicio-cliente_id').val();
  data.push({name: '_action', value: 'add'});
  $.ajax({
    type: 'POST',
    url: baseUrl + '/servicio/add-presupuesto?cliente_id=' + cliente_id,
    data: data,
    success: function (data) {
      $('#add-presupuesto').html(data);
      agregarAcciones();
    }
  });
}
function delRowPresupuesto(id) {
  $('#add-presupuesto').find('tr[data-key=' + id + ']').remove();
}

function agregarAcciones() {
  var container = $('#add-presupuesto');
  var inputs = container.find('[id*="-precio"]');
  var input_total = $('#servicio-total');

  $.each(inputs, function (index, obj) {
    $(obj).keyup(function () {
      capturarTotal();
    })
  });

  function capturarTotal() {
    var total = 0;
    inputs.each(function (ind, el) {
      if ($(el).val() != "")
        total += parseInt($(el).val())
    });
    input_total.val(total);
  }

  var input_selects = container.find('select[name*="Presupuesto"]');
  $.each(input_selects, function (index, obj) {
    var el = $(obj);
    el.change(function () {
      var selected = el.find(':selected');
      var label = selected.parent().attr('label');
      if (label == 'Repuestos') {
        el.parent().find('input[name*="model"]').val("app\\models\\Repuestos");
      } else {
        el.parent().find('input[name*="model"]').val("app\\models\\Trabajo")
      }
      $.get(baseUrl+"/servicio/precio",{model:label,id:el.val()}).success(function(data){
        console.log(el.parent().find('input[name*="precio"]'));
        el.parent().parent().parent().find('input[name*="precio"]').val(data.precio);
      });
      el.parent().find('input[name*="model_id"]').val(el.val());
    });
    setTimeout(setSelect2Values(),1000);
  })
}

function addRowServicioAEquipo() {
  var data = $('#add-servicio-aequipo').find(':input').serializeArray();
  var cliente_id = $('#servicio-cliente_id').val();
  data.push({name: '_action', value: 'add'});
  $.ajax({
    type: 'POST',
    url: baseUrl + '/servicio/add-servicio-aequipo?cliente_id=' + cliente_id,
    data: data,
    success: function (data) {
      $('#add-servicio-aequipo').html(data);
      setAddEquipos();
    }
  });
}
function setAddEquipos() {
  var dom_link = $('#add_equipo');
  var dom_select_cliente = $('#servicio-cliente_id');
  if(dom_select_cliente.val() != ''){
    $('#complementos-servicio').show();
  }

  var isNewRecord = ($("#servicio-id").val() == '');
  console.log(isNewRecord);

  dom_select_cliente.change(function () {
    if(dom_select_cliente.val() != ''){
      $('#complementos-servicio').show();
      if (isNewRecord){
        addRowServicioAEquipo();
      }
    }else{
      $('#complementos-servicio').hide();
    }
  });
  var previus_link = dom_link.attr('href');
  dom_link.click(function () {
    if (dom_select_cliente.val() != "") {
      dom_link.attr('href', previus_link + dom_select_cliente.val());
      return true;
    }
    return false;
  })
}
$(document).ready(function () {
  setAddEquipos();
});



function setSelect2Values() {
  var input_ids = $('input[name*="model_id"]');
  $.each(input_ids, function (index, object) {
    var el = $(object);
    var model = el.parent().find('input[value*="model"]').val();
    var select = null;
    console.log(model, model == 'app\\models\\Trabajo', model === 'app\\models\\Repuestos');
    if (model == 'app\\models\\Trabajo') {
      select = el.parent().find('select');

      select.find('optgroup[label="Trabajos"]')
        .find('option[value="' + el.val() + '"]')
        .attr('selected', true);

      console.log(select.find('optgroup[label="Trabajos"]')
        .find('option[value="' + el.val() + '"]'))

    } else if (model == 'app\\models\\Repuestos') {
      select = el.parent().find('select');

      select.find('optgroup[label="Repuestos"]')
        .find('option[value="' + el.val() + '"]')
        .attr('selected', true);

      console.log(select.find('optgroup[label="Repuestos"]')
        .find('option[value="' + el.val() + '"]'))
    }
    var options = {
      "allowClear": true,
      "theme": "krajee",
      "width": "100%",
      "placeholder": "Seleccione Producto o Servicio",
      "language": "es"
    };
    if(select != null){
      select.select2("destroy");
      select.select2(options);
    }

  })

}