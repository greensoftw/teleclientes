function setAgregarPresupuesto() {
  $('body').on('submit', 'form.agregar-presupuesto', function () {
    var form = $(this);
    console.log(form);
    if (form.find('.has-error').length)
    {
      return false;
    }
    // submit form
    $.ajax({
      url    : form.attr('action'),
      type   : 'post',
      data   : form.serialize(),
      success: function (response)
      {
        form.parents('.tab-pane').html(response);
        setTimeout(function () {
          setAgregarPresupuesto();
        })
      },
      error  : function ()
      {
        console.log('internal server error');
      }
    });
    return false;
  });
  var options = {
    "allowClear": true,
    "theme": "krajee",
    "width": "100%",
    "placeholder": "Seleccione Producto o Servicio",
    "language": "es"
  };
  $('.select-model_id').each(function (index, object) {
    $(object).select2(options);
  });
  var input_selects = $('select[name*="Presupuesto"]');
  $.each(input_selects, function (index, obj) {
    var el = $(obj);
    el.change(function () {
      var selected = el.find(':selected');
      var label = selected.parent().attr('label');
      if (label == 'Repuestos') {
        el.parent().parent().find('input[name*="model"]').val("app\\models\\Repuestos");
      } else {
        el.parent().parent().find('input[name*="model"]').val("app\\models\\Trabajo")
      }
      $.get(baseUrl+"/servicio/precio",{model:label,id:el.val()}).success(function(data){
        el.parent().parent().find('input[name*="precio"]').val(data.precio)
      });
      el.parent().parent().find('input[name*="model_id"]').val(el.val());
    });
  })
}
$(document).ready(function () {
  setAgregarPresupuesto();
});