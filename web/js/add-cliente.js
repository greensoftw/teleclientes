/**
 * Created by jhon on 12/12/16.
 */

function addRowEquipo() {
  var data = $('#add-equipo').find(':input').serializeArray();
  data.push({name: '_action', value : 'add'});
  $.ajax({
    type: 'POST',
    url: baseUrl+'/cliente/add-equipo',
    data: data,
    success: function (response) {
      $('#add-equipo').html(response);
      setSelect2Values(data);
    }
  });
}
function delRowEquipo(id) {
  $('#add-equipo').find('tr[data-key=' + id + ']').remove();
}
function setSelect2Values(data) {
  console.log(data);
  var options = {
    "allowClear": true,
    "theme": "krajee",
    "width": "100%",
    "placeholder": "Seleccione un distribuidor",
    "language": "es"
  };
  $('#add-equipo').find('select').each(function (index,obj) {
    var select = $(obj);
    var parts = select.attr('id').split('-');

    if(select.attr('id').contains(parts[2])){
      options.placeholder = select.attr('placeholder');
      $.each(data,function (ix, object) {
        if(object.name.contains("["+parts[1]+"]") && object.name.contains(parts[2])){
          select.val(object.value);
        }
      })
    }
    select.select2("destroy");
    select.select2(options);
  })
}

if (typeof String.prototype.contains === 'undefined')
{ String.prototype.contains = function(it) { return this.indexOf(it) != -1; }; }