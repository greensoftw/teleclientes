(function($) {
	'use strict';
	$.vegas('slideshow', {
		delay:8000,
		backgrounds:[
		{ src:'img/vegas/slide-bg01.jpg', fade:4000 },
		{ src:'img/vegas/slide-bg02.jpg', fade:4000 },
		{ src:'img/vegas/slide-bg03.jpg', fade:4000 }
		]
	})('overlay', {
		src:'img/bg-overlay.png'
	});
})(jQuery);
