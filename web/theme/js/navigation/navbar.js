$(document).ready(function () {
  $.fn.scrollTo = function (options) {
    var settings = {offset: -89, speed: 'slow', override: null, easing: null};
    if (options) {
      if (options.override) {
        options.override = (override('#') != -1) ? options.override : '#' + options.override
      }
      $.extend(settings, options)
    }
    return this.each(function (i, el) {
      $(el).click(function (e) {
        var idToLookAt;
        if ($(el).attr('href').match(/#/) !== null) {
          e.preventDefault();
          idToLookAt = (settings.override) ? settings.override : $(el).attr('data-id');
          if (history.pushState) {
            history.pushState(null, null, idToLookAt);
            $('html,body').stop().animate({scrollTop: $(idToLookAt).offset().top + settings.offset}, settings.speed, settings.easing)
          } else {
            $('html,body').stop().animate({scrollTop: $(idToLookAt).offset().top + settings.offset}, settings.speed, settings.easing, function (e) {
              window.location.hash = idToLookAt
            })
          }
        }
      })
    })
  };
  $('#GoToHome, #GoToDescription, #GoToAbout, #GoToFeatures, #GoToBlog, #GoToPricing, #GoToContact')
      .scrollTo({speed: 1400});
  var headerWrapper = parseInt($('.navbar').height());
  var offsetTolerance = 89;
  $(window).scroll(function () {
    var scrollPosition = parseInt($(this).scrollTop());
    $('.navbar-nav li a').each(function () {
      var thisHref = $(this).attr('href');
      var thisID = $(this).attr('data-id');
      if ($(thisID).length){
        var thisTruePosition = parseInt($(thisID).offset().top);
        var thisPosition = thisTruePosition - headerWrapper - offsetTolerance;
        if (scrollPosition >= thisPosition) {
          $('.selected').removeClass('selected');
          $('.navbar-nav li a[href="' + thisHref + '"]').addClass('selected')
        }
      }
    });
    var bottomPage = parseInt($(document).height()) - parseInt($(window).height());
    if (scrollPosition == bottomPage || scrollPosition >= bottomPage) {
      $('.selected').removeClass('selected');
      $('.navbar-nav li a:last').addClass('selected')
    }
  })
});