<?php
/* @var $this yii\web\View */
/* @var $config \app\models\Empresa*/
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
$this->title = 'Configuracion de datos'
?>
<div class="col-md-12 col-lg-12 col-sm-12">
  <div class="box-header">
    <h3 class="box-title">Configuraciones de la empresa</h3>
  </div>
  <div class="box-body">
    <div class="col-lg-4 col-md-4 col-sm-4 col-lg-offset-4 col-md-offset-4 col-sm-offset-4">
      <?php if($config->picture !=null && $config != null): ?>
        <?= Html::img('@web/'.$config->picture,['width'=>'150px']) ?>
      <?php endif; ?>
    </div>

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="col-lg-6 col-md-6 col-sm-6">
      <?= $form->field($config, 'nombre')->textInput(['maxlength' => true]) ?>
      <?= $form->field($config, 'nit')->textInput(['maxlength' => true]) ?>
      <?= $form->field($config, 'tel_fijo')->textInput(['maxlength' => true]) ?>
      <?= $form->field($config, 'tel_cel')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6">
      <?= $form->field($config, 'direccion')->textInput(['maxlength' => true]) ?>
      <?= $form->field($config, 'email')->textInput(['maxlength' => true]) ?>
      <?= $form->field($config, 'web')->textInput(['maxlength' => true]) ?>
      <?= $form->field($config, 'picture')->fileInput(); ?>
    </div>
    <div class="col-md-12 col-lg-12 form-group">
      <?= Html::submitButton($config->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $config->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>

    </div>
    <?php ActiveForm::end(); ?>
  </div>
</div>