<?php
/* @var $this yii\web\View */
use sintret\chat\ChatRoom;

?>

<?php
echo ChatRoom::widget([
    'url' => \yii\helpers\Url::to(['/chat/send-chat']),
  ]
);
?>