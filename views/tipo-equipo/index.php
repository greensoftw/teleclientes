<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\TipoEquipoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = 'Tipos de Equipos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipo-equipo-index row">
  <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

  <p>
    <?= Html::a('Crear Tipo Equipo', ['create'], ['class' => 'btn btn-success']) ?>
  </p>
  <?php
  $gridColumn = [
    ['class' => 'yii\grid\SerialColumn'],
    ['attribute' => 'id', 'visible' => false],
    'nombre',
    [
      'class' => 'kartik\grid\ActionColumn',
      'template' => "{update} {delete}"
    ],
  ];
  ?>
  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $gridColumn,
    'pjax' => true,
    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-tipo-equipo']],
    'panel' => [
      'type' => GridView::TYPE_PRIMARY,
      'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
    ],
    'export' => false,
    // your toolbar can include the additional full export menu
    'toolbar' => [
      '{export}',
      ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumn,
        'target' => ExportMenu::TARGET_BLANK,
        'fontAwesome' => true,
        'dropdownOptions' => [
          'label' => 'Todo',
          'class' => 'btn btn-default',
          'itemsBefore' => [
            '<li class="dropdown-header">Exportar todo el contenido</li>',
          ],
        ],
        'exportConfig' => [
          ExportMenu::FORMAT_PDF => false
        ]
      ]),
    ],
  ]); ?>

</div>
