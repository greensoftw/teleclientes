<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TipoEquipo */

$this->title = 'Actualizar Tipo Equipo: ' . ' ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Tipos de Equipos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => "Tipo de equipo : $model->nombre", 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="tipo-equipo-update row">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
