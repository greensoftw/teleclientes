<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\TipoEquipo */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tipo Equipos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipo-equipo-view">

    <div class="row">
        <div class="col-sm-9">
            <h3><?= 'Tipo Equipo'.': '. Html::encode($this->title) ?></h3>
        </div>
    </div>

    <div>
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'nombre',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerCodigoIsis->totalCount){
    $gridColumnCodigoIsis = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'marca.id',
                'label' => 'Marca'
            ],
                'codigo',
        'descripcion',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCodigoIsis,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Código de Error'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnCodigoIsis
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerEquipo->totalCount){
    $gridColumnEquipo = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'cliente.id',
                'label' => 'Cliente'
            ],
        [
                'attribute' => 'marca.id',
                'label' => 'Marca'
            ],
        [
                'attribute' => 'distribuidor.id',
                'label' => 'Distribuidor'
            ],
                'modelo',
        'serial',
        'nota',
        'tiempo_mantenimiento',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerEquipo,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Equipo'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnEquipo
    ]);
}
?>
    </div>
</div>
