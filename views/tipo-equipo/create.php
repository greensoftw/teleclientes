<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TipoEquipo */

$this->title = 'Crear Tipo Equipo';
$this->params['breadcrumbs'][] = ['label' => 'Tipo Equipo', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipo-equipo-create row">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
