<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TipoEquipo */
/* @var $providerCodigoIsis */
/* @var $providerEquipo */
$this->title = "Tipo de equipo : $model->nombre";
$this->params['breadcrumbs'][] = ['label' => 'Tipos de Equipos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipo-equipo-view row">

  <div>
    <div class="form-group">

      <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
          'confirm' => 'Seguro desea eliminar este elemento?',
          'method' => 'post',
        ],
      ])
      ?>
    </div>
  </div>

  <div>
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      'nombre',
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>

  <div>
    <?php
    if ($providerCodigoIsis->totalCount) {
      $gridColumnCodigoIsis = [
        ['attribute' => 'id', 'visible' => false],
        [
          'attribute' => 'marca.id',
          'label' => 'Marca'
        ],
        'codigo',
        'descripcion',
      ];
      echo GridView::widget([
        'dataProvider' => $providerCodigoIsis,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-codigo-isis']],
        'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Código de Error'),
        ],
        'export' => false,
        'columns' => $gridColumnCodigoIsis
      ]);
    }
    ?>
  </div>

  <div>
    <?php
    if ($providerEquipo->totalCount) {
      $gridColumnEquipo = [
        ['attribute' => 'id', 'visible' => false],
        [
          'attribute' => 'cliente.id',
          'label' => 'Cliente'
        ],
        [
          'attribute' => 'marca.id',
          'label' => 'Marca'
        ],
        [
          'attribute' => 'distribuidor.id',
          'label' => 'Distribuidor'
        ],
        'modelo',
        'serial',
        'nota',
        'tiempo_mantenimiento',
      ];
      echo GridView::widget([
        'dataProvider' => $providerEquipo,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-equipo']],
        'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Equipo'),
        ],
        'export' => false,
        'columns' => $gridColumnEquipo
      ]);
    }
    ?>
  </div>
</div>
