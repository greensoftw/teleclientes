<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;

/**
 * @var $model \app\models\Servicio
 */
$dataProvider = new ArrayDataProvider([
  'allModels' => $model->abonoServicios,
  'key' => 'id'
]);
$gridColumns = [
  ['class' => 'yii\grid\SerialColumn'],
  ['attribute' => 'id', 'visible' => false],
  'monto:currency',
  'fecha:datetime'
];
?>
<div class="col-lg-12">
  <h3>Abonos registrados</h3>
  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns,
    'containerOptions' => ['style' => 'overflow: auto'],
    'pjax' => true,
    'beforeHeader' => [
      [
        'options' => ['class' => 'skip-export']
      ]
    ],
    'export' => [
      'fontAwesome' => true
    ],
    'bordered' => true,
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'hover' => true,
    'showPageSummary' => false,
    'persistResize' => false,
  ]); ?>
</div>