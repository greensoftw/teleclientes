<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Servicio */

?>
<div class="servicio-view">

  <div class="row">
    <div class="col-sm-9">
      <h2><?= Html::encode("Orden de servicio N°: $model->numero") ?></h2>
    </div>
  </div>

  <div class="row">
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      [
        'attribute' => 'empleado.usuario.nombres',
        'label' => 'Empleado',
      ],
      [
        'attribute' => 'estadoActual.estado',
        'label' => 'Estado actual'
      ],
      [
        'value' => Yii::$app->formatter->asDatetime($model->ingreso->fecha),
        'label' => 'Ingresado'
      ],
      'fecha_tecnico:datetime',
      ['attribute' => 'cliente.direccion',
        'label' => 'Dirección del cliente'
      ],
      [
        'attribute' => 'cliente.telefonos',
        'label' => 'Télefonos del cliente'
      ],
      [
        'attribute' => 'cliente.usuario.email',
        'label' => 'Correo del cliente'
      ],
      'total:currency',
      $model->pagado ?
        'pagado:boolean' :
        [
          'label' => 'Abonado',
          'value' => $model->getAbonado(),
          'format' => 'currency'
        ],
      $model->pagado ? ['attribute' => 'pagado', 'visible' => false] :
        [
          'label' => 'Saldo en deuda',
          'value' => $model->getEnDeuda(),
          'format' => 'currency'
        ],
      'no_facturado',
      'cerrado:boolean',
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>
</div>