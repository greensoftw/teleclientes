<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ServicioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = 'Servicios';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="servicio-index row">
  <?php
  $gridColumn = [
    ['class' => 'yii\grid\SerialColumn'],
    [
      'class' => 'kartik\grid\ExpandRowColumn',
      'width' => '50px',
      'value' => function ($model, $key, $index, $column) {
        return GridView::ROW_COLLAPSED;
      },
      'detail' => function ($model, $key, $index, $column) {
        return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
      },
      'headerOptions' => ['class' => 'kartik-sheet-style'],
      'expandOneOnly' => false
    ],
    ['attribute' => 'numero'],
    [
      'attribute' => 'ingreso.fecha',
      'format' => 'date',
      'label' => 'fecha'
    ],
    [
      'attribute' => 'servicioAEquipo.equipo',
      'label' => 'equipo',
      'format' => 'raw'
    ],
    [
      'attribute' => 'marca.nombre',
      'label' => 'garantia'
    ],
    [
      'value' => 'estadoActual',
      'label' => 'Estado actual - Fecha'
    ],
    'total:currency',
    'no_facturado',
  ];
  ?>
  <div class="col-lg-12 row">
    <div class="col-lg-6 row">
      <h2><strong>Cliente : </strong> <?= Yii::$app->user->identity->nombres ?></h2>
    </div>
    <div class="col-lg-6">
      <?= Html::a('PDF',['pdf'],['class'=>'btn btn-lg btn-danger pull-right']) ?>
    </div>
    <div class="col-lg-12 row">
      <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-servicio']],
        'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'toolbar' => false
      ]); ?>
    </div>

  </div>
</div>
<?php $this->registerJsFile('@web/js/view-servicio.js', [
  'position' => $this::POS_END,
  'depends' => [\yii\web\JqueryAsset::className()]
]) ?>

