<?php
/**
 * Created by PhpStorm.
 * User: jhon
 * Date: 12/01/17
 * Time: 12:09 AM
 * @var $this \yii\web\View
 * @var $registro \app\models\SignupForm
 * @var $empresa \app\models\EmpresaTelecliente
 */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
$this->title = 'Registrate en Teleclientes.com';
?>
<section id="home" class="home-section default-bg color2">
    <div class="container">
        <div class="row">
            <div class="col-md-6 marginbot40">
                <img src="<?= \yii\helpers\Url::to("@web/theme/img/slider/mobile-phone.png") ?>" class="img-responsive"
                     alt=""/>
            </div>
            <div class="col-md-6">
                <div class="headline">
                    <h4>Teleclientes - Registrate</h4>
                </div>
                <?php $form = ActiveForm::begin() ?>
                <?= $form->field($registro, 'username', ['labelOptions' => ['class' => 'label-white'],
                    'inputOptions' => ['class' => 'form-control input-lg', 'placeholder' => 'Nombre de usuario']])
                    ->hint('Nombre de usuario sin espacios, par ingresar') ?>
                <?= $form->field($registro, 'nombres', ['labelOptions' => ['class' => 'label-white'],
                    'inputOptions' => ['class' => 'form-control input-lg', 'placeholder' => 'Nombre completo']])
                    ->hint('Por favor indique su nombre') ?>
                <?= $form->field($registro, 'email', ['labelOptions' => ['class' => 'label-white'],
                    'inputOptions' => ['class' => 'form-control input-lg', 'placeholder' => 'Email']]) ?>
                <?= $form->field($registro, 'password', ['labelOptions' => ['class' => 'label-white'],
                    'inputOptions' => ['class' => 'form-control input-lg', 'placeholder' => 'Contraseña']])
                    ->passwordInput() ?>
                <?= $form->field($empresa, 'nombre', ['labelOptions' => ['class' => 'label-white'],
                    'inputOptions' => ['class' => 'form-control input-lg', 'placeholder' => 'Nombre de su empresa']])
                    ->hint('indique el nombre de su empresa') ?>
                <div class="form-group">
                    <?= Html::submitButton('Enviar', ['class' => 'btn btn-default btn-lg']) ?>
                </div>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</section>