<?php
/**
 * User: jhon
 * Date: 10/01/17
 * Time: 05:29 PM
 * @var $this \yii\web\View
 */
use yii\helpers\Html;

$this->title = "Bienvenido a teleclientes";
?>
<section id="home" class="home-section default-bg color2">
  <div class="container">
    <div class="row">
      <div class="col-md-6 marginbot40">
        <img src="<?= \yii\helpers\Url::to("@web/theme/img/slider/mobile-phone.png") ?>" class="img-responsive" alt=""/>
      </div>
      <div class="col-md-6 home-container">
        <div class="headline">
          <h1>Teleclientes.com</h1>
          <p>El software moderno para la administración de ASC, CSA, CAS</p>
        </div>
        <p>
          Teleclientes es un software para centros de servicios de toda indole,
          refrigeración, audio-video, talleres de reparacion y mantenimiento etc.
        </p>
        <p>
          Programe mantenimientos y servicios a sus clientes en fechas especificas,
          le damos dos aplicativos diferentes para ingresos de servicios y ejecución de los mismos
          para sus mecanicos.
        </p>
        <p>
          pruebelo y nunca perdera sus cliente muy facil su manejo.
        </p>
      </div>
    </div>
  </div>
</section>
<div id="intro" class="dark-section">
  <div class="container">
    <div class="row text-center">
      <div class="col-md-10 col-md-offset-1">
        <h4 class="headtitle">
          Lo ideal para la administracion de ASC, CSA, CAS
        </h4>
        <p>Teleclientes.com</p>
        <?= Html::a('Pidelo ahora', ['sitio/registrar'], ['class' => 'btn btn-primary btn-lg']) ?>
      </div>
    </div>
  </div>
</div>
<!-- End intro -->

<!-- Start description -->
<section id="description" class="half-wrapper">
  <div class="container half-full">
    <div class="row">
      <div class="col-md-6 half-column set-left">
        <div class="half-col-containt">
          <div class="heading">
            <h5>Descripcion</h5>
            <p>
              Teleclientes es un software ideal para la administracion de su centro de servicio
            </p>
            <span></span>
          </div>
          <p>
            Ingrese on-line las 24 horas del dia, lleve un mejor control de su centro de servicio,
            administre todo lo relacionado con su negocio de forma agil y practica a muy bajo precio.
          </p>
          <ul class="check-list">
            <li><i class="fa fa-check"></i> Acceso online las 24 horas.</li>
            <li><i class="fa fa-check"></i> Generacion de archivos PDF.</li>
            <li><i class="fa fa-check"></i> Acceso para sus clientes.</li>
            <li><i class="fa fa-check"></i> Envio de estadisticas a Marcas, Almacenes y demas para quien trabajas.</li>
            <li><i class="fa fa-check"></i> De una mejor imagen de su empresa.</li>
          </ul>
          <p class="btn-horizontal">
            <?= Html::a('Ingresar', ['/sitio/login'], ['class' => 'btn btn-primary']) ?>
          </p>
        </div>
      </div>
      <div class="col-md-6 half-column set-right">
        <div class="fluid">
          <img src="<?= \yii\helpers\Url::to("@web/theme/img/img-app01.png") ?>" class="img-responsive" alt=""/>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="features" class="half-wrapper">
  <div class="container half-full">
    <div class="row">
      <div class="col-md-6 half-column set-left">
        <div class="fluid">
          <div class="imac-wrapper">
            <div class="imac-slider">
              <div class="imac-device">
                <ul class="slides">
                  <li><img src="<?= \yii\helpers\Url::to("@web/theme/img/imac-slide/slide1.png") ?>"
                           class="img-responsive" alt=""/></li>
                  <li><img src="<?= \yii\helpers\Url::to("@web/theme/img/imac-slide/slide2.png") ?>"
                           class="img-responsive" alt=""/></li>
                  <li><img src="<?= \yii\helpers\Url::to("@web/theme/img/imac-slide/slide3.png") ?>"
                           class="img-responsive" alt=""/></li>
                </ul>
              </div>
            </div>
            <img src="<?= \yii\helpers\Url::to("@web/theme/img/imac-slider.png") ?>" class="img-responsive" alt=""/>
          </div>
        </div>
      </div>
      <div class="col-md-6 half-column set-right">
        <div class="half-col-containt">
          <div class="heading">
            <h5>Caracteristicas</h5>
            <p>
              Clave de características, esta es una de las razones por que elegir teleclientes
            </p>
            <span></span>
          </div>
          <ul class="feature-list">
            <li>
              <span class="icon-screen-desktop icon-2x icon-color icon-left"></span>
              <h5>Full responsive</h5>
              <p>
                Teleclientes funciona en todos los tamaños de pantalla
              </p>
            </li>
            <li>
              <span class="icon-wrench icon-2x icon-color icon-left"></span>
              <h5>Facil manejo</h5>
              <p>
                Su facil manejo hara que en par de dias ya estes usando el sistema perfectamente
              </p>
            </li>
            <li>
              <span class="icon-chemistry icon-2x icon-color icon-left"></span>
              <h5>Simple y Limpio</h5>
              <p>
                Es simple y accesible para cualquier operario.
              </p>
            </li>
            <li>
              <span class="icon-envelope-letter icon-2x icon-color icon-left"></span>
              <h5>Envio de correos</h5>
              <p>
                Comparte tu trabajo con las marcas y comerciantes de los equipos para quienes trabajas
              </p>
            </li>
            <li>
              <span class="icon-notebook icon-2x icon-color icon-left"></span>
              <h5>Documentacion</h5>
              <p>
                Teleclientes ofrese una documentación y tutoriales para que aprendas a usarlo rapidamente
              </p>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="pricing" class="wrapper">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="heading centered">
          <h5>Precios</h5>
          <p>
            Selecciona el paquete que mas te convenga
          </p>
          <span></span>
        </div>
      </div>
      <div class="col-md-4">
        <div class="pricing-wrapper">
          <div class="pricing-head">
            <span class="pricing-price">Demo</span>
            <h4>Prueba 60 dias</h4>
          </div>
          <div class="pricing-bottom">
            <?= Html::a('Probar', ['sitio/registrar'], ['class' => 'btn btn-primary']) ?>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="pricing-wrapper">
          <div class="pricing-head">
            <sup>$</sup>
            <span class="pricing-price">60 Mil</span>
            <h4>Mensual</h4>
          </div>
          <div class="pricing-bottom">
            <?= Html::a('Probar', ['sitio/comprar', 'lap' => 30], ['class' => 'btn btn-primary']) ?>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="pricing-wrapper">
          <div class="pricing-head">
            <sup>$</sup>
            <span class="pricing-price">600 mil</span>
            <h4>Pago unico anual</h4>
          </div>
          <div class="pricing-bottom">
            <?= Html::a('Probar', ['sitio/comprar', 'lap' => 12], ['class' => 'btn btn-primary']) ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="contact" class="half-wrapper">
  <div class="container half-full">
    <div class="row">
      <div class="col-md-6 half-column set-left">
        <div class="half-col-containt">
          <div class="heading">
            <h5>Contactanos</h5>
            <p>
              Si desea saber mas, no dude en preguntarnos.
            </p>
            <span></span>
          </div>
          <form id="contactform" action="<?= \yii\helpers\Url::to(['sitio/contacto']) ?>" method="post"
                class="validateform" name="leaveContact">

            <ul class="listForm">
              <li>
                <label>Name :</label>
                <input class="form-control input-name input-lg" type="text" name="name" data-rule="maxlen:4"
                       data-msg="Espacio requerido" placeholder="Por favor ingrese su nombre."/>
                <div class="validation"></div>
              </li>
              <li>
                <label>Email :</label>
                <input class="form-control input-email input-lg" type="text" name="email" data-rule="email"
                       data-msg="Por favor ingrese un correo para ponernos en contacto con usted"
                       placeholder="Por favor ingrese un correo para ponernos en contacto con usted"/>
                <div class="validation"></div>
              </li>
              <li>
                <label>Mensaje :</label>
                <textarea class="form-control input-lg" rows="8" name="message" data-rule="required"
                          data-msg="Por favor redacte su pregunta duda o inquietud"
                          placeholder="Por favor redacte su pregunta duda o inquietud ..."></textarea>
                <div class="validation"></div>
              </li>
              <li>
                <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary']) ?>
              </li>
            </ul>
          </form>
        </div>
      </div>
      <div class="col-md-6 half-column set-right">
        <div class="fluid">
          <img src="<?= \yii\helpers\Url::to("@web/theme/img/img-app02.png") ?>" class="img-responsive" alt=""/>
        </div>
      </div>
    </div>
  </div>
</section>