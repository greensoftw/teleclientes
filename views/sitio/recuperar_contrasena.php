<?php
/**
 * User: jhon
 * Date: 12/01/17
 * Time: 12:09 AM
 * @var $this \yii\web\View
 * @var $model \app\models\Newpass
 */
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Html;

$this->title = 'Recordar contraseña - Teleclientes.com';
?>
<section id="home" class="home-section default-bg color2">
    <div class="container">
        <div class="row">
            <div class="col-md-6 marginbot40">
                <img src="<?= \yii\helpers\Url::to("@web/theme/img/slider/mobile-phone.png") ?>" class="img-responsive"
                     alt=""/>
            </div>
            <div class="col-md-6">
                <div class="headline">
                    <h5>Teleclientes - Recordar contraseña</h5>
                </div>
                <?php $form = ActiveForm::begin() ?>
                <?= $form->field($model, 'password', ['labelOptions' => ['class' => 'label-white'],
                    'inputOptions' => ['class' => 'form-control input-lg', 'placeholder' => 'Ingrese su nueva contraseña']]) ?>
                <?= $form->field($model, 'password2', ['labelOptions' => ['class' => 'label-white'],
                    'inputOptions' => ['class' => 'form-control input-lg', 'placeholder' => 'Correo de usuario']]) ?>
                <div class="form-group">
                    <?= Html::submitButton('Confirmar', ['class' => 'btn btn-default btn-lg']) ?>
                </div>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</section>