<?php
/**
 * User: jhon
 * Date: 12/01/17
 * Time: 12:09 AM
 * @var $this \yii\web\View
 * @var $model \app\models\UserRepass
 * @var $empresas \app\models\EmpresaTelecliente[]
 */
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Html;

$this->title = 'Recordar contraseña - Teleclientes.com';
?>
<section id="home" class="home-section default-bg color2">
    <div class="container">
        <div class="row">
            <div class="col-md-6 marginbot40">
                <img src="<?= \yii\helpers\Url::to("@web/theme/img/slider/mobile-phone.png") ?>" class="img-responsive"
                     alt=""/>
            </div>
            <div class="col-md-6">
                <div class="headline">
                    <h5>Teleclientes - Recordar contraseña</h5>
                </div>
                <?php $form = ActiveForm::begin() ?>
                <?= $form->field($model, 'empresa_id', ['labelOptions' => ['class' => 'label-white'],
                    'inputOptions' => ['class' => 'form-control input-lg', 'placeholder' => 'Seleccione su empresa']])
                    ->dropDownList(\yii\helpers\ArrayHelper::map($empresas, 'id', 'nombre'), ['prompt' => 'Seleccione']) ?>
                <?= $form->field($model, 'correo', ['labelOptions' => ['class' => 'label-white'],
                    'inputOptions' => ['class' => 'form-control input-lg', 'placeholder' => 'Correo de usuario']]) ?>
                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'captchaAction' => ['sitio/captcha'],
                    'template' => '
                                <div class="row">
                                    <div class="col-lg-6">
                                    {image}<br>
                                    <small>Click en la imagen para cambiar</small>
                                    </div>
                                    <div class="col-lg-6">{input}</div>
                                </div>',
                    'options'=>['class'=>'form-control input-lg','placeholder'=>'Ingrese el contenido de la imagen'],
                ])->label(false)->hint('') ?>
                <div class="form-group">
                    <?= Html::submitButton('Enviar solicitud', ['class' => 'btn btn-default btn-lg']) ?>
                </div>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</section>