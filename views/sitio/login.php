<?php
/**
 * Created by PhpStorm.
 * User: jhon
 * Date: 12/01/17
 * Time: 12:09 AM
 * @var $this \yii\web\View
 * @var $model \app\models\LoginForm
 * @var $empresas \app\models\EmpresaTelecliente[]
 */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Ingresar - Teleclientes.com';
?>
<section id="home" class="home-section default-bg color2">
  <div class="container">
    <div class="row">
      <div class="col-md-6 marginbot40">
        <img src="<?= \yii\helpers\Url::to("@web/theme/img/slider/mobile-phone.png") ?>" class="img-responsive"
             alt=""/>
      </div>
      <div class="col-md-6">
        <div class="headline">
          <h4>Teleclientes - Ingresa</h4>
        </div>
        <?php $form = ActiveForm::begin() ?>
        <?= $form->field($model, 'empresa_id', ['labelOptions' => ['class' => 'label-white'],
          'inputOptions' => ['class' => 'form-control input-lg', 'placeholder' => 'Seleccione su empresa']])
          ->dropDownList(\yii\helpers\ArrayHelper::map($empresas, 'id', 'nombre'), ['prompt' => 'Seleccione'])
          ->hint('Nombre de usuario sin espacios, par ingresar') ?>
        <?= $form->field($model, 'username', ['labelOptions' => ['class' => 'label-white'],
          'inputOptions' => ['class' => 'form-control input-lg', 'placeholder' => 'Nombre de usuario']]) ?>
        <?= $form->field($model, 'password', ['labelOptions' => ['class' => 'label-white'],
          'inputOptions' => ['class' => 'form-control input-lg', 'placeholder' => 'contraseña']])->passwordInput() ?>
        <?= $form->field($model, 'rememberMe', ['labelOptions' => ['class' => 'label-white']])->checkbox() ?>

        <div class="form-group row">
          <div class="col-lg-4">
            <?= Html::submitButton('Enviar', ['class' => 'btn btn-default btn-xs btn-block']) ?>
          </div>
          <div class="col-lg-8">
            <?= Html::a('Olvide mi contraseña', ['/sitio/recordar-contrasena'], ['class' => 'btn btn-default btn-xs btn-block']) ?>
          </div>
        </div>
        <div class="form-group">
          <?= Html::a('Registre su empresa y pruebe gratis teleclientes por 2 meses', ['/sitio/registrar'],['class' => 'btn btn-default btn-xs']) ?>
        </div>
        <?php ActiveForm::end() ?>
      </div>
    </div>
  </div>
</section>