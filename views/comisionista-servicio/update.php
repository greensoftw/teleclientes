<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ComisionistaServicio */

$this->title = 'Actualizar Comisionista Servicio: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Comisionista Servicios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="comisionista-servicio-update row">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
