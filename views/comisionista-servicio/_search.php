<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\ComisionistaServicioSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-comisionista-servicio-search">
  <div class="form-group row">
    <?php $form = ActiveForm::begin([
      'action' => ['index'],
      'method' => 'get',
      'fieldConfig' => ['template' => '<div class="col-lg-3">{label} {input}</div>']
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'trabajo_id')->widget(\kartik\widgets\Select2::className(), [
      'data' => \yii\helpers\ArrayHelper::map(\app\models\Trabajo::find()
        ->orderBy('nombre')->asArray()->all(), 'id', 'nombre'),
      'options' => ['placeholder' => 'Choose Trabajo'],
      'pluginOptions' => [
        'allowClear' => true
      ],
    ]); ?>

    <?= $form->field($model, 'nombre_cliente')->textInput(['maxlength' => true, 'placeholder' => 'Nombre Cliente']) ?>

    <?= $form->field($model, 'correo')->textInput(['maxlength' => true, 'placeholder' => 'Correo']) ?>

    <?= $form->field($model, 'direccion_cliente')->textInput(['maxlength' => true, 'placeholder' => 'Direccion Cliente']) ?>

    <?= $form->field($model, 'telefono_cliente')->textInput(['maxlength' => true, 'placeholder' => 'Telefono Cliente']) ?>

    <?= $form->field($model, 'comisionista_id')->widget(\kartik\widgets\Select2::className(), [
      'data' => \yii\helpers\ArrayHelper::map(\app\models\Comisionista::find()
        ->joinWith('usuario')->orderBy('usuario.nombres')->asArray()->all(), 'id', 'usuario.nombres'),
      'options' => ['placeholder' => 'Choose Comisionista'],
      'pluginOptions' => [
        'allowClear' => true
      ],
    ]); ?>

    <?= $form->field($model, 'arreglado')->dropDownList(\app\helpers\GSHelper::$dropBooleans) ?>

    <?= $form->field($model, 'concretado')->dropDownList(\app\helpers\GSHelper::$dropBooleans) ?>

    <?= $form->field($model, 'cerrado')->dropDownList(\app\helpers\GSHelper::$dropBooleans) ?>
  </div>
  <div class="form-group col-lg-12 row text-right">
    <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
    <?= Html::resetButton('Borrar', ['class' => 'btn btn-default']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</div>
