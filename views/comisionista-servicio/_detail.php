<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ComisionistaServicio */

?>
<div class="comisionista-servicio-view">

  <div class="row">
    <div class="col-sm-9">
      <h2><?= Html::encode("Comision N° : $model->id") ?></h2>
    </div>
  </div>

  <div class="row">
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      [
        'attribute' => 'trabajo.nombre',
        'label' => 'Trabajo',
      ],
      'nombre_cliente',
      'correo',
      'direccion_cliente',
      'telefono_cliente',
      [
        'attribute' => 'comisionista.usuario.nombres',
        'label' => 'Comisionista',
      ],
      'arreglado:boolean',
      'concretado:boolean',
      'cerrado:boolean',
      [
        'label' => 'Concretar',
        'value' => (!$model->concretado?
          Html::a('Concretar Servicio', ['/servicio/create', 'com_id' => $model->id], ['class' => 'btn btn-primary']) :
          '(Ya concretado)'),
        'format'=>'raw'
      ]
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>
</div>