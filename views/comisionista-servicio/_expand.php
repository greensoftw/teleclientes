<?php
use kartik\tabs\TabsX;
use yii\helpers\Html;

/** @var \app\models\base\ComisionistaServicio $model */
$items = [
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Detalle'),
    'content' => $this->render('_detail', [
      'model' => $model,
    ]),
  ]
];
if ($model->servicio)
  $items[] =
    [
      'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Servicio'),
      'content' => $this->render('_dataServicio', [
        'model' => $model,
        'row' => $model->servicio,
      ]),
    ];
?>
<div class="panel">
  <div class="panel-body">
    <?= TabsX::widget([
      'items' => $items,
      'position' => TabsX::POS_ABOVE,
      'encodeLabels' => false,
      'class' => 'tes',
      'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
      ],
    ]);
    ?>    </div>
</div>
