<?php
use yii\widgets\DetailView;

/**
 * @var $model \app\models\base\ComisionistaServicio
 */
$gridColumn = [
  ['attribute' => 'id', 'visible' => false],
  [
    'attribute' => 'empleado.usuario.nombres',
    'label' => 'Empleado',
  ],
  [
    'attribute' => 'estadoActual.estado',
    'label' => 'Estado actual'
  ],
  [
    'value' => Yii::$app->formatter->asDatetime($model->servicio->ingreso->fecha),
    'label' => 'Ingresado'
  ],
  'fecha_tecnico:datetime',
  [
    'attribute' => 'cliente.usuario.nombres',
    'label' => 'Cliente',
  ],
  [
    'attribute' => 'comisionServicio.comisionista.usuario.nombres',
    'label' => 'Comisionista',
  ],
  [
    'attribute' => 'comisionServicio.monto',
    'label' => 'Monto de la comisión',
    'format'=>'currency'
  ],
  ['attribute' => 'cliente.direccion',
    'label' => 'Dirección del cliente'
  ],
  [
    'attribute' => 'cliente.telefonos',
    'label' => 'Télefonos del cliente'
  ],
  [
    'attribute' => 'cliente.usuario.email',
    'label' => 'Correo del cliente'
  ],
  'total:currency',
  'pagado:boolean',
  'no_facturado',
  'cerrado:boolean',
];
echo DetailView::widget([
  'model' => $model->servicio,
  'attributes' => $gridColumn
]);
