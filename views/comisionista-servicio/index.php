<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ComisionistaServicioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = 'Comisionista Servicios';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="comisionista-servicio-index row">
  <p>
    <?= Html::a('Busqueda avanzada', '#', ['class' => 'btn btn-info search-button']) ?>
  </p>
  <div class="search-form" style="display:<?= Yii::$app->request->get($searchModel->formName()) ? 'block' : 'none' ?>">
    <?= $this->render('_search', ['model' => $searchModel]); ?>
  </div>
  <?php
  $gridColumn = [
    ['class' => 'yii\grid\SerialColumn'],
    [
      'class' => 'kartik\grid\ExpandRowColumn',
      'width' => '50px',
      'value' => function ($model, $key, $index, $column) {
        return GridView::ROW_COLLAPSED;
      },
      'detail' => function ($model, $key, $index, $column) {
        return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
      },
      'headerOptions' => ['class' => 'kartik-sheet-style'],
      'expandOneOnly' => true
    ],
    ['attribute' => 'id', 'visible' => false],
    [
      'attribute' => 'trabajo_id',
      'label' => 'Trabajo',
      'value' => function (\app\models\ComisionistaServicio $model) {
        return $model->trabajo->nombre;
      }
    ],
    'nombre_cliente',
    'correo',
    'direccion_cliente',
    'telefono_cliente',
    [
      'attribute' => 'comisionista_id',
      'label' => 'Comisionista',
      'value' => function (\app\models\ComisionistaServicio $model) {
        return $model->comisionista->usuario->nombres;
      },
    ],
    'arreglado:boolean',
    'concretado:boolean',
    'cerrado:boolean',
    [
      'class' => 'kartik\grid\ActionColumn',
      'template' => mdm\admin\components\Helper::filterActionColumn("{view}{update}")
    ],
  ];
  ?>
  <div>
    <?= GridView::widget([
      'dataProvider' => $dataProvider,
      'columns' => $gridColumn,
      'pjax' => true,
      'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-comisionista-servicio']],
      'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
      ],
      // your toolbar can include the additional full export menu
      'toolbar' => [
        '{export}',
        ExportMenu::widget([
          'dataProvider' => $dataProvider,
          'columns' => $gridColumn,
          'target' => ExportMenu::TARGET_BLANK,
          'fontAwesome' => true,
          'dropdownOptions' => [
            'label' => 'Todo',
            'class' => 'btn btn-default',
            'itemsBefore' => [
              '<li class="dropdown-header">Exportar todo el contenido</li>',
            ],
          ],
        ]),
      ],
    ]); ?>
  </div>


</div>
