<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ComisionistaServicio */
/* @var $providerComisionista */
/* @var $providerTrabajo */
/* @var $providerServicio */
/* @var $clienteNoExiste boolean */

$this->title = "Comision servicio N° : $model->id";
$this->params['breadcrumbs'][] = ['label' => 'Comisionista Servicios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comisionista-servicio-view row">

  <div>
    <div class="form-group">
      <?=
      Html::a('<i class="fa fa-file"></i> ' . 'PDF',
        ['pdf', 'id' => $model->id],
        [
          'class' => 'btn btn-danger',
          'target' => '_blank',
          'data-toggle' => 'tooltip',
          'title' => 'se generara en una nueva ventana'
        ]
      ) ?>

      <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
      <?php if ($clienteNoExiste): ?>
        <?= Html::a('Crear cliente', ['/cliente/create', 'com_id' => $model->comisionista_id, 'ser_id' => $model->id],
          ['class' => 'btn btn-primary', 'target' => '_blanck']) ?>
      <?php endif; ?>
      <?php if (!$model->concretado): ?>
        <?= Html::a('Concretar Servicio', ['/servicio/create', 'com_id' => $model->id], ['class' => 'btn btn-primary']) ?>
      <?php endif; ?>
      <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
          'confirm' => 'Seguro desea eliminar este elemento?',
          'method' => 'post',
        ],
      ])
      ?>
    </div>
  </div>
  <?php
  $gridColumn = [
    ['attribute' => 'id', 'visible' => false],
    [
      'attribute' => 'trabajo.nombre',
      'label' => 'Trabajo',
    ],
    [
      'attribute' => 'comisionista.usuario.nombres',
      'label' => 'Comisionista',
    ],
    'nombre_cliente',
    'correo',
    'direccion_cliente',
    'telefono_cliente',
    'monto:currency',
    'arreglado:boolean',
    'concretado:boolean',
    'cerrado:boolean',
  ];
  echo DetailView::widget([
    'model' => $model,
    'attributes' => $gridColumn
  ]);
  ?>
  <?php if (!$model->concretado and !$model->cerrado): ?>
    <div class="col-lg-6">
      <h2 class="text-danger">Cerrar servicio remitido</h2>
      <?php $form = \yii\bootstrap\ActiveForm::begin(['action' => ['cerrar', 'id' => $model->id]]) ?>
      <?= $form->field($model, 'razon')
        ->textarea(['placeholder' => 'Indique la razon por cual no ha sido concretado el servicio']) ?>
      <div class="form-group">
        <?= Html::submitButton('Guardar y cerrar', ['class' => 'btn btn-danger']) ?>
      </div>
      <?php \yii\bootstrap\ActiveForm::end() ?>
    </div>
  <?php endif; ?>

  <?php if ($model->servicio): ?>
    <h3 class="text-info">Detalles del servicio : </h3>
    <div class="col-lg-12">
      <?php
      $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        ['attribute' => 'numero'],
        [
          'attribute' => 'empleado.usuario.nombres',
          'label' => 'Empleado',
        ],
        [
          'attribute' => 'estadoActual.estado',
          'label' => 'Estado actual'
        ],
        [
          'value' => Yii::$app->formatter->asDatetime($model->servicio->ingreso->fecha),
          'label' => 'Ingresado'
        ],
        'fecha_tecnico:datetime',
        [
          'attribute' => 'cliente.usuario.nombres',
          'label' => 'Cliente',
        ],
        [
          'attribute' => 'comisionServicio.comisionista.usuario.nombres',
          'label' => 'Comisionista',
        ],
        [
          'attribute' => 'comisionServicio.monto',
          'label' => 'Monto de la comisión',
          'format'=>'currency'
        ],
        ['attribute' => 'cliente.direccion',
          'label' => 'Dirección del cliente'
        ],
        [
          'attribute' => 'cliente.telefonos',
          'label' => 'Télefonos del cliente'
        ],
        [
          'attribute' => 'cliente.usuario.email',
          'label' => 'Correo del cliente'
        ],
        'total:currency',
        'pagado:boolean',
        'no_facturado',
        'cerrado:boolean',
      ];
      echo DetailView::widget([
        'model' => $model->servicio,
        'attributes' => $gridColumn
      ]);
      ?>
    </div>
  <?php endif; ?>
</div>