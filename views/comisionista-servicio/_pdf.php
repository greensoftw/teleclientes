<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\ComisionistaServicio */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Comisionista Servicios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comisionista-servicio-view">

    <div class="row">
        <div class="col-sm-9">
            <h3><?= 'Comisionista Servicio'.': '. Html::encode($this->title) ?></h3>
        </div>
    </div>

    <div>
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'trabajo.id',
                'label' => 'Trabajo'
            ],
        'nombre_cliente',
        'correo',
        'direccion_cliente',
        'telefono_cliente',
        [
                'attribute' => 'comisionista.id',
                'label' => 'Comisionista'
            ],
        'arreglado:boolean',
        'concretado:boolean',
        'cerrado:boolean',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerServicio->totalCount){
    $gridColumnServicio = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'empleado.id',
                'label' => 'Empleado'
            ],
        [
                'attribute' => 'cliente.id',
                'label' => 'Cliente'
            ],
                'total',
        'pagado',
        'no_facturado',
        'cerrado',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerServicio,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Servicio'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnServicio
    ]);
}
?>
    </div>
</div>
