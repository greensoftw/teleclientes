<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ComisionistaServicio */

$this->title = 'Crear Comisionista Servicio';
$this->params['breadcrumbs'][] = ['label' => 'Comisionista Servicios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comisionista-servicio-create row">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
