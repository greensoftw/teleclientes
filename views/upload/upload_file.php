<?php
/**
 * User: jhon
 * Date: 15/12/16
 * Time: 04:17 PM
 * @var $action array
 */
$model = new \app\models\Archivo()
?>
<?php $form = \kartik\form\ActiveForm::begin([
  'action'=>$action,
  'options'=>['enctype'=>'multipart/form-data'],
  'enableAjaxValidation'=>true
]) ?>
<?= $form->field($model,'titulo') ?>
<?= $form->field($model,'file')->widget(\kartik\file\FileInput::className(),[

]) ?>
<?php \kartik\form\ActiveForm::end() ?>

