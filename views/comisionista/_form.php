<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Comisionista */
/* @var $modelUser app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comisionista-form row">

  <?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true
  ]); ?>

  <?= $form->errorSummary($model); ?>

  <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

  <?= $form->field($modelUser, 'nombres')->textInput(['maxlength' => true, 'placeholder' => 'Nombres de usuario']) ?>

  <?= $form->field($model, 'documento')->textInput(['maxlength' => true, 'placeholder' => 'Documento identidad']) ?>

  <?= $form->field($modelUser, 'email')->textInput(['maxlength' => true, 'placeholder' => 'Email']) ?>

  <?= $form->field($model, 'distribuidor_id')->widget(\kartik\widgets\Select2::className(), [
    'data' => \yii\helpers\ArrayHelper::map(\app\models\Distribuidor::find()->orderBy('id')->asArray()->all(), 'id', 'nombre'),
    'options' => ['placeholder' => 'Seleccione un distribuidor (Opcional)'],
    'pluginOptions' => [
      'allowClear' => true
    ],
  ]); ?>

  <?= $form->field($model, 'telefonos')->textInput(['maxlength' => true, 'placeholder' => 'Telefonos']) ?>

  <div class="form-group">
    <?php if (Yii::$app->controller->action->id != 'save-as-new'): ?>
      <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if (Yii::$app->controller->action->id != 'create'): ?>
      <?= Html::submitButton('Crear una copia', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
    <?= Html::a('Cancelar', Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</div>
