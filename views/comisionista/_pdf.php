<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Comisionista */
/**
 * @var $providerComisionistaServicio
 * @var $providerAbonoComisionista
 */

$this->title = $model->usuario->nombres;
$this->params['breadcrumbs'][] = ['label' => 'Comisionistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comisionista-view">

  <div class="row">
    <div class="col-sm-9">
      <h3><?= 'Comisionista' . ' ' . Html::encode($this->title) ?></h3>
    </div>
  </div>

  <div class="row">
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      [
        'attribute' => 'usuario.nombres',
        'label' => 'Usuario'
      ],
      [
        'attribute' => 'distribuidor.nombre',
        'label' => 'Distribuidor'
      ],
      'telefonos',
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>
  <div class="row">
    <?php
    if ($providerComisionistaServicio->totalCount) {
      $gridColumnComisionistaServicio = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
          'attribute' => 'trabajo.nombre',
          'label' => 'Trabajo'
        ],
        'monto:currency',
        'nombre_cliente',
        'correo',
        'direccion_cliente',
        'telefono_cliente',
        'arreglado:boolean',
        'concretado:boolean',
        'cerrado:boolean',
      ];
      echo GridView::widget([
        'dataProvider' => $providerComisionistaServicio,
        'panel' => [
          'type' => GridView::TYPE_DEFAULT,
          'heading' => Html::encode('Servicios referidos'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>',
        'toggleData' => false,
        'columns' => $gridColumnComisionistaServicio
      ]);
    }
    ?>
  </div>
  <div class="row">
    <?php
    if ($providerAbonoComisionista->totalCount) {
      $gridColumnAbonoComisionista = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'fecha',
        'monto',
      ];
      echo GridView::widget([
        'dataProvider' => $providerAbonoComisionista,
        'panel' => [
          'type' => GridView::TYPE_DEFAULT,
          'heading' => Html::encode('Pagos Realizados'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>',
        'toggleData' => false,
        'columns' => $gridColumnAbonoComisionista
      ]);
    }
    ?>
  </div>
</div>
