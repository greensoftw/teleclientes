<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Comisionista */

?>
<div class="comisionista-view">
  <div class="row">
    <div class="col-sm-9">
      <h2><?= Html::encode($model->usuario->nombres) ?></h2>
    </div>
  </div>
  <div class="row">
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      [
        'attribute' => 'usuario.nombres',
        'label' => 'Usuario',
      ],
      [
        'attribute' => 'usuario.email',
        'label' => 'Email',
      ],
      [
        'attribute' => 'documento',
        'label' => 'Documento identidad',
      ],
      [
        'attribute' => 'distribuidor.nombre',
        'label' => 'Distribuidor',
      ],
      'telefonos',
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>
</div>