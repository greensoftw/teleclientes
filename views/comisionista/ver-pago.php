<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AbonoComisionista */
/* @var $providerComisionista */
$this->title = "Pago N° $model->id";
$this->params['breadcrumbs'][] = ['label' => 'Abono Comisionista', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Comisionista : '.$model->comisionista->usuario->nombres,
  'url' => ['view','id'=>$model->comisionista_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="abono-comisionista-view row">

  <div class="">
    <div class="form-group">

      <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
          'confirm' => 'Seguro desea eliminar este elemento?',
          'method' => 'post',
        ],
      ])
      ?>
    </div>
  </div>

  <div class="">
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      [
        'attribute' => 'comisionista.usuario.nombres',
        'label' => 'Comisionista',
      ],
      'fecha:date',
      'monto:currency',
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>
  <?php
  $providerComisionistaServicio = new \yii\data\ArrayDataProvider([
      'allModels'=>$model->comisionistaServicios
  ]);
  if($providerComisionistaServicio->totalCount){
    $gridColumnComisionistaServicio = [
      ['attribute' => 'id', 'visible' => false],
      [
        'attribute' => 'trabajo.id',
        'label' => 'Trabajo'
      ],
      [
        'attribute' => 'comisionista.id',
        'label' => 'Comisionista'
      ],
      'nombre_cliente',
      'correo',
      'direccion_cliente',
      'telefono_cliente',
      'monto',
    ];
    echo GridView::widget([
      'dataProvider' => $providerComisionistaServicio,
      'pjax' => true,
      'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-comisionista-servicio']],
      'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Comisiones arregladas'),
      ],
      'export' => false,
      'columns' => $gridColumnComisionistaServicio
    ]);
  }
  ?>

</div>