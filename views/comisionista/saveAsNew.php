<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Comisionista */

$this->title = 'Save As New Comisionista: '. ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Comisionistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="comisionista-create row">
    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>
</div>
