<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Comisionista */
/* @var $providerAbonoComisionista */
/* @var $providerDistribuidor */
/* @var $providerUsuario */
/* @var $providerComisionistaServicio */

$this->title = "comisionista : " . $model->usuario->nombres;
$this->params['breadcrumbs'][] = ['label' => 'Comisionistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comisionista-view row">

  <div>
    <div class="form-group">
      <?=
      Html::a('<i class="fa fa-file"></i> ' . 'PDF',
        ['pdf', 'id' => $model->id],
        [
          'class' => 'btn btn-danger',
          'target' => '_blank',
          'data-toggle' => 'tooltip',
          'title' => 'Will open the generated PDF file in a new window'
        ]
      ) ?>
      <?= Html::a('Actualiar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
      <?php if ($count = $model->getComisionistaServicios()->where(['arreglado' => 0, 'concretado' => 1, 'cerrado' => 1])->count() > 0) {
        echo Html::a('Pagar ' . $count . ' Comsiones ', ['pagar', 'id' => $model->id], ['class' => 'btn btn-primary']);
      } ?>
    </div>
  </div>

  <div>
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      [
        'attribute' => 'usuario.nombres',
        'label' => 'Usuario',
      ],
      [
        'attribute' => 'usuario.email',
        'label' => 'Email',
      ],
      [
        'attribute' => 'documento',
        'label' => 'Documento identidad',
      ],
      [
        'attribute' => 'distribuidor.nombre',
        'label' => 'Distribuidor',
      ],
      [
        'label' => 'Total ganado que debe pagar',
        'value' => Yii::$app->formatter->asCurrency($model->getTotal()),
        'format' => 'raw'
      ],
      [
        'label' => 'Total ganado pendiente',
        'value' => Yii::$app->formatter->asCurrency($model->getTotalPendiente()),
        'format' => 'raw'
      ],
      'telefonos',
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>

  <div>
    <?php
    if ($providerComisionistaServicio->totalCount) {
      $gridColumnComisionistaServicio = [
        ['attribute' => 'id', 'visible' => false],
        [
          'attribute' => 'trabajo.nombre',
          'label' => 'Trabajo'
        ],
        'nombre_cliente',
        'correo',
        'direccion_cliente',
        'telefono_cliente',
        'arreglado:boolean',
        'concretado:boolean',
        'cerrado:boolean',
        'razon'
      ];
      echo GridView::widget([
        'dataProvider' => $providerComisionistaServicio,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-comisionista-servicio']],
        'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Servicios remitidos pendientes'),
        ],
        'columns' => $gridColumnComisionistaServicio
      ]);
    }
    ?>
  </div>
  <div>
    <?php
    if ($providerAbonoComisionista->totalCount) {
      $gridColumnAbonoComisionista = [
        ['attribute' => 'id', 'visible' => false],
        'fecha:date',
        'monto:currency',
        [
          'class' => 'yii\grid\ActionColumn',
          'template' => "{view}",
          'buttons' => [
            'view' => function ($url, $model, $key) {
              return \yii\bootstrap\Html::a('<i class="fa fa-eye"></i>', ['ver-pago', 'id' => $model->id]);
            }
          ]
        ],
      ];
      echo GridView::widget([
        'dataProvider' => $providerAbonoComisionista,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-abono-comisionista']],
        'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Pagos Realizados'),
        ],
        'columns' => $gridColumnAbonoComisionista
      ]);
    }
    ?>
  </div>
</div>
