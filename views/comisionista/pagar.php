<?php

use kartik\grid\GridView;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Comisionista */
/* @var $pago \app\models\AbonoComisionista */
/**
 * @var $comisiones \app\models\base\ComisionistaServicio[]
 */

$this->title = $model->usuario->nombres;
$this->params['breadcrumbs'][] = ['label' => 'Comisionistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comisionista-view row">
  <div class="col-lg-6">
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      [
        'attribute' => 'usuario.nombres',
        'label' => 'Usuario',
      ],
      [
        'attribute' => 'distribuidor.nombre',
        'label' => 'Distribuidor',
      ],
      'telefonos',
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>

  </div>
  <div class="col-lg-6">
    <?php $form = ActiveForm::begin([
        'enableAjaxValidation'=>true
    ]) ?>
    <?= $form->field($pago,'monto') ?>
    <?= Html::submitButton('Confirmar',['class'=>'btn btn-primary']) ?>
    <?php ActiveForm::end() ?>
  </div>
  <div class="col-lg-12">
    <?php
    $gridColumnComisionistaServicio = [
      ['attribute' => 'id', 'visible' => false],
      [
        'attribute' => 'trabajo.nombre',
        'label' => 'Trabajo'
      ],
      'nombre_cliente',
      'monto:currency',
      'direccion_cliente',
      'telefono_cliente',
      'correo',
      [
          'label'=>'servicio',
        'value'=>function(\app\models\base\ComisionistaServicio $model){
          return Html::a($model->servicio->numero,
            ['/servicio/view','id'=>$model->servicio->id],['class'=>'bt btn-default','target'=>'_blanck']);
        }
      ]
    ];
    echo GridView::widget([
      'dataProvider' => new \yii\data\ArrayDataProvider([
        'allModels'=>$comisiones
      ]),
      'pjax' => true,
      'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-comisionista-servicio']],
      'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Servicios Remitidos'),
      ],
      'columns' => $gridColumnComisionistaServicio,
      'toolbar'=>false
    ]);
    ?>
  </div>
</div>
