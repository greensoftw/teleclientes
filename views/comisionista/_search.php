<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\ComisionistaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-comisionista-search">

  <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
  ]); ?>

  <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

  <?= $form->field($model, 'usuario_id')->widget(\kartik\widgets\Select2::className(), [
    'data' => \yii\helpers\ArrayHelper::map(\app\models\Usuario::find()->orderBy('id')->asArray()->all(), 'id', 'nombres'),
    'options' => ['placeholder' => 'Seleccione Usuario'],
    'pluginOptions' => [
      'allowClear' => true
    ],
  ]); ?>

  <?= $form->field($model, 'distribuidor_id')->widget(\kartik\widgets\Select2::className(), [
    'data' => \yii\helpers\ArrayHelper::map(\app\models\Distribuidor::find()->orderBy('id')->asArray()->all(), 'id', 'nombres'),
    'options' => ['placeholder' => 'Seleccione Distribuidor'],
    'pluginOptions' => [
      'allowClear' => true
    ],
  ]); ?>

  <?= $form->field($model, 'telefonos')->textInput(['maxlength' => true, 'placeholder' => 'Telefonos']) ?>

  <div class="form-group">
    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
