<?php


/* @var $this yii\web\View */
/* @var $model app\models\Comisionista */
/* @var $modelUser app\models\Usuario */

$this->title = 'Crear Comisionista';
$this->params['breadcrumbs'][] = ['label' => 'Comisionistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comisionista-create">
  <?= $this->render('_form', [
    'model' => $model,
    'modelUser' => $modelUser
  ]) ?>
</div>
