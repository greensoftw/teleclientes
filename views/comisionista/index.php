<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ComisionistaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = 'Comisionistas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comisionista-index row">
  <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
  <?php
  $gridColumn = [
    ['class' => 'yii\grid\SerialColumn'],
    [
      'class' => 'kartik\grid\ExpandRowColumn',
      'width' => '50px',
      'value' => function ($model, $key, $index, $column) {
        return GridView::ROW_COLLAPSED;
      },
      'detail' => function ($model, $key, $index, $column) {
        return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
      },
      'headerOptions' => ['class' => 'kartik-sheet-style'],
      'expandOneOnly' => true
    ],
    ['attribute' => 'id', 'visible' => false],
    [
      'attribute' => 'usuario_id',
      'label' => 'Usuario',
      'value' => function (\app\models\Comisionista $model) {
        return $model->usuario->nombres;
      },
      'filterType' => GridView::FILTER_SELECT2,
      'filter' => \yii\helpers\ArrayHelper::map(\app\models\Usuario::find()
        ->orderBy(['nombres'=>SORT_DESC])->innerJoinWith('comisionista')
        ->asArray()->all(), 'id', 'nombres'),
      'filterWidgetOptions' => [
        'pluginOptions' => ['allowClear' => true],
      ],
      'filterInputOptions' => ['placeholder' => 'Usuario', 'id' => 'grid-comisionista-search-usuario_id']
    ],
    'documento',
    [
      'attribute' => 'distribuidor_id',
      'label' => 'Distribuidor',
      'value' => 'distribuidor.nombre',
      'filterType' => GridView::FILTER_SELECT2,
      'filter' => \yii\helpers\ArrayHelper::map(\app\models\Distribuidor::find()->asArray()->all(), 'id', 'nombre'),
      'filterWidgetOptions' => [
        'pluginOptions' => ['allowClear' => true],
      ],
      'filterInputOptions' => ['placeholder' => 'Distribuidor', 'id' => 'grid-comisionista-search-distribuidor_id']
    ],
    'total:currency',
    'totalPendiente:currency',
    'telefonos',
    [
      'class' => 'kartik\grid\ActionColumn',
      'template'=>mdm\admin\components\Helper::filterActionColumn("{view} {update}")
    ],
  ];
  ?>
  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $gridColumn,
    'pjax' => true,
    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-comisionista']],
    'panel' => [
      'type' => GridView::TYPE_PRIMARY,
      'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
    ],
    // your toolbar can include the additional full export menu
    'toolbar' => [
      '{export}',
      ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumn,
        'target' => ExportMenu::TARGET_BLANK,
        'fontAwesome' => true,
        'dropdownOptions' => [
          'label' => 'Todo',
          'class' => 'btn btn-default',
          'itemsBefore' => [
            '<li class="dropdown-header">Exportar todo el contenido</li>',
          ],
        ],
      ]),
    ],
  ]); ?>

</div>
