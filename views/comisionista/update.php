<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Comisionista */
/* @var $modelUser app\models\Usuario */

$this->title = 'Actualizar Comisionista: ' . ' ' . $model->usuario->nombres;
$this->params['breadcrumbs'][] = ['label' => 'Comisionistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->usuario->nombres, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="comisionista-update row">
    <?= $this->render('_form', [
        'model' => $model,
        'modelUser' => $modelUser
    ]) ?>

</div>
