<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;

$dataProvider = new ArrayDataProvider([
  'allModels' => $model->servicioEstados,
  'key' => 'id'
]);
$gridColumns = [
  ['class' => 'yii\grid\SerialColumn'],
  ['attribute' => 'id', 'visible' => false],
  'fecha:datetime',
  'estado',
  'descripcion'
];
?>
<div class="col-lg-12">
  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns,
    'containerOptions' => ['style' => 'overflow: auto'],
    'pjax' => true,
    'beforeHeader' => [
      [
        'options' => ['class' => 'skip-export']
      ]
    ],
    'export' => [
      'fontAwesome' => true
    ],
    'bordered' => true,
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'hover' => true,
    'showPageSummary' => false,
    'persistResize' => false,
  ]); ?>
</div>
