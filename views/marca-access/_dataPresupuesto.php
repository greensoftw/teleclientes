<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

/**
 * @var $model \app\models\Servicio
 * @var $this \yii\web\View
 * @var $mensaje string|bool
 */

$dataProvider = new ArrayDataProvider([
  'allModels' => $model->presupuestos,
  'key' => 'id',
  'pagination' => [
    'pageSize' => 10,
    'route' => 'servicio/index'
  ]
]);
$gridColumns = [
  ['class' => 'yii\grid\SerialColumn'],
  ['attribute' => 'id', 'visible' => false],
  [
    'attribute' => 'relacion.nombre',
    'label' => 'Trabajo - Repuesto'
  ],
  'precio:currency',
];
?>
<div id="add-presupuesto-<?= $model->id ?>">
  <div class="col-lg-12">
    <h3>Detalles del presupuesto</h3>
    <?= GridView::widget([
      'id' => 'grid_presupuestos_' . $model->id,
      'dataProvider' => $dataProvider,
      'columns' => $gridColumns,
      'containerOptions' => ['style' => 'overflow: auto'],
      'pjax' => true,
      'beforeHeader' => [
        [
          'options' => ['class' => 'skip-export']
        ]
      ],
      'pjaxSettings' => [
        'formSelector' => false
      ],
      'export' => [
        'fontAwesome' => true
      ],
      'bordered' => true,
      'striped' => true,
      'condensed' => true,
      'responsive' => true,
      'hover' => true,
      'showPageSummary' => false,
      'persistResize' => false,
    ]); ?>
    <h3>Total : <?= Yii::$app->formatter->asCurrency($model->total) ?></h3>
  </div>
</div>