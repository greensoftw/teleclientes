<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\ServicioSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-servicio-search col-lg-12 row">
  <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'fieldConfig' => [
      'options' => ['class' => 'col-lg-3']
    ]
  ]); ?>
  <div class="form-group row">

    <?= $form->field($model, 'numero_garantia')->textInput(['placeholder' => 'Numero de garantia']) ?>

    <?= $form->field($model, 'numero') ?>

    <?= $form->field($model, 'fecha_desde')->widget(\kartik\datecontrol\DateControl::classname(), [
      'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
      'saveFormat' => 'php:Y-m-d',
      'ajaxConversion' => true,
      'options' => [
        'pluginOptions' => [
          'placeholder' => 'Seleccione Fecha',
          'autoclose' => true,
        ]
      ],
    ]); ?>
    <?= $form->field($model, 'fecha_hasta')->widget(\kartik\datecontrol\DateControl::classname(), [
      'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
      'saveFormat' => 'php:Y-m-d',
      'ajaxConversion' => true,
      'options' => [
        'pluginOptions' => [

          'placeholder' => 'Seleccione Fecha',
          'autoclose' => true,
        ]
      ],
    ]); ?>
    <?= $form->field($model, 'marca_id')->dropDownList(\app\helpers\GSHelper::$dropBooleans) ?>
  </div>
  <div class="form-group col-lg-12 row">
    <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
    <?= Html::resetButton('Borrar', ['class' => 'btn btn-default']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
