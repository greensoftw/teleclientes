<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Cliente */
/* @var $empresa \app\models\Empresa */

$this->title = $model->usuario->nombres;
$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cliente-view">
  <table style="width: 100%">
    <tr>
      <td width="25%">
        <?= Html::img("@web/$empresa->picture", ['alt' => $empresa->nombre, 'style' => 'max-width: 200px']) ?>
      </td>
      <td width="50%" style="text-align: center">
        <div style="text-align: center">
          <h1><?= $empresa->nombre ?></h1>
          <h4><?= $empresa->nit ?></h4>
          <h4><?= $empresa->tel_fijo ?> - <?= $empresa->tel_cel ?></h4>
          <h4><?= $empresa->direccion ?></h4>
          <h4><?= $empresa->web ?></h4>
          <h4></h4>
        </div>
      </td>
      <td></td>
    </tr>
  </table>
  <div class="row">
    <h3><?= 'Cliente' . ': ' . Html::encode($this->title) ?></h3>
  </div>

  <div class="row">
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      [
        'attribute' => 'usuario.nombres',
        'label' => 'Usuario'
      ],
      'telefonos',
      'direccion',
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>

  <div class="row">
    <?php foreach ($model->equipos as $equipo): ?>
      <table class="kv-grid-table table table-bordered table-striped kv-table-wrap">
        <tr>
          <td colspan="6"><h4>Equipo</h4></td>
        </tr>
        <tr>
          <th>Tipo</th>
          <th>Marca</th>
          <th>Distribuidor</th>
          <th>modelo</th>
          <th>serial</th>
          <th>Ubicacion</th>
        </tr>
        <tr>
          <td>
            <?= $equipo->tipoEquipo->nombre ?>
          </td>
          <td>
            <?= $equipo->marca->nombre ?>
          </td>
          <td>
            <?= Yii::$app->formatter->asText($equipo->distribuidor) ?>
          </td>
          <td>
            <?= $equipo->modelo ?>
          </td>
          <td>
            <?= $equipo->serial ?>
          </td>
          <td>
            <?= $equipo->nota ?>
          </td>
        </tr>
        <tr>
          <td colspan="6">
            <h4>servicios.</h4>
            <table class="table">
              <tr>
                <th>N° de orden</th>
                <th>Fecha de ingreso</th>
                <th>Servicio(s)</th>
                <th>Estado</th>
                <th>Valor servicio</th>
                <th>N° de factura</th>
                <th>Fecha entrega</th>
              </tr>
              <?php $servicios = $equipo->getServicioAEquipos()->orderBy(['id'=>SORT_DESC])->all();
              foreach ($servicios as $servicio):?>
                <tr>
                  <td><?= $servicio->servicio->numero ?></td>
                  <td><?= Yii::$app->formatter->asDate($servicio->servicio->ingreso->fecha) ?></td>
                  <td><?php foreach ($servicio->servicio->presupuestos as $key => $pres){
                     echo ($key+1) ." ".$pres->relacion->nombre."<br>";
                    } ?>
                  </td>
                  <td><?= $servicio->servicio->estadoActual->estado ?></td>
                  <td><?= Yii::$app->formatter->asCurrency($servicio->servicio->total) ?></td>
                  <td><?= $servicio->servicio->no_facturado ?></td>
                  <td><?= Yii::$app->formatter->asDate($servicio->servicio->entrega? $servicio->servicio->entrega->fecha : null) ?></td>
                </tr>
              <?php endforeach; ?>
            </table>

          </td>
        </tr>
      </table>
    <?php endforeach; ?>

  </div>
</div>
