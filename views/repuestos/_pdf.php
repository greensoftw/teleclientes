<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Repuestos */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Repuestos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="repuestos-view">

  <div class="row">
    <div class="col-sm-9">
      <h3><?= 'Repuesto' . ': ' . Html::encode($this->title) ?></h3>
    </div>
  </div>

  <div>
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      'codigo',
      'no_parte',
      'nombre',
      'costo',
      'precio',
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>
</div>
