<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Repuestos */

?>
<div class="repuestos-view">

    <div class="row">
        <div class="col-sm-9">
            <h3><?= Html::encode($model->nombre) ?></h3>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'codigo',
        'no_parte',
        'nombre',
        'costo',
        'precio',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>