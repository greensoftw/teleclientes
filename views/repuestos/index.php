<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\RepuestosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = 'Repuestos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="repuestos-index row">
  <?php
  $gridColumn = [
    ['class' => 'yii\grid\SerialColumn'],
    [
      'class' => 'kartik\grid\ExpandRowColumn',
      'width' => '50px',
      'value' => function ($model, $key, $index, $column) {
        return GridView::ROW_COLLAPSED;
      },
      'detail' => function ($model, $key, $index, $column) {
        return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
      },
      'headerOptions' => ['class' => 'kartik-sheet-style'],
      'expandOneOnly' => true
    ],
    ['attribute' => 'id', 'visible' => false],
    'codigo',
    'no_parte',
    'nombre',
    'costo',
    'precio',
    [
      'class' => 'kartik\grid\ActionColumn',
      'template'=>mdm\admin\components\Helper::filterActionColumn("{view}{update}")
    ],
  ];
  ?>
  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $gridColumn,
    'pjax' => true,
    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-repuestos']],
    'panel' => [
      'type' => GridView::TYPE_PRIMARY,
      'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
    ],
    // your toolbar can include the additional full export menu
    'toolbar' => [
      '{export}',
      ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumn,
        'target' => ExportMenu::TARGET_BLANK,
        'fontAwesome' => true,
        'dropdownOptions' => [
          'label' => 'Todo',
          'class' => 'btn btn-default',
          'itemsBefore' => [
            '<li class="dropdown-header">Exportar todo el contenido</li>',
          ],
        ],
      ]),
    ],
  ]); ?>

</div>
