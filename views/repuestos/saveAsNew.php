<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Repuestos */

$this->title = 'Crear una copia Repuesto: '. ' ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Repuestos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Crear una copia';
?>
<div class="repuestos-create row">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
