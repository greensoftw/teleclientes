<?php
use kartik\tabs\TabsX;
use yii\helpers\Html;

$items = [
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Detalle'),
    'content' => $this->render('_detail', [
      'model' => $model,
    ]),
  ],
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Compras hechas'),
    'content' => $this->render('_dataItemCompra', [
      'model' => $model,
      'row' => $model->itemCompras,
    ]),
  ],
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Presupuesto'),
    'content' => $this->render('_dataPresupuesto', [
      'model' => $model,
      'row' => $model->presupuestos,
    ]),
  ],
];
?>
<div class="panel">
  <div class="panel-body">
    <?= TabsX::widget([
      'items' => $items,
      'position' => TabsX::POS_ABOVE,
      'encodeLabels' => false,
      'class' => 'tes',
      'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
      ],
    ]); ?>
  </div>
</div>
