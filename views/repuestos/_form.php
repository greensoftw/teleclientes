<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Repuestos */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="repuestos-form">

  <?php $form = ActiveForm::begin(); ?>

  <?= $form->errorSummary($model); ?>

  <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
  <?= $form->field($model, 'marca_id')->widget(\kartik\widgets\Select2::classname(), [
    'data' => \yii\helpers\ArrayHelper::map(\app\models\Marca::find()->orderBy('nombre')->asArray()->all(), 'id', 'nombre'),
    'options' => ['placeholder' => 'Seleccione una Marca'],
    'pluginOptions' => [
      'allowClear' => true
    ],
  ]); ?>

  <?= $form->field($model, 'codigo')->textInput(['maxlength' => true, 'placeholder' => 'Codigo']) ?>

  <?= $form->field($model, 'no_parte')->textInput(['maxlength' => true, 'placeholder' => 'No Parte']) ?>

  <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'placeholder' => 'Nombre']) ?>

  <?= $form->field($model, 'costo')->textInput(['placeholder' => 'Costo']) ?>

  <?= $form->field($model, 'precio')->textInput(['placeholder' => 'Precio']) ?>

  <div class="form-group">
    <?php if (Yii::$app->controller->action->id != 'save-as-new'): ?>
      <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if (Yii::$app->controller->action->id == 'save-as-new'): ?>
      <?= Html::submitButton('Crear copia', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
    <?= Html::a('Cancelar', Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
