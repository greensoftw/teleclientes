<?php
/**
 * Created by PhpStorm.
 * User: Andres RS
 * Date: 15/12/2016
 * Time: 7:14 PM
 * @var $this \yii\web\View
 * @var $title string
 * @var $content string
 */
?>


<div class="col-lg-8 col-md-8 col-sm-8 col-lg-offset-2 col-md-offset-2">
  <div class="alert alert-success text-center">
    <h1><?= $title ?></h1>
    <p><?= $content ?></p>
  </div>
</div>

