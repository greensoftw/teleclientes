<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Trabajo'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
        [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Comisionista Servicio'),
        'content' => $this->render('_dataComisionistaServicio', [
            'model' => $model,
            'row' => $model->comisionistaServicios,
        ]),
    ],
    ];
?>
<div class="panel">
    <div class="panel-body">
        <?= TabsX::widget([
        'items' => $items,
        'position' => TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'class' => 'tes',
        'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
        ],
        ]);
        ?>    </div>
</div>
