<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Trabajo */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Trabajos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trabajo-view">

    <div class="row">
        <div class="col-sm-9">
            <h3><?= 'Trabajo'.': '. Html::encode($this->title) ?></h3>
        </div>
    </div>

    <div>
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'nombre',
        'descripcion',
        'precio',
        'visible_comisionista:boolean',
        'comision',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerComisionistaServicio->totalCount){
    $gridColumnComisionistaServicio = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
                'nombre_cliente',
        'correo',
        'direccion_cliente',
        'telefono_cliente',
        [
                'attribute' => 'comisionista.id',
                'label' => 'Comisionista'
            ],
        'arreglado:boolean',
        'concretado:boolean',
        'cerrado:boolean',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerComisionistaServicio,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Comisionista Servicio'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnComisionistaServicio
    ]);
}
?>
    </div>
</div>
