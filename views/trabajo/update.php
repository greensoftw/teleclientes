<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Trabajo */

$this->title = 'Actualizar Trabajo: ' . ' ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Trabajos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="trabajo-update row">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
