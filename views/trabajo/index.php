<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\TrabajoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = 'Trabajos';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="trabajo-index row">
  <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

  <p>
    <?= Html::a('Crear Trabajo', ['create'], ['class' => 'btn btn-success']) ?>
    <?= Html::a('Busqueda avanzada', '#', ['class' => 'btn btn-info search-button']) ?>
  </p>
  <div class="search-form" style="display:<?= Yii::$app->request->get($searchModel->formName()) ? 'block' : 'none' ?>">
    <?= $this->render('_search', ['model' => $searchModel]); ?>
  </div>
  <?php
  $gridColumn = [
    ['class' => 'yii\grid\SerialColumn'],
    [
      'class' => 'kartik\grid\ExpandRowColumn',
      'width' => '50px',
      'value' => function ($model, $key, $index, $column) {
        return GridView::ROW_COLLAPSED;
      },
      'detail' => function ($model, $key, $index, $column) {
        return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
      },
      'headerOptions' => ['class' => 'kartik-sheet-style'],
      'expandOneOnly' => true
    ],
    ['attribute' => 'id', 'visible' => false],
    'nombre',
    'descripcion',
    'precio',
    [
      'attribute'=>'visible_comisionista',
      'value' => 'visible_comisionista',
      'format' => 'boolean',
      'filterType' => GridView::FILTER_CHECKBOX,
      'filterInputOptions'=>['label'=>"SI/NO",]
    ],
    'comision',
    [
      'class' => 'kartik\grid\ActionColumn',
      'template'=>\mdm\admin\components\Helper::filterActionColumn("{view} {update}")
    ],
  ];
  ?>
  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $gridColumn,
    'pjax' => true,
    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-trabajo']],
    'panel' => [
      'type' => GridView::TYPE_PRIMARY,
      'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
    ],
    'toolbar' => [
      '{export}',
      ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumn,
        'target' => ExportMenu::TARGET_BLANK,
        'fontAwesome' => true,
        'dropdownOptions' => [
          'label' => 'Todo',
          'class' => 'btn btn-default',
          'itemsBefore' => [
            '<li class="dropdown-header">Exportar todo el contenido</li>',
          ],
        ],
      ]),
    ],
  ]); ?>

</div>
