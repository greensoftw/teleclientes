<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Trabajo */

$this->title = 'Crear una copia Trabajo: '. ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Trabajos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Crear una copia';
?>
<div class="trabajo-create row">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
