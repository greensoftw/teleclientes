<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Trabajo */
/* @var $providerComisionistaServicio */
$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Trabajos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trabajo-view row">

  <div>
    <div class="form-group">
      <?=
      Html::a('<i class="fa fa-file"></i> ' . 'PDF',
        ['pdf', 'id' => $model->id],
        [
          'class' => 'btn btn-danger',
          'target' => '_blank',
          'data-toggle' => 'tooltip',
          'title' => 'se generara en una nueva ventana'
        ]
      ) ?>
      <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </div>
  </div>

  <div>
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      'nombre',
      'descripcion',
      'precio:currency',
      'visible_comisionista:boolean',
      'comision:currency',
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>

  <div>
    <?php
    if ($providerComisionistaServicio->totalCount) {
      $gridColumnComisionistaServicio = [
        ['attribute' => 'id', 'visible' => false],
        'nombre_cliente',
        'correo',
        'direccion_cliente',
        'telefono_cliente',
        [
          'attribute' => 'comisionista.id',
          'label' => 'Comisionista'
        ],
        'arreglado:boolean',
        'concretado:boolean',
        'cerrado:boolean',
      ];
      echo GridView::widget([
        'dataProvider' => $providerComisionistaServicio,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-comisionista-servicio']],
        'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Comisionista Servicio'),
        ],
        'columns' => $gridColumnComisionistaServicio
      ]);
    }
    ?>
  </div>
</div>
