<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Empleado */
/* @var $modelUser app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */
/* @var $roles \yii\rbac\Role[] */
?>

<div class="empleado-form">

  <?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true
  ]); ?>

  <?= $form->errorSummary($model); ?>

  <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

  <?= $form->field($modelUser, 'nombres')->textInput(['maxlength' => true, 'placeholder' => 'Nombres del usuario']) ?>

  <?= $form->field($modelUser, 'email')->textInput(['maxlength' => true, 'placeholder' => 'email']) ?>

  <?= $form->field($model, 'telefono')->textInput(['maxlength' => true, 'placeholder' => 'Telefono']) ?>

  <?= $form->field($model, 'direccion')->textInput(['maxlength' => true, 'placeholder' => 'Direccion']) ?>

  <?= $form->field($model, 'tipo_empleado')
    ->dropDownList(\app\models\Empleado::$dropTipos, ['prompt' => 'Seleccione']) ?>

  <?= $form->field($model, 'sueldo')->textInput(['placeholder' => 'Sueldo'])
    ->hint('Indique el sueldo que le otorga al epleado mensual, ese se le sumara al saldo cada mes en 
    la fecha del proximo pago, ') ?>

  <?= $form->field($model, 'proximo_pago')->widget(\kartik\datecontrol\DateControl::className(), [
    'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
    'saveFormat' => 'php:Y-m-d',
    'ajaxConversion' => true,
    'options' => [
      'pluginOptions' => [
        'placeholder' => 'Seleccione Proximo Pago',
        'autoclose' => true,
      ]
    ],
  ])->hint(''); ?>
  <?= $form->field($model,'porcentaje')->dropDownList(range(1,100),['prompt'=>'Seleccione']) ?>

  <?= $form->field($model, 'saldo')->textInput(['placeholder' => 'Saldo']) ?>

  <?= $form->field($model, 'is_tecnico')->checkbox()->hint('Marque como tecnico para ser usado a la hora de crear servicio') ?>

  <?= $form->field($model, 'perfil')->textarea(['row' => 3, 'maxlength' => true,
    'placeholder' => 'Describa el perfil del empleado (Ej . Secretaria)']) ?>
  <?php if ($model->isNewRecord): ?>
    <?= $form->field($model, 'rol')->dropDownList(\yii\helpers\ArrayHelper::map($roles, 'name', 'name'),['prompt'=>'Seleccione'])
    ->hint('')?>
  <?php else: ?>
    <div class="text-info" style="margin-bottom: 10px; font-size: 16px">Para actualizar, roles, permisos, y quitar acceso a la plataforma valla
      <?= Html::a('control de acceso',['/admin/assignment/view','id'=>$modelUser->id]) ?></div>
  <?php endif; ?>
  <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?= Html::a('Cancelar', Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</div>
<?php $this->registerJs(/** @lang JavaScript */
  ' var fecha = $("#empleado-proximo_pago-disp");
    var sueldo = $("#' . Html::getInputId($model, 'sueldo') . '");
    var tipo = $("#' . Html::getInputId($model, 'tipo_empleado') . '");
    var porcentaje = $("#' . Html::getInputId($model, 'porcentaje') . '");
    if(tipo.val() != "' . $model::CON_SUELDO . '"){
      fecha.prop(\'disabled\', true);
      sueldo.prop(\'disabled\', true);
      porcentaje.prop(\'disabled\', true);
    }
  tipo.change(function(){
    var el = $(this);
    if(el.val() == "' . $model::CON_COMISION . '"){
      fecha.prop(\'disabled\', true);
      sueldo.prop(\'disabled\', true);
    }else{
      fecha.prop(\'disabled\', false);
      sueldo.prop(\'disabled\', false);
      porcentaje.prop(\'disabled\', true);
    }
    if(el.val() == "'.$model::CON_COMISION_AN_SUELDO.'" || el.val() == "'.$model::CON_COMISION.'"){
      porcentaje.prop(\'disabled\', false);
    }
  })'); ?>
