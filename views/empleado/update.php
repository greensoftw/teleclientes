<?php

/* @var $this yii\web\View */
/* @var $model app\models\Empleado */
/* @var $modelUser app\models\Usuario */

$this->title = 'Actualizar Empleado: ' . ' ' . $model->usuario->nombres;
$this->params['breadcrumbs'][] = ['label' => 'Empleados', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->usuario->nombres, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="empleado-update row">
  <?= $this->render('_form', [
    'model' => $model,
    'modelUser' => $modelUser,
    'roles' => $roles
  ]) ?>
</div>
