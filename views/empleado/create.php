<?php


/* @var $this yii\web\View */
/* @var $model app\models\Empleado */
/* @var $modelUser app\models\Usuario */

$this->title = 'Crear Empleado';
$this->params['breadcrumbs'][] = ['label' => 'Empleados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empleado-create row">
  <?= $this->render('_form', [
    'model' => $model,
    'modelUser' => $modelUser,
    'roles' => $roles
  ]) ?>
</div>
