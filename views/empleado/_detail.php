<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Empleado */

?>
<div class="empleado-view">

  <div class="row">
    <div class="col-sm-9">
      <h2><?= Html::encode($model->usuario->nombres) ?></h2>
    </div>
  </div>

  <div class="row">
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      [
        'attribute' => 'usuario.nombres',
        'label' => 'Usuario',
      ],
      [
        'attribute' => 'usuario.email',
        'label' => 'Email',
      ],
      'telefono',
      'tipo_empleado',
      'percent:percent',
      'sueldo:currency',
      'proximo_pago:date',
      'saldo:currency',
      'is_tecnico:boolean',
      'perfil',
    ];
    if ($model->is_tecnico) {
      $gridColumn[] = [
        'label' => 'Total de comisiones ganadas',
        'value' => $model->getEmpleadoComisions()->where(['arreglado' => 0])->sum('monto'),
        'format' => 'currency'
      ];
      $gridColumn[] = [
        'label' => 'Total de Dinero recogido por entregar',
        'value' => $model->getDeuda(),
        'format' => 'currency'
      ];
      $gridColumn[] = [
        'label' => 'Saldo de cobro anterior',
        'value' => $model->getSaldoDebe(),
        'format' => 'currency'
      ];
    }
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>
</div>