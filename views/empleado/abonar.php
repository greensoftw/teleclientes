<?php

use app\models\Empleado;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Empleado */
/* @var $pago \app\models\PagoEmpleado */
/* @var $ultimoPago \app\models\PagoEmpleado */
/* @var $comision int */
/* @var $saldoDebe int */

$this->title = "Empleado : " . $model->usuario->nombres;
$this->params['breadcrumbs'][] = ['label' => 'Empleados', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => "Empleado : " . $model->usuario->nombres, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empleado-view row">
  <div class="col-lg-6">
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      [
        'attribute' => 'usuario.nombres',
        'label' => 'Usuario',
      ],
      'telefono',
      'tipo_empleado',
      'sueldo',
      'proximo_pago:date',
      'saldo',
      'is_tecnico:boolean',
      'perfil',
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>
  <div class="col-lg-6">
    <?php if ($ultimoPago != null): ?>
      <?php $gridColumn = [
        'fecha:date',
        'monto:currency',
        'saldo_anterior:currency',
      ];
      echo DetailView::widget([
        'model' => $ultimoPago,
        'attributes' => $gridColumn
      ]); ?>
    <?php else: ?>
      <div class="alert alert-info">El empleado no tiene pagos previos</div>
    <?php endif; ?>
    <?php if ($model->is_tecnico): ?>
      <p class="success">Saldo que debe : <?= $saldoDebe ?></p>
    <?php endif; ?>
    <?php $form = ActiveForm::begin([
      'enableAjaxValidation' => true
    ]) ?>
    <div class="form-group">
      <p class="text-success">El Empleado tiene un saldo de: <?= Yii::$app->formatter->asCurrency($model->saldo) ?>
        <?php if ($model->tipo_empleado == Empleado::CON_COMISION ||
          $model->tipo_empleado == Empleado::CON_COMISION_AN_SUELDO
        ): ?>
          Mas <?= Yii::$app->formatter->asCurrency($comision) ?> de Comsiones
        <?php endif; ?>
      </p>
      <h3 class="text-primary">Total para pagar: <?= Yii::$app->formatter->asCurrency($pago->saldo_anterior) ?></h3>
    </div>
    <?= $form->field($pago, 'monto') ?>
    <?php if ($saldoDebe): ?>
      <?= $form->field($pago, 'cobrar_saldo_debe')->checkbox() ?>
    <?php endif; ?>
    <?= Html::submitButton('Confirmar', ['class' => 'btn btn-primary']) ?>
    <?php ActiveForm::end() ?>
  </div>
  <div class="col-lg-12 row">
    <?= $this->render('_dataEmpleadoComision', [
      'row' => $comisiones
    ]) ?>
  </div>
</div>
