<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

$dataProvider = new ArrayDataProvider([
  'allModels' => $row,
  'key' => 'id'
]);
$gridColumns = [
  ['class' => 'yii\grid\SerialColumn'],
  ['attribute' => 'id', 'visible' => false],
  [
    'attribute' => 'servicio.numero',
    'label' => 'Servicio'
  ],
  'monto:currency',
  'fecha:datetime',
  [
    'label'=>'Ver servicio',
    'value'=>function(\app\models\AbonoServicio $model){
      return \kartik\helpers\Html::a('<span class="glyphicon glyphicon-copy"></span> Orden N° '.$model->servicio->numero,
        ['/servicio/view','id'=>$model->servicio_id],['class'=>'btn btn-default','target'=>'_blanck']);
    },
    'format'=>'raw'
  ]
];

echo GridView::widget([
  'dataProvider' => $dataProvider,
  'columns' => $gridColumns,
  'containerOptions' => ['style' => 'overflow: auto'],
  'pjax' => true,
  'beforeHeader' => [
    [
      'options' => ['class' => 'skip-export']
    ]
  ],
  'export' => [
    'fontAwesome' => true
  ],
  'bordered' => true,
  'striped' => true,
  'condensed' => true,
  'responsive' => true,
  'hover' => true,
  'showPageSummary' => false,
  'persistResize' => false,
]);
