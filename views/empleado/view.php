<?php

use kartik\grid\GridView;
use kartik\tabs\TabsX;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Empleado */
/* @var $providerUsuario */
/* @var $providerEmpleadoComision \yii\data\ArrayDataProvider */
/* @var $providerServicio */
/* @var $roles array */
$this->title = "Empleado : " . $model->usuario->nombres;
$this->params['breadcrumbs'][] = ['label' => 'Empleados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empleado-view row">

  <div>
    <div class="form-group">
      <?=
      Html::a('<i class="fa fa-file"></i> PDF',
        ['pdf', 'id' => $model->id],
        [
          'class' => 'btn btn-danger',
          'target' => '_blank',
          'data-toggle' => 'tooltip',
          'title' => 'se generara en una nueva ventana'
        ]
      ) ?>

      <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('Pagar o Abonar', ['abonar', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('Recoger dinero de pagos', ['cobrar', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </div>
  </div>

  <div class="col-lg-12 row">
    <div class="col-lg-8 row">
      <?php
      $gridColumn = [
        [
          'attribute' => 'usuario.nombres',
          'label' => 'Usuario',
        ],
        [
          'attribute' => 'usuario.email',
          'label' => 'Correo',
        ],
        'direccion',
        'telefono',
        'tipo_empleado',
        'percent:percent',
        'sueldo',
        'proximo_pago:date',
        'saldo',
        'is_tecnico:boolean',
        'perfil',
      ];
      if ($model->is_tecnico) {
        $gridColumn[] = [
          'label' => 'Total de comisiones ganadas',
          'value' => $model->getEmpleadoComisions()->where(['arreglado' => 0])->sum('monto'),
          'format' => 'currency'
        ];
        $gridColumn[] = [
          'label' => 'Total de Dinero recogido por entregar',
          'value' => $model->getDeuda(),
          'format' => 'currency'
        ];
        $gridColumn[] = [
          'label' => 'Saldo de cobro anterior',
          'value' => $model->getSaldoDebe(),
          'format' => 'currency'
        ];
      }
      echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
      ]);
      ?>
    </div>
    <div class="col-lg-4">
      <?php if ($roles !== false): ?>
        <?php if ($roles != null): ?>
          <table class="table table-bordered">
            <tr>
              <td>Roles asignados</td>
              <td></td>
            </tr>
            <?php foreach ($roles as $rol): ?>
              <tr>
                <td><?= $rol->roleName ?></td>
                <td>Permisos</td>
              </tr>
              <?php $permisos = Yii::$app->authManager->getPermissionsByRole($rol->roleName) ?>
              <?php Yii::error(\yii\helpers\Json::encode($permisos)) ?>
              <?php foreach ($permisos as $permiso): ?>
                <tr>
                  <td></td>
                  <td><?= $permiso->description ?></td>
                </tr>
              <?php endforeach; ?>
            <?php endforeach; ?>
          </table>
        <?php else: ?>
          <?= \kartik\alert\Alert::widget([
            'type' => \kartik\alert\Alert::TYPE_DANGER,
            'body' => 'Este empleado no tiene acceso a la pataforma ' .
              Html::a('Clic para asignar roles.', ['/admin/assignment/view', 'id' => $model->usuario_id], ['class' => 'btn btn-primary'])
          ]) ?>
        <?php endif; ?>
      <?php endif; ?>
    </div>
  </div>
  <div class="col-lg-12 row">
    <?php $items = []; ?>
    <?php if ($model->is_tecnico) {
      $items[] = [
        'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Servicios pendiente (' .
            $model->getServicios()->where(['cerrado' => 0])->count() . ")"),
        'content' => $this->render('_dataServicio', [
          'model' => $model,
          'row' => $model->getServicios()->where(['cerrado' => 0])->all(),
        ]),
      ];
      $items[] = [
        'label' => '<i class="glyphicon glyphicon-book"></i> Pagos por entregar a la empresa',
        'content' => $this->render('_dataAbonoServicio', [
          'row' => $model->getAbonoServicios()->where(['is', 'cobro_empleado_id', null])->all()
        ])
      ];
      $items[] = [
        'label' => '<i class="glyphicon glyphicon-book"></i> Historial de entregas',
        'content' => $this->render('_dataCobroEmpleado', [
          'row' => $model->cobroEmpleados,
        ])
      ];
    }
    ?>
    <?php $items[] = [
      'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('historial de pagos'),
      'content' => $this->render('_dataPagoEmpleado', [
        'model' => $model,
        'row' => $model->pagoEmpleados,
      ]),
    ] ?>
    <?= TabsX::widget([
      'items' => $items,
      'position' => TabsX::POS_ABOVE,
      'encodeLabels' => false,
      'class' => 'tes',
      'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
      ],
    ]);
    ?>
  </div>
  <div class="form-group">
    <div>
      <h3 class="text-success">Adjuntar archivo</h3>
      <?= $this->render('/extra-views/upload_file', [
        'action' => ['upload-file', 'id' => $model->id]
      ]) ?>
    </div>
    <?php if ($model->files) ?>
    <?php $archivosProvider = new \yii\data\ArrayDataProvider([
      'allModels' => $model->files
    ]) ?>
    <?php
    $gridColumn = [
      'title',
      'tipoArchivo',
      [
        'class' => 'yii\grid\ActionColumn',
        'template' => "{view-file} {delete-file}",
        'buttons' => [
          'view-file' => function ($url, $model) {
            return Html::a('<i class="fa fa-eye"></i>', ['view-file', 'id' => $model->id],
              ['class' => 'btn btn-primary',]);
          },
          'delete-file' => function ($url, $model) {
            return Html::a('<i class="fa fa-trash"></i>', ['delete-file', 'id' => $model->id],
              ['class' => 'btn btn-danger', 'data-confirm' => 'Seguro desea eliminar este archivo']);
          }
        ],
        'header' => 'Opciones'
      ]
    ];
    echo GridView::widget([
      'dataProvider' => $archivosProvider,
      'pjax' => true,
      'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-empleado-comision']],
      'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Archivos ')
      ],
      'columns' => $gridColumn
    ]);
    ?>
  </div>
</div>
