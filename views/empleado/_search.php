<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\EmpleadoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-empleado-search">
  <div class="form-group row">
    <?php $form = ActiveForm::begin([
      'action' => ['index'],
      'method' => 'get',
      'fieldConfig' => ['template' => '<div class="col-lg-3">{label} {input}</div>']
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'usuario_id')->widget(\kartik\widgets\Select2::className(), [
      'data' => \yii\helpers\ArrayHelper::map(\app\models\Usuario::find()
        ->innerJoinWith('empleado')
        ->orderBy('nombres')->asArray()->all(), 'id', 'nombres'),
      'options' => ['placeholder' => 'Selecione Usuario'],
      'pluginOptions' => [
        'allowClear' => true
      ],
    ]); ?>

    <?= $form->field($model, 'telefono')->textInput(['maxlength' => true, 'placeholder' => 'Telefono']) ?>

    <?= $form->field($model, 'direccion')->textInput(['maxlength' => true, 'placeholder' => 'Direccion']) ?>

    <?= $form->field($model, 'tipo_empleado')->dropDownList(\app\models\Empleado::$dropTipos,
      ['maxlength' => true, 'placeholder' => 'Tipo Empleado', 'prompt' => 'Seleccione']) ?>

    <?= $form->field($model, 'is_tecnico')->dropDownList(\app\helpers\GSHelper::$dropBooleans) ?>


  </div>
  <div class="form-group col-lg-12">
    <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</div>

