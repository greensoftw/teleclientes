<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

$dataProvider = new ArrayDataProvider([
  'allModels' => $row,
  'key' => 'id'
]);
$gridColumns = [
  ['class' => 'yii\grid\SerialColumn'],
  ['attribute' => 'id', 'visible' => false],
  [
    'attribute' => 'cliente.usuario.nombres',
    'label' => 'Cliente'
  ],
  [
    'attribute' => 'comisionServicio.comisionista.usuario.nombres',
    'label' => 'Comision Servicio'
  ],
  'total:currency',
  'pagado:boolean',
  'no_facturado',
  [
    'label'=>'Ver servicio',
    'value'=>function(\app\models\Servicio $model){
      return \kartik\helpers\Html::a('<span class="glyphicon glyphicon-copy"></span> Orden N° '.$model->numero,
        ['/servicio/view','id'=>$model->id],['class'=>'btn btn-default','target'=>'_blanck']);
    },
    'format'=>'raw'
  ]
];

echo GridView::widget([
  'dataProvider' => $dataProvider,
  'columns' => $gridColumns,
  'containerOptions' => ['style' => 'overflow: auto'],
  'pjax' => true,
  'beforeHeader' => [
    [
      'options' => ['class' => 'skip-export']
    ]
  ],
  'export' => [
    'fontAwesome' => true
  ],
  'bordered' => true,
  'striped' => true,
  'condensed' => true,
  'responsive' => true,
  'hover' => true,
  'showPageSummary' => false,
  'persistResize' => false,
]);
