<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;

$dataProvider = new ArrayDataProvider([
  'allModels' => $row,
  'key' => 'id'
]);
$gridColumns = [
  ['class' => 'yii\grid\SerialColumn'],
  ['attribute' => 'id', 'visible' => false],
  [
    'attribute' => 'servicio.numero',
    'label' => 'Servicio'
  ],
  'fecha:date',
  'monto:currency',
  'arreglado',
  [
    'class' => 'kartik\grid\ActionColumn',
    'template' => "{view}",
    'buttons' => [
      'view' => function ($url, \app\models\EmpleadoComision $model) {
        return Html::a('<span class="glyphicon glyphicon-copy"></span> Orden N° ' .
          $model->servicio->numero, ['/servicio/view','id'=>$model->servicio_id], ['class' => 'btn btn-default', 'title' => 'Ver orden de servicio N°'
          . $model->servicio->numero,'target'=>'_black','data-pjax'=>0]);
      },
    ]
  ],
];

echo GridView::widget([
  'dataProvider' => $dataProvider,
  'columns' => $gridColumns,
  'containerOptions' => ['style' => 'overflow: auto'],
  'pjax' => true,
  'beforeHeader' => [
    [
      'options' => ['class' => 'skip-export']
    ]
  ],
  'export' => [
    'fontAwesome' => true
  ],
  'bordered' => true,
  'striped' => true,
  'condensed' => true,
  'responsive' => true,
  'hover' => true,
  'showPageSummary' => false,
  'persistResize' => false,
]);
