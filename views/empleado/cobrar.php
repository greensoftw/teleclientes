<?php

use app\models\Empleado;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var $model Empleado
 * @var $cobro \app\models\CobroEmpleado
 * @var $ultimoCobro \app\models\CobroEmpleado
 * @var $cobroTotal int
 * @var $cobros \app\models\AbonoServicio[]
 */

$this->title = "Empleado : " . $model->usuario->nombres;
$this->params['breadcrumbs'][] = ['label' => 'Empleados', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => "Empleado : " . $model->usuario->nombres, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empleado-view row">
  <div class="col-lg-6">
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      [
        'attribute' => 'usuario.nombres',
        'label' => 'Usuario',
      ],
      'telefono',
      'tipo_empleado',
      'sueldo',
      'proximo_pago:date',
      'saldo',
      'is_tecnico:boolean',
      'perfil',
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>
  <div class="col-lg-6">
    <?php if ($ultimoCobro != null): ?>
      <?php $gridColumn = [
        'fecha:date',
        'monto:currency',
        'saldo:currency',
      ];
      echo DetailView::widget([
        'model' => $ultimoCobro,
        'attributes' => $gridColumn
      ]); ?>
    <?php else: ?>
      <div class="alert alert-info">El empleado no tiene cobros previos</div>
    <?php endif; ?>
    <?php $form = ActiveForm::begin([
      'enableAjaxValidation' => true
    ]) ?>
    <div class="form-group">
      <p class="text-success">
        <?php if ($ultimoCobro != null and $ultimoCobro->saldo > 0): ?>
        El Empleado debe un saldo de: <?= Yii::$app->formatter->asCurrency($ultimoCobro->saldo) ?>
          Mas un total de: <?= Yii::$app->formatter->asCurrency($cobroTotal) ?>
        <?php else: ?>
          Total : <?= Yii::$app->formatter->asCurrency($cobroTotal) ?>
        <?php endif; ?>
      </p>
      <h3 class="text-primary">Total para pagar: <?= Yii::$app->formatter->asCurrency($cobroTotal) ?></h3>
    </div>
    <?= $form->field($cobro, 'monto') ?>
    <?= Html::submitButton('Confirmar', ['class' => 'btn btn-primary']) ?>
    <?php ActiveForm::end() ?>
  </div>
  <div class="col-lg-12 row">
    <?= $this->render('_dataAbonoServicio', [
      'row' => $cobros
    ]) ?>
  </div>
</div>
