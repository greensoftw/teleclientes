<?php
use kartik\tabs\TabsX;
use yii\helpers\Html;

/**
 * @var $model \app\models\Empleado
 */
$items = [
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Detalle del Empleado'),
    'content' => $this->render('_detail', [
      'model' => $model,
    ]),
  ],
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('historial de pagos'),
    'content' => $this->render('_dataPagoEmpleado', [
      'model' => $model,
      'row' => $model->pagoEmpleados,
    ]),
  ],
];
?>
<?php if ($model->is_tecnico) {
  $items[] = [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Servicios pendiente (' .
        $model->getServicios()->where(['cerrado' => 0])->count() . ")"),
    'content' => $this->render('_dataServicio', [
      'model' => $model,
      'row' => $model->getServicios()->where(['cerrado' => 0])->all(),
    ]),
  ];
  $items[] = [
    'label' => '<i class="glyphicon glyphicon-book"></i> Pagos por entregar a la empresa',
    'content' => $this->render('_dataAbonoServicio', [
      'row' => $model->getAbonoServicios()->where(['is', 'cobro_empleado_id', null])->all()
    ])
  ];
  $items[] = [
    'label' => '<i class="glyphicon glyphicon-book"></i> Historial de entregas',
    'content' => $this->render('_dataCobroEmpleado', [
      'row' => $model->cobroEmpleados,
    ])
  ];
} ?>

<div class="panel">
  <div class="panel-body">
    <?= TabsX::widget([
      'items' => $items,
      'position' => TabsX::POS_ABOVE,
      'encodeLabels' => false,
      'class' => 'tes',
      'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
      ],
    ]);
    ?>    </div>
</div>
