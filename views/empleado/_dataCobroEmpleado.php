<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

/**
 * @var $model \app\models\base\Empleado
 * @var $row \app\models\CobroEmpleado[]
 */
$dataProvider = new ArrayDataProvider([
  'allModels' => $row,
  'key' => 'id'
]);
$gridColumns = [
  ['class' => 'yii\grid\SerialColumn'],
  ['attribute' => 'id', 'visible' => false],
  'fecha:datetime',
  'monto:currency',
  'saldo:currency'
];

echo GridView::widget([
  'dataProvider' => $dataProvider,
  'columns' => $gridColumns,
  'containerOptions' => ['style' => 'overflow: auto'],
  'pjax' => true,
  'beforeHeader' => [
    [
      'options' => ['class' => 'skip-export']
    ]
  ],
  'export' => [
    'fontAwesome' => true
  ],
  'bordered' => true,
  'striped' => true,
  'condensed' => true,
  'responsive' => true,
  'hover' => true,
  'showPageSummary' => false,
  'persistResize' => false,
]);
