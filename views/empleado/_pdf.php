<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Empleado */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Empleado', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empleado-view">

  <div class="row">
    <div class="col-sm-9">
      <h2><?= 'Empleado' . ' ' . Html::encode($this->title) ?></h2>
    </div>
  </div>

  <div class="row">
    <?php
    $gridColumn = [
      [
        'attribute' => 'usuario.nombres',
        'label' => 'Usuario',
      ],
      [
        'attribute' => 'usuario.email',
        'label' => 'Correo',
      ],
      'direccion',
      'telefono',
      'tipo_empleado',
      'percent:percent',
      'sueldo',
      'proximo_pago:date',
      'saldo',
      'is_tecnico:boolean',
      'perfil',
    ];
    if ($model->is_tecnico) {
      $gridColumn[] = [
        'label' => 'Total de comisiones ganadas',
        'value' => $model->getEmpleadoComisions()->where(['arreglado' => 0])->sum('monto'),
        'format' => 'currency'
      ];
      $gridColumn[] = [
        'label' => 'Total de Dinero recogido por entregar',
        'value' => $model->getDeuda(),
        'format' => 'currency'
      ];
      $gridColumn[] = [
        'label' => 'Saldo de cobro anterior',
        'value' => $model->getSaldoDebe(),
        'format' => 'currency'
      ];
    }
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>

  <div class="row">
    <?php
    if ($providerServicio->totalCount) {
      $gridColumnServicio = [

        ['attribute' => 'id', 'visible' => false],
        'numero',
        [
          'attribute' => 'cliente.usuario.nombres',
          'label' => 'Cliente'
        ],
        [
          'attribute' => 'comisionServicio.comisionista.usuario.nombres',
          'label' => 'Comision Servicio'
        ],
        'total:currency',
        'pagado:boolean',
        'no_facturado',
        'cerrado:boolean',
        [
          'attribute' => 'marca.nombre',
          'label' => 'Garantia',
        ],
        'numero_garantia',
        'fecha_tecnico:datetime',
      ];
      echo Gridview::widget([
        'dataProvider' => $providerServicio,
        'panel' => [
          'type' => GridView::TYPE_ACTIVE,
          'heading' => Html::encode('Servicio'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnServicio
      ]);
    }
    ?>
  </div>
  <div class="row">
    <?php
    if ($providerAbonoServicio->totalCount) {
      $gridColumnAbonoServicio = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
          'attribute' => 'servicio.id',
          'label' => 'Servicio'
        ],
        'monto',
        'fecha',
        [
          'attribute' => 'cobroEmpleado.id',
          'label' => 'Cobro Empleado'
        ],
      ];
      echo Gridview::widget([
        'dataProvider' => $providerAbonoServicio,
        'panel' => [
          'type' => GridView::TYPE_ACTIVE,
          'heading' => Html::encode('Historial de pagos recibidos'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnAbonoServicio
      ]);
    }
    ?>
  </div>

  <div class="row">
    <?php
    if ($providerCobroEmpleado->totalCount) {
      $gridColumnCobroEmpleado = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'fecha',
        'monto',
        'saldo',
      ];
      echo Gridview::widget([
        'dataProvider' => $providerCobroEmpleado,
        'panel' => [
          'type' => GridView::TYPE_ACTIVE,
          'heading' => Html::encode('Historial de entregas de dinero'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnCobroEmpleado
      ]);
    }
    ?>
  </div>

  <div class="row">
    <?php
    if ($providerEmpleadoComision->totalCount) {
      $gridColumnEmpleadoComision = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
          'attribute' => 'servicio.numero',
          'label' => 'Servicio'
        ],
        [
          'attribute' => 'pagoEmpleado.id',
          'label' => 'Pago Empleado'
        ],
        'fecha',
        'monto',
        'arreglado',
      ];
      echo Gridview::widget([
        'dataProvider' => $providerEmpleadoComision,
        'panel' => [
          'type' => GridView::TYPE_ACTIVE,
          'heading' => Html::encode('Comisiones Ganadas'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnEmpleadoComision
      ]);
    }
    ?>
  </div>

  <div class="row">
    <?php
    if ($providerPagoEmpleado->totalCount) {
      $gridColumnPagoEmpleado = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'fecha',
        'monto',
        'saldo_anterior',
        'comentario',
      ];
      echo Gridview::widget([
        'dataProvider' => $providerPagoEmpleado,
        'panel' => [
          'type' => GridView::TYPE_ACTIVE,
          'heading' => Html::encode('Historial de pagos'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnPagoEmpleado
      ]);
    }
    ?>
  </div>
</div>
