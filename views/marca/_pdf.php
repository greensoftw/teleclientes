<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Marca */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Marcas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marca-view">

  <div>
    <div class="col-sm-9">
      <h3><?= 'Marca' . ': ' . Html::encode($this->title) ?></h3>
    </div>
  </div>

  <div>
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      [
        'attribute' => 'usuario.nombres',
        'label' => 'Usuario'
      ],
      'nombre',
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>

  <div>
    <?php
    if ($providerCodigoIsis->totalCount) {
      $gridColumnCodigoIsis = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'codigo',
        'descripcion',
      ];
      echo Gridview::widget([
        'dataProvider' => $providerCodigoIsis,
        'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => Html::encode('Código de Error'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnCodigoIsis
      ]);
    }
    ?>
  </div>

  <div>
    <?php
    if ($providerEquipo->totalCount) {
      $gridColumnEquipo = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
          'attribute' => 'cliente.id',
          'label' => 'Cliente'
        ],
        [
          'attribute' => 'distribuidor.id',
          'label' => 'Distribuidor'
        ],
        'modelo',
        'serial',
        'nota',
      ];
      echo Gridview::widget([
        'dataProvider' => $providerEquipo,
        'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => Html::encode('Equipo'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnEquipo
      ]);
    }
    ?>
  </div>

  <div>
    <?php
    if ($providerMarcaDepartamento->totalCount) {
      $gridColumnMarcaDepartamento = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'nombre',
        'email:email',
      ];
      echo Gridview::widget([
        'dataProvider' => $providerMarcaDepartamento,
        'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => Html::encode('Marca Departamento'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnMarcaDepartamento
      ]);
    }
    ?>
  </div>
</div>
