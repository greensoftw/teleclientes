<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\MarcaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = 'Marcas';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="marca-index row">
  <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

  <p>
    <?= Html::a('Busqueda avanzada', '#', ['class' => 'btn btn-info search-button']) ?>
  </p>
  <div class="search-form" style="display:<?= Yii::$app->request->get($searchModel->formName()) ? 'block' : 'none' ?>">
    <?= $this->render('_search', ['model' => $searchModel]); ?>
  </div>
  <?php
  $gridColumn = [
    [
      'class' => 'kartik\grid\ExpandRowColumn',
      'width' => '50px',
      'value' => function ($model, $key, $index, $column) {
        return GridView::ROW_COLLAPSED;
      },
      'detail' => function ($model, $key, $index, $column) {
        return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
      },
      'headerOptions' => ['class' => 'kartik-sheet-style'],
      'expandOneOnly' => true
    ],
    ['attribute' => 'id', 'visible' => false],
    [
      'attribute' => 'usuario_id',
      'label' => 'Usuario',
      'value' => function ($model) {
        return $model->usuario->nombres;
      },
      'filterType' => GridView::FILTER_SELECT2,
      'filter' => \yii\helpers\ArrayHelper::map(\app\models\Usuario::find()
        ->innerJoinWith('marca')->orderBy('nombres')
        ->asArray()->all(), 'id', 'nombres'),
      'filterWidgetOptions' => [
        'pluginOptions' => ['allowClear' => true],
      ],
      'filterInputOptions' => ['placeholder' => 'Usuario', 'id' => 'grid-marca-search-usuario_id']
    ],
    'nombre',
    'direccion',
    'telefonos',
    'ciudad',
    [
      'class' => 'kartik\grid\ActionColumn',
      'template'=>mdm\admin\components\Helper::filterActionColumn("{view}{update}")
    ],
  ];
  ?>
  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $gridColumn,
    'pjax' => true,
    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-marca']],
    'panel' => [
      'type' => GridView::TYPE_PRIMARY,
      'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
    ],
    // your toolbar can include the additional full export menu
    'toolbar' => [
      '{export}',
      ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumn,
        'target' => ExportMenu::TARGET_BLANK,
        'fontAwesome' => true,
        'dropdownOptions' => [
          'label' => 'Todo',
          'class' => 'btn btn-default',
          'itemsBefore' => [
            '<li class="dropdown-header">Exportar todo el contenido</li>',
          ],
        ],
      ]),
    ],
  ]); ?>

</div>
