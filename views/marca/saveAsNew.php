<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Marca */
/* @var $modelUser app\models\Usuario */

$this->title = 'Crear una copia Marca: '. ' ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Marcas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Crear una copia';
?>
<div class="marca-create row">
    <?= $this->render('_form', [
        'model' => $model,
        'modelUser' => $modelUSer,
    ]) ?>
</div>
