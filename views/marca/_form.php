<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Marca */
/* @var $modelUser app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos' => \yii\web\View::POS_END,
  'viewParams' => [
    'class' => 'CodigoIsis',
    'relID' => 'codigo-isis',
    'value' => \yii\helpers\Json::encode($model->codigoIses),
    'isNewRecord' => ($model->isNewRecord) ? 1 : 0
  ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos' => \yii\web\View::POS_END,
  'viewParams' => [
    'class' => 'MarcaDepartamento',
    'relID' => 'marca-departamento',
    'value' => \yii\helpers\Json::encode($model->marcaDepartamentos),
    'isNewRecord' => ($model->isNewRecord) ? 1 : 0
  ]
]);
?>

<div class="marca-form">

  <?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true
  ]); ?>

  <?= $form->errorSummary($model); ?>

  <?= $form->field($modelUser, 'nombres')
    ->label('Nombres del Gerente de la marca')
    ->textInput(['maxlength' => true, 'placeholder' => 'Nombres del Gerente de la marca']) ?>

  <?= $form->field($modelUser, 'email')->textInput(['maxlength' => true, 'placeholder' => 'email']) ?>

  <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'placeholder' => 'Nombre de la marca']) ?>

  <?= $form->field($model, 'ciudad')->textInput(['maxlength' => true, 'placeholder' => 'Ciudad de la marca']) ?>

  <?= $form->field($model, 'direccion')->textInput(['maxlength' => true, 'placeholder' => 'Dirección de la marca']) ?>

  <?= $form->field($model, 'telefonos')->textInput(['maxlength' => true, 'placeholder' => 'Télefonos de la marca']) ?>

  <?php
  $forms = [
    [
      'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Codigos de Error'),
      'content' => $this->render('_formCodigoIsis', [
        'row' => \yii\helpers\ArrayHelper::toArray($model->codigoIses),
      ]),
    ],
    [
      'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Departamentos'),
      'content' => $this->render('_formMarcaDepartamento', [
        'row' => \yii\helpers\ArrayHelper::toArray($model->marcaDepartamentos),
      ]),
    ],
  ];
  echo kartik\tabs\TabsX::widget([
    'items' => $forms,
    'position' => kartik\tabs\TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'pluginOptions' => [
      'bordered' => true,
      'sideways' => true,
      'enableCache' => false,
    ],
  ]);
  ?>
  <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?= Html::a('Cancelar', Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
