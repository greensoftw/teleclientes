<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Marca */
/* @var $modelUser app\models\Usuario */

$this->title = 'Crear Marca';
$this->params['breadcrumbs'][] = ['label' => 'Marcas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marca-create row">
    <?= $this->render('_form', [
        'model' => $model,
        'modelUser' => $modelUser
    ]) ?>
</div>
