<div class="form-group" id="add-codigo-isis">
<?php
use kartik\builder\TabularForm;
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;

$dataProvider = new ArrayDataProvider([
  'allModels' => $row,
  'pagination' => [
    'pageSize' => -1
  ]
]);
echo TabularForm::widget([
  'dataProvider' => $dataProvider,
  'formName' => 'CodigoIsis',
  'checkboxColumn' => false,
  'actionColumn' => false,
  'attributeDefaults' => [
    'type' => TabularForm::INPUT_TEXT,
  ],
  'attributes' => [
    "id" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
    'tipo_equipo_id' => [
      'label' => 'Tipo equipo',
      'type' => TabularForm::INPUT_WIDGET,
      'widgetClass' => \kartik\widgets\Select2::className(),
      'options' => [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\TipoEquipo::find()->orderBy('id')->asArray()->all(), 'id',
          'nombre'),
        'options' => ['placeholder' => 'Seleccione un tipo de equipo'],
      ],
      'columnOptions' => ['width' => '200px']
    ],
    'codigo' => ['type' => TabularForm::INPUT_TEXT],
    'descripcion' => ['type' => TabularForm::INPUT_TEXT],
  ],
  'gridSettings' => [
    'panel' => [
      'heading' => false,
      'type' => GridView::TYPE_DEFAULT,
      'before' => false,
      'footer' => false,
      'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Agregar Codigo de Error', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowCodigoIsis()']),
    ]
  ]
]);
echo "    </div>\n\n";
?>