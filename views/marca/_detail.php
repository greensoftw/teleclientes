<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Marca */

?>
<div class="marca-view">

  <div class="row">
    <div class="col-sm-9">
      <h3><?= Html::encode($model->nombre) ?></h3>
    </div>
  </div>

  <div class="row">
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      [
        'attribute' => 'usuario.nombres',
        'label' => 'Usuario',
      ],
      [
        'attribute' => 'usuario.email',
        'label' => 'Usuario',
      ],
      'nombre',
      'direccion',
      'ciudad',
      'telefonos'
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>
</div>