<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Marca */
/* @var $providerCodigoIsis */
/* @var $providerEquipo */
/* @var $providerUsuario */
/* @var $providerMarcaDepartamento */
$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Marcas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marca-view row">

  <div>
    <div class="form-group">
      <?=
      Html::a('<i class="fa fa-file"></i> ' . 'PDF',
        ['pdf', 'id' => $model->id],
        [
          'class' => 'btn btn-danger',
          'target' => '_blank',
          'data-toggle' => 'tooltip',
          'title' => 'se generara en una nueva ventana'
        ]
      ) ?>
      <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </div>
  </div>

  <div>
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      [
        'attribute' => 'usuario.nombres',
        'label' => 'Usuario',
      ],
      [
        'attribute' => 'usuario.email',
        'label' => 'Correo',
      ],
      'nombre',
      'direccion',
      'ciudad',
      'telefonos'
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>

  <div>
    <?php
    if ($providerCodigoIsis->totalCount) {
      $gridColumnCodigoIsis = [
        ['attribute' => 'id', 'visible' => false],
        'codigo',
        'descripcion',
      ];
      echo GridView::widget([
        'dataProvider' => $providerCodigoIsis,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-codigo-isis']],
        'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Código de Error'),
        ],
        'columns' => $gridColumnCodigoIsis
      ]);
    }
    ?>
  </div>

  <div>
    <?php
    if ($providerEquipo->totalCount) {
      $gridColumnEquipo = [
        ['attribute' => 'id', 'visible' => false],
        [
          'attribute' => 'cliente.id',
          'label' => 'Cliente'
        ],
        [
          'attribute' => 'distribuidor.id',
          'label' => 'Distribuidor'
        ],
        'modelo',
        'serial',
        'nota',
      ];
      echo GridView::widget([
        'dataProvider' => $providerEquipo,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-equipo']],
        'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Equipo'),
        ],
        'columns' => $gridColumnEquipo
      ]);
    }
    ?>
  </div>

  <div>
    <?php
    if ($providerMarcaDepartamento->totalCount) {
      $gridColumnMarcaDepartamento = [
        ['attribute' => 'id', 'visible' => false],
        'nombre',
        'email:email',
      ];
      echo GridView::widget([
        'dataProvider' => $providerMarcaDepartamento,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-marca-departamento']],
        'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Marca Departamento'),
        ],
        'columns' => $gridColumnMarcaDepartamento
      ]);
    }
    ?>
  </div>
</div>
