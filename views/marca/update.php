<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Marca */
/* @var $modelUser app\models\Usuario */

$this->title = 'Actualizar Marca: ' . ' ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Marcas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="marca-update row">
    <?= $this->render('_form', [
        'model' => $model,
        'modelUser'=>$modelUser
    ]) ?>
</div>
