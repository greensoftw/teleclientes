<?php
use kartik\tabs\TabsX;
use yii\helpers\Html;

$items = [
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Marca'),
    'content' => $this->render('_detail', [
      'model' => $model,
    ]),
  ],
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Código de Error'),
    'content' => $this->render('_dataCodigoIsis', [
      'model' => $model,
      'row' => $model->codigoIses,
    ]),
  ],
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Departamentos'),
    'content' => $this->render('_dataMarcaDepartamento', [
      'model' => $model,
      'row' => $model->marcaDepartamentos,
    ]),
  ],
];
?>
<div class="panel">
  <div class="panel-body">
    <?= TabsX::widget([
      'items' => $items,
      'position' => TabsX::POS_ABOVE,
      'encodeLabels' => false,
      'class' => 'tes',
      'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
      ],
    ]);
    ?>    </div>
</div>
