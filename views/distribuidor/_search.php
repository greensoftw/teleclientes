<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\DistribuidorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-distribuidor-search">
  <div class="form-group row">
    <?php $form = ActiveForm::begin([
      'action' => ['index'],
      'method' => 'get',
      'fieldConfig' => ['template' => '<div class="col-lg-3">{label} {input}</div>']
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'usuario_id')->widget(\kartik\widgets\Select2::className(), [
      'data' => \yii\helpers\ArrayHelper::map(\app\models\Usuario::find()
        ->innerJoinWith('distribuidor')
        ->orderBy('nombre')->asArray()->all(), 'id', 'nombres'),
      'options' => ['placeholder' => 'Seleccione Usuario'],
      'pluginOptions' => [
        'allowClear' => true
      ],
    ]); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'placeholder' => 'Nombre']) ?>

    <?= $form->field($model, 'direccion')->textInput(['maxlength' => true, 'placeholder' => 'Direccion']) ?>

    <?= $form->field($model, 'telefono')->textInput(['maxlength' => true, 'placeholder' => 'Telefono']) ?>
  </div>
  <div class="form-group">
    <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
