<?php
/**
 * @var $model \app\models\base\Distribuidor
 */
use kartik\tabs\TabsX;
use yii\helpers\Html;

$items = [
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Detalles del distribuidor'),
    'content' => $this->render('_detail', [
      'model' => $model,
    ]),
  ],
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Comisionistas'),
    'content' => $this->render('_dataComisionista', [
      'model' => $model,
      'row' => $model->comisionistas,
    ]),
  ],
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Departamentos'),
    'content' => $this->render('_dataDistribuidorDepartamento', [
      'model' => $model,
      'row' => $model->distribuidorDepartamentos,
    ]),
  ],
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Equipos'),
    'content' => $this->render('_dataEquipo', [
      'model' => $model,
      'row' => $model->equipos
    ]),
  ],
];
?>
<div class="panel">
  <div class="panel-body">
    <?= TabsX::widget([
      'items' => $items,
      'position' => TabsX::POS_ABOVE,
      'encodeLabels' => false,
      'class' => 'tes',
      'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
      ],
    ]); ?>
  </div>
</div>
