<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Distribuidor */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Distribuidores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="distribuidor-view">

  <div class="row">
    <div class="button-group">
      <?=
      Html::a('<i class="fa fa-file"></i> ' . 'PDF',
        ['pdf', 'id' => $model->id],
        [
          'class' => 'btn btn-danger',
          'target' => '_blank',
          'data-toggle' => 'tooltip',
          'title' => 'Will open the generated PDF file in a new window'
        ]
      ) ?>
      <?= Html::a('actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </div>
  </div>

  <div class="row">
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      [
        'attribute' => 'usuario.nombres',
        'label' => 'Usuario',
      ],
      'nombre',
      'direccion',
      'telefono',
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>

  <div class="row">
    <?php
    if ($providerComisionista->totalCount) {
      $gridColumnComisionista = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
          'attribute' => 'usuario.nombres',
          'label' => 'Usuario'
        ],
        'telefonos',
      ];
      echo GridView::widget([
        'dataProvider' => $providerComisionista,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-comisionista']],
        'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Comisionista'),
        ],
        'columns' => $gridColumnComisionista
      ]);
    }
    ?>
  </div>

  <div class="row">
    <?php
    if ($providerDistribuidorDepartamento->totalCount) {
      $gridColumnDistribuidorDepartamento = [
        ['attribute' => 'id', 'visible' => false],
        'nombre',
        'email:email',
      ];
      echo GridView::widget([
        'dataProvider' => $providerDistribuidorDepartamento,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-distribuidor-departamento']],
        'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Departamento'),
        ],
        'columns' => $gridColumnDistribuidorDepartamento
      ]);
    }
    ?>
  </div>
</div>
