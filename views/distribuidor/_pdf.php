<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Distribuidor */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Distribuidores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="distribuidor-view">

  <div class="row">
    <div class="col-sm-9">
      <h2><?= 'Distribuidor : ' . ' ' . Html::encode($this->title) ?></h2>
    </div>
  </div>

  <div class="row">
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      [
        'attribute' => 'usuario.nombres',
        'label' => 'Usuario'
      ],
      'nombre',
      'direccion',
      'telefono',
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>

  <div class="row">
    <?php
    if ($providerComisionista->totalCount) {
      $gridColumnComisionista = [
        ['attribute' => 'id', 'visible' => false],
        [
          'attribute' => 'usuario.nombres',
          'label' => 'Usuario'
        ],
        'nombres',
        'telefonos',
      ];
      echo GridView::widget([
        'dataProvider' => $providerComisionista,
        'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => Html::encode('Comisionista'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>',
        'toggleData' => false,
        'columns' => $gridColumnComisionista
      ]);
    }
    ?>
  </div>

  <div class="row">
    <?php
    if ($providerDistribuidorDepartamento->totalCount) {
      $gridColumnDistribuidorDepartamento = [
        ['attribute' => 'id', 'visible' => false],
        'nombre',
        'email:email',
      ];
      echo GridView::widget([
        'dataProvider' => $providerDistribuidorDepartamento,
        'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => Html::encode('Distribuidor Departamento'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>',
        'toggleData' => false,
        'columns' => $gridColumnDistribuidorDepartamento
      ]);
    }
    ?>
  </div>
</div>
