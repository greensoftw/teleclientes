<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Distribuidor */
/* @var $modelUser app\models\Usuario */

$this->title = 'Crear Distribuidor';
$this->params['breadcrumbs'][] = ['label' => 'Distribuidores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="distribuidor-create row">
    <?= $this->render('_form', [
        'model' => $model,
        'modelUser' => $modelUser
    ]) ?>
</div>
