<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Distribuidor */
/* @var $modelUser app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos' => \yii\web\View::POS_END,
  'viewParams' => [
    'class' => 'Comisionista',
    'relID' => 'comisionista',
    'value' => \yii\helpers\Json::encode($model->comisionistas),
    'isNewRecord' => ($model->isNewRecord) ? 1 : 0
  ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos' => \yii\web\View::POS_END,
  'viewParams' => [
    'class' => 'DistribuidorDepartamento',
    'relID' => 'distribuidor-departamento',
    'value' => \yii\helpers\Json::encode($model->distribuidorDepartamentos),
    'isNewRecord' => ($model->isNewRecord) ? 1 : 0
  ]
]);
?>

<div class="distribuidor-form">

  <?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true
  ]); ?>

  <?= $form->errorSummary($model); ?>

  <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

  <?= $form->field($modelUser, 'nombres')->textInput(['maxlength' => true, 'placeholder' => 'Nombre del dueño o administrador del establecimiento'])
    ->label('Nombre del dueño o administrador del establecimiento') ?>

  <?= $form->field($modelUser, 'email')->textInput(['maxlength' => true, 'placeholder' => 'Email']) ?>

  <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'placeholder' => 'Nombre de establecimiento comercial'])
    ->label('Nombre de establecimiento comercial') ?>

  <?= $form->field($model, 'direccion')->textInput(['maxlength' => true, 'placeholder' => 'Direccion']) ?>

  <?= $form->field($model, 'telefono')->textInput(['maxlength' => true, 'placeholder' => 'Telefono']) ?>

  <?php
  $forms = [
    [
      'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Departamentos'),
      'content' => $this->render('_formDistribuidorDepartamento', [
        'row' => \yii\helpers\ArrayHelper::toArray($model->distribuidorDepartamentos),
      ]),
    ]
  ];
  echo kartik\tabs\TabsX::widget([
    'items' => $forms,
    'position' => kartik\tabs\TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'pluginOptions' => [
      'bordered' => true,
      'sideways' => true,
      'enableCache' => false,
    ],
  ]);
  ?>
  <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?= Html::a('Cancelar', Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
