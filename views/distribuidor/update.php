<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Distribuidor */
/* @var $modelUser app\models\Usuario */

$this->title = 'actualizar Distribuidor : ' . ' ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Distribuidors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="distribuidor-update row">
    <?= $this->render('_form', [
        'model' => $model,
        'modelUser' => $modelUser
    ]) ?>
</div>
