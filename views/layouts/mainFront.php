<?php
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = false;
\app\assets\FrontAsset::register($this);
if (Yii::$app->controller->action->getUniqueId() == 'sitio/index'){
  $this->registerJsFile('@web/theme/js/navigation/navbar.js',[
    'position' => $this::POS_END,
    'depends' => [\yii\web\JqueryAsset::className()]
  ]);
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<div id="loading" class="loading-invisible">
    <i class="pe-7s-refresh pe-spin pe-3x pe-va"></i><br/>
    <p>Please wait...</p>
</div>
<?php $this->beginBody() ?>
<?php
NavBar::begin([
    'brandLabel' =>
        Html::img('@web/theme/img/logo.png', ['alt' => 'Teleclientes.com', 'class' => 'default-logo', 'style' => '']).
        Html::img('@web/theme/img/logo01.png', ['alt' => 'Teleclientes.com', 'class' => 'scroller-logo', 'style' => '']),
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar navbar-default navbar-fixed-top',
    ],
]);
echo Nav::widget([
    'encodeLabels' => false,
    'options' => ['class' => 'nav navbar-nav navbar-right'],
    'items' => [
        ['label' => 'inicio', 'url' => ['sitio/index#home'], 'linkOptions' => ['id' => 'GoToHome','data-id'=>'#home']],
        ['label' => 'Visión general','url'=>['sitio/index#description'],'linkOptions' => ['id' => 'GoToDescription','data-id'=>'#description']],
        ['label' => 'Caracteristicas','url'=>['sitio/index#features'],'linkOptions' => ['id' => 'GoToFeatures','data-id'=>'#features']],
        ['label' => 'Precio','url'=>['sitio/index#pricing'],'linkOptions' => ['id' => 'GoToPricing','data-id'=>'#pricing']],
        ['label' => 'Contactenos','url'=>['sitio/index#contact'],'linkOptions' => ['id' => 'GoToContact','data-id'=>'#contact']],

        ['label' => 'Ingreso clientes','url'=>['sitio/login'],'linkOptions' => ['class' => 'btn btn-danger']],
    ],
]); ?>
<?php NavBar::end(); ?>
<?php if (($msm = Yii::$app->session->getAllFlashes()) !== null): ?>
    <?php foreach ($msm as $type => $menssage): ?>
        <?php $type = explode(',', $type)[0] ?>
        <div class="alert alert-<?php echo $type ?> fade in">
            <button data-dismiss="alert" class="close" type="button">
                <i class="fa fa-remove"></i>
            </button><?php echo $menssage ?>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
<?= $content ?>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-6 widget">
                <h5>Sobre nosotros</h5>
                <p>
                    somos una empresa comprometida con el servicio ....
                </p>
                <ul class="social-link">
                    <li><a href="http://facebook.com/teleclientes"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="http://facebook.com/telecliente"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="http://facebook.com/telecliente"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="http://facebook.com/telecliente"><i class="fa fa-google-plus"></i></a></li>
                </ul>
            </div>
            <div class="col-md-6 widget">
                <h5>Conoce mas.</h5>
                <ul class="footer-link">
                    <li><a href="#">Caracteristicas</a></li>
                    <li><a href="#">Testimonios</a></li>
                    <li><a href="#">Nuestr equipo</a></li>
                    <li><a href="#">Afiliate</a></li>
                    <li><a href="#">Centro de ayuda</a></li>
                    <li><a href="#">Politica de privacidad</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="subfooter">
        <p><?= date('Y') ?> © Copyright <a href="http://greensoftw.com/">teleclientes.com</a> Todos los derechos reservados.</p>
    </div>
</footer>
<script type="text/javascript">
    document.getElementById("loading").className = "loading-visible";
    var hideDiv = function(){document.getElementById("loading").className = "loading-invisible";};
    var oldLoad = window.onload;
    var newLoad = oldLoad ? function(){hideDiv.call(this);oldLoad.call(this);} : hideDiv;
    window.onload = newLoad;
</script>
<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>
