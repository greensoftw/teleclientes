<?php
/**
 * content.php
 *
 * @copyright Copyright &copy; Pedro Plowman, https://github.com/p2made, 2016
 * @author Pedro Plowman
 * @package p2made/yii2-sb-admin-theme
 * @license MIT
 * @var $content string
 */

use p2made\widgets\Alert;
use yii\widgets\Breadcrumbs;

?>
<div id="page-wrapper">
  <div class="container-fluid">
    <header class="row">
      <div>
        <h4 class="page-header"><?php echo $this->title; ?></h4>
        <?= Breadcrumbs::widget([
          'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
          'options' => ['class' => 'breadcrumb'],
          'homeLink' => ['label' => 'Inicio', 'url' => ['/site/index']]
        ]) ?>
        <?= Alert::widget() ?>
      </div>
    </header>
    <?= $content ?>
  </div>
</div>
