<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?= Html::csrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>
  <?php $this->head() ?>
</head>
<body style="width:100%;  background:url('../images/fondoazul.jpg') center center;background-size: cover;">
<?php $this->beginBody() ?>

<div class="wrap">
  <?php
  NavBar::begin([
    'brandLabel' => Html::img("@web/theme/img/logo01.png", ['style' => 'width: 300px;', 'class' => 'img-thumbnail']),
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
      'class' => 'navbar navbar-default navbar-static-top',
    ],
    'containerOptions' => [
      'style' => 'min-height: 65px;'
    ]
  ]);
  echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right pull-right', 'style' => 'margin-top: 2%;'],

  ]);
  NavBar::end();
  ?>
  <div class="container">
    <?= $content ?>
  </div>
</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
