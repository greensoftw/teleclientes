<?php
/**
 * navigation-left.php
 *
 * @copyright Copyright &copy; Pedro Plowman, https://github.com/p2made, 2016
 * @author Pedro Plowman
 * @package p2made/yii2-sb-admin-theme
 * @license MIT
 */

use mdm\admin\components\MenuHelper;
use p2made\helpers\FA;
use yii\bootstrap\Html;
use yii\bootstrap\Nav;

$arrowIcon = FA::i('arrow')->tag('span');
?>
<section class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li class="sidebar-search">
<!--        <div class="input-group custom-search-form">-->
<!--          <input type="text" class="form-control" placeholder="Search...">-->
<!--          <span class="input-group-btn">-->
<!--						<button class="btn btn-default" type="button">-->
<!--							<i class="fa fa-search"></i>-->
<!--						</button>-->
<!--					</span>-->
<!--        </div>-->
        <?php /** @var \app\models\Empresa $conf */
        $conf = Yii::$app->controller->empresa_config; ?>
        <?= Html::img("@web/$conf->picture",['class'=>'img-responsive']) ?>
      </li>
      <li>
        <?= Html::a('Inicio', ['/site/access']) ?>
      </li>
      <?= Nav::widget([
        'items' => MenuHelper::getAssignedMenu(Yii::$app->user->id, null, null, true),
      ]);
      ?>
      <li>
        <?= Html::a('Cambiar Contraseña', ['/cuenta/contrasena']) ?>
      </li>
      <li>
        <?= Html::a(FA::fw('table') . 'salir', ['/site/logout'], ['data-method' => 'post']) ?>
      </li>
    </ul>
  </div>
</section>
