<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ComisionistaServicio */

$this->title = 'Registrar servicio Servicio';
$this->params['breadcrumbs'][] = ['label' => 'Servicios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comisionista-servicio-create row">
  <p><?= \kartik\helpers\Html::a('regresar',['index'],['class'=>'btn btn-primary']) ?></p>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
