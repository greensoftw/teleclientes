<?php
/**
 * Created by PhpStorm.
 * User: jhon
 * Date: 22/02/17
 * Time: 09:50 PM
 * @var $provider \yii\data\ActiveDataProvider
 */
use kartik\grid\GridView;
$gridColumns = [
  ['class' => 'yii\grid\SerialColumn'],
  ['attribute' => 'id', 'visible' => false],
  'fecha:date',
  'monto:currency',
];
?>
<div class="row">
  <p><?= \kartik\helpers\Html::a('regresar',['index'],['class'=>'btn btn-primary']) ?></p>
  <div>
    <?= GridView::widget([
      'dataProvider' => $provider,
      'columns' => $gridColumns,
      'containerOptions' => ['style' => 'overflow: auto'],
      'pjax' => true,
      'beforeHeader' => [
        [
          'options' => ['class' => 'skip-export']
        ]
      ],
      'export' => false,
      'bordered' => true,
      'striped' => true,
      'condensed' => true,
      'responsive' => true,
      'hover' => true,
      'showPageSummary' => false,
      'persistResize' => false,
    ]); ?>
  </div>
</div>


