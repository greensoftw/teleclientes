<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ComisionistaServicioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/**
 * @var $total
 * @var $totalPendiente
 */

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = 'Comisionista Servicios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comisionista-servicio-index row">
  <div class="col-lg-12 row over">
    <div class="col-md-4">
      <h1><strong>Comisionista: <?= Yii::$app->user->identity->nombres ?></strong></h1>
      <p>Total ganado (disponible para cobrar) :</p>
        <h4><?= Yii::$app->formatter->asCurrency($total != null ? $total : 0) ?></h4>
      <p>Total ganado (pendiente de finalizar) : </p>
        <h4><?= Yii::$app->formatter->asCurrency($totalPendiente != null ? $totalPendiente : 0) ?></h4>
    </div>
    <div class="col-md-4 over">
      <div class="alert alert-info">Los servicio con el total marcado en rojo estan pendientes a ser terminados.</div>
      <div class="alert alert-info">Los servicios son borrados automaticamente a los 45 dias.</div>
    </div>
    <div class="col-md-4">
      <h1>Opciones:</h1>
      <?= Html::a('Registrar servicio', ['registrar'], ['class' => 'btn btn-success btn-block']) ?>
      <?= Html::a('Historial de pagos recibidos', ['historial-pagos'], ['class' => 'btn btn-primary btn-block']) ?>
    </div>
  </div>
  <?php
  $gridColumn = [
    [
      'attribute' => 'trabajo_id',
      'label' => 'Trabajo',
      'value' => function (\app\models\ComisionistaServicio $model) {
        return $model->trabajo->nombre;
      }
    ],
    'nombre_cliente',
    'correo',
    'direccion_cliente',
    'telefono_cliente',
    [
      'label' => 'Comsión',
      'value' => function (\app\models\ComisionistaServicio $model) {
        return Html::tag('p', Yii::$app->formatter->asCurrency($model->monto),
          ['class' => ($model->concretado and $model->cerrado) ? 'text-success' : 'text-danger']);
      },
      'format' => 'raw'
    ],
    'arreglado:boolean',
    'concretado:boolean',
    [
      'label' => '',
      'value' => function (\app\models\ComisionistaServicio $model) {
        if (!$model->concretado and $model->cerrado)
          return Html::tag('p', $model->razon, ['class' => 'text-danger']) .
            Html::a('Borrar servicio', ['borrar', 'id' => $model->id], ['class' => 'btn btn-danger btn-xs']);
        return Html::tag('p', 'Pendiente');
      },
      'format' => 'raw'
    ],
  ];
  ?>
  <div class="clear">
    <?= GridView::widget([
      'dataProvider' => $dataProvider,
      'columns' => $gridColumn,
      'pjax' => true,
      'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-comisionista-servicio']],
      'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
      ],
      // your toolbar can include the additional full export menu
      'toolbar' => [
        '{export}',
        ExportMenu::widget([
          'dataProvider' => $dataProvider,
          'columns' => $gridColumn,
          'target' => ExportMenu::TARGET_BLANK,
          'fontAwesome' => true,
          'dropdownOptions' => [
            'label' => 'Todo',
            'class' => 'btn btn-default',
            'itemsBefore' => [
              '<li class="dropdown-header">Exportar todo el contenido</li>',
            ],
          ],
        ]),
      ],
    ]); ?>
  </div>


</div>
