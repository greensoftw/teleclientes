<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ComisionistaServicio */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comisionista-servicio-form">

  <?php $form = ActiveForm::begin(); ?>

  <?= $form->errorSummary($model); ?>

  <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

  <?= $form->field($model, 'trabajo_id')->widget(\kartik\widgets\Select2::className(), [
    'data' => \yii\helpers\ArrayHelper::map(\app\models\Trabajo::find()->orderBy('id')->asArray()->all(), 'id', 'nombre'),
    'options' => ['placeholder' => 'Seleccione un Trabajo'],
    'pluginOptions' => [
      'allowClear' => true
    ],
  ]); ?>

  <?= $form->field($model, 'nombre_cliente')->textInput(['maxlength' => true, 'placeholder' => 'Nombre Cliente']) ?>

  <?= $form->field($model, 'correo')->textInput(['maxlength' => true, 'placeholder' => 'Correo']) ?>

  <?= $form->field($model, 'direccion_cliente')->textInput(['maxlength' => true, 'placeholder' => 'Direccion Cliente']) ?>

  <?= $form->field($model, 'telefono_cliente')->textInput(['maxlength' => true, 'placeholder' => 'Telefono Cliente']) ?>

  <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?= Html::a('Cancelar', Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
