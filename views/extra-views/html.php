<?php
/**
 * User: jhon
 * Date: 23/12/16
 * Time: 02:00 PM
 * @var $this \yii\web\View
 * @var $model \app\models\Archivo
 */
?>
<?php $archivo = file_get_contents("$model->ruta"); ?>
<h4 class="text-primary">Este es el contenido :
  <?= \yii\bootstrap\Html::a("Descargar <i class='fa fa-download'></i>",
    ['/site/download-file', 'id' => $model->id], ['class' => 'btn btn-primary']) ?></h4>
<div>
  <?= Yii::$app->formatter->asHtml($archivo); ?>
</div>
