<?php
/**
 * User: jhon
 * Date: 23/12/16
 * Time: 02:00 PM
 * @var $this \yii\web\View
 * @var $model \app\models\Archivo
 */
?>
<div class="col-lg-8 col-offset-2">
    <h4 class="text-primary">Este es el contenido :
        <?= \yii\bootstrap\Html::a("Descargar  <i class='fa fa-download'></i>",
            ['/site/download-file', 'id' => $model->id], ['class' => 'btn btn-primary']) ?></h4>
  <?= \yii\bootstrap\Html::img("@web/$model->ruta",['class'=>'img-responsive']) ?>
</div>

