<?php
/**
 * User: jhon
 * Date: 23/12/16
 * Time: 02:00 PM
 * @var $this \yii\web\View
 * @var $model \app\models\Archivo
 */
?>
<h4 class="text-primary">Este archivo no puede ser visualizado en linea
  <?= \yii\bootstrap\Html::a("Descargar <i class='fa fa-download'></i>",
    ['/site/download-file', 'id' => $model->id], ['class' => 'btn btn-primary']) ?></h4>