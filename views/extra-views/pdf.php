<?php
/**
 * User: jhon
 * Date: 23/12/16
 * Time: 02:00 PM
 * @var $this \yii\web\View
 * @var $model \app\models\Archivo
 */
?>
<h4 class="text-primary">Este es el contenido :
  <?= \yii\bootstrap\Html::a("Descargar <i class='fa fa-download'></i>",
    ['/site/download-file', 'id' => $model->id], ['class' => 'btn btn-primary']) ?></h4>
<embed src="<?= \yii\helpers\Url::to("@web/$model->ruta") ?>" alt="<?= $model->title ?>" style="width: 100%; height: 1000px">
</embed>
