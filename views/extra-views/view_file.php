<?php
/**
 * User: jhon
 * Date: 16/12/16
 * Time: 12:31 PM
 *
 * @var $model \app\models\Archivo
 * @var $columns array
 * @var $this \yii\web\View
 */
use yii\widgets\DetailView;
$this->title = "Ver archivo ";
?>
<div class="view-file">
  <?= \yii\bootstrap\Html::a('Regresar',Yii::$app->request->referrer,['class'=>'btn btn-success']) ?>
  <?= \app\helpers\FileViewer::widget([
      'model'=>$model
  ]) ?>
</div>
