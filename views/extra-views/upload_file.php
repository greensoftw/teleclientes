<?php
/**
 * User: jhon
 * Date: 15/12/16
 * Time: 04:17 PM
 * @var $action array
 */
$model = new \app\models\Archivo()
?>
<?php $form = \kartik\form\ActiveForm::begin([
  'action' => $action,
  'options' => ['enctype' => 'multipart/form-data'],
  'enableClientValidation' => false,
  'enableClientScript' => false
]) ?>
<?= $form->field($model, 'title')->textInput(['id' => 'titulo-de-archivo']) ?>
<?= $form->field($model, 'file',['inputOptions'=>['id'=>'file'.$model->id]])
  ->widget(\kartik\file\FileInput::className(), [
    'options'=>['id'=>uniqid()]
  ]) ?>
<?php \kartik\form\ActiveForm::end() ?>