<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pagina */

$this->title = $model->titulo;
$this->params['breadcrumbs'][] = ['label' => 'Pagina', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pagina-view">

  <div class="row">
    <div class="col-sm-9">
      <h2><?= 'Pagina: ' . Html::encode($this->title) ?></h2>
    </div>
    <div class="col-sm-3" style="margin-top: 15px">
      <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </div>
  </div>

  <div class="row">
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      'titulo',
      'texto:ntext',
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>
</div>
