<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Pagina */

$this->title = 'Create Pagina';
$this->params['breadcrumbs'][] = ['label' => 'Pagina', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pagina-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
