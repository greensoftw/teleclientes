<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pagina */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="pagina-form">

  <?php if ($model->id == 1): ?>
    <div class="alert alert-info">
      <h3>
        <strong>Utilice las siguientes variables: </strong>
      </h3>
      <p>(NOMBRE COMISIONISTA)</p>
      <P>(NOMBRE EMPRESA)</P>
      <P>(DOCUMENTO COMISIONISTA)</P>
      <P>(NIT EMPRESA)</P>
      <P>(FECHA DE REGISTRO)</P>
      <small>Las variables seran reemplazadas por los valores necesarios.</small>
    </div>
  <?php endif; ?>
  <?php $form = ActiveForm::begin(); ?>
  <?= $form->errorSummary($model); ?>
  <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
  <?= $form->field($model, 'titulo')->textInput(['placeholder' => 'Titulo']) ?>
  <?= $form->field($model, 'texto')->widget(\dosamigos\ckeditor\CKEditor::className(), [
  ]) ?>
  <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
