<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pagina */

$this->title = 'Actualizar Pagina: ' . ' ' . $model->titulo;
$this->params['breadcrumbs'][] = ['label' => 'Pagina', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->titulo, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="pagina-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
