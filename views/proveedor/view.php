<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Proveedor */
/* @var $providerCompra app\models\Compra */
/* @var $providerProveedorDepartamento app\models\ProveedorDepartamento */

$this->title = "Proveedor : $model->nombre";
$this->params['breadcrumbs'][] = ['label' => 'Proveedores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proveedor-view">

  <div class="row">
    <div class="button-group">
      <?=
      Html::a('<i class="fa fa-file"></i> ' . 'PDF',
        ['pdf', 'id' => $model->id],
        [
          'class' => 'btn btn-danger',
          'target' => '_blank',
          'data-toggle' => 'tooltip',
          'title' => 'Will open the generated PDF file in a new window'
        ]
      ) ?>
      <?= Html::a('Crear una copia', ['save-as-new', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
      <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
          'confirm' => 'Seguro desea borrar este registro?',
          'method' => 'post',
        ],
      ])
      ?>
    </div>
  </div>

  <div class="row">
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      [
        'attribute' => 'usuario.nombres',
        'label' => 'Usuario',
      ],
      'nombre',
      'direccion',
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>

  <div class="row">
    <?php
    if ($providerCompra->totalCount) {
      $gridColumnCompra = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'fecha',
        'pagado:boolean',
        'total',
      ];
      echo GridView::widget([
        'dataProvider' => $providerCompra,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-compra']],
        'panel' => [
          'type' => GridView::TYPE_DEFAULT,
          'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Compra'),
        ],
        'columns' => $gridColumnCompra
      ]);
    }
    ?>
  </div>

  <div class="row">
    <?php
    if ($providerProveedorDepartamento->totalCount) {
      $gridColumnProveedorDepartamento = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'nombre',
        'email:email',
      ];
      echo GridView::widget([
        'dataProvider' => $providerProveedorDepartamento,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-proveedor-departamento']],
        'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Proveedor Departamento'),
        ],
        'columns' => $gridColumnProveedorDepartamento
      ]);
    }
    ?>
  </div>
</div>
