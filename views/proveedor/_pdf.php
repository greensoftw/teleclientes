<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Proveedor */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Proveedores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proveedor-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Proveedor : '.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'usuario.nombres',
                'label' => 'Usuario'
            ],
        'nombre',
        'direccion',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerCompra->totalCount){
    $gridColumnCompra = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
                'fecha',
        'pagado:boolean',
        'total',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCompra,
        'panel' => [
            'type' => GridView::TYPE_DEFAULT,
            'heading' => Html::encode('Compra'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>',
        'toggleData' => false,
        'columns' => $gridColumnCompra
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerProveedorDepartamento->totalCount){
    $gridColumnProveedorDepartamento = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
                'nombre',
        'email:email',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerProveedorDepartamento,
        'panel' => [
            'type' => GridView::TYPE_DEFAULT,
            'heading' => Html::encode('Departamento'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>',
        'toggleData' => false,
        'columns' => $gridColumnProveedorDepartamento
    ]);
}
?>
    </div>
</div>
