<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Proveedor */
/* @var $modelUser app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos' => \yii\web\View::POS_END,
  'viewParams' => [
    'class' => 'ProveedorDepartamento',
    'relID' => 'proveedor-departamento',
    'value' => \yii\helpers\Json::encode($model->proveedorDepartamentos),
    'isNewRecord' => ($model->isNewRecord) ? 1 : 0
  ]
]);
?>

<div class="proveedor-form">

  <?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
  ]); ?>

  <?= $form->errorSummary($model); ?>

  <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

  <?= $form->field($modelUser, 'nombres')->textInput(['maxlength' => true, 'placeholder' => 'Nombres del usuario']) ?>

  <?= $form->field($modelUser, 'email')->textInput(['maxlength' => true, 'placeholder' => 'Email del usuario']) ?>

  <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'placeholder' => 'Nombre']) ?>

  <?= $form->field($model, 'direccion')->textInput(['maxlength' => true, 'placeholder' => 'Direccion']) ?>

  <?php
  $forms = [
    [
      'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Departamentos'),
      'content' => $this->render('_formProveedorDepartamento', [
        'row' => \yii\helpers\ArrayHelper::toArray($model->proveedorDepartamentos),
      ]),
    ],
  ];
  echo kartik\tabs\TabsX::widget([
    'items' => $forms,
    'position' => kartik\tabs\TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'pluginOptions' => [
      'bordered' => true,
      'sideways' => true,
      'enableCache' => false,
    ],
  ]);
  ?>
  <div class="form-group">
    <?php if (Yii::$app->controller->action->id != 'save-as-new'): ?>
      <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if (Yii::$app->controller->action->id != 'create'): ?>
      <?= Html::submitButton('Guardar como nuevo', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
    <?= Html::a('Cancelar', Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
  </div>
  <?php ActiveForm::end(); ?>

</div>
