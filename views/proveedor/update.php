<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Proveedor */
/* @var $modelUser app\models\Usuario */

$this->title = 'Actualizar Proveedor: ' . ' ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Proveedores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="proveedor-update">
  <?= $this->render('_form', [
    'model' => $model,
    'modelUser' => $modelUser
  ]) ?>
</div>
