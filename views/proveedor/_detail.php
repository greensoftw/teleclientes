<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Proveedor */

?>
<div class="proveedor-view">
  <div class="row">
    <div class="col-sm-9">
      <h2><?= Html::encode($model->nombre) ?></h2>
    </div>
  </div>
  <div class="row">
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      [
        'attribute' => 'usuario.nombres',
        'label' => 'Usuario',
      ],
      'nombre',
      'direccion',
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>
</div>