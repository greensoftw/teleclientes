<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Proveedor */

$this->title = 'crear copia del Proveedor: ' . ' ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Proveedores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'crear copia';
?>
<div class="proveedor-create">
  <?= $this->render('_form', [
    'model' => $model,
    'modelUser' => $modelUser
  ]) ?>

</div>
