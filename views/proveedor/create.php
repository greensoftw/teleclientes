<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Proveedor */
/* @var $modelUser app\models\Usuario */

$this->title = 'Crear Proveedor';
$this->params['breadcrumbs'][] = ['label' => 'Proveedores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proveedor-create">

    <?= $this->render('_form', [
        'model' => $model,
        'modelUser' => $modelUser
    ]) ?>

</div>
