<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ServicioProgramadoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = 'Servicio Programados';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="servicio-programado-index row">
  <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

  <p>
    <?= Html::a('Busqueda avanzada', '#', ['class' => 'btn btn-info search-button']) ?>
  </p>
  <div class="search-form" style="display:<?= Yii::$app->request->get($searchModel->formName()) ? 'block' : 'none' ?>">
    <?= $this->render('_search', ['model' => $searchModel]); ?>
  </div>
  <?php
  $gridColumn = [
    ['class' => 'yii\grid\SerialColumn'],
    [
      'class' => 'kartik\grid\ExpandRowColumn',
      'width' => '50px',
      'value' => function ($model, $key, $index, $column) {
        return GridView::ROW_COLLAPSED;
      },
      'detail' => function ($model, $key, $index, $column) {
        return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
      },
      'headerOptions' => ['class' => 'kartik-sheet-style'],
      'expandOneOnly' => true
    ],
    ['attribute' => 'id', 'visible' => false],
    [
      'attribute' => 'cliente_id',
      'label' => 'Cliente',
      'value' => function (\app\models\ServicioProgramado $model) {
        return $model->cliente->usuario->nombres;
      },
    ],
    [
      'attribute' => 'servicio_id',
      'label' => 'Servicio',
      'value' => function ($model) {
        return $model->servicio ? "Creado del servicio N° $model->id" : '(no definido)';
      },
    ],
    [
      'attribute' => 'equipo_id',
      'label' => 'Equipo',
      'value' => function ($model) {
        return $model->equipo ? $model->equipo : '(no definido)';
      },
    ],
    'fecha',
    [
      'class' => 'kartik\grid\ActionColumn',
      'template'=>mdm\admin\components\Helper::filterActionColumn("{view} {update} {delete}")
    ],
  ];
  ?>
  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumn,
    'pjax' => true,
    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-servicio-programado']],
    'panel' => [
      'type' => GridView::TYPE_PRIMARY,
      'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
    ],
    // your toolbar can include the additional full export menu
    'toolbar' => [
      '{export}',
      ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumn,
        'target' => ExportMenu::TARGET_BLANK,
        'fontAwesome' => true,
        'dropdownOptions' => [
          'label' => 'Full',
          'class' => 'btn btn-default',
          'itemsBefore' => [
            '<li class="dropdown-header">Export All Data</li>',
          ],
        ],
      ]),
    ],
  ]); ?>

</div>
