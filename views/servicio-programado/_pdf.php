<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\ServicioProgramado */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Servicio Programados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="servicio-programado-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Servicio Programado'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'cliente.usuario.nombres',
                'label' => 'Cliente'
            ],
        [
                'attribute' => 'servicio.id',
                'label' => 'Servicio'
            ],
        [
                'attribute' => 'equipo.id',
                'label' => 'Equipo'
            ],
        'fecha',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
