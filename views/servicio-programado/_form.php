<?php

use kartik\widgets\DepDrop;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ServicioProgramado */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="servicio-programado-form">

  <?php $form = ActiveForm::begin(); ?>

  <?= $form->errorSummary($model); ?>

  <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

  <?= $form->field($model, 'cliente_id')->widget(\kartik\widgets\Select2::classname(), [
    'data' => \yii\helpers\ArrayHelper::map(\app\models\Cliente::find()
      ->joinWith('usuario')
      ->orderBy('usuario.nombres')
      ->all(), 'id', 'usuario.nombres'),
    'options' => ['placeholder' => 'Seleccione un Cliente'],
    'pluginOptions' => [
      'allowClear' => true
    ],
  ]); ?>
  <?= $form->field($model, 'equipo_id')->widget(DepDrop::classname(), [
    'options' => ['placeholder' => 'Seleccione ...'],
    'type' => DepDrop::TYPE_SELECT2,
    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
    'pluginOptions' => [
      'depends' => [Html::getInputId($model,'cliente_id')],
      'url' => Url::to(['/servicio-programado/equipo-ajax']),
      'loadingText' => 'Cargando equipos ...',
    ]
  ]); ?>

  <?= $form->field($model, 'fecha')->widget(\kartik\datecontrol\DateControl::classname(), [
    'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
    'saveFormat' => 'php:Y-m-d',
    'ajaxConversion' => true,
    'options' => [
      'pluginOptions' => [
        'placeholder' => 'Seleccione una fecha',
        'autoclose' => true,
      ]
    ],
  ]); ?>

  <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
