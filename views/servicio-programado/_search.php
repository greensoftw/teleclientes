<?php

use kartik\widgets\DepDrop;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\ServicioProgramadoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-servicio-programado-search">

  <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
  ]); ?>

  <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

  <?= $form->field($model, 'cliente_id')->widget(\kartik\widgets\Select2::classname(), [
    'data' => \yii\helpers\ArrayHelper::map(\app\models\Cliente::find()->orderBy('id')->all(), 'id', 'usuario.nombres'),
    'options' => ['placeholder' => 'Selecione Cliente'],
    'pluginOptions' => [
      'allowClear' => true
    ],
  ]); ?>

  <?= $form->field($model, 'servicio_id') ?>

  <?= $form->field($model, 'equipo_id')->widget(DepDrop::classname(), [
    'options' => ['placeholder' => 'Seleccione ...'],
    'type' => DepDrop::TYPE_SELECT2,
    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
    'pluginOptions' => [
      'depends' => [Html::getInputId($model, 'cliente_id')],
      'url' => Url::to(['/servicio-programado/equipo-ajax']),
      'loadingText' => 'Cargando equipos ...',
    ]
  ]); ?>

  <?= $form->field($model, 'fecha')->widget(\kartik\datecontrol\DateControl::classname(), [
    'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
    'saveFormat' => 'php:Y-m-d',
    'ajaxConversion' => true,
    'options' => [
      'pluginOptions' => [
        'placeholder' => 'Selecione Fecha',
        'autoclose' => true,
      ]
    ],
  ]); ?>

  <div class="form-group">
    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
