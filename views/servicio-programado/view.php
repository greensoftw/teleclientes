<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ServicioProgramado */

$this->title = 'Servicio Programado' . ' N° ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Servicio Programados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="servicio-programado-view">

  <div class="row">
    <div class="col-sm-3" style="margin-top: 15px; margin-bottom: 15px">
      <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
          'confirm' => 'Seguro desea eliminar este item',
          'method' => 'post',
        ],
      ])
      ?>
    </div>
  </div>

  <div class="row">
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      [
        'attribute' => 'cliente.usuario.nombres',
        'label' => 'Cliente',
      ],
      [
        'attribute' => 'servicio.id',
        'label' => 'Servicio',
      ],
      [
        'attribute' => 'equipo',
        'label' => 'Equipo',
      ],
      'fecha',
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>
</div>
