<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ServicioProgramado */

$this->title = 'Agregar Servicio Programado';
$this->params['breadcrumbs'][] = ['label' => 'Servicios Programados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="servicio-programado-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
