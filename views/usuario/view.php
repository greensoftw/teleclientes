<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuario-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Usuario'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'password',
        'nombres',
        'email:email',
        'authKey',
        'access_token',
        'password_reset_token',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerArchivoPdf->totalCount){
    $gridColumnArchivoPdf = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'nombre',
            'descripcion',
            'ruta_archivo',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerArchivoPdf,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-archivo-pdf']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Archivo Pdf'),
        ],
        'export' => false,
        'columns' => $gridColumnArchivoPdf
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerCliente->totalCount){
    $gridColumnCliente = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'nombres',
            'telefonos',
            'direccion',
            'correo',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCliente,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-cliente']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Cliente'),
        ],
        'export' => false,
        'columns' => $gridColumnCliente
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerComisionista->totalCount){
    $gridColumnComisionista = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        [
                'attribute' => 'distribuidor.id',
                'label' => 'Distribuidor'
            ],
            'nombres',
            'telefonos',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerComisionista,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-comisionista']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Comisionista'),
        ],
        'export' => false,
        'columns' => $gridColumnComisionista
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerDistribuidor->totalCount){
    $gridColumnDistribuidor = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'nombre',
            'direccion',
            'telefono',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerDistribuidor,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-distribuidor']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Distribuidor'),
        ],
        'export' => false,
        'columns' => $gridColumnDistribuidor
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerEmpleado->totalCount){
    $gridColumnEmpleado = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'telefono',
            'tipo_empleado',
            'sueldo',
            'proximo_pago',
            'saldo',
            'is_tecnico:boolean',
            'perfil',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerEmpleado,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-empleado']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Empleado'),
        ],
        'export' => false,
        'columns' => $gridColumnEmpleado
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerMarca->totalCount){
    $gridColumnMarca = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'nombre',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerMarca,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-marca']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Marca'),
        ],
        'export' => false,
        'columns' => $gridColumnMarca
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerMovimientosCaja->totalCount){
    $gridColumnMovimientosCaja = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'fecha',
            'monto',
            'descripcion',
                ];
    echo Gridview::widget([
        'dataProvider' => $providerMovimientosCaja,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-movimientos-caja']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Movimientos Caja'),
        ],
        'export' => false,
        'columns' => $gridColumnMovimientosCaja
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerProveedor->totalCount){
    $gridColumnProveedor = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'nombre',
            'direccion',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerProveedor,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-proveedor']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Proveedor'),
        ],
        'export' => false,
        'columns' => $gridColumnProveedor
    ]);
}
?>
    </div>
</div>
