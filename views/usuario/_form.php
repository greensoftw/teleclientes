<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'ArchivoPdf', 
        'relID' => 'archivo-pdf', 
        'value' => \yii\helpers\Json::encode($model->archivoPdfs),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Cliente', 
        'relID' => 'cliente', 
        'value' => \yii\helpers\Json::encode($model->clientes),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Comisionista', 
        'relID' => 'comisionista', 
        'value' => \yii\helpers\Json::encode($model->comisionistas),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Distribuidor', 
        'relID' => 'distribuidor', 
        'value' => \yii\helpers\Json::encode($model->distribuidors),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Empleado', 
        'relID' => 'empleado', 
        'value' => \yii\helpers\Json::encode($model->empleados),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Marca', 
        'relID' => 'marca', 
        'value' => \yii\helpers\Json::encode($model->marcas),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'MovimientosCaja', 
        'relID' => 'movimientos-caja', 
        'value' => \yii\helpers\Json::encode($model->movimientosCajas),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Proveedor', 
        'relID' => 'proveedor', 
        'value' => \yii\helpers\Json::encode($model->proveedors),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="usuario-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true, 'placeholder' => 'Username']) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true, 'placeholder' => 'Password']) ?>

    <?= $form->field($model, 'nombres')->textInput(['maxlength' => true, 'placeholder' => 'Nombres']) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => 'Email']) ?>

    <?= $form->field($model, 'authKey')->textInput(['maxlength' => true, 'placeholder' => 'AuthKey']) ?>

    <?= $form->field($model, 'access_token')->textInput(['maxlength' => true, 'placeholder' => 'Access Token']) ?>

    <?= $form->field($model, 'password_reset_token')->textInput(['maxlength' => true, 'placeholder' => 'Password Reset Token']) ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('ArchivoPdf'),
            'content' => $this->render('_formArchivoPdf', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->archivoPdfs),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Cliente'),
            'content' => $this->render('_formCliente', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->clientes),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Comisionista'),
            'content' => $this->render('_formComisionista', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->comisionistas),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Distribuidor'),
            'content' => $this->render('_formDistribuidor', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->distribuidors),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Empleado'),
            'content' => $this->render('_formEmpleado', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->empleados),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Marca'),
            'content' => $this->render('_formMarca', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->marcas),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('MovimientosCaja'),
            'content' => $this->render('_formMovimientosCaja', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->movimientosCajas),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Proveedor'),
            'content' => $this->render('_formProveedor', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->proveedors),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
