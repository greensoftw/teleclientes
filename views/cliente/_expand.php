<?php
use kartik\tabs\TabsX;
use yii\helpers\Html;

$items = [
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Detalles'),
    'content' => $this->render('_detail', [
      'model' => $model,
    ]),
  ],
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Equipos'),
    'content' => $this->render('_dataEquipo', [
      'model' => $model,
      'row' => $model->equipos,
    ]),
  ],
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Historial de Servicios'),
    'content' => $this->render('_dataServicio', [
      'model' => $model,
      'row' => $model->servicios,
    ]),
  ],
];
?>
<div class="panel">
  <div class="panel-body">
    <?= TabsX::widget([
      'items' => $items,
      'position' => TabsX::POS_ABOVE,
      'encodeLabels' => false,
      'class' => 'tes',
      'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
      ],
    ]);
    ?>    </div>
</div>
