<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Cliente */
/* @var $modelUser app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */
/* @var $equipos \app\models\Equipo[] */
?>

<div class="cliente-form">

  <?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true
  ]); ?>

  <?= $form->errorSummary($model); ?>

  <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

  <?= $form->field($modelUser, 'nombres')->textInput(['maxlength' => true, 'placeholder' => 'Nombres y apellidos del cliente'])
  ->label('Nombres y apellidos del cliente')?>

  <?= $form->field($modelUser, 'email')->textInput(['maxlength' => true, 'placeholder' => 'email']) ?>

  <?= $form->field($model, 'telefonos')->textInput(['maxlength' => true, 'placeholder' => 'Telefonos']) ?>

  <?= $form->field($model, 'direccion')->textInput(['maxlength' => true, 'placeholder' => 'Direccion']) ?>

  <?php
  $forms = [
    [
      'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Equipos'),
      'content' => $this->render('_formEquipo', [
        'row' => $equipos,
      ]),
    ],
  ];
  echo kartik\tabs\TabsX::widget([
    'items' => $forms,
    'position' => kartik\tabs\TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'pluginOptions' => [
      'bordered' => true,
      'sideways' => true,
      'enableCache' => false,
    ],
  ]);
  ?>
  <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?= Html::a('Cancelar', Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
<?php $this->registerJsFile('@web/js/add-cliente.js',[
    'depends'=>\yii\web\JqueryAsset::className()
]) ?>
