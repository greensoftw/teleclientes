<div class="form-group" id="add-equipo">
  <?php
  /**
   * @var $row \app\models\Equipo[]
   */
  use yii\helpers\Html;

  ?>
  <div class="panel panel-default">
    <table class="kv-grid-table table table-hover kv-table-wrap">
      <?php foreach ($row as $key => $model): ?>
        <tr>
          <td colspan="2">Equipo # <?= $key + 1 ?></td>
          <td><?= \kartik\widgets\Select2::widget([
              'id' => Html::getInputId($model, "[$key]marca_id"),
              'value' => $model->marca_id,
              'name' => Html::getInputName($model, "[$key]marca_id"),
              'data' => \yii\helpers\ArrayHelper::map(\app\models\Marca::find()
                ->orderBy('nombre')->all(), 'id', 'nombre'),
              'options' => ['placeholder' => 'seleccione una marca'],
              'pluginOptions' => [
                'allowClear' => true
              ],
            ]) ?>
          </td>
        </tr>
        <tr>
          <td>
            <?= \kartik\widgets\Select2::widget([
              'id' => Html::getInputId($model, "[$key]distribuidor_id"),
              'name' => Html::getInputName($model, "[$key]distribuidor_id"),
              'value' => $model->distribuidor_id,
              'data' => \yii\helpers\ArrayHelper::map(\app\models\Distribuidor::find()
                ->orderBy('nombre')->all(), 'id', 'nombre'),
              'options' => ['placeholder' => 'seleccione un distribuidor'],
              'pluginOptions' => [
                'allowClear' => true
              ],
            ]) ?>
          </td>
          <td>
            <?= Html::activeDropDownList($model, "[$key]tiempo_mantenimiento",
              \app\models\Equipo::$tiempos_mantenimientos, ['class' => 'form-control', 'prompt' => 'Seleccione']) ?>
          </td>
          <td>
            <?= \kartik\widgets\Select2::widget([
              'id' => Html::getInputId($model, "[$key]tipo_equipo_id"),
              'name' => Html::getInputName($model, "[$key]tipo_equipo_id"),
              'value' => $model->tipo_equipo_id,
              'data' => \yii\helpers\ArrayHelper::map(\app\models\TipoEquipo::find()
                ->orderBy('nombre')->all(), 'id', 'nombre'),
              'options' => ['placeholder' => 'seleccione un tipo de equipo'],
              'pluginOptions' => [
                'allowClear' => true
              ],
            ]) ?>
          </td>
        </tr>
        <tr>
          <td><?= Html::activeTextInput($model, "[$key]modelo", ['class' => 'form-control', 'Placeholder' => 'Modelo']) ?></td>
          <td><?= Html::activeTextInput($model, "[$key]serial", ['class' => 'form-control', 'Placeholder' => 'Serial']) ?></td>
          <td><?= Html::activeTextInput($model, "[$key]nota", ['class' => 'form-control', 'Placeholder' => 'Ubicacion']) ?></td>
        </tr>
      <?php endforeach; ?>
    </table>

  </div>
  <div class="kv-panel-after">
    <?= Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Agregar Equipo', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowEquipo()']) ?>
  </div>
</div>
