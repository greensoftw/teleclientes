<?php


/* @var $this yii\web\View */
/* @var $model app\models\Cliente */
/* @var $modelUser app\models\Usuario */

$this->title = 'Crear Cliente';
$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cliente-create row">
  <?= $this->render('_form', [
    'model' => $model,
    'modelUser' => $modelUser,
    'equipos'=>$equipos
  ]) ?>
</div>
