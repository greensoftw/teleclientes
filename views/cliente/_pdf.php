<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Cliente */

$this->title = $model->usuario->nombres;
$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cliente-view">

  <div class="row">
      <h3><?= 'Cliente' . ': ' . Html::encode($this->title) ?></h3>
  </div>

  <div class="row">
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      [
        'attribute' => 'usuario.nombres',
        'label' => 'Usuario'
      ],
      'telefonos',
      'direccion',
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>

  <div class="row">
    <table class="kv-grid-table table table-bordered table-striped kv-table-wrap">
      <?php foreach ($model->equipos as $equipo): ?>
        <tr>
          <td>Marca</td>
          <td>Distribuidor</td>
          <td>modelo</td>
          <td>serial</td>
          <td>Ubicacion</td>
        </tr>
        <tr>
          <td>
            <?= $equipo->marca->nombre ?>
          </td>
          <td>
            <?= Yii::$app->formatter->asText($equipo->distribuidor) ?>
          </td>
          <td>
            <?= $equipo->modelo ?>
          </td>
          <td>
            <?= $equipo->serial ?>
          </td>
          <td>
            <?= $equipo->nota ?>
          </td>
        </tr>
        <tr>
          <td colspan="5">
            <h4>Listado de servicios al equipo</h4>
            <table class="table">
              <tr>
                <td>Fecha de ingreso</td>
                <td>Falla</td>
                <td>Total</td>
                <td>Incluye otros equipos?</td>
              </tr>
              <?php $servicios = $equipo->servicioAEquipos;
              foreach ($servicios as $servicio):?>
                <tr>
                  <td><?= Yii::$app->formatter->asDate($servicio->servicio->ingreso->fecha) ?></td>
                  <td><?= $servicio->descripcion ?></td>
                  <td><?= Yii::$app->formatter->asCurrency($servicio->servicio->total) ?></td>
                  <td><?= Yii::$app->formatter->asBoolean($servicio->servicio->getServicioAEquipos()->count()>1) ?></td>
                </tr>
              <?php endforeach; ?>
            </table>

          </td>
        </tr>
        <tr>
          <td colspan="5">
            <hr>
          </td>
        </tr>
      <?php endforeach; ?>
    </table>
  </div>
</div>
