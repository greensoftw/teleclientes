<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Cliente */
/* @var $providerUsuario */
/* @var $providerEquipo */
/* @var $providerServicio */
$this->title = $model->usuario->nombres;
$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cliente-view row">
  <div>
    <div class="form-group">
      <?=
      Html::a('<i class="fa fa-file"></i> ' . 'PDF',
        ['pdf', 'id' => $model->id],
        [
          'class' => 'btn btn-danger',
          'target' => '_blank',
          'data-toggle' => 'tooltip',
          'title' => 'se generara en una nueva ventana'
        ]
      ) ?>
      <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('Crear servicio', ['/servicio/create', 'cliente_id' => $model->id], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
          'confirm' => 'Seguro desea eliminar este elemento?',
          'method' => 'post',
        ],
      ])
      ?>
    </div>
  </div>

  <div>
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      [
        'attribute' => 'usuario.nombres',
        'label' => 'Usuario',
      ],
      [
        'attribute'=>'usuario.email',
        'label'=>'email'
      ],
      'telefonos',
      'direccion',
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>

  <div>
    <?php
    if ($providerEquipo->totalCount) {
      $gridColumnEquipo = [
        ['attribute' => 'id', 'visible' => false],
        [
          'attribute' => 'marca.nombre',
          'label' => 'Marca'
        ],
        [
          'attribute' => 'distribuidor.nombre',
          'label' => 'Distribuidor'
        ],
        'modelo',
        'serial',
        'nota',
      ];
      echo GridView::widget([
        'dataProvider' => $providerEquipo,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-equipo']],
        'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Equipo'),
        ],
        'columns' => $gridColumnEquipo
      ]);
    }
    ?>
  </div>

  <div>
    <?php
    if ($providerServicio->totalCount) {
      $gridColumnServicio = [
        ['attribute' => 'id', 'visible' => false],
        [
          'attribute' => 'empleado.usuario.nombres',
          'label' => 'Empleado'
        ],
        [
          'attribute' => 'comisionServicio.id',
          'label' => 'Comision Servicio'
        ],
        'total:currency',
        'pagado:boolean',
        'no_facturado',
        'cerrado:boolean',
      ];
      echo GridView::widget([
        'dataProvider' => $providerServicio,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-servicio']],
        'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Servicio'),
        ],
        'columns' => $gridColumnServicio
      ]);
    }
    ?>
  </div>
</div>
