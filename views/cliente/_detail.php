<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Cliente */

?>
<div class="cliente-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->usuario->nombres) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'usuario.nombres',
            'label' => 'Usuario',
        ],
        'telefonos',
        'direccion',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>