<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;
/** @var \app\models\base\Equipo $model */

$dataProvider = new ArrayDataProvider([
  'allModels' => $model->servicioAEquipos,
  'key' => 'id'
]);
$gridColumns = [
  ['class' => 'yii\grid\SerialColumn'],
  ['attribute' => 'id', 'visible' => false],
  [
    'attribute' => 'servicio.empleado.usuario.nombres',
    'label' => 'Empleado'
  ],
  'servicio.total:currency',
  'servicio.pagado:boolean',
  'servicio.no_facturado',
  'servicio.cerrado:boolean',
];

echo GridView::widget([
  'dataProvider' => $dataProvider,
  'columns' => $gridColumns,
  'containerOptions' => ['style' => 'overflow: auto'],
  'pjax' => true,
  'beforeHeader' => [
    [
      'options' => ['class' => 'skip-export']
    ]
  ],
  'export' => [
    'fontAwesome' => true
  ],
  'bordered' => true,
  'striped' => true,
  'condensed' => true,
  'responsive' => true,
  'hover' => true,
  'showPageSummary' => false,
  'persistResize' => false,
]);
