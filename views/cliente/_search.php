<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\ClienteSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-cliente-search">

  <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'fieldConfig' => ['template' => "<div class='col-lg-3'>{label}\n{input}</div>"]
  ]); ?>
  <div class="form-group">
    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'user_id')->widget(\kartik\widgets\Select2::className(), [
      'data' => \yii\helpers\ArrayHelper::map(\app\models\Usuario::find()
        ->innerJoinWith('cliente')
        ->orderBy('nombres')->asArray()->all(), 'id', 'nombres'),
      'options' => ['placeholder' => 'Seleccione un usuario'],
      'pluginOptions' => [
        'allowClear' => true
      ],
    ]); ?>

    <?= $form->field($model, 'telefonos')->textInput(['maxlength' => true, 'placeholder' => 'Telefonos']) ?>

    <?= $form->field($model, 'direccion')->textInput(['maxlength' => true, 'placeholder' => 'Direccion']) ?>

    <?= $form->field($model, 'correo')->textInput(['maxlength' => true, 'placeholder' => 'Correo']) ?>
  </div>
  <div class="form-group col-lg-12">
    <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
    <?= Html::resetButton('Borrar', ['class' => 'btn btn-default']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
