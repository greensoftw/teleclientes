<?php

/* @var $this yii\web\View */
/* @var $model app\models\Cliente */
/* @var $modelUser app\models\Usuario */

$this->title = 'Actualizar Cliente: ' . ' ' . $model->usuario->nombres;
$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->usuario->nombres, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="cliente-update row">
  <?= $this->render('_form', [
    'model' => $model,
    'modelUser' => $modelUser,
    'equipos'=>$equipos
  ]) ?>
</div>
