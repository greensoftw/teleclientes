<div class="form-group" id="add-servicio">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'Servicio',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        'empleado_id' => [
            'label' => 'Empleado',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Empleado::find()
                  ->orderBy('usuario.nombres')->asArray()->all(), 'id', 'id'),
                'options' => ['placeholder' => 'Choose Empleado'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'comision_servicio_id' => [
            'label' => 'Comisionista servicio',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\ComisionistaServicio::find()
                  ->orderBy('nombre')->asArray()->all(), 'id', 'id'),
                'options' => ['placeholder' => 'Choose Comisionista servicio'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'total' => ['type' => TabularForm::INPUT_TEXT],
        'fecha_ingreso' => ['type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\datecontrol\DateControl::className(),
            'options' => [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
                'saveFormat' => 'php:Y-m-d H:i:s',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => 'Choose Fecha Ingreso',
                        'autoclose' => true,
                    ]
                ],
            ]
        ],
        'fecha_entrega' => ['type' => TabularForm::INPUT_TEXT],
        'pagado' => ['type' => TabularForm::INPUT_CHECKBOX,
            'options' => [
                'style' => 'position : relative; margin-top : -9px'
            ]
        ],
        'no_facturado' => ['type' => TabularForm::INPUT_TEXT],
        'cerrado' => ['type' => TabularForm::INPUT_TEXT],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowServicio(' . $key . '); return false;', 'id' => 'servicio-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Servicio', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowServicio()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

