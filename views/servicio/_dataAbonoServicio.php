<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;

/**
 * @var $model \app\models\Servicio
 */
$dataProvider = new ArrayDataProvider([
  'allModels' => $model->abonoServicios,
  'key' => 'id'
]);
$gridColumns = [
  ['class' => 'yii\grid\SerialColumn'],
  ['attribute' => 'id', 'visible' => false],
  'monto:currency',
  'fecha:datetime'
];
?>
<div class="col-lg-8">
  <h3>Abonos registrados</h3>
  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns,
    'containerOptions' => ['style' => 'overflow: auto'],
    'pjax' => true,
    'beforeHeader' => [
      [
        'options' => ['class' => 'skip-export']
      ]
    ],
    'export' => [
      'fontAwesome' => true
    ],
    'bordered' => true,
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'hover' => true,
    'showPageSummary' => false,
    'persistResize' => false,
  ]); ?>
</div>
<div class="col-lg-4">
  <h3 class="text-info">Registrar abono</h3>
  <?php if(!$model->pagado): ?>
    <?php $abono = new \app\models\AbonoServicio(); ?>
    <?php $form = \kartik\form\ActiveForm::begin([
      'enableAjaxValidation'=>true,
      'action'=>['abonar','id'=>$model->id]
    ]) ?>
    <?= $form->field($abono,'monto') ?>
    <?= Html::submitButton('Confirmar',['class'=>'btn btn-primary']) ?>
    <?php \kartik\form\ActiveForm::end() ?>
  <?php else: ?>
    <div class="alert alert-info">
      <h3>Servicio pagado accion no disponible</h3>
    </div>
  <?php endif; ?>
</div>