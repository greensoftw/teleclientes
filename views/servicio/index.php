<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ServicioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = 'Servicios';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="row">
  <p>
    <?= Html::a('Busqueda avanzada', '#', ['class' => 'btn btn-info search-button']) ?>
  </p>
  <div class="search-form" style="display:<?= Yii::$app->request->get($searchModel->formName()) ? 'block' : 'none' ?>">
    <?= $this->render('_search', ['model' => $searchModel]); ?>
  </div>
  <p>Ayuda? : <br>
    <button class="btn btn-default"><span class="glyphicon glyphicon-expand"></span></button>
     Toque este boton en cada servicio, tendra accion directa a ; agregar equipos, Actualizar estado, Actualizar presupuesto.
    <br>
    <button class="btn btn-default"><span class="glyphicon glyphicon-copy"></span></button>
    Toque este boton en cada servicio para acceder a los detalles del servicio.
    <br>
    <button class="btn btn-default"><span class="glyphicon glyphicon-edit"></span></button>
    Toque este boton en cada servicio para aactualizar detalles del servicio.
  </p>
  <?php
  $gridColumn = [
    ['class' => 'yii\grid\SerialColumn'],
    [
      'class' => 'kartik\grid\ExpandRowColumn',
      'width' => '50px',
      'value' => function ($model, $key, $index, $column) {
        return GridView::ROW_COLLAPSED;
      },
      'detail' => function ($model, $key, $index, $column) {
        return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
      },
      'headerOptions' => ['class' => 'kartik-sheet-style'],
      'expandOneOnly' => true
    ],
    ['attribute' => 'id', 'visible' => false],
    [
      'attribute' => 'empleado_id',
      'label' => 'Empleado',
      'value' => function (\app\models\Servicio $model) {
        if ($model->empleado)
          return $model->empleado->usuario->nombres;
        return '(Sin Asignar)';
      },
      'visible' => !Yii::$app->user->can('Tecnico')
    ],
    ['attribute' => 'cliente_id',
      'label' => 'Cliente',
      'value' => function (\app\models\Servicio $model) {
        return $model->cliente->usuario->nombres;
      }
    ],
    [
      'attribute' => 'fecha_tecnico',
      'format' => 'datetime',

    ],
    [
      'value' => 'estadoActual',
      'label' => 'Estado actual - Fecha'
    ],

    'total:currency',
    [
      'class' => 'kartik\grid\ActionColumn',
      'template' => \mdm\admin\components\Helper::filterActionColumn("{view} {update}"),
      'buttons'=>[
          'view'=>function($url,$model){
            return Html::a('<span class="glyphicon glyphicon-copy"></span> Orden N° '.
              $model->numero,$url,['class'=>'btn btn-default','title'=>'Ver orden de servicio N°'.$model->id]);
          },
          'update'=>function($url,$model){
            return Html::a('<span class="glyphicon glyphicon-pencil"></span> Orden N° '.
              $model->numero,$url,['class'=>'btn btn-default','title'=>'Editar orden de servicio N°'.$model->id]);
          }
      ]
    ],
  ];
  ?>
  <div>
    <?= GridView::widget([
      'dataProvider' => $dataProvider,
      'columns' => $gridColumn,
      'pjax' => true,
      'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-servicio']],
      'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
      ],
      'toolbar' => [
        '{export}',
        ExportMenu::widget([
          'dataProvider' => $dataProvider,
          'columns' => $gridColumn,
          'target' => ExportMenu::TARGET_BLANK,
          'fontAwesome' => true,
          'dropdownOptions' => [
            'label' => 'Todo',
            'class' => 'btn btn-default',
            'itemsBefore' => [
              '<li class="dropdown-header">Exportar todo el contenido</li>',
            ],
          ],
        ]),
      ],
    ]); ?>
  </div>
</div>
<?php $this->registerJsFile('@web/js/view-servicio.js', [
  'position' => $this::POS_END,
  'depends' => [\yii\web\JqueryAsset::className()]
]) ?>

