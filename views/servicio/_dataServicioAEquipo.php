<?php
/**
 * @var $model \app\models\Servicio
 */

$gridColumns = [
  ['attribute' => 'id', 'visible' => false],
  [
    'value' => (string) $model->servicioAEquipo->equipo,
    'label' => 'Equipo',
    'format' => 'html'
  ],
  [
    'attribute' => 'codigoIsis.codigo',
    'label' => 'Código de Error'
  ],
  'descripcion',
  [
    'label' => 'Imagenes',
    'value' =>
      Yii::$app->view->render('_imagenes', [
        'model' => $model
      ]),
    'format' => 'raw'
  ]
];

echo \yii\widgets\DetailView::widget([
  'model' => $model->servicioAEquipo,
  'attributes' => $gridColumns
]);
