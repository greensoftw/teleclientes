<?php
use kartik\tabs\TabsX;
use yii\helpers\Html;

/**
 * @var $model \app\models\Servicio
 */

$items = [
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Detalles'),
    'content' => $this->render('_detail', [
      'model' => $model,
    ]),
  ],
  $model->servicioAEquipo?
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Equipo'),
    'content' => $this->render('_dataServicioAEquipo', [
      'model' => $model,
    ]),
  ]
    :
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Ingresar nuevo equipo'),
    'content' => $this->render('_form_add_equipo', [
      'model' => $model
    ]),
    'visible' => !$model->servicioAEquipo
  ],
  [
    'options' => [],
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Presupuesto - Actualizar'),
    'content' => $this->render('_dataPresupuesto', [
      'model' => $model,
    ]),
    'visible' => \mdm\admin\components\Helper::checkRoute('servicio/agregar-presupuesto')
  ],
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Estados - Actualizar'),
    'content' => $this->render('_dataServicioEstado', [
      'model' => $model,
    ]),
    'visible' => \mdm\admin\components\Helper::checkRoute('servicio/agregar-estado')
  ],
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Abonos'),
    'content' => $this->render('_dataAbonoServicio', [
      'model' => $model,
    ]),
    'visible' => \mdm\admin\components\Helper::checkRoute('servicio/abonar')
  ],
];
?>
<div class="panel">
  <div class="panel-body">
    <?= TabsX::widget([
      'items' => $items,
      'position' => TabsX::POS_ABOVE,
      'encodeLabels' => false,
      'class' => 'tes',
      'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
      ],
    ]);
    ?>
  </div>
</div>
