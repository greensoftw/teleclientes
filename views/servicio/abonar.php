<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Servicio */
/* @var $abono \app\models\base\AbonoServicio*/
$this->title = "Registrar abono : $model->id";
$this->params['breadcrumbs'][] = ['label' => 'Servicios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => "Orden de Servicio N° $model->id", 'url' => ['view','id'=>$model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="servicio-view row">
  <div class="col-lg-6">
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      [
        'attribute' => 'empleado.usuario.nombres',
        'label' => 'Empleado',
      ],
      [
        'attribute' => 'estadoActual.estado',
        'label' => 'Estado actual'
      ],
      [
        'value' => Yii::$app->formatter->asDatetime($model->ingreso->fecha),
        'label' => 'Ingresado'
      ],
      'fecha_tecnico:datetime',
      [
        'attribute' => 'cliente.usuario.nombres',
        'label' => 'Cliente',
      ],
      [
        'attribute' => 'comisionServicio.comisionista.usuario.nombres',
        'label' => 'Comisionista',
      ],
      [
        'attribute' => 'comisionServicio.monto',
        'label' => 'Monto de la comisión',
        'format'=>'currency'
      ],
      ['attribute' => 'cliente.direccion',
        'label' => 'Dirección del cliente'
      ],
      [
        'attribute' => 'cliente.telefonos',
        'label' => 'Télefonos del cliente'
      ],
      [
        'attribute' => 'cliente.usuario.email',
        'label' => 'Correo del cliente'
      ],
      'total:currency',
      'pagado:boolean',
      'no_facturado',
      'cerrado:boolean',
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>
  <div class="col-lg-6">
    <h3>Registrar abono</h3>
    <?php if (!$model->pagado): ?>
    <?php $form = \kartik\form\ActiveForm::begin([
      'enableAjaxValidation'=>true
    ]) ?>
    <?= $form->field($abono,'monto') ?>
    <?= Html::submitButton('Confirmar',['class'=>'btn btn-primary']) ?>
    <?php \kartik\form\ActiveForm::end() ?>
    <?php else: ?>
      <div class="alert alert-info">
        <h3>Servicio pagado accion no disponible</h3>
      </div>
    <?php endif; ?>
  </div>
</div>
