<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

/**
 * @var $model \app\models\Servicio
 * @var $this \yii\web\View
 * @var $mensaje string|bool
 */

$dataProvider = new ArrayDataProvider([
  'allModels' => $model->presupuestos,
  'key' => 'id',
  'pagination' => [
    'pageSize' => 10,
    'route' => 'servicio/index'
  ]
]);
$gridColumns = [
  ['class' => 'yii\grid\SerialColumn'],
  ['attribute' => 'id', 'visible' => false],
  [
    'attribute' => 'relacion.nombre',
    'label' => 'Trabajo - Repuesto'
  ],
  'precio:currency',
  [
    'class' => 'kartik\grid\ActionColumn',
    'template' => "{delete}",
    'buttons' => [
      'delete' => function ($key, $model) {
        return \kartik\helpers\Html::a('<span class="glyphicon glyphicon-trash"></span>',
          ['borrar-presupuesto', 'id' => $model->id], ['class' => 'btn btn-default']);
      }
    ]
  ],
];
?>
<div id="add-presupuesto-<?= $model->id ?>">
  <div class="col-lg-6">
    <h3>Detalles del presupuesto</h3>
    <?= GridView::widget([
      'id' => 'grid_presupuestos_' . $model->id,
      'dataProvider' => $dataProvider,
      'columns' => $gridColumns,
      'containerOptions' => ['style' => 'overflow: auto'],
      'pjax' => true,
      'beforeHeader' => [
        [
          'options' => ['class' => 'skip-export']
        ]
      ],
      'pjaxSettings' => [
        'formSelector' => false
      ],
      'export' => [
        'fontAwesome' => true
      ],
      'bordered' => true,
      'striped' => true,
      'condensed' => true,
      'responsive' => true,
      'hover' => true,
      'showPageSummary' => false,
      'persistResize' => false,
    ]); ?>
    <h3>Total : <?= Yii::$app->formatter->asCurrency($model->total) ?></h3>
  </div>
  <div class="col-lg-6">
    <h3>Agregar presupuesto</h3>
    <?php if (isset($mensaje) and $mensaje !== false): ?>
      <div class="alert alert-info">
        <h4><?= $mensaje ?></h4>
      </div>
    <?php endif; ?>
    <?php if (!$model->cerrado): ?>
      <?php $presu = new \app\models\Presupuesto([
        'servicio_id' => $model->id
      ]);
      $form = \kartik\form\ActiveForm::begin([
        'action' => ['agregar-presupuesto'],
        'options' => ['class' => 'agregar-presupuesto'],
        'id' => 'add-pres_' . $model->id,
        'enableClientValidation' => false,
        'enableClientScript' => false,
      ])

      ?>
      <?= $form->field($presu, 'servicio_id')->hiddenInput()->label(false) ?>
      <?= $form->field($presu, 'model_id')->dropDownList([
        'Repuestos' => \yii\helpers\ArrayHelper::map(\app\models\Repuestos::find()
          ->orderBy('nombre')->all(), 'id', 'nombre'),
        'Trabajos' => \yii\helpers\ArrayHelper::map(\app\models\Trabajo::find()
          ->orderBy('nombre')->all(), 'id', 'nombre')
      ], ['class' => 'select-model_id', 'prompt' => 'Seleccione']) ?>
      <?= $form->field($presu, 'model')->hiddenInput()->label(false) ?>

      <?= $form->field($presu, 'precio') ?>
      <?= \yii\helpers\Html::submitButton('Confirmar', ['class' => 'btn btn-primary']) ?>
      <?php \kartik\form\ActiveForm::end() ?>
    <?php else: ?>
      <div class="alert alert-info">
        <h3>Servicio cerrado, acción no disponible</h3>
      </div>
    <?php endif; ?>
  </div>
</div>