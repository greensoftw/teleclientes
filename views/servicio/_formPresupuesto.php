<div class="form-group" id="add-presupuesto">
  <?php
  use yii\data\ArrayDataProvider;
  use yii\helpers\Html;

  $dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
      'pageSize' => -1
    ]
  ]);
  ?>
  <div class="panel panel-default">
    <table class="kv-grid-table table table-hover kv-table-wrap">
      <tr>
        <td>Servicio - Producto</td>
        <td>Precio</td>
      </tr>
      <?php /** @var \app\models\base\Presupuesto $model */
      foreach ($dataProvider->allModels as $key => $model): ?>
        <tr>
          <td>
            <?= \kartik\select2\Select2::widget([
              'id' => Html::getInputId($model,"[$key]model_id"),
              'name' => Html::getInputName($model, "[$key]model_id"),
              'data' => [
                'Repuestos' => \yii\helpers\ArrayHelper::map(\app\models\Repuestos::find()
                  ->orderBy('nombre')->all(), 'id', 'nombre'),
                'Trabajos' => \yii\helpers\ArrayHelper::map(\app\models\Trabajo::find()
                  ->orderBy('nombre')->all(), 'id', 'nombre')
              ],
              'options' => ['placeholder' => 'Seleccione Producto o Servicio'],
              'pluginOptions' => [
                'allowClear' => true
              ],
            ]) ?>
            <?= Html::activeHiddenInput($model,"[$key]model_id") ?>
            <?= Html::activeHiddenInput($model, "[$key]model") ?>
          </td>
          <td>
            <?= Html::activeTextInput($model, "[$key]precio", ['class' => 'form-control', 'placeholder' => 'Precio del servicio']) ?>
          </td>
        </tr>
      <?php endforeach; ?>
    </table>
  </div>
  <div class="kv-panel-after">
    <?= Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Agregar Presupuesto',
      ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowPresupuesto()']);
    ?>
  </div>
  <?php  ?>


