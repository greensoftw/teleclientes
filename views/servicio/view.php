<?php

use kartik\grid\GridView;
use kartik\tabs\TabsX;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Servicio */
$this->title = "Orden de Servicio N° $model->numero";
$this->params['breadcrumbs'][] = ['label' => 'Servicios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$search = "$('#add-equipo-button').click(function(){
	$('#agregar-equipo').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="servicio-view row">
  <div>
    <div class="form-group">
      <?=
      Html::a('<i class="fa fa-file"></i> ' . 'PDF',
        ['pdf', 'id' => $model->id],
        [
          'class' => 'btn btn-danger',
          'target' => '_blank',
          'data-toggle' => 'tooltip',
          'title' => 'se generara en una nueva ventana'
        ]
      ) ?>
      <?php if (!$model->pagado): ?>
        <?= Html::a('Registrar abono', ['abonar', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
      <?php endif; ?>
      <?php if (!$model->cerrado): ?>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Enviar enlace de confirmacion al cliente', ['enviarenlace', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Agregar un estado', ['agregar-estado', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
      <?php endif; ?>
      <?php if (!$model->empleadoComision and \mdm\admin\components\Helper::checkRoute('servicio/comisionar') and $model->pagado and $model->cerrado): ?>
        <?= Html::a('Comisionar', ['comisionar', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
      <?php endif; ?>
    </div>
  </div>

  <div>
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      [
        'attribute' => 'empleado.usuario.nombres',
        'label' => 'Empleado',
      ],
      [
        'attribute' => 'estadoActual.estado',
        'label' => 'Estado actual'
      ],
      [
        'value' => Yii::$app->formatter->asDatetime($model->ingreso->fecha),
        'label' => 'Ingresado'
      ],
      'fecha_tecnico:datetime',
      [
        'attribute' => 'cliente.usuario.nombres',
        'label' => 'Cliente',
      ],
      [
        'attribute' => 'comisionServicio.comisionista.usuario.nombres',
        'label' => 'Comisionista',
      ],
      [
        'attribute' => 'comisionServicio.monto',
        'label' => 'Monto de la comisión',
        'format' => 'currency'
      ],
      ['attribute' => 'cliente.direccion',
        'label' => 'Dirección del cliente'
      ],
      [
        'attribute' => 'cliente.telefonos',
        'label' => 'Télefonos del cliente'
      ],
      [
        'attribute' => 'cliente.usuario.email',
        'label' => 'Correo del cliente'
      ],
      'total:currency',
      $model->pagado ?
        'pagado:boolean' :
        [
          'label' => 'Abonado',
          'value' => $model->getAbonado(),
          'format' => 'currency'
        ],
      $model->pagado ? ['visible'=>false]:
        [
          'label' => 'Saldo en deuda',
          'value' => $model->getEnDeuda(),
          'format' => 'currency'
        ],
      'no_facturado',
      'cerrado:boolean',
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>
  <div>
    <?php
    $items = [
      $model->servicioAEquipo ?
        [
          'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Equipo'),
          'content' => $this->render('_dataServicioAEquipo', [
            'model' => $model,
          ]),
        ]
        :
        [
          'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Ingresar nuevo equipo'),
          'content' => $this->render('_form_add_equipo', [
            'model' => $model
          ]),
          'visible' => !$model->servicioAEquipo
        ],
      [
        'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Presupuesto - Actualizar'),
        'content' => $this->render('_dataPresupuesto', [
          'model' => $model,
          'row' => $model->presupuestos,
        ]),
        'visible' => \mdm\admin\components\Helper::checkRoute('servicio/agregar-presupuesto')
      ],
      [
        'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Estados - Actualizar'),
        'content' => $this->render('_dataServicioEstado', [
          'model' => $model,
          'row' => $model->servicioEstados,
        ]),
        'visible' => \mdm\admin\components\Helper::checkRoute('servicio/agregar-estado')
      ],
      [
        'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Abonos'),
        'content' => $this->render('_dataAbonoServicio', [
          'model' => $model,
          'row' => $model->abonoServicios,
        ]),
        'visible' => \mdm\admin\components\Helper::checkRoute('servicio/abonar')
      ],
    ];
    ?>
    <?= TabsX::widget([
      'items' => $items,
      'position' => TabsX::POS_ABOVE,
      'encodeLabels' => false,
      'class' => 'tes',
      'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
      ],
    ]);
    ?>
  </div>
  <div class="col-lg-12 row">
    <div class="form-group">
      <div>
        <h3 class="text-success">Adjuntar archivo</h3>
        <?= $this->render('/extra-views/upload_file', [
          'action' => ['upload-file', 'id' => $model->id]
        ]) ?>
      </div>
      <?php if ($model->files) ?>
      <?php $archivosProvider = new \yii\data\ArrayDataProvider([
        'allModels' => $model->files
      ]) ?>
      <?php
      $gridColumn = [
        'title',
        'tipoArchivo',
        [
          'class' => 'yii\grid\ActionColumn',
          'template' => "{view-file} {delete-file}",
          'buttons' => [
            'view-file' => function ($url, $model) {
              return Html::a('<i class="fa fa-eye"></i>', ['view-file', 'id' => $model->id],
                ['class' => 'btn btn-primary',]);
            },
            'delete-file' => function ($url, $model) {
              return Html::a('<i class="fa fa-trash"></i>', ['delete-file', 'id' => $model->id],
                ['class' => 'btn btn-danger', 'data-confirm' => 'Seguro desea eliminar este archivo']);
            }
          ],
          'header' => 'Opciones'
        ]
      ];
      echo GridView::widget([
        'dataProvider' => $archivosProvider,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-empleado-comision']],
        'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Archivos ')
        ],
        'columns' => $gridColumn
      ]);
      ?>
    </div>
  </div>

</div>
<?php $this->registerJsFile('@web/js/view-servicio.js', [
  'position' => $this::POS_END,
  'depends' => [\yii\web\JqueryAsset::className(),\kartik\select2\Select2Asset::className()]
]) ?>

