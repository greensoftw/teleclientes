<?php
/**
 * @var $model \app\models\Servicio
 */
use app\models\Equipo;

$equipo = new Equipo();
$servicioAEquipo = new \app\models\ServicioAEquipo([
  'servicio_id' => $model->id
])
?>
<div id="ingresar-nuevo-equipo" style="overflow: hidden">
  <div class="col-lg-12 row">
    <?php $form = \yii\bootstrap\ActiveForm::begin([
      'action' => ['agregar-equipo', 'id' => $model->id],
      'enableAjaxValidation' => true,
      'fieldConfig'=>['template'=>"<div class='col-lg-6'>{label}\n{input}\n{error}</div>"]
    ]) ?>
    <?= $form->field($equipo, 'marca_id')->widget(\kartik\widgets\Select2::className(), [
      'data' => \yii\helpers\ArrayHelper::map(\app\models\Marca::find()
        ->orderBy('nombre')->asArray()->all(), 'id', 'nombre'),
      'options' => ['placeholder' => 'seleccione una marca'],
      'pluginOptions' => [
        'allowClear' => true
      ],
    ]) ?>
    <?= $form->field($equipo, 'distribuidor_id')->widget(\kartik\widgets\Select2::className(), [
      'data' => \yii\helpers\ArrayHelper::map(\app\models\Distribuidor::find()
        ->orderBy('nombre')->asArray()->all(), 'id', 'nombre'),
      'options' => ['placeholder' => 'seleccione una marca'],
      'pluginOptions' => [
        'allowClear' => true
      ],
    ]) ?>
    <?= $form->field($equipo, 'tiempo_mantenimiento')
      ->dropDownList(Equipo::$tiempos_mantenimientos, ['prompt' => 'Seleccione']) ?>
    <?= $form->field($equipo, 'tipo_equipo_id')->widget(\kartik\widgets\Select2::className(), [
      'data' => \yii\helpers\ArrayHelper::map(\app\models\TipoEquipo::find()->orderBy('nombre')
        ->asArray()->all(), 'id', 'nombre')
    ]) ?>
    <?= $form->field($equipo, 'modelo', ['inputOptions' => ['placeholder' => 'Modelo del equipo']]) ?>

    <?= $form->field($equipo, 'serial', ['inputOptions' => ['placeholder' => 'Serial del equipo']]) ?>
    
    <?= $form->field($equipo, 'nota', ['inputOptions' => ['placeholder' => 'Ubicacion del equipo']]) ?>
  </div>
  <div class="col-lg-12 row">
    <h3>Detalles para el servicio</h3>
    <?= $form->field($servicioAEquipo, 'codigo_isis_id')->widget(\kartik\widgets\Select2::className(), [
      'data' => \yii\helpers\ArrayHelper::map(\app\models\CodigoIsis::find()->orderBy('codigo')->all(), 'id',
        function (\app\models\CodigoIsis $model) {
          return $model->codigo . " - " . $model->descripcion;
        }),
      'options' => ['placeholder' => 'seleccione una Código de error (Opcional)'],
      'pluginOptions' => [
        'allowClear' => true
      ]
    ]) ?>
    <?= $form->field($servicioAEquipo, 'descripcion') ?>
  </div>
  <div class="form-group col-lg-12">
    <?= \yii\bootstrap\Html::submitButton('Guardar', ['class' => 'btn btn-primary pull-right']) ?>
  </div>
  <?php \yii\bootstrap\ActiveForm::end() ?>
</div>
