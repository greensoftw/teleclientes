<?php

/* @var $this yii\web\View */
/* @var $model app\models\Servicio */
/* @var $estado app\models\ServicioEstado */

$this->title = 'Actualizar Servicio N° ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Servicios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => "Orden de Servicio N° $model->id", 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="servicio-update row">
  <?= $this->render('_form', [
    'model' => $model,
    'estado' => $estado
  ]) ?>
</div>
<?php $this->registerJs('setSelect2Values()') ?>
