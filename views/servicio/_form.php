<?php

use app\models\base\ServicioAEquipo;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Servicio */
/* @var $form yii\widgets\ActiveForm */
/* @var $estado \app\models\ServicioEstado */
?>

<div class="servicio-form">

  <?php $form = ActiveForm::begin(); ?>

  <?= $form->errorSummary($model); ?>

  <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

  <?= $form->field($model, 'numero') ?>

  <?= $form->field($model, 'marca_id')->widget(\kartik\widgets\Select2::className(), [
    'data' => ArrayHelper::map(\app\models\Marca::find()
      ->orderBy('nombre')->asArray()->all(), 'id', 'nombre'),
    'options' => ['placeholder' => 'seleccione una marca'],
    'pluginOptions' => [
      'allowClear' => true
    ],
  ])->hint('Seleccione una marca si este servicio es por garantia'); ?>

  <?= $form->field($model, 'numero_garantia')
    ->textInput(['placeholder' => 'Ingrese su numero de garantia asignado']) ?>

  <?php if (!Yii::$app->user->can('Tecnico')): ?>
    <?= $form->field($model, 'empleado_id')->widget(\kartik\widgets\Select2::className(), [
      'data' => ArrayHelper::map(\app\models\Empleado::find()
        ->where(['is_tecnico' => true])
        ->orderBy('id')->all(), 'id', 'usuario.nombres'),
      'options' => ['placeholder' => 'seleccione un tecnico'],
      'pluginOptions' => [
        'allowClear' => true
      ],
    ]); ?>
  <?php endif; ?>
  <?php if (!Yii::$app->user->can('Tecnico')): ?>
    <?= $form->field($model, 'cliente_id')->widget(\kartik\widgets\Select2::className(), [
      'data' => ArrayHelper::map(\app\models\Cliente::find()
        ->joinWith('usuario')
        ->orderBy('usuario.nombres')->all(), 'id', 'usuario.nombres'),
      'options' => ['placeholder' => 'Seleccione un Cliente'],
      'pluginOptions' => [
        'allowClear' => true
      ],
    ]); ?>
  <?php endif ?>

  <?= $form->field($model, 'no_facturado')->textInput(['maxlength' => true, 'placeholder' => 'No Facturado'])
    ->hint('Ingrese el numero de factura usado. (opcional)') ?>
  <?= $form->field($model, 'total')->hiddenInput(['placeholder' => 'Total a Cobrar'])->label(false) ?>
  <?php if ($estado->estado == \app\models\ServicioEstado::INGRESADO): ?>
    <?= $form->field($estado, 'fecha')->widget(\kartik\datecontrol\DateControl::classname(), [
      'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
      'saveFormat' => 'php:Y-m-d H:i:s',
      'ajaxConversion' => true,
      'options' => [
        'pluginOptions' => [
          'placeholder' => 'Seleccione una fecha',
          'autoclose' => true,
        ]
      ],
    ])->label('Fecha de ingreso'); ?>
  <?php endif; ?>
  <?= $form->field($model, 'fecha_tecnico')->widget(\kartik\datecontrol\DateControl::classname(), [
    'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
    'saveFormat' => 'php:Y-m-d H:i:s',
    'ajaxConversion' => true,
    'options' => [
      'pluginOptions' => [
        'placeholder' => 'Seleccione una fecha',
        'autoclose' => true,
      ]
    ],
  ]); ?>
  <?php $modelsae = new ServicioAEquipo(); $values = []?>
  <?php if (!$model->isNewRecord) {
    $values = ArrayHelper::map($model->cliente->equipos,'id',function($model){return (string) $model;});
    $modelsae = $model->servicioAEquipo? $model->servicioAEquipo : new ServicioAEquipo();
  } ?>
  <?php
  $forms = [
    [
      'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Equipos a reparar'),
      'content' => $this->render('_formServicioAEquipo', [
        'model' => $modelsae,
        'values' => $values
      ]),
    ],
    [
      'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Presupuesto'),
      'content' => $this->render('_formPresupuesto', [
        'row' => $model->presupuestos,
      ]),
    ],

  ];
  ?>
  <div id="complementos-servicio" style="display: none">
    <?= kartik\tabs\TabsX::widget([
      'items' => $forms,
      'position' => kartik\tabs\TabsX::POS_ABOVE,
      'encodeLabels' => false,
      'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false,
      ],
    ]); ?>
  </div>
  <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?= Html::a('Cancelar', Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</div>
<?php $this->registerJsFile('@web/js/servicio.js', [
  'position' => $this::POS_END,
  'depends' => [\yii\web\JqueryAsset::className()]
]) ?>
