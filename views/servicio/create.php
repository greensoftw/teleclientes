<?php


/* @var $this yii\web\View */
/* @var $model app\models\Servicio */
/* @var $estado \app\models\ServicioEstado */

$this->title = 'Crear Servicio';
$this->params['breadcrumbs'][] = ['label' => 'Servicios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="servicio-create row">
  <?= $this->render('_form', [
    'model' => $model,
    'estado' => $estado
  ]) ?>
</div>
