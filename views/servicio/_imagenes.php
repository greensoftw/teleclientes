<?php
/**
 * Created by PhpStorm.
 * User: jhon
 * Date: 7/02/17
 * Time: 08:40 PM
 * @var $model \app\models\Servicio
 * @var $this \yii\web\View
 */
use app\helpers\LightBox;

\p2made\assets\Lightbox2Asset::register($this);
?>
<?php
$files = [];
/** @var \app\models\Archivo $file */
foreach ($model->servicioAEquipo->files as $file) {
  $files[] = [
    'thumb' => \yii\helpers\Url::to("@web/$file->ruta"),
    'original' => \yii\helpers\Url::to("@web/$file->ruta"),
    'title' => $file->title . " " .
      \kartik\helpers\Html::a('<span class="fa fa-trash"></span>', ['delete-file', 'id' => $file->id]) . " " .
      \kartik\helpers\Html::a('<span class="fa fa-cloud-download"></span>', ['site/download-file', 'id' => $file->id]),
    'thumbOptions' => ['class' => 'img-responsive img-thumbnail', 'style' => 'width: 50px; height: 50px'],
    'group' => 'equipo-' . $model->servicioAEquipo->equipo_id,
    'format'=>$file->getTipoArchivo(),
    'altUrl'=>['view-file','id'=>$file->id],
    'altTitle'=>$file->title
  ];
} ?>
<div class="form-group col-lg-12">
  <?= Lightbox::widget([
    'files' => $files
  ]) ?>
</div>
<div class="col-lg-12">
  <?= $this->render('/extra-views/upload_file',[
    'action'=>['upload-file-equipo','id'=>$model->id,'equipo_id'=>$model->servicioAEquipo->id]
  ]) ?>
</div>
