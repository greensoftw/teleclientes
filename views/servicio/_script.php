<?php
use yii\helpers\Url;

/**
 * @var $model \app\models\base\Servicio
 */
?>
<script>
  function addRow<?= $class ?>() {
    var data = $('#add-<?= $relID?> :input').serializeArray();
    var cliente_id = $('#<?= \yii\bootstrap\Html::getInputId($model,'cliente_id') ?>').val();
    data.push({name: '_action', value: 'add'});
    $.ajax({
      type: 'POST',
      url: '<?php echo Url::to(['add-' . $relID]); ?>?cliente_id='+cliente_id,
      data: data,
      success: function (data) {
        $('#add-<?= $relID?>').html(data);
      }
    });
  }
  function delRow<?= $class ?>(id) {
    $('#add-<?= $relID?> tr[data-key=' + id + ']').remove();
  }
</script>
