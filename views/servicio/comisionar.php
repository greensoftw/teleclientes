<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Servicio */
/* @var $comision \app\models\EmpleadoComision*/
$this->title = "Comisionar servicio : $model->id";
$this->params['breadcrumbs'][] = ['label' => 'Servicios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => "Orden de Servicio N° $model->id", 'url' => ['view','id'=>$model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="servicio-view row">
  <div class="col-lg-6">
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      [
        'attribute' => 'empleado.usuario.nombres',
        'label' => 'Empleado',
      ],
      [
        'attribute' => 'estadoActual.estado',
        'label' => 'Estado actual'
      ],
      [
        'value' => Yii::$app->formatter->asDatetime($model->ingreso->fecha),
        'label' => 'Ingresado'
      ],
      'fecha_tecnico:datetime',
      [
        'attribute' => 'cliente.usuario.nombres',
        'label' => 'Cliente',
      ],
      [
        'attribute' => 'comisionServicio.comisionista.usuario.nombres',
        'label' => 'Comisionista',
      ],
      [
        'attribute' => 'comisionServicio.monto',
        'label' => 'Monto de la comisión',
        'format'=>'currency'
      ],
      ['attribute' => 'cliente.direccion',
        'label' => 'Dirección del cliente'
      ],
      [
        'attribute' => 'cliente.telefonos',
        'label' => 'Télefonos del cliente'
      ],
      [
        'attribute' => 'cliente.usuario.email',
        'label' => 'Correo del cliente'
      ],
      'total:currency',
      'pagado:boolean',
      'no_facturado',
      'cerrado:boolean',
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>
  <div class="col-lg-6">
    <h2 class="text-info">Registar comisión para el empleado</h2>
    <div class="alert alert-info">
      <p><strong>Nota:</strong>

      </p>
    </div>
    <?php $form = \kartik\form\ActiveForm::begin([
    ]) ?>
    <?= $form->field($comision,'monto') ?>
    <?= $form->field($comision,'fecha')->widget(\kartik\datecontrol\DateControl::classname(), [
      'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
      'saveFormat' => 'php:Y-m-d H:i:s',
      'ajaxConversion' => true,
      'options' => [
        'pluginOptions' => [
          'placeholder' => 'Seleccione una fecha',
          'autoclose' => true,
        ]
      ],
    ]) ?>
    <?= Html::submitButton('Confirmar',['class'=>'btn btn-primary']) ?>
    <?php \kartik\form\ActiveForm::end() ?>
  </div>
</div>
