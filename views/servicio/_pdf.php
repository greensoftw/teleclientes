<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Servicio */

$this->title = "Orden de servicio N°: $model->id";
$this->params['breadcrumbs'][] = ['label' => 'Servicios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="servicio-view">

  <div class="row">
    <div class="col-sm-9">
      <h3><?= 'Servicio' . ': ' . Html::encode($this->title) ?></h3>
    </div>
  </div>

  <div>
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      [
        'attribute' => 'empleado.usuario.nombres',
        'label' => 'Empleado'
      ],
      [
        'attribute' => 'cliente.usuario.nombres',
        'label' => 'Cliente'
      ],
      [
          'attribute'=>'marca.nombre',
        'label'=>'Garantia'
      ],
      'total:currency',
      'pagado:boolean',
      'no_facturado',
      'cerrado:boolean',
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>
  <div class="row">
    <?php
    if ($providerServicioAEquipo->totalCount) {
      $gridColumnServicioAEquipo = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
          'value' => function (\app\models\base\ServicioAEquipo $model) {
            return $model->equipo->marca->nombre . " - " . $model->equipo->tipoEquipo->nombre . "<br>"
              . $model->equipo->serial . ", " . $model->equipo->modelo;
          },
          'label' => 'Equipo',
          'format' => 'html'
        ],
        [
          'value' => function (\app\models\ServicioAEquipo $model) {
            if ($model->codigoIsis)
              return $model->codigoIsis->codigo . " - " . $model->codigoIsis->descripcion;
            return "(No Definido)";
          },
          'label' => 'Código de Error'
        ],
        'descripcion',
      ];
      echo Gridview::widget([
        'dataProvider' => $providerServicioAEquipo,
        'panel' => [
          'type' => GridView::TYPE_DEFAULT,
          'heading' => Html::encode('Equipos'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnServicioAEquipo
      ]);
    }
    ?>
  </div>
  <div class="row">
    <?php
    if ($providerPresupuesto->totalCount) {
      $gridColumnPresupuesto = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
          'attribute' => 'relacion.nombre',
          'label' => 'Repuesto o Servicio'
        ],
        'precio:currency',
      ];
      echo Gridview::widget([
        'dataProvider' => $providerPresupuesto,
        'panel' => [
          'type' => GridView::TYPE_DEFAULT,
          'heading' => Html::encode('Presupuesto'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnPresupuesto
      ]);
    }
    ?>
  </div>
  <div class="row">
    <?php
    if ($providerServicioEstado->totalCount) {
      $gridColumnServicioEstado = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'estado',
        'descripcion',
      ];
      echo Gridview::widget([
        'dataProvider' => $providerServicioEstado,
        'panel' => [
          'type' => GridView::TYPE_DEFAULT,
          'heading' => Html::encode('Historial de Estado'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnServicioEstado
      ]);
    }
    ?>
  </div>
  <div class="row">
    <?php
    if ($providerAbonoServicio->totalCount) {
      $gridColumnAbonoServicio = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'monto',
        'fecha',
      ];
      echo Gridview::widget([
        'dataProvider' => $providerAbonoServicio,
        'panel' => [
          'type' => GridView::TYPE_DEFAULT,
          'heading' => Html::encode('Abonos'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnAbonoServicio
      ]);
    }
    ?>
  </div>
</div>
