<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\ServicioSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-servicio-search col-lg-12 row">
  <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'fieldConfig' => [
      'options' => ['class'=>'col-lg-3']
    ]
  ]); ?>
  <div class="form-group row">
    <?= $form->field($model, 'estado')
      ->dropDownList(\app\models\ServicioEstado::$dropEstados, ['prompt' => 'Seleccione']) ?>

    <?= $form->field($model,'numero') ?>

    <?= $form->field($model,'numero_garantia')->textInput(['placeholder'=>'Numero de garantia']) ?>

    <?= $form->field($model, 'marca_id')->widget(\kartik\widgets\Select2::className(), [
      'data' => \yii\helpers\ArrayHelper::map(\app\models\Marca::find()->orderBy('nombre')
        ->all(), 'id', 'nombre'),
      'options' => ['placeholder' => 'Garantia'],
      'pluginOptions' => [
        'allowClear' => true
      ],
    ]); ?>
    <?= $form->field($model, 'empleado_id')->widget(\kartik\widgets\Select2::className(), [
      'data' => ArrayHelper::merge(['0' => 'Sin asignar'],
        ArrayHelper::map(\app\models\Empleado::find()
          ->joinWith('usuario')
          ->orderBy('usuario.nombres')
          ->where(['is_tecnico' => 1])->all(), 'id', 'usuario.nombres')),
      'options' => ['placeholder' => 'Tecnico'],
      'pluginOptions' => [
        'allowClear' => true
      ],
    ]); ?>
    <?= $form->field($model, 'cliente_id')->widget(\kartik\widgets\Select2::className(), [
      'data' => \yii\helpers\ArrayHelper::map(\app\models\Cliente::find()
        ->joinWith('usuario')
        ->orderBy('usuario.nombres')
        ->all(), 'id', 'usuario.nombres'),
      'options' => ['placeholder' => 'cliente'],
      'pluginOptions' => [
        'allowClear' => true
      ],
    ]); ?>
    <?= $form->field($model, 'cerrado')->dropDownList(\app\helpers\GSHelper::$dropBooleans) ?>
    <p class="col-lg-12"><strong>Nota:</strong> Si busca entre fechas y estado, apareceran los servicios marcados con
      ese estado en el
      lapso de tiempo que realice la busqueda</p>
    <?= $form->field($model, 'comision_servicio_id')->widget(\kartik\widgets\Select2::className(), [
      'data' => \yii\helpers\ArrayHelper::map(\app\models\Comisionista::find()
        ->joinWith('usuario')
        ->orderBy('usuario.nombres')->all(), 'id', 'usuario.nombres'),
      'options' => ['placeholder' => 'seleccione un Comisionista'],
      'pluginOptions' => [
        'allowClear' => true
      ],
    ]); ?>
    <?= $form->field($model, 'fecha_desde')->widget(\kartik\datecontrol\DateControl::classname(), [
      'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
      'saveFormat' => 'php:Y-m-d',
      'ajaxConversion' => true,
      'options' => [
        'pluginOptions' => [
          'placeholder' => 'Seleccione Fecha',
          'autoclose' => true,
        ]
      ],
    ]); ?>
    <?= $form->field($model, 'fecha_hasta')->widget(\kartik\datecontrol\DateControl::classname(), [
      'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
      'saveFormat' => 'php:Y-m-d',
      'ajaxConversion' => true,
      'options' => [
        'pluginOptions' => [

          'placeholder' => 'Seleccione Fecha',
          'autoclose' => true,
        ]
      ],
    ]); ?>
    <?= $form->field($model, 'pagado')->dropDownList(\app\helpers\GSHelper::$dropBooleans) ?>
  </div>
  <div class="form-group col-lg-12 row">
    <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
    <?= Html::resetButton('Borrar', ['class' => 'btn btn-default']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
