<div class="form-group" id="add-servicio-aequipo">
  <?php
  /**
   * @var $model \app\models\ServicioAEquipo
   * @var $values array
   */
  use yii\helpers\Html;

  ?>
  <div class="panel panel-default">
    <div class="col-lg-12 row" style="padding-top: 20px">
      <div class="col-lg-6">
        <?= Html::activeRadioList($model, 'equipo_id', $values, ['encode' => false]) ?>
      </div>
      <div class="col-lg-6">
        <div class="form-group">
          <?= Html::label('Seleccione un codigo de error') ?>
          <?= \kartik\select2\Select2::widget([
            'id' => 'codigo_isis_' . rand(0, 1000),
            'name' => Html::getInputName($model, "codigo_isis_id"),
            'data' => \yii\helpers\ArrayHelper::map(\app\models\CodigoIsis::find()
              ->orderBy('descripcion')->all(), 'id',
              function ($model) {
                return $model->codigo . " - " . $model->descripcion;
              }),
            'options' => ['placeholder' => 'Seleccione un código de error'],
            'pluginOptions' => [
              'allowClear' => true
            ],
          ]) ?>
        </div>
        <div class="form-group">
          <?= Html::label('Indique la falla segun el cliente') ?>
          <?= Html::activeTextInput($model, "descripcion",
            ['maxlengt' => true, 'placeholder' => 'Falla segun cliente', 'class' => 'form-control']) ?>

        </div>
      </div>
    </div>
  </div>
  <div class="kv-panel-after">
    <?= Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Agregar equipos / Actualizar',
      ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowServicioAEquipo()']);
    ?><?= Html::a('Agregar equipos al cliente', ['/cliente/update?id='], ['class' => 'btn btn-primary',
      'id' => 'add_equipo', 'target' => 'black']) ?>
  </div>
</div>




