<?php
/**
 * Created by PhpStorm.
 * User: jhon
 * Date: 7/02/17
 * Time: 02:20 PM
 * @var $model \app\models\Newpass
 * @var $this \yii\web\View
 */
$this->title = 'Cambiar contraseña';
?>
<div class="col-lg-6 col-lg-offset-3">
  <?php $form = \kartik\form\ActiveForm::begin() ?>
  <?= $form->field($model, 'oldpassword') ?>
  <?= $form->field($model, 'password') ?>
  <?= $form->field($model, 'password2') ?>
  <?= \kartik\helpers\Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
  <?php \kartik\form\ActiveForm::end() ?>
</div>
