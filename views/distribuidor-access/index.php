<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ServicioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = 'Servicios';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="servicio-index row">
  <?php
  $gridColumn = [
    ['class' => 'yii\grid\SerialColumn'],
    [
      'class' => 'kartik\grid\ExpandRowColumn',
      'width' => '50px',
      'value' => function ($model, $key, $index, $column) {
        return GridView::ROW_COLLAPSED;
      },
      'detail' => function ($model, $key, $index, $column) {
        return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
      },
      'headerOptions' => ['class' => 'kartik-sheet-style'],
      'expandOneOnly' => false
    ],
    [
      'attribute' => 'numero'
    ],
    [
      'attribute' => 'numero_garantia', 'label' => 'Orden N° (Garantia)'
    ],

    [
      'attribute' => 'ingreso.fecha',
      'format' => 'date',
      'label' => 'fecha'
    ],
    [
      'attribute' => 'servicioAEquipo.equipo.stringDistribuidor',
      'label' => 'equipo',
      'format' => 'raw'
    ],
    [
      'value' => function (\app\models\Servicio $model) {
        return $model->marca ? 'SI' : 'NO';
      },
      'label' => 'garantia'
    ],
    [
      'label' => 'Servicio(s)',
      'value' => function (\app\models\Servicio $model) {
        $cat = "";
        foreach ($model->presupuestos as $key => $item) {
          $cat .= ($key + 1) . ". " . $item->relacion->nombre . "<br>";
        }
        return $cat;
      },
      'format' => 'raw'
    ],
    'total:currency',
    'no_facturado',
    [
      'label' => 'Fecha de entrega',
      'attribute' => 'entrega.fecha'
    ]
  ];
  ?>
  <div>
    <div>
      <h2><strong>Distribuidor : </strong> <?= Yii::$app->user->identity->distribuidor->nombre ?></h2>
    </div>
    <div>
      <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-servicio']],
        'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'toolbar' => [
          '{export}',
          ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'showConfirmAlert' => false,
            'target' => ExportMenu::TARGET_SELF,
            'fontAwesome' => true,
            'dropdownOptions' => [
              'label' => 'Todo',
              'class' => 'btn btn-default',
              'itemsBefore' => [
                '<li class="dropdown-header">Exportar todo el contenido</li>',
              ],
            ],
            'noExportColumns' => [1],
            'filename' => 'Listado de servicios',
          ]),
        ],
      ]); ?>
    </div>

  </div>
</div>
<?php $this->registerJsFile('@web/js/view-servicio.js', [
  'position' => $this::POS_END,
  'depends' => [\yii\web\JqueryAsset::className()]
]) ?>

