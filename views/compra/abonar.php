<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Compra */
/* @var $abono \app\models\AbonoCompra */
$this->title = "Abonar";
$this->params['breadcrumbs'][] = ['label' => 'Compras', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => "Compra interna N°: $model->id", 'url' => ['view','id'=>$model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="compra-view row">
  <div class="row">
    <div class="col-lg-6">
      <?php
      $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
          'attribute' => 'proveedor.nombre',
          'label' => 'Proveedor',
        ],
        'fecha',
        'pagado:boolean',
        'total:currency',
      ];
      echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
      ]);
      ?>
    </div>
    <div class="col-lg-6">
      <?php $form = ActiveForm::begin([
        'enableAjaxValidation' => true
      ]) ?>
      <?= $form->field($abono, 'monto') ?>
      <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
      <?php ActiveForm::end() ?>
    </div>
  </div>

</div>
