<div class="form-group" id="add-item-compra">
  <?php
  use kartik\builder\TabularForm;
  use kartik\grid\GridView;
  use yii\data\ArrayDataProvider;
  use yii\helpers\Html;

  $dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
      'pageSize' => -1
    ]
  ]);
  echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'ItemCompra',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
      'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
      "id" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
      'repuesto_id' => [
        'label' => 'Repuestos',
        'type' => TabularForm::INPUT_WIDGET,
        'widgetClass' => \kartik\widgets\Select2::className(),
        'options' => [
          'data' => \yii\helpers\ArrayHelper::map(\app\models\Repuestos::find()->orderBy('id')->asArray()->all(), 'id',
            'nombre'),
          'options' => ['placeholder' => 'Seleccione un Repuesto'],
        ],
        'columnOptions' => ['width' => '200px']
      ],
      'precio' => ['type' => TabularForm::INPUT_TEXT],
      'del' => [
        'type' => 'raw',
        'label' => '',
        'value' => function ($model, $key) {
          return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' => 'Delete', 'onClick' => 'delRowItemCompra(' . $key . '); return false;', 'id' => 'item-compra-del-btn']);
        },
      ],
    ],
    'gridSettings' => [
      'panel' => [
        'heading' => false,
        'type' => GridView::TYPE_DEFAULT,
        'before' => false,
        'footer' => false,
        'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Agregar Item Compra', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowItemCompra()']),
      ]
    ]
  ]);
  echo "    </div>\n\n";
  ?>

