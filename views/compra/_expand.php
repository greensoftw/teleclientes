<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Compra'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
        [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Abono Compra'),
        'content' => $this->render('_dataAbonoCompra', [
            'model' => $model,
            'row' => $model->abonoCompras,
        ]),
    ],
                [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Item Compra'),
        'content' => $this->render('_dataItemCompra', [
            'model' => $model,
            'row' => $model->itemCompras,
        ]),
    ],
    ];
?>
<div class="panel">
    <div class="panel-body">
        <?= TabsX::widget([
        'items' => $items,
        'position' => TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'class' => 'tes',
        'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
        ],
        ]);
        ?>    </div>
</div>
