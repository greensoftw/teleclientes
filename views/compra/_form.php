<?php

use yii\helpers\Html;
use yii\web\JqueryAsset;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Compra */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="compra-form">

  <?php $form = ActiveForm::begin(); ?>

  <?= $form->errorSummary($model); ?>

  <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

  <?= $form->field($model, 'proveedor_id')->widget(\kartik\widgets\Select2::className(), [
    'data' => \yii\helpers\ArrayHelper::map(\app\models\Proveedor::find()->orderBy('id')->asArray()->all(), 'id', 'nombre'),
    'options' => ['placeholder' => 'Seleccione un Proveedor'],
    'pluginOptions' => [
      'allowClear' => true
    ],
  ]); ?>

  <?= $form->field($model, 'fecha')->widget(\kartik\datecontrol\DateControl::classname(), [
    'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
    'saveFormat' => 'php:Y-m-d H:i:s',
    'ajaxConversion' => true,
    'options' => [
      'pluginOptions' => [
        'placeholder' => 'Seleccione una Fecha',
        'autoclose' => true,
      ]
    ],
  ]); ?>

  <?= $form->field($model, 'total')->textInput(['placeholder' => 'Total']) ?>

  <?php
  $forms = [
    [
      'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Items a comprar'),
      'content' => $this->render('_formItemCompra', [
        'row' => \yii\helpers\ArrayHelper::toArray($model->itemCompras),
      ]),
    ],
  ];
  echo kartik\tabs\TabsX::widget([
    'items' => $forms,
    'position' => kartik\tabs\TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'pluginOptions' => [
      'bordered' => true,
      'sideways' => true,
      'enableCache' => false,
    ],
  ]);
  ?>
  <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?= Html::a('Cancelar', Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
<?php $this->registerJsFile("@web/js/compra.js",[
    'depents'=>JqueryAsset::className()
]) ?>