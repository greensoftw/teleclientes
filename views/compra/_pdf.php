<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Compra */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Compras', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="compra-view">

    <div class="row">
        <div class="col-sm-9">
            <h3><?= 'Compra'.': '. Html::encode($this->title) ?></h3>
        </div>
    </div>

    <div>
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'proveedor.id',
                'label' => 'Proveedor'
            ],
        'fecha',
        'pagado:boolean',
        'total',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerAbonoCompra->totalCount){
    $gridColumnAbonoCompra = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
                'fecha',
        'monto',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAbonoCompra,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Abono Compra'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnAbonoCompra
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerItemCompra->totalCount){
    $gridColumnItemCompra = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
                [
                'attribute' => 'repuesto.id',
                'label' => 'Repuesto'
            ],
        'precio',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerItemCompra,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Item Compra'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnItemCompra
    ]);
}
?>
    </div>
</div>
