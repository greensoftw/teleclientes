<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Compra */
/* @var $providerAbonoCompra */
/* @var $providerProveedor */
/* @var $providerItemCompra */
$this->title = "Compra interna N° : $model->id";
$this->params['breadcrumbs'][] = ['label' => 'Compras', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="compra-view row">

  <div class="row">
    <div class="form-group">
      <?=
      Html::a('<i class="fa fa-file"></i> ' . 'PDF',
        ['pdf', 'id' => $model->id],
        [
          'class' => 'btn btn-danger',
          'target' => '_blank',
          'data-toggle' => 'tooltip',
          'title' => 'se generara en una nueva ventana'
        ]
      ) ?>

      <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
          'confirm' => 'Seguro desea eliminar este elemento?',
          'method' => 'post',
        ],
      ])
      ?>
      <?php if (!$model->pagado): ?>
        <?= Html::a('Abonar',['abonar','id'=>$model->id],['class'=>'btn btn-primary']) ?>
      <?php endif ?>
    </div>
  </div>

  <div class="row">
    <?php
    $gridColumn = [
      ['attribute' => 'id', 'visible' => false],
      [
        'attribute' => 'proveedor.nombre',
        'label' => 'Proveedor',
      ],
      'fecha',
      'pagado:boolean',
      'total:currency',
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>

  <div class="row">
    <?php
    if ($providerItemCompra->totalCount) {
      $gridColumnItemCompra = [
        ['attribute' => 'id', 'visible' => false],
        [
          'attribute' => 'repuesto.nombre',
          'label' => 'Repuesto'
        ],
        'precio:currency',
      ];
      echo GridView::widget([
        'dataProvider' => $providerItemCompra,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-item-compra']],
        'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Item Compra'),
        ],
        'columns' => $gridColumnItemCompra
      ]);
    }
    ?>
  </div>

  <div class="row">
    <?php
    if ($providerAbonoCompra->totalCount) {
      $gridColumnAbonoCompra = [
        ['attribute' => 'id', 'visible' => false],
        'fecha',
        'monto',
      ];
      echo GridView::widget([
        'dataProvider' => $providerAbonoCompra,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-abono-compra']],
        'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Abono Compra'),
        ],
        'columns' => $gridColumnAbonoCompra
      ]);
    }
    ?>
  </div>
</div>
