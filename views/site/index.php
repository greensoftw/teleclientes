<?php

/* @var $this yii\web\View */
/* @var $serviciosIncompletos integer */
/* @var $estados array */
/* @var $sinAsignar integer */
/* @var $cerradosPorCobrar integer */
/* @var $totalPorCobrar integer */
/* @var $servicioIncompletos integer */

$this->title = 'Bienvenido a teleclientes';
?>
<div class="site-index row">
  <div class="col-lg-12 form-group row">
    <div class="col-lg-4 col-md-6">
      <div class="panel panel-yellow">
        <div class="panel-heading">
          <div class="row">
            <div class="col-xs-3">
              <i class="fa fa-file fa-5x"></i>
            </div>
            <div class="col-xs-9 text-right">
              <div class="huge"><?= $sinAsignar ?></div>
              <div>Servicios sin asignar</div>
            </div>
          </div>
        </div>
        <a href="<?= \yii\helpers\Url::to(['/servicio/index','ServicioSearch'=>['empleado_id'=>'0']]) ?>">
          <div class="panel-footer">
            <span class="pull-left">Ver todos</span>
            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
            <div class="clearfix"></div>
          </div>
        </a>
      </div>
    </div>
    <div class="col-lg-4 col-md-6">
      <div class="panel panel-green">
        <div class="panel-heading">
          <div class="row">
            <div class="col-xs-3">
              <i class="fa fa-file fa-5x"></i>
            </div>
            <div class="col-xs-9 text-right">
              <div class="huge"><?= $serviciosIncompletos ?></div>
              <div>Servicios en progreso</div>
            </div>
          </div>
        </div>
        <a href="<?= \yii\helpers\Url::to(['/servicio/index','ServicioSearch'=>['cerrado'=>0]]) ?>">
          <div class="panel-footer">
            <span class="pull-left">Ver todos</span>
            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
            <div class="clearfix"></div>
          </div>
        </a>
      </div>
    </div>
    <div class="col-lg-4 col-md-6">
      <div class="panel panel-red">
        <div class="panel-heading">
          <div class="row">
            <div class="col-xs-3">
              <i class="fa fa-file fa-5x"></i>
            </div>
            <div class="col-xs-9 text-right">
              <div class="huge"><?= $cerradosPorCobrar ?> - <?= $totalPorCobrar ?></div>
              <div>Servicios terminados por cobrar</div>
            </div>
          </div>
        </div>
        <a href="<?= \yii\helpers\Url::to(['/servicio/index','ServicioSearch'=>['cerrado'=>1,'pagado'=>0]]) ?>">
          <div class="panel-footer">
            <span class="pull-left">Ver todos</span>
            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
            <div class="clearfix"></div>
          </div>
        </a>
      </div>
    </div>
  </div>
  <div class="col-lg-12 form-group">
    <h3><i class="fa fa-file"></i> Servicios en cada estado</h3>
  </div>
  <div>
    <?php foreach ($estados as $key => $estado): ?>
      <div class="col-lg-3 col-md-6">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <div class="row">
              <div class="col-xs-3">
                <i class="fa fa-file fa-5x"></i>
              </div>
              <div class="col-xs-9 text-right">
                <div class="huge"><?= $estado ?></div>
                <div><?= $key ?></div>
              </div>
            </div>
          </div>
          <a href="<?= \yii\helpers\Url::to(['/servicio/index','ServicioSearch'=>['estado'=>$key]]) ?>">
            <div class="panel-footer">
              <span class="pull-left">Ver todos</span>
              <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
              <div class="clearfix"></div>
            </div>
          </a>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
</div>
